/**
 * Automatically generated file. DO NOT MODIFY
 */
package cn.com.yuntian.wojia;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "cn.com.yuntian.wojia";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1.1.3";
}
