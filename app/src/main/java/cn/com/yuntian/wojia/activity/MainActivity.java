package cn.com.yuntian.wojia.activity;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Toast;


import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;


import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.fragment.LoginFragment;
import cn.com.yuntian.wojia.logic.LoginProcesss;
import cn.com.yuntian.wojia.logic.OnPressBackListener;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.UserInfo;


public class MainActivity extends SlidingFragmentActivity implements OnPressBackListener {

	private static boolean isExit = false;
	private Fragment selectFragment;
	private static Handler mHandler = new Handler() {  
		  
        @Override  
        public void handleMessage(Message msg) {  
            super.handleMessage(msg);  
            isExit = false;  
        }  
    };  
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new Handler().postDelayed((
			new Runnable() {
		        public void run () {
		        	LoginProcesss lp = new LoginProcesss(MainActivity.this);
		    		lp.autoLogin(false);
//		    		overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
//		    		UmengUpdateAgent.update(MainActivity.this);
		        }
			}
		), 1750);
		
		// set the behind view
		setBehindContentView(R.layout.frame_menu );	// ����SlidingMenu�Ĳ���
		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);	// ����SlidingMenu�򿪺��ұ����µĿ��
		sm.setFadeDegree(0.35f);
		// ����sliding menu�ļ�������ģʽ
		// TOUCHMODE_FULLSCREEN ȫ��ģʽ�������Ĳ�����ͨ������Ҳ���Դ�SlidingMenu
		// TOUCHMODE_MARGIN ��Եģʽ��������sliding������Ҫ����Ļ��Ե�����ſ��Դ�sliding menu
		// TOUCHMODE_NONE ��Ȼ�ǲ���ͨ�����ƴ���
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	} 
	
	@Override  
    public boolean onKeyDown(int keyCode, KeyEvent event) {  
        if (keyCode == KeyEvent.KEYCODE_BACK) {

        	if (LoginFragment.getLoginPageStatus()) {
        		exit();
            	return false;
        	} else {
        		if (CheckUtil.requireCheck(UserInfo.UserInfo.getUserName())) {
        			if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
        				exit();
    	            	return false;
        			} else {
        				return super.onKeyDown(keyCode, event); 
        			}
        			
        		} else {
        			int count = getSupportFragmentManager().getBackStackEntryCount();
        			if (count > 0) {
        				BackStackEntry backStack = getSupportFragmentManager().getBackStackEntryAt(count - 1);
        				if ("register".equals(backStack.getName())) {
        					return super.onKeyDown(keyCode, event); 
        				} else {
        					LoginFragment loginFragment = new LoginFragment();
                			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                			transaction.replace(R.id.fragment_container, loginFragment);
                			transaction.commit();
                			return false;
        				}
        			} else {
        				LoginFragment loginFragment = new LoginFragment();
            			FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            			transaction.replace(R.id.fragment_container, loginFragment);
            			transaction.commit();
            			return false;
        			}
        		}

        	}
              
        } else {  
            return super.onKeyDown(keyCode, event);  
        }  
    }  
	
	public void exit(){  
        if (!isExit) {  
            isExit = true;  
            Toast.makeText(getApplicationContext(), 
            		this.getResources().getString(R.string.lang_exit_message), 
            		Toast.LENGTH_SHORT).show();  
            mHandler.sendEmptyMessageDelayed(0, 2000);  
        } else {  
            finish();  
            System.exit(0);  
        }  
    }

	@Override
	public void setSelectFragment(Fragment fragment){
		this.selectFragment = fragment;
	}

	@Override
	public void onPressBack() {
		Log.e("MainActivity", "onPressBack: " );
//		selectFragment.getFragmentManager().popBackStackImmediate();
	}
}
