package cn.com.yuntian.wojia.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class ATable {

	protected SQLiteOpenHelper sqllite;

	ATable(SQLiteOpenHelper sqllite) {
		this.sqllite = sqllite;
	}

	abstract String getTableName();

	abstract String createTableSql();

	public boolean tabbleIsExist(SQLiteDatabase db, String tableName) {
		boolean result = false;
		if (tableName == null) {
			return false;
		}

		Cursor cursor = null;

		String sql = "select count(*) as c from sqlite_master where type ='table' and name ='"
				+ tableName + "' ";
		cursor = db.rawQuery(sql, null);
		if (cursor.moveToNext()) {
			int count = cursor.getInt(0);
			if (count > 0) {
				result = true;
			}
		}

		if (null != cursor && !cursor.isClosed()) {
			cursor.close();
		}

		return result;
	}

	public boolean checkColumnExist(SQLiteDatabase db, String tableName,
			String columnName) {
		boolean result = false;
		Cursor cursor = null;

		cursor = db.rawQuery("SELECT * FROM " + tableName + " LIMIT 0", null);
		result = cursor != null && cursor.getColumnIndex(columnName) != -1;

		if (null != cursor && !cursor.isClosed()) {
			cursor.close();
		}

		return result;
	}
}
