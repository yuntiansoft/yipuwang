package cn.com.yuntian.wojia.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.YiPuWTempBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class HailinDB extends SQLiteOpenHelper{
	/**
	 * 数据库名
	 */
	public static final String DB_NAME = "HailinDB";
	/**
	 * 数据库版本
	 */
	public static final int VERSION = 15;
	
	// 用户表
	private TableUser tUser;
	
	// 设备总表
	private TableDevice tDevice;
	
	// 美式温控器表
	private TableAmerTemp tAmerTemp;

	// PM2.5表
	private TablePM25 tPM25;
	
	// PM2.5&Voc表
	private TablePM25Voc tPM25Voc;
	
	// 历史表
	private TableHistory tHistory;
	
	// 绿动表
	private TableGreenTemp tGreenTemp;
	
	// 采暖温控器表
	private TableHeatingTemp tHeatingTemp;
	
	// Co2&Voc表
	private TableCo2Voc tCo2Voc;
	
	// 灵动表(新采暖温控器)
	private TableLingDongTemp tLingDongTemp;
	
	// 分体控制仪
	private TableFtkzy tFtkzy;

	//宜普旺供暖
	private TableYiPuWTemp tYiPuWTemp;

	public TableUser getTUser() {
		return tUser;
	}

	public TableDevice getTDevice() {
		return tDevice;
	}
	
	public TableAmerTemp getTAmerTemp() {
		return tAmerTemp;
	}
	
	public TablePM25 getTPM25() {
		return tPM25;
	}
	
	public TablePM25Voc getTPM25Voc() {
		return tPM25Voc;
	}
	
	public TableGreenTemp getTGreenTemp() {
		return tGreenTemp;
	}
	
	public TableHistory getTHistory() {
		return tHistory;
	}
	
	public TableHeatingTemp getTHeatingTemp() {
		return tHeatingTemp;
	}
	
	public TableCo2Voc getTCo2Voc() {
		return tCo2Voc;
	}
	
	public TableLingDongTemp getTLingDongTemp() {
		return tLingDongTemp;
	}
	
	public TableFtkzy getTFtkzy() {
		return tFtkzy;
	}

	public TableYiPuWTemp gettYiPuWTemp() {
		return tYiPuWTemp;
	}

	public HailinDB(Context context) {
		super(context, DB_NAME, null, VERSION);
		tUser = new TableUser(this);
		tDevice = new TableDevice(this);
		tPM25 = new TablePM25(this);
		tPM25Voc = new TablePM25Voc(this);
		tAmerTemp = new TableAmerTemp(this);
		tGreenTemp = new TableGreenTemp(this);
		tHistory = new TableHistory(this);
		tHeatingTemp = new TableHeatingTemp(this);
		tCo2Voc = new TableCo2Voc(this);
		tLingDongTemp = new TableLingDongTemp(this);
		tFtkzy = new TableFtkzy(this);
		tYiPuWTemp = new TableYiPuWTemp(this);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// 创建数据库时 被调用，且只被调用一次

		// 创建用户表
		db.execSQL(tUser.createTableSql());
		// 创建设备总表
		db.execSQL(tDevice.createTableSql());
		// 创建设备总表索引
		db.execSQL(tDevice.createTableIndex());
		// 创建美式温控器表
		db.execSQL(tAmerTemp.createTableSql());
		// 创建美式温控器表索引
		db.execSQL(tAmerTemp.createTableIndex());
		// 创建PM2.5表
		db.execSQL(tPM25.createTableSql());
		// 创建PM2.5表索引
		db.execSQL(tPM25.createTableIndex());
		// 创建PM2.5&Voc表
		db.execSQL(tPM25Voc.createTableSql());
		// 创建PM2.5&Voc表索引
		db.execSQL(tPM25Voc.createTableIndex());
		// 创建绿动表
		db.execSQL(tGreenTemp.createTableSql());
		// 创建绿动表索引
		db.execSQL(tGreenTemp.createTableIndex());
		// 创建历史表
		db.execSQL(tHistory.createTableSql());
		// 创建采暖温控器表
		db.execSQL(tHeatingTemp.createTableSql());
		// 创建Co2&Voc表
		db.execSQL(tCo2Voc.createTableSql());
		// 创建Co2&Voc表索引
		db.execSQL(tCo2Voc.createTableIndex());
		// 创建灵动表
		db.execSQL(tLingDongTemp.createTableSql());
		// 创建灵动表索引
		db.execSQL(tLingDongTemp.createTableIndex());
		// 创建分体控制仪表
		db.execSQL(tFtkzy.createTableSql());
		// 创建分体控制仪索引
		db.execSQL(tFtkzy.createTableIndex());
		//创建宜普旺供暖表
		db.execSQL(tYiPuWTemp.createTableSql());
		//创建宜普旺供暖索引
		db.execSQL(tYiPuWTemp.createTableIndex());
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// 创建历史表
		if (!tHistory.tabbleIsExist(db, Constants.TB_NAME_TABLE_HISTORY)) {
			db.execSQL(tHistory.createTableSql());
		}
			
		// 创建绿动表
		if (!tGreenTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.STATUS_ON_OFF)) {
			db.execSQL(tGreenTemp.updateTableSql());
		}
		if (!tGreenTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.TEMP_COOL_SAVE_ENERGY)) {
			db.execSQL(tGreenTemp.updateTableSqlForCoolEnergy());
			db.execSQL(tGreenTemp.updateTableSqlForHeatEnergy());
		}
		if (!tGreenTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.INTS_HEAT_STAT)) {
			db.execSQL(tGreenTemp.updateTableSqlForIntsHeatStat());
		}
		
		if (!tGreenTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.HEAT_OUT_STAT)) {
			db.execSQL(tGreenTemp.updateTableSqlForHeatOutStat());
		}
		//绿动表加字段
		if (!tGreenTemp.checkColumnExist(db,
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.TEMP_HEAT_DEFAULT_MIN)) {
			db.execSQL(tGreenTemp.updateTableSqlForTempHeatDefaultMin());
		}

		if (!tGreenTemp.checkColumnExist(db,
				Constants.TB_NAME_TABLE_GREEN_TEMP, Constants.TEMP_HEAT_DEFAULT_MAX)) {
			db.execSQL(tGreenTemp.updateTableSqlForTempHeatDefaultMax());
		}



		// 创建PM2.5&Voc表
		if (!tPM25Voc.tabbleIsExist(db, Constants.TB_NAME_TABLE_PM25_VOC)) {
			db.execSQL(tPM25Voc.createTableSql());
			db.execSQL(tPM25Voc.createTableIndex());
		}
		
		// 创建采暖温控器表
		if (!tHeatingTemp.tabbleIsExist(db, Constants.TB_NAME_TABLE_HEATING_TEMP)) {
			db.execSQL(tHeatingTemp.createTableSql());
			db.execSQL(tHeatingTemp.createTableIndex());
		}
		if (!tHeatingTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_HEATING_TEMP, Constants.TEMP_HEAT_SAVE_ENERGY)) {
			db.execSQL(tHeatingTemp.updateTableSql());
		}
		if (!tHeatingTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_HEATING_TEMP, Constants.STATUS)) {
			db.execSQL(tHeatingTemp.updateTableForStatus());
		}
		
		// 创建采暖温控器表
		if (!tCo2Voc.tabbleIsExist(db, Constants.TB_NAME_TABLE_CO2_VOC)) {
			db.execSQL(tCo2Voc.createTableSql());
			db.execSQL(tCo2Voc.createTableIndex());
		}
		
		// 美式温控器表加字段
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.COLD_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForColdStatus());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.HEAT_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForHeatStatus());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.AUTO_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForAutoStatus());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.EMER_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForEmerStatus());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.PERM_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForPermStatus());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.ORI_MOD)) {
			db.execSQL(tAmerTemp.updateTableSqlForOriMod());
		}
		if (!tAmerTemp.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_AMER_TEMP, Constants.ORI_STATUS)) {
			db.execSQL(tAmerTemp.updateTableSqlForOriStatus());
		}
		
		// 用户表加字段
		if (!tUser.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_USER, Constants.USER_TYPE)) {
			db.execSQL(tUser.updateTableSqlForUserType());
		}
		if (!tUser.checkColumnExist(db, 
				Constants.TB_NAME_TABLE_USER, Constants.USER_STATUS)) {
			db.execSQL(tUser.updateTableSqlForUserStatus());
		}
		
		// 创建灵动表
		if (!tLingDongTemp.tabbleIsExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP)) {
			db.execSQL(tLingDongTemp.createTableSql());
			db.execSQL(tLingDongTemp.createTableIndex());
		}
		// 创建灵动Schedule字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.SCHEDULE)) {
			db.execSQL(tLingDongTemp.updateTableSqlForSchedule());
		}
		// 创建灵动联动输入字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.LINKAGE_INPUT)) {
			db.execSQL(tLingDongTemp.updateTableSqlForLinkageInput());
		}
		// 创建灵动联动输出字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.LINKAGE_OUTPUT)) {
			db.execSQL(tLingDongTemp.updateTableSqlForLinkageOutput());
		}
		// 创建灵动强制关闭锅炉字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.CLOSE_BOILER)) {
			db.execSQL(tLingDongTemp.updateTableSqlForCloseBoiler());
		}
		
		// 创建灵动制热默认温度最大值字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.TEMP_HEAT_DEFAULT_MAX)) {
			db.execSQL(tLingDongTemp.updateTableSqlForDefaultHeatMax());
		}
		
		// 创建灵动制热默认温度最小值字段
		if (!tLingDongTemp.checkColumnExist(db, Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.TEMP_HEAT_DEFAULT_MIN)) {
			db.execSQL(tLingDongTemp.updateTableSqlForDefaultHeatMin());
		}

		
		// 创建分体控制仪表
		if (!tFtkzy.tabbleIsExist(db, Constants.TB_NAME_TABLE_FTKZY)) {
			db.execSQL(tFtkzy.createTableSql());
			db.execSQL(tFtkzy.createTableIndex());
		}
		
		// 创建分体控制仪表
		if (!tFtkzy.checkColumnExist(db, Constants.TB_NAME_TABLE_FTKZY, 
				Constants.COLL_PUMP_ON_DEV)) {
			db.execSQL(tFtkzy.updateTableSqlForColl());
			db.execSQL(tFtkzy.updateTableSqlForPipe());
			db.execSQL(tFtkzy.updateTableSqlForAuxi());
		}
		
		// 创建设备表App和Pc控制权限字段
		if (!tDevice.checkColumnExist(db, Constants.TB_NAME_TABLE_DEVICE, 
				Constants.ADMIN_ONLY)) {
			db.execSQL(tDevice.updateTableSqlForAdminOnly());
		}

		//采暖温控器表加字段：制热温度默认最大值和制热温度默认最小值
		if (!tHeatingTemp.checkColumnExist(db,Constants.TB_NAME_TABLE_HEATING_TEMP,Constants.TEMP_HEAT_DEFAULT_MAX)){
			db.execSQL(tHeatingTemp.updateTableForTempHeatDefaultMax());
		}

		if (!tHeatingTemp.checkColumnExist(db,Constants.TB_NAME_TABLE_HEATING_TEMP,Constants.TEMP_HEAT_DEFAULT_MIN)){
			db.execSQL(tHeatingTemp.updateTableForTempHeatDefaultMin());
		}

	}


}












