package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableAmerTemp extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableAmerTemp(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_AMER_TEMP;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_AMER_TEMP +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.DIS_HUMI + " text," +
			Constants.TEMP_HEAT + " text," +
			Constants.STATUS + " text," +
			Constants.TEMP_COOL + " text," +
			Constants.MOD + " text," +
			Constants.HUMI + " text," +
			Constants.FAN_MOD + " text," +
			Constants.PROMABLE + " text," +
			Constants.DEAD_ZONE_TEMP + " text," +
			Constants.COLD_STATUS + " text," +
			Constants.HEAT_STATUS + " text," +
			Constants.AUTO_STATUS + " text," +
			Constants.EMER_STATUS + " text," +
			Constants.PERM_STATUS + " text," +
			Constants.ORI_MOD + " text," +
			Constants.ORI_STATUS + " text" +
		")";
	}
	
	public String updateTableSqlForColdStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.COLD_STATUS + " text";
	}
	
	public String updateTableSqlForHeatStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.HEAT_STATUS + " text";
	}
	
	public String updateTableSqlForAutoStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.AUTO_STATUS + " text";
	}
	
	public String updateTableSqlForEmerStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.EMER_STATUS + " text";
	}
	
	public String updateTableSqlForPermStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.PERM_STATUS + " text";
	}
	
	public String updateTableSqlForOriMod() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.ORI_MOD + " text";
	}
	
	public String updateTableSqlForOriStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_AMER_TEMP 
			+ " add " + Constants.ORI_STATUS + " text";
	}
	
	public String createTableIndex() {
		return "create index idxAmerMac on " + Constants.TB_NAME_TABLE_AMER_TEMP 
				+"(" + Constants.MAC +")";
	}

	private AmerTempBean creatRowResult(Cursor cursor) {
		
		AmerTempBean result = new AmerTempBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		result.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		result.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		result.setTempCool(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COOL)));
		result.setMod(cursor.getString(cursor.getColumnIndex(Constants.MOD)));
		result.setHumi(cursor.getString(cursor.getColumnIndex(Constants.HUMI)));
		result.setFanMod(cursor.getString(cursor.getColumnIndex(Constants.FAN_MOD)));
		result.setPromable(cursor.getString(cursor.getColumnIndex(Constants.PROMABLE)));
		result.setDeadZoneTemp(cursor.getString(cursor.getColumnIndex(Constants.DEAD_ZONE_TEMP)));
		result.setColdStatus(cursor.getString(cursor.getColumnIndex(Constants.COLD_STATUS)));
		result.setHeatStatus(cursor.getString(cursor.getColumnIndex(Constants.HEAT_STATUS)));
		result.setAutoStatus(cursor.getString(cursor.getColumnIndex(Constants.AUTO_STATUS)));
		result.setEmerStatus(cursor.getString(cursor.getColumnIndex(Constants.EMER_STATUS)));
		result.setPermStatus(cursor.getString(cursor.getColumnIndex(Constants.PERM_STATUS)));
		result.setOriMod(cursor.getString(cursor.getColumnIndex(Constants.ORI_MOD)));
		result.setOriStatus(cursor.getString(cursor.getColumnIndex(Constants.ORI_STATUS)));
		
		return result;
	}
	
	public void add(AmerTempBean amerTempBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, amerTempBean.getMac());
		values.put(Constants.USER_NAME, amerTempBean.getUserName());
		values.put(Constants.DIS_TEMP, amerTempBean.getDisTemp());
		values.put(Constants.DIS_HUMI, amerTempBean.getDisHumi());
		values.put(Constants.TEMP_HEAT, amerTempBean.getTempHeat());
		values.put(Constants.STATUS, amerTempBean.getStatus());
		values.put(Constants.TEMP_COOL, amerTempBean.getTempCool());
		values.put(Constants.MOD, amerTempBean.getMod());
		values.put(Constants.HUMI, amerTempBean.getHumi());
		values.put(Constants.FAN_MOD, amerTempBean.getFanMod());
		values.put(Constants.PROMABLE, amerTempBean.getPromable());
		values.put(Constants.DEAD_ZONE_TEMP, amerTempBean.getDeadZoneTemp());
		values.put(Constants.COLD_STATUS, amerTempBean.getColdStatus());
		values.put(Constants.HEAT_STATUS, amerTempBean.getHeatStatus());
		values.put(Constants.AUTO_STATUS, amerTempBean.getAutoStatus());
		values.put(Constants.EMER_STATUS, amerTempBean.getEmerStatus());
		values.put(Constants.PERM_STATUS, amerTempBean.getPermStatus());
		values.put(Constants.ORI_MOD, amerTempBean.getOriMod());
		values.put(Constants.ORI_STATUS, amerTempBean.getOriStatus());
		
		db.insert(Constants.TB_NAME_TABLE_AMER_TEMP, "", values);
	}
	
	public void addAll(List<AmerTempBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_AMER_TEMP,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<AmerTempBean> findAll(){
		List<AmerTempBean> list = new ArrayList<AmerTempBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_AMER_TEMP, null, null, null, null, null, null);
		AmerTempBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public AmerTempBean findOne(String[] args){
		AmerTempBean amerTempBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_AMER_TEMP, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			amerTempBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return amerTempBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_AMER_TEMP, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_AMER_TEMP, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












