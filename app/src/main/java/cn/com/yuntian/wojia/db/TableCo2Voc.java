package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.Co2VocBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableCo2Voc extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableCo2Voc(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_CO2_VOC;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_CO2_VOC +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_VOC + " text," +
			Constants.DIS_CO2 + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxCo2VocMac on " + Constants.TB_NAME_TABLE_CO2_VOC 
				+"(" + Constants.MAC +")";
	}

	private Co2VocBean creatRowResult(Cursor cursor) {
		
		Co2VocBean result = new Co2VocBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisVoc(cursor.getString(cursor.getColumnIndex(Constants.DIS_VOC)));
		result.setDisCo2(cursor.getString(cursor.getColumnIndex(Constants.DIS_CO2)));
		
		return result;
	}
	
	public void add(Co2VocBean Co2VocBean) {
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, Co2VocBean.getMac());
		values.put(Constants.USER_NAME, Co2VocBean.getUserName());
		values.put(Constants.DIS_VOC, Co2VocBean.getDisVoc());
		values.put(Constants.DIS_CO2, Co2VocBean.getDisCo2());
		
		db.insert(Constants.TB_NAME_TABLE_CO2_VOC, "", values);
	}
	
	public void addAll(List<Co2VocBean> lstBean) {
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param) {
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_CO2_VOC,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<Co2VocBean> findAll(String[] args) {
		List<Co2VocBean> list = new ArrayList<Co2VocBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? ";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_CO2_VOC, null, selection, args, null, null, null);
		Co2VocBean bn = null;
		while (cursor.moveToNext()) {
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public Co2VocBean findOne(String[] args) {
		Co2VocBean Co2VocBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_CO2_VOC, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			Co2VocBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		return Co2VocBean;
	}
	
	public void delete(Map<String, String> param) {
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_CO2_VOC, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm) {
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_CO2_VOC, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












