package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.Co2VocBean;
import cn.com.yuntian.wojia.logic.FtkzyBean;
import cn.com.yuntian.wojia.logic.GreenTempBean;
import cn.com.yuntian.wojia.logic.HeatingTempBean;
import cn.com.yuntian.wojia.logic.LingDongTempBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.PM25Bean;
import cn.com.yuntian.wojia.logic.PM25VocBean;
import cn.com.yuntian.wojia.logic.YiPuWTempBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;


/**
 */
public class TableDevice extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableDevice(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_DEVICE;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_DEVICE +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.USER_NAME + " text not null," +
			Constants.MAC + " text not null," +
			Constants.DEV_TYPE + " text," +
			Constants.DIS_DEV_NAME + " text," +
			Constants.IS_ONLINE + " text," +
			Constants.ADMIN_ONLY + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxDevMac on " + Constants.TB_NAME_TABLE_DEVICE 
				+"(" + Constants.MAC +")";
	}
	
	public String updateTableSqlForAdminOnly() {
		return "alter table "+ Constants.TB_NAME_TABLE_DEVICE 
			+" add " + Constants.ADMIN_ONLY + " text";
	}

	private ListItemBean creatRowResult(Cursor cursor) {
		
		ListItemBean result = new ListItemBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		result.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		result.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		return result;
	}
	
	public void add(ListItemBean listItemBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, listItemBean.getMac());
		values.put(Constants.USER_NAME, listItemBean.getUserName());
		values.put(Constants.DIS_DEV_NAME, listItemBean.getDeviceName());
		values.put(Constants.DEV_TYPE, listItemBean.getDeviceType());
		values.put(Constants.IS_ONLINE, listItemBean.getIsOnline());
		values.put(Constants.ADMIN_ONLY, listItemBean.getAdminOnly());
		
		db.insert(Constants.TB_NAME_TABLE_DEVICE, "", values);
	}
	
	public void addAll(List<ListItemBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_DEVICE,
				values,
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public ListItemBean findOne(String[] args){
		ListItemBean listItemBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_DEVICE, null, selection, args, null, null, null);
		if (cursor.moveToNext()) {
			listItemBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		return listItemBean;
	}
	
	public List<ListItemBean> findAll(){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? ";
		String[] value = {UserInfo.UserInfo.getUserName()};
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_DEVICE, null, selection, value, null, null, Constants.TABLE_ID);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();		
		return list;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_DEVICE, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
//	public void deleteForType(String[] args){
//		SQLiteDatabase db = sqllite.getWritableDatabase();
//		db.delete(Constants.TB_NAME_TABLE_DEVICE, 
//				Constants.DEV_TYPE + " IN(?) ",
//				args);
//	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_DEVICE, 
				Constants.USER_NAME + " = '" + userNm + "'" , null);
	}
	
	public List<ListItemBean> findAllDevice(){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		List<ListItemBean> pm25List = findPm25();
//		List<ListItemBean> amerList = findAmerTemp(null, null);
		List<ListItemBean> greenList = findGreenTemp();
		List<ListItemBean> pm25VocList = findPm25Voc();
		List<ListItemBean> heatingList = findHeatingTemp();
		List<ListItemBean> co2VocList = findCo2Voc();
		List<ListItemBean> lingDongList = findLingDongTemp();
		List<ListItemBean> ftkzyList = findFtkzy();
		List<ListItemBean> yiPuWTempBeanList = findYiPuWTemp();
		for (ListItemBean bean : pm25List) {
			list.add(bean);
		}
//		for (ListItemBean bean : amerList) {
//			list.add(bean);
//		}
		for (ListItemBean bean : greenList) {
			list.add(bean);
		}
		for (ListItemBean bean : pm25VocList) {
			list.add(bean);
		}
		for (ListItemBean bean : heatingList) {
			list.add(bean);
		}
		for (ListItemBean bean : co2VocList) {
			list.add(bean);
		}
		for (ListItemBean bean : lingDongList) {
			list.add(bean);
		}
		for (ListItemBean bean : ftkzyList) {
			list.add(bean);
		}
		for (ListItemBean bean : yiPuWTempBeanList) {
			list.add(bean);
		}
		Collections.sort(list);
		
		return list;
	}

	public List<ListItemBean> findYiPuWTemp() {
		List<ListItemBean> list = new ArrayList<ListItemBean>();

		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, "
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_YIPUW_TEMP + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C."
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getYiPuWRowResult(cursor);
			list.add(bn);
		}

		cursor.close();

		return list;
	}

	private ListItemBean getYiPuWRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		YiPuWTempBean yiPuWTempBean = new YiPuWTempBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));

		yiPuWTempBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		yiPuWTempBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		yiPuWTempBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		yiPuWTempBean.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		yiPuWTempBean.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		yiPuWTempBean.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		yiPuWTempBean.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		yiPuWTempBean.setTempHeatSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_SAVE_ENERGY)));
		yiPuWTempBean.setHeatMode(cursor.getString(cursor.getColumnIndex(Constants.HEAT_MODE)));
		yiPuWTempBean.setForbidMode(cursor.getString(cursor.getColumnIndex(Constants.FORBID_MODE)));

		listBean.setYiPuWTempBean(yiPuWTempBean);

		return listBean;
	}

	public List<ListItemBean> findPm25(){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_PM25 + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getPm25RowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	private ListItemBean getPm25RowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		PM25Bean pm25Bean = new PM25Bean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		pm25Bean.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		pm25Bean.setDisPm25In(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_IN)));
		pm25Bean.setDisPm25Out(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_OUT)));
		pm25Bean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		pm25Bean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		pm25Bean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		
		listBean.setPm25bean(pm25Bean);
		
		return listBean;
	}
	
	public List<ListItemBean> findAmerTemp(String logicMark, String devType){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_AMER_TEMP + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		
		if (CheckUtil.requireCheck(devType)) {
			where = where + " AND A." + Constants.DEV_TYPE + logicMark + "'" + devType + "'";
		} 
		
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getAmerRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	private ListItemBean getAmerRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		AmerTempBean amerBean = new AmerTempBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		amerBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		amerBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		amerBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		amerBean.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		amerBean.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		amerBean.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		amerBean.setTempCool(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COOL)));
		amerBean.setMod(cursor.getString(cursor.getColumnIndex(Constants.MOD)));
		amerBean.setHumi(cursor.getString(cursor.getColumnIndex(Constants.HUMI)));
		amerBean.setFanMod(cursor.getString(cursor.getColumnIndex(Constants.FAN_MOD)));
		amerBean.setPromable(cursor.getString(cursor.getColumnIndex(Constants.PROMABLE)));
		amerBean.setDeadZoneTemp(cursor.getString(cursor.getColumnIndex(Constants.DEAD_ZONE_TEMP)));
		amerBean.setColdStatus(cursor.getString(cursor.getColumnIndex(Constants.COLD_STATUS)));
		amerBean.setHeatStatus(cursor.getString(cursor.getColumnIndex(Constants.HEAT_STATUS)));
		amerBean.setAutoStatus(cursor.getString(cursor.getColumnIndex(Constants.AUTO_STATUS)));
		amerBean.setEmerStatus(cursor.getString(cursor.getColumnIndex(Constants.EMER_STATUS)));
		amerBean.setPermStatus(cursor.getString(cursor.getColumnIndex(Constants.PERM_STATUS)));
		amerBean.setOriMod(cursor.getString(cursor.getColumnIndex(Constants.ORI_MOD)));
		amerBean.setOriStatus(cursor.getString(cursor.getColumnIndex(Constants.ORI_STATUS)));
		
		listBean.setAmerTempBean(amerBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findGreenTemp(){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_GREEN_TEMP + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		
		
//		String orderBy = " ORDER BY C." + Constants.TABLE_ID;
//		String subDevSql = "(SELECT A.* FROM " +  Constants.TB_NAME_TABLE_DEVICE + " A, "
//				+ Constants.TB_NAME_TABLE_USER + " B WHERE A.USER_NAME = B.USER_NAME) C, ";
//		String subGreenSql = "(SELECT A.* FROM " +  Constants.TB_NAME_TABLE_GREEN_TEMP + " A, "
//				+ Constants.TB_NAME_TABLE_USER + " B WHERE A.USER_NAME = B.USER_NAME) D ";
//		String sql = "SELECT C.*, D.* FROM " + subDevSql + subGreenSql
//				+  "WHERE C." + Constants.MAC + " = D." + Constants.MAC + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getGreenRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
//	public List<ListItemBean> findTest() {
//		List<ListItemBean> list = new ArrayList<ListItemBean>();
//		SQLiteDatabase db = sqllite.getReadableDatabase();
////		String sql = "SELECT A.* FROM " +  Constants.TB_NAME_TABLE_DEVICE + " A, "
////				+ Constants.TB_NAME_TABLE_USER + " B WHERE A.USER_NAME = '" + "862555020072476'" ;
//		String sql = "SELECT A.* FROM " +  Constants.TB_NAME_TABLE_DEVICE + " A "
//				+ "  WHERE A.USER_NAME = '" + "862555020072476'" ;
//		Cursor cursor = db.rawQuery(sql, null);
//
//		while(cursor.moveToNext()){
//			ListItemBean listBean = new ListItemBean();
//			listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
//			listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
//			listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
//			listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
//			listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
//			listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
//			list.add(listBean);
//		}
//		
//		cursor.close();
//		return list;
//	}
	
	private ListItemBean getGreenRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		GreenTempBean greenBean = new GreenTempBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		greenBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		greenBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		greenBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		greenBean.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		greenBean.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		greenBean.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		greenBean.setTempCool(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COOL)));
		greenBean.setMod(cursor.getString(cursor.getColumnIndex(Constants.MOD)));
		greenBean.setFanMod(cursor.getString(cursor.getColumnIndex(Constants.FAN_MOD)));
		greenBean.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		greenBean.setIntsHeatStat(cursor.getString(cursor.getColumnIndex(Constants.INTS_HEAT_STAT)));
		greenBean.setHeatOutStat(cursor.getString(cursor.getColumnIndex(Constants.HEAT_OUT_STAT)));
		
		listBean.setGreenTempBean(greenBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findPm25Voc(){
		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_PM25_VOC + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getPm25VocRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	private ListItemBean getPm25VocRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		PM25VocBean pm25VocBean = new PM25VocBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		pm25VocBean.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		pm25VocBean.setDisPm25In(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_IN)));
		pm25VocBean.setDisPm25Out(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_OUT)));
		pm25VocBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		pm25VocBean.setDisVoc(cursor.getString(cursor.getColumnIndex(Constants.DIS_VOC)));
		pm25VocBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		pm25VocBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		
		listBean.setPm25VocBean(pm25VocBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findHeatingTemp(){

		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_HEATING_TEMP + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getHeatingRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;

	}
	
	private ListItemBean getHeatingRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		HeatingTempBean heatingBean = new HeatingTempBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		heatingBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		heatingBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		heatingBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		heatingBean.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		heatingBean.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		heatingBean.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		heatingBean.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		heatingBean.setTempHeatSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_SAVE_ENERGY)));
		
		listBean.setHeatingTempBean(heatingBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findCo2Voc(){

		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_CO2_VOC + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getCo2VocRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;

	}
	
	private ListItemBean getCo2VocRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		Co2VocBean co2VocBean = new Co2VocBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		co2VocBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		co2VocBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		co2VocBean.setDisCo2(cursor.getString(cursor.getColumnIndex(Constants.DIS_CO2)));
		co2VocBean.setDisVoc(cursor.getString(cursor.getColumnIndex(Constants.DIS_VOC)));
		
		listBean.setCo2VocBean(co2VocBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findLingDongTemp(){

		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_LINGDONG_TEMP + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getLingDongRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;

	}
	
	private ListItemBean getLingDongRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		LingDongTempBean lingDongBean = new LingDongTempBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		lingDongBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		lingDongBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		lingDongBean.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		lingDongBean.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		lingDongBean.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		lingDongBean.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		lingDongBean.setTempOut(cursor.getString(cursor.getColumnIndex(Constants.TEMP_OUT)));
		lingDongBean.setTempEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_ENERGY)));
		lingDongBean.setTempComfort(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COMFORT)));
		lingDongBean.setHeatMode(cursor.getString(cursor.getColumnIndex(Constants.HEAT_MODE)));
		lingDongBean.setLinkageInput(cursor.getString(cursor.getColumnIndex(Constants.LINKAGE_INPUT)));
		lingDongBean.setLinkageOutput(cursor.getString(cursor.getColumnIndex(Constants.LINKAGE_OUTPUT)));
		lingDongBean.setCloseBoiler(cursor.getString(cursor.getColumnIndex(Constants.CLOSE_BOILER)));
		lingDongBean.setSchedule(cursor.getString(cursor.getColumnIndex(Constants.SCHEDULE)));
		lingDongBean.setTempHeatDefaultMax(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MAX)));
		lingDongBean.setTempHeatDefaultMin(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MIN)));
		
		listBean.setLingDongTempBean(lingDongBean);
		
		return listBean;
	}
	
	public List<ListItemBean> findFtkzy(){

		List<ListItemBean> list = new ArrayList<ListItemBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String table = Constants.TB_NAME_TABLE_DEVICE + " A, " 
				+ Constants.TB_NAME_TABLE_USER + " B, "
				+ Constants.TB_NAME_TABLE_FTKZY + " C ";
		String where = "WHERE A." + Constants.USER_NAME + "= B."
				+ Constants.USER_NAME + " AND A."
				+ Constants.USER_NAME + "= C." 
				+ Constants.USER_NAME + " AND A."
				+ Constants.MAC + "= C." + Constants.MAC;
		String orderBy = " ORDER BY A." + Constants.TABLE_ID;
		String sql = "SELECT C.*, A.* FROM " + table + where + orderBy;
		Cursor cursor = db.rawQuery(sql, null);
		ListItemBean bn = null;
		while(cursor.moveToNext()){
			bn = getFtkzyRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;

	}
	
	private ListItemBean getFtkzyRowResult(Cursor cursor) {
		ListItemBean listBean = new ListItemBean();
		FtkzyBean ftkzyBean = new FtkzyBean();
		listBean.setTableId(cursor.getString(cursor.getColumnIndex(Constants.TABLE_ID)));
		listBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		listBean.setDeviceName(cursor.getString(cursor.getColumnIndex(Constants.DIS_DEV_NAME)));
		listBean.setDeviceType(cursor.getString(cursor.getColumnIndex(Constants.DEV_TYPE)));
		listBean.setIsOnline(cursor.getString(cursor.getColumnIndex(Constants.IS_ONLINE)));
		listBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		listBean.setAdminOnly(cursor.getString(cursor.getColumnIndex(Constants.ADMIN_ONLY)));
		
		ftkzyBean.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		ftkzyBean.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		ftkzyBean.setCollTempOne(cursor.getString(cursor.getColumnIndex(Constants.COLL_TEMP_ONE)));
		ftkzyBean.setTankTempTwo(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_TWO)));
		ftkzyBean.setTankTempThree(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_THREE)));
		ftkzyBean.setBackTempFour(cursor.getString(cursor.getColumnIndex(Constants.BACK_TEMP_FOUR)));
		ftkzyBean.setTankTempFlg(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_FLG)));
		ftkzyBean.setCollPump(cursor.getString(cursor.getColumnIndex(Constants.COLL_PUMP)));
		ftkzyBean.setPipePump(cursor.getString(cursor.getColumnIndex(Constants.PIPE_PUMP)));
		ftkzyBean.setAuxiHeat(cursor.getString(cursor.getColumnIndex(Constants.AUXI_HEAT)));
		ftkzyBean.setCollPumpOnDev(cursor.getString(cursor.getColumnIndex(Constants.COLL_PUMP_ON_DEV)));
		ftkzyBean.setPipePumpOnDev(cursor.getString(cursor.getColumnIndex(Constants.PIPE_PUMP_ON_DEV)));
		ftkzyBean.setAuxiHeatOnDev(cursor.getString(cursor.getColumnIndex(Constants.AUXI_HEAT_ON_DEV)));
		
		listBean.setFtkzyBean(ftkzyBean);
		
		return listBean;
	}
	
}












