package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.FtkzyBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableFtkzy extends ATable {

	TableFtkzy(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_FTKZY;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_FTKZY +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.COLL_TEMP_ONE + " text," +
			Constants.TANK_TEMP_TWO + " text," +
			Constants.TANK_TEMP_THREE + " text," +
			Constants.BACK_TEMP_FOUR + " text," +
			Constants.TANK_TEMP_FLG + " text," +
			Constants.COLL_PUMP + " text," +
			Constants.PIPE_PUMP + " text," +
			Constants.AUXI_HEAT + " text," +
			Constants.COLL_PUMP_ON_DEV + " text," +
			Constants.PIPE_PUMP_ON_DEV + " text," +
			Constants.AUXI_HEAT_ON_DEV + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxFtkzyMac on " + Constants.TB_NAME_TABLE_FTKZY 
				+"(" + Constants.MAC +")";
	}
	
	public String updateTableSqlForColl() {
		return "alter table "+ Constants.TB_NAME_TABLE_FTKZY 
			+" add " + Constants.COLL_PUMP_ON_DEV + " text";
	}
	
	public String updateTableSqlForPipe() {
		return "alter table "+ Constants.TB_NAME_TABLE_FTKZY 
			+" add " + Constants.PIPE_PUMP_ON_DEV + " text";
	}
	
	public String updateTableSqlForAuxi() {
		return "alter table "+ Constants.TB_NAME_TABLE_FTKZY 
			+" add " + Constants.AUXI_HEAT_ON_DEV + " text";
	}

	private FtkzyBean creatRowResult(Cursor cursor) {
		
		FtkzyBean result = new FtkzyBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setCollTempOne(cursor.getString(cursor.getColumnIndex(Constants.COLL_TEMP_ONE)));
		result.setTankTempTwo(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_TWO)));
		result.setTankTempThree(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_THREE)));
		result.setBackTempFour(cursor.getString(cursor.getColumnIndex(Constants.BACK_TEMP_FOUR)));
		result.setTankTempFlg(cursor.getString(cursor.getColumnIndex(Constants.TANK_TEMP_FLG)));
		result.setCollPump(cursor.getString(cursor.getColumnIndex(Constants.COLL_PUMP)));
		result.setPipePump(cursor.getString(cursor.getColumnIndex(Constants.PIPE_PUMP)));
		result.setAuxiHeat(cursor.getString(cursor.getColumnIndex(Constants.AUXI_HEAT)));
		result.setCollPumpOnDev(cursor.getString(cursor.getColumnIndex(Constants.COLL_PUMP_ON_DEV)));
		result.setPipePumpOnDev(cursor.getString(cursor.getColumnIndex(Constants.PIPE_PUMP_ON_DEV)));
		result.setAuxiHeatOnDev(cursor.getString(cursor.getColumnIndex(Constants.AUXI_HEAT_ON_DEV)));
		
		return result;
	}
	
	public void add(FtkzyBean ftkzyBean){
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, ftkzyBean.getMac());
		values.put(Constants.USER_NAME, ftkzyBean.getUserName());
		values.put(Constants.COLL_TEMP_ONE, ftkzyBean.getCollTempOne());
		values.put(Constants.TANK_TEMP_TWO, ftkzyBean.getTankTempTwo());
		values.put(Constants.TANK_TEMP_THREE, ftkzyBean.getTankTempThree());
		values.put(Constants.BACK_TEMP_FOUR, ftkzyBean.getBackTempFour());
		values.put(Constants.TANK_TEMP_FLG, ftkzyBean.getTankTempFlg());
		values.put(Constants.COLL_PUMP, ftkzyBean.getCollPump());
		values.put(Constants.PIPE_PUMP, ftkzyBean.getPipePump());
		values.put(Constants.AUXI_HEAT, ftkzyBean.getAuxiHeat());
		values.put(Constants.COLL_PUMP_ON_DEV, ftkzyBean.getCollPumpOnDev());
		values.put(Constants.PIPE_PUMP_ON_DEV, ftkzyBean.getPipePumpOnDev());
		values.put(Constants.AUXI_HEAT_ON_DEV, ftkzyBean.getAuxiHeatOnDev());
		
		db.insert(Constants.TB_NAME_TABLE_FTKZY, "", values);
	}
	
	public void addAll(List<FtkzyBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_FTKZY,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<FtkzyBean> findAll(){
		List<FtkzyBean> list = new ArrayList<FtkzyBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_FTKZY, null, null, null, null, null, null);
		FtkzyBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public FtkzyBean findOne(String[] args){
		FtkzyBean ftkzyBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_FTKZY, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			ftkzyBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return ftkzyBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_FTKZY, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_FTKZY, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












