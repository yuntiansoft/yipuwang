package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.GreenTempBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableGreenTemp extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableGreenTemp(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_GREEN_TEMP;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_GREEN_TEMP +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.TEMP_HEAT + " text," +
			Constants.STATUS + " text," +
			Constants.TEMP_COOL + " text," +
			Constants.MOD + " text," +
			Constants.FAN_MOD + " text," +
			Constants.SAVE_ENERGY + " text," +
			Constants.STATUS_ON_OFF + " text," +
			Constants.TEMP_COOL_SAVE_ENERGY + " text," +
			Constants.TEMP_HEAT_SAVE_ENERGY + " text," +
			Constants.INTS_HEAT_STAT + " text," +
			Constants.TEMP_HEAT_DEFAULT_MIN + " text," +
			Constants.TEMP_HEAT_DEFAULT_MAX + " text," +
			Constants.HEAT_OUT_STAT + " text" +
		")";
	}
	
	public String updateTableSql() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP 
			+" add " + Constants.STATUS_ON_OFF + " text";
	}
	
	public String updateTableSqlForCoolEnergy() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP 
			+ " add " + Constants.TEMP_COOL_SAVE_ENERGY + " text";
	}
	
	public String updateTableSqlForHeatEnergy() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP 
			+ " add " + Constants.TEMP_HEAT_SAVE_ENERGY + " text";
	}
	
	public String updateTableSqlForIntsHeatStat() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP 
			+ " add " + Constants.INTS_HEAT_STAT + " text";
	}
	
	public String updateTableSqlForHeatOutStat() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP 
			+ " add " + Constants.HEAT_OUT_STAT + " text";
	}
	
	public String createTableIndex() {
		return "create index idxGreenMac on " + Constants.TB_NAME_TABLE_GREEN_TEMP 
				+"(" + Constants.MAC +")";
	}

	public String updateTableSqlForTempHeatDefaultMin() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP
				+ " add " + Constants.TEMP_HEAT_DEFAULT_MIN + " text";
	}

	public String updateTableSqlForTempHeatDefaultMax() {
		return "alter table "+ Constants.TB_NAME_TABLE_GREEN_TEMP
				+ " add " + Constants.TEMP_HEAT_DEFAULT_MAX + " text";
	}
	
	private GreenTempBean creatRowResult(Cursor cursor) {
		
		GreenTempBean result = new GreenTempBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		result.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		result.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		result.setTempCool(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COOL)));
		result.setMod(cursor.getString(cursor.getColumnIndex(Constants.MOD)));
		result.setFanMod(cursor.getString(cursor.getColumnIndex(Constants.FAN_MOD)));
		result.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		result.setTempCoolSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COOL_SAVE_ENERGY)));
		result.setTempHeatSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_SAVE_ENERGY)));
		result.setIntsHeatStat(cursor.getString(cursor.getColumnIndex(Constants.INTS_HEAT_STAT)));
		result.setHeatOutStat(cursor.getString(cursor.getColumnIndex(Constants.HEAT_OUT_STAT)));
		result.setTempHeatDefaultMin(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MIN)));
		result.setTempHeatDefaultMAx(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MAX)));
		return result;
	}
	
	public void add(GreenTempBean greenTempBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, greenTempBean.getMac());
		values.put(Constants.USER_NAME, greenTempBean.getUserName());
		values.put(Constants.DIS_TEMP, greenTempBean.getDisTemp());
		values.put(Constants.TEMP_HEAT, greenTempBean.getTempHeat());
		values.put(Constants.STATUS, greenTempBean.getStatus());
		values.put(Constants.STATUS_ON_OFF, greenTempBean.getStatusOnOff());
		values.put(Constants.TEMP_COOL, greenTempBean.getTempCool());
		values.put(Constants.MOD, greenTempBean.getMod());
		values.put(Constants.FAN_MOD, greenTempBean.getFanMod());
		values.put(Constants.SAVE_ENERGY, greenTempBean.getSaveEnergy());
		values.put(Constants.TEMP_COOL_SAVE_ENERGY, greenTempBean.getTempCoolSaveEnergy());
		values.put(Constants.TEMP_HEAT_SAVE_ENERGY, greenTempBean.getTempHeatSaveEnergy());
		values.put(Constants.INTS_HEAT_STAT, greenTempBean.getIntsHeatStat());
		values.put(Constants.HEAT_OUT_STAT, greenTempBean.getHeatOutStat());
		values.put(Constants.TEMP_HEAT_DEFAULT_MIN,greenTempBean.getTempHeatDefaultMin());
		values.put(Constants.TEMP_HEAT_DEFAULT_MAX,greenTempBean.getTempHeatDefaultMAx());
		
		db.insert(Constants.TB_NAME_TABLE_GREEN_TEMP, "", values);
	}
	
	public void addAll(List<GreenTempBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_GREEN_TEMP,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<GreenTempBean> findAll(){
		List<GreenTempBean> list = new ArrayList<GreenTempBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_GREEN_TEMP, null, null, null, null, null, null);
		GreenTempBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public GreenTempBean findOne(String[] args){
		GreenTempBean greenTempBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_GREEN_TEMP, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			greenTempBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return greenTempBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_GREEN_TEMP, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_GREEN_TEMP, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}


}












