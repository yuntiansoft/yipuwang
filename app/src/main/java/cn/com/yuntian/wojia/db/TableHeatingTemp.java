package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.HeatingTempBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableHeatingTemp extends ATable {

	/**
	 * ��ů�¿�����
	 */
	TableHeatingTemp(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_HEATING_TEMP;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_HEATING_TEMP +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.TEMP_HEAT + " text," +
			Constants.SAVE_ENERGY + " text," +
			Constants.STATUS_ON_OFF + " text," +
			Constants.STATUS + " text," +
				Constants.TEMP_HEAT_DEFAULT_MAX + " text," +
				Constants.TEMP_HEAT_DEFAULT_MIN + " text," +
			Constants.TEMP_HEAT_SAVE_ENERGY + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxHeatingMac on " + Constants.TB_NAME_TABLE_HEATING_TEMP 
				+"(" + Constants.MAC +")";
	}
	
	public String updateTableSql() {
		return "alter table "+ Constants.TB_NAME_TABLE_HEATING_TEMP 
			+" add " + Constants.TEMP_HEAT_SAVE_ENERGY + " text";
	}
	
	public String updateTableForStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_HEATING_TEMP 
			+" add " + Constants.STATUS + " text";
	}

	public String updateTableForTempHeatDefaultMax(){
		return "alter table "+ Constants.TB_NAME_TABLE_HEATING_TEMP
				+" add " + Constants.TEMP_HEAT_DEFAULT_MAX + " text";
	}

	public String updateTableForTempHeatDefaultMin(){
		return "alter table "+ Constants.TB_NAME_TABLE_HEATING_TEMP
				+" add " + Constants.TEMP_HEAT_DEFAULT_MIN + " text";
	}



	private HeatingTempBean creatRowResult(Cursor cursor) {
		
		HeatingTempBean result = new HeatingTempBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		result.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		result.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		result.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		result.setTempHeatSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_SAVE_ENERGY)));
		result.setTempHeatDefaultMax(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MAX)));
		result.setTempHeatDefaultMin(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MIN)));
		return result;
	}
	
	public void add(HeatingTempBean HeatingTempBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, HeatingTempBean.getMac());
		values.put(Constants.USER_NAME, HeatingTempBean.getUserName());
		values.put(Constants.DIS_TEMP, HeatingTempBean.getDisTemp());
		values.put(Constants.TEMP_HEAT, HeatingTempBean.getTempHeat());
		values.put(Constants.STATUS_ON_OFF, HeatingTempBean.getStatusOnOff());
		values.put(Constants.STATUS, HeatingTempBean.getStatus());
		values.put(Constants.SAVE_ENERGY, HeatingTempBean.getSaveEnergy());
		values.put(Constants.TEMP_HEAT_SAVE_ENERGY, HeatingTempBean.getTempHeatSaveEnergy());
		values.put(Constants.TEMP_HEAT_DEFAULT_MAX,HeatingTempBean.getTempHeatDefaultMax());
		values.put(Constants.TEMP_HEAT_DEFAULT_MIN,HeatingTempBean.getTempHeatDefaultMin());
		db.insert(Constants.TB_NAME_TABLE_HEATING_TEMP, "", values);
	}
	
	public void addAll(List<HeatingTempBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_HEATING_TEMP,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<HeatingTempBean> findAll(){
		List<HeatingTempBean> list = new ArrayList<HeatingTempBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_HEATING_TEMP, null, null, null, null, null, null);
		HeatingTempBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public HeatingTempBean findOne(String[] args){
		HeatingTempBean HeatingTempBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_HEATING_TEMP, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			HeatingTempBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return HeatingTempBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_HEATING_TEMP, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_HEATING_TEMP, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












