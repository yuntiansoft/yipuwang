package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.HistoryBean;
import cn.com.yuntian.wojia.util.Constants;

/**
 */
public class TableHistory extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableHistory(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_HISTORY;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_HISTORY +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.TIME + " text," +
			Constants.DIS_HUMI + " text," +
			Constants.DIS_TEMP + " text," +
			Constants.DIS_PM25_IN + " text," +
			Constants.DIS_PM25_OUT + " text" +
		")";
	}

	private HistoryBean creatRowResult(Cursor cursor) {
		
		HistoryBean result = new HistoryBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setTime(cursor.getString(cursor.getColumnIndex(Constants.TIME)));
		result.setDisPm25In(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_IN)));
		result.setDisPm25Out(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_OUT)));
		
		return result;
	}
	
	public void add(HistoryBean HistoryBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, HistoryBean.getMac());
		values.put(Constants.USER_NAME, HistoryBean.getUserName());
		values.put(Constants.DIS_HUMI, HistoryBean.getDisHumi());
		values.put(Constants.DIS_TEMP, HistoryBean.getDisTemp());
		values.put(Constants.TIME, HistoryBean.getTime());
		values.put(Constants.DIS_PM25_IN, HistoryBean.getDisPm25In());
		values.put(Constants.DIS_PM25_OUT, HistoryBean.getDisPm25Out());
		
		db.insert(Constants.TB_NAME_TABLE_HISTORY, "", values);
	}
	
	public void addAll(List<HistoryBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_HISTORY,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<HistoryBean> findAll(){
		List<HistoryBean> list = new ArrayList<HistoryBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_HISTORY, null, null, null, null, null, null);
		HistoryBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public List<HistoryBean> findList(String[] args){
		List<HistoryBean> list = new ArrayList<HistoryBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_HISTORY, null, selection, args, null, null, Constants.TIME);
		while (cursor.moveToNext()) {
			HistoryBean HistoryBean = creatRowResult(cursor);
			list.add(HistoryBean);
		}
		
		cursor.close();	
		return list;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_HISTORY, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_HISTORY, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












