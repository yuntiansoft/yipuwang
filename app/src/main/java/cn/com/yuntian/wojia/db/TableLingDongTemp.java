package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import cn.com.yuntian.wojia.db.ATable;
import cn.com.yuntian.wojia.logic.LingDongTempBean;
import cn.com.yuntian.wojia.util.Constants;

/**
 */
public class TableLingDongTemp extends ATable {

	/**
	 * �鶯���²�ů�¿�������
	 */
	TableLingDongTemp(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_LINGDONG_TEMP;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.TEMP_HEAT + " text," +
			Constants.STATUS_ON_OFF + " text," +
			Constants.STATUS + " text," +
			Constants.TEMP_OUT + " text," +
			Constants.TEMP_ENERGY + " text," +
			Constants.TEMP_COMFORT + " text," +
			Constants.HEAT_MODE + " text," +
			Constants.LINKAGE_INPUT + " text," +
			Constants.LINKAGE_OUTPUT + " text," +
			Constants.CLOSE_BOILER + " text," +
			Constants.TEMP_HEAT_DEFAULT_MIN + " text," +
			Constants.TEMP_HEAT_DEFAULT_MAX + " text," +
			Constants.SCHEDULE + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxLingDongMac on " + Constants.TB_NAME_TABLE_LINGDONG_TEMP 
				+"(" + Constants.MAC +")";
	}
	
	public String updateTableSqlForSchedule() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.SCHEDULE + " text";
	}
	
	public String updateTableSqlForLinkageInput() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.LINKAGE_INPUT + " text";
	}
	
	public String updateTableSqlForLinkageOutput() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.LINKAGE_OUTPUT + " text";
	}
	
	public String updateTableSqlForCloseBoiler() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.CLOSE_BOILER + " text";
	}
	
	public String updateTableSqlForDefaultHeatMax() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.TEMP_HEAT_DEFAULT_MAX + " text";
	}
	
	public String updateTableSqlForDefaultHeatMin() {
		return "alter table "+ Constants.TB_NAME_TABLE_LINGDONG_TEMP 
			+" add " + Constants.TEMP_HEAT_DEFAULT_MIN + " text";
	}


	private LingDongTempBean creatRowResult(Cursor cursor) {
		
		LingDongTempBean result = new LingDongTempBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		result.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		result.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		result.setTempOut(cursor.getString(cursor.getColumnIndex(Constants.TEMP_OUT)));
		result.setTempEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_ENERGY)));
		result.setTempComfort(cursor.getString(cursor.getColumnIndex(Constants.TEMP_COMFORT)));
		result.setHeatMode(cursor.getString(cursor.getColumnIndex(Constants.HEAT_MODE)));
		result.setLinkageInput(cursor.getString(cursor.getColumnIndex(Constants.LINKAGE_INPUT)));
		result.setLinkageOutput(cursor.getString(cursor.getColumnIndex(Constants.LINKAGE_OUTPUT)));
		result.setCloseBoiler(cursor.getString(cursor.getColumnIndex(Constants.CLOSE_BOILER)));
		result.setSchedule(cursor.getString(cursor.getColumnIndex(Constants.SCHEDULE)));
		result.setTempHeatDefaultMax(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MAX)));
		result.setTempHeatDefaultMin(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MIN)));

		return result;
	}
	
	public void add(LingDongTempBean lingDongTempBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, lingDongTempBean.getMac());
		values.put(Constants.USER_NAME, lingDongTempBean.getUserName());
		values.put(Constants.DIS_TEMP, lingDongTempBean.getDisTemp());
		values.put(Constants.TEMP_HEAT, lingDongTempBean.getTempHeat());
		values.put(Constants.STATUS_ON_OFF, lingDongTempBean.getStatusOnOff());
		values.put(Constants.STATUS, lingDongTempBean.getStatus());
		values.put(Constants.TEMP_OUT, lingDongTempBean.getTempOut());
		values.put(Constants.TEMP_ENERGY, lingDongTempBean.getTempEnergy());
		values.put(Constants.TEMP_COMFORT, lingDongTempBean.getTempComfort());
		values.put(Constants.HEAT_MODE, lingDongTempBean.getHeatMode());
		values.put(Constants.LINKAGE_INPUT, lingDongTempBean.getLinkageInput());
		values.put(Constants.LINKAGE_OUTPUT, lingDongTempBean.getLinkageOutput());
		values.put(Constants.CLOSE_BOILER, lingDongTempBean.getCloseBoiler());
		values.put(Constants.SCHEDULE, lingDongTempBean.getSchedule());
		values.put(Constants.TEMP_HEAT_DEFAULT_MAX, lingDongTempBean.getTempHeatDefaultMax());
		values.put(Constants.TEMP_HEAT_DEFAULT_MIN, lingDongTempBean.getTempHeatDefaultMin());

		db.insert(Constants.TB_NAME_TABLE_LINGDONG_TEMP, "", values);
	}
	
	public void addAll(List<LingDongTempBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_LINGDONG_TEMP,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<LingDongTempBean> findAll(){
		List<LingDongTempBean> list = new ArrayList<LingDongTempBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_LINGDONG_TEMP, null, null, null, null, null, null);
		LingDongTempBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public LingDongTempBean findOne(String[] args){
		LingDongTempBean lingdongTempBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_LINGDONG_TEMP, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			lingdongTempBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return lingdongTempBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_LINGDONG_TEMP, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












