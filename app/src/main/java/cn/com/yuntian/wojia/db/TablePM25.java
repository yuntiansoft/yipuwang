package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.PM25Bean;
import cn.com.yuntian.wojia.util.Constants;

/**
 */
public class TablePM25 extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TablePM25(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_PM25;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_PM25 +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.DIS_HUMI + " text," +
			Constants.DIS_PM25_IN + " text," +
			Constants.DIS_PM25_OUT + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxPMMac on " + Constants.TB_NAME_TABLE_PM25 
				+"(" + Constants.MAC +")";
	}

	private PM25Bean creatRowResult(Cursor cursor) {
		
		PM25Bean result = new PM25Bean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		result.setDisPm25In(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_IN)));
		result.setDisPm25Out(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_OUT)));
		
		return result;
	}
	
	public void add(PM25Bean pm25Bean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, pm25Bean.getMac());
		values.put(Constants.USER_NAME, pm25Bean.getUserName());
		values.put(Constants.DIS_TEMP, pm25Bean.getDisTemp());
		values.put(Constants.DIS_HUMI, pm25Bean.getDisHumi());
		values.put(Constants.DIS_PM25_IN, pm25Bean.getDisPm25In());
		values.put(Constants.DIS_PM25_OUT, pm25Bean.getDisPm25Out());
		
		db.insert(Constants.TB_NAME_TABLE_PM25, "", values);
	}
	
	public void addAll(List<PM25Bean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_PM25,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<PM25Bean> findAll(String[] args){
		List<PM25Bean> list = new ArrayList<PM25Bean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? ";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_PM25, null, selection, args, null, null, null);
		PM25Bean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public PM25Bean findOne(String[] args){
		PM25Bean pm25Bean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_PM25, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			pm25Bean = creatRowResult(cursor);
		}
		
		cursor.close();	
		return pm25Bean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_PM25, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_PM25, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












