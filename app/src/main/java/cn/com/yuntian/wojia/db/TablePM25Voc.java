package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.logic.PM25VocBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TablePM25Voc extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TablePM25Voc(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_PM25_VOC;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_PM25_VOC +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.DIS_HUMI + " text," +
			Constants.DIS_VOC + " text," +
			Constants.DIS_PM25_IN + " text," +
			Constants.DIS_PM25_OUT + " text" +
		")";
	}
	
	public String createTableIndex() {
		return "create index idxPMVocMac on " + Constants.TB_NAME_TABLE_PM25_VOC 
				+"(" + Constants.MAC +")";
	}

	private PM25VocBean creatRowResult(Cursor cursor) {
		
		PM25VocBean result = new PM25VocBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setDisHumi(cursor.getString(cursor.getColumnIndex(Constants.DIS_HUMI)));
		result.setDisVoc(cursor.getString(cursor.getColumnIndex(Constants.DIS_VOC)));
		result.setDisPm25In(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_IN)));
		result.setDisPm25Out(cursor.getString(cursor.getColumnIndex(Constants.DIS_PM25_OUT)));
		
		return result;
	}
	
	public void add(PM25VocBean PM25VocBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, PM25VocBean.getMac());
		values.put(Constants.USER_NAME, PM25VocBean.getUserName());
		values.put(Constants.DIS_TEMP, PM25VocBean.getDisTemp());
		values.put(Constants.DIS_HUMI, PM25VocBean.getDisHumi());
		values.put(Constants.DIS_VOC, PM25VocBean.getDisVoc());
		values.put(Constants.DIS_PM25_IN, PM25VocBean.getDisPm25In());
		values.put(Constants.DIS_PM25_OUT, PM25VocBean.getDisPm25Out());
		
		db.insert(Constants.TB_NAME_TABLE_PM25_VOC, "", values);
	}
	
	public void addAll(List<PM25VocBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_PM25_VOC,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<PM25VocBean> findAll(String[] args){
		List<PM25VocBean> list = new ArrayList<PM25VocBean>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? ";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_PM25_VOC, null, selection, args, null, null, null);
		PM25VocBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public PM25VocBean findOne(String[] args){
		PM25VocBean PM25VocBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_PM25_VOC, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			PM25VocBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		return PM25VocBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_PM25_VOC, 
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_PM25_VOC, 
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












