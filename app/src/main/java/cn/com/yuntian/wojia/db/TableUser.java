package cn.com.yuntian.wojia.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableUser extends ATable {

	/**
	 * �û���Ϣ��
	 */
	TableUser(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}
	
	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_USER;
	}
	
	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_USER +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.USER_NAME + " text not null," +
			Constants.PASSWORD + " text not null," +
			Constants.TOKEN_TYPE + " text not null," +
			Constants.ACESS_TOKEN + " text not null," +
			Constants.USER_TYPE + " text," +
			Constants.USER_STATUS + " text," +
			Constants.TEMP_UNIT + " text" +
		")";
	}
	
	public String updateTableSqlForUserType() {
		return "alter table "+ Constants.TB_NAME_TABLE_USER 
			+ " add " + Constants.USER_TYPE + " text";
	}
	
	public String updateTableSqlForUserStatus() {
		return "alter table "+ Constants.TB_NAME_TABLE_USER 
			+ " add " + Constants.USER_STATUS + " text";
	}

	private Map<String, String> creatRowResult(Cursor cursor) {
		
		Map<String, String> result = new HashMap<String, String>();
		
		int colcnt = cursor.getColumnCount();
		String colnm, val = null;
		for (int i=0; i<colcnt; i++) {
			colnm = cursor.getColumnName(i);
			val = cursor.getString(i);
			result.put(colnm, val);
		}
		
		return result;
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_USER,
				values,
				Constants.USER_NAME + " = '" + param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void add(Map<String, String> user){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		Iterator<String> keys = user.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, user.get(key));
		}
		
		db.insert(Constants.TB_NAME_TABLE_USER, "", values);
	}
	
	public List<Map<String, String>> findAll(){
		
		List<Map<String, String>> list = new ArrayList<Map<String, String>>();
		try {
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_USER, null, null, null, null, null, null);
		Map<String, String> bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return list;
	}
	
	public void delete(){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_USER, null, null);
	}
	
}












