package cn.com.yuntian.wojia.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cn.com.yuntian.wojia.logic.HeatingTempBean;
import cn.com.yuntian.wojia.logic.YiPuWTempBean;
import cn.com.yuntian.wojia.util.Constants;


/**
 */
public class TableYiPuWTemp extends ATable {

	/**
	 * ��ů�¿�����
	 */
	TableYiPuWTemp(SQLiteOpenHelper sqllite) {
		super(sqllite);
	}

	@Override
	String getTableName() {
		return Constants.TB_NAME_TABLE_YIPUW_TEMP;
	}

	@Override
	String createTableSql() {
		return "create table "+ Constants.TB_NAME_TABLE_YIPUW_TEMP +"(" +
			Constants.TABLE_ID + " integer primary key autoincrement," +
			Constants.MAC + " text not null," +
			Constants.USER_NAME + " text not null," +
			Constants.DIS_TEMP + " text," +
			Constants.TEMP_HEAT + " text," +
			Constants.SAVE_ENERGY + " text," +
			Constants.STATUS_ON_OFF + " text," +
			Constants.STATUS + " text," +
				Constants.TEMP_HEAT_DEFAULT_MAX + " text," +
				Constants.TEMP_HEAT_DEFAULT_MIN + " text," +
				Constants.HEAT_MODE + " text," +
				Constants.FORBID_MODE + " text," +
			Constants.TEMP_HEAT_SAVE_ENERGY + " text" +
		")";
	}

	public String createTableIndex() {
		return "create index idxHeatingMac on " + Constants.TB_NAME_TABLE_YIPUW_TEMP
				+"(" + Constants.MAC +")";
	}




	private YiPuWTempBean creatRowResult(Cursor cursor) {

		YiPuWTempBean result = new YiPuWTempBean();
		result.setMac(cursor.getString(cursor.getColumnIndex(Constants.MAC)));
		result.setUserName(cursor.getString(cursor.getColumnIndex(Constants.USER_NAME)));
		result.setDisTemp(cursor.getString(cursor.getColumnIndex(Constants.DIS_TEMP)));
		result.setTempHeat(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT)));
		result.setStatusOnOff(cursor.getString(cursor.getColumnIndex(Constants.STATUS_ON_OFF)));
		result.setStatus(cursor.getString(cursor.getColumnIndex(Constants.STATUS)));
		result.setSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.SAVE_ENERGY)));
		result.setTempHeatSaveEnergy(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_SAVE_ENERGY)));
		result.setTempHeatDefaultMax(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MAX)));
		result.setTempHeatDefaultMin(cursor.getString(cursor.getColumnIndex(Constants.TEMP_HEAT_DEFAULT_MIN)));
		result.setHeatMode(cursor.getString(cursor.getColumnIndex(Constants.HEAT_MODE)));
		result.setForbidMode(cursor.getString(cursor.getColumnIndex(Constants.FORBID_MODE)));
		return result;
	}
	
	public void add(YiPuWTempBean yiPuWTempBean){
		// insert into ��() values()
		SQLiteDatabase db = sqllite.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(Constants.MAC, yiPuWTempBean.getMac());
		values.put(Constants.USER_NAME, yiPuWTempBean.getUserName());
		values.put(Constants.DIS_TEMP, yiPuWTempBean.getDisTemp());
		values.put(Constants.TEMP_HEAT, yiPuWTempBean.getTempHeat());
		values.put(Constants.STATUS_ON_OFF, yiPuWTempBean.getStatusOnOff());
		values.put(Constants.STATUS, yiPuWTempBean.getStatus());
		values.put(Constants.SAVE_ENERGY, yiPuWTempBean.getSaveEnergy());
		values.put(Constants.TEMP_HEAT_SAVE_ENERGY, yiPuWTempBean.getTempHeatSaveEnergy());
		values.put(Constants.TEMP_HEAT_DEFAULT_MAX,yiPuWTempBean.getTempHeatDefaultMax());
		values.put(Constants.TEMP_HEAT_DEFAULT_MIN,yiPuWTempBean.getTempHeatDefaultMin());
		values.put(Constants.HEAT_MODE,yiPuWTempBean.getHeatMode());
		values.put(Constants.FORBID_MODE,yiPuWTempBean.getForbidMode());
		db.insert(Constants.TB_NAME_TABLE_YIPUW_TEMP, "", values);
	}
	
	public void addAll(List<YiPuWTempBean> lstBean){
		if (lstBean != null) {
			for (int i = 0; i < lstBean.size(); i++) {
				add(lstBean.get(i));
			}
		}
	}
	
	public void update(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<String> keys = param.keySet().iterator();
		while (keys.hasNext()) {
			String key = keys.next();
			values.put(key, param.get(key));
		}
		db.update(
				Constants.TB_NAME_TABLE_YIPUW_TEMP,
				values,
				Constants.MAC + " = '" + param.get(Constants.MAC) 
				+ "' AND "+ Constants.USER_NAME + " = '" 
				+ param.get(Constants.USER_NAME)+ "'",
				null);
	}
	
	public List<YiPuWTempBean> findAll(){
		List<YiPuWTempBean> list = new ArrayList<>();
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_YIPUW_TEMP, null, null, null, null, null, null);
		YiPuWTempBean bn = null;
		while(cursor.moveToNext()){
			bn = creatRowResult(cursor);
			list.add(bn);
		}
		
		cursor.close();
		
		return list;
	}
	
	public YiPuWTempBean findOne(String[] args){
		YiPuWTempBean yiPuWTempBean = null;
		
		SQLiteDatabase db = sqllite.getReadableDatabase();
		String selection = Constants.USER_NAME+ " = ? AND " 
				+ Constants.MAC + " = ?";
		Cursor cursor = db.query(
				Constants.TB_NAME_TABLE_YIPUW_TEMP, null, selection, args, null, null, null);
		if(cursor.moveToNext()){
			yiPuWTempBean = creatRowResult(cursor);
		}
		
		cursor.close();	
		
		return yiPuWTempBean;
	}
	
	public void delete(Map<String, String> param){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_YIPUW_TEMP,
				Constants.MAC + "='" + param.get(Constants.MAC) 
				+ "' AND " + Constants.USER_NAME + "='"
				+ param.get(Constants.USER_NAME) + "'",
				null);
	}
	
	public void deleteAll(String userNm){
		SQLiteDatabase db = sqllite.getWritableDatabase();
		db.delete(Constants.TB_NAME_TABLE_YIPUW_TEMP,
				Constants.USER_NAME + "='" + userNm + "'" , null);
	}
	
}












