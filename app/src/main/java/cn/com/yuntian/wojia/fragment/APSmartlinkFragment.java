package cn.com.yuntian.wojia.fragment;

import java.io.Serializable;
import java.net.DatagramPacket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
//import android.app.Dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;

import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

//import android.security.Credentials;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;

import android.text.TextWatcher;

import android.util.Log;
import android.view.Gravity;

import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.View;
import android.view.View.OnClickListener;

import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;



import com.hf.smartlink.android.AccessPoint;


import com.hf.smartlink.android.AccessPointAdapter;
import com.hf.smartlink.android.Repeater;
import com.hf.smartlink.android.Scanner;


import com.hf.smartlink.android.WifiAutomaticConnecter;
import com.hf.smartlink.android.WifiEnabler;
import com.hf.smartlink.model.ATCommand;
import com.hf.smartlink.model.ATCommandListener;
import com.hf.smartlink.model.Module;
import com.hf.smartlink.model.NetworkProtocol;
import com.hf.smartlink.model.WifiStatus;
import com.hf.smartlink.net.UdpBroadcast;
import com.hf.smartlink.net.UdpUnicast;
import com.hf.smartlink.utils.Utils;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.OnPressBackListener;
import cn.com.yuntian.wojia.util.AppContext;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;


/**
 * SmartlinkFragment
 *
 * @author chenwh
 */
public class APSmartlinkFragment extends Fragment implements OnClickListener {
    private static final int MSG_ENTER_CMD = 1;
    private static final int MSG_RETRY_ENTER_CMD = 2;
    private static final int MSG_ENABLE_WIFI = 3;

    private final IntentFilter mFilter;
    private final BroadcastReceiver mReceiver;
    String TAG = "SmartLink | ";
    private Handler mNotifierHandler;

    private WifiManager mWifiManager;
    private WifiEnabler mWifiEnabler;
    private NetworkInfo.DetailedState mLastState;
    private WifiInfo mLastInfo;
    private Scanner mScanner;
    private WifiAutomaticConnecter mWifiAutomaticConnecter;
    private boolean mResetNetworks = true;
    private List<AccessPoint> mLatestAccessPoints;
    private List<Module> mModules;
    private WifiStatus mLastWifiStatus;
    private long mLastCMD;
    private boolean mIsCMDMode;


    /**
     * The flag to indicate the activity is destroyed
     */
    private boolean mIsExit;
    private int mFailedTimes;
    private UdpBroadcast mScanBroadcast;
    private UdpUnicast mUdpUnicast;
    private ATCommand mATCommand;
    private ATCommandListener mATCommandListener;
    /**
     * The response of AT command
     */
    private StringBuffer mAtResponse = new StringBuffer();
    private Handler mNetworkHandler;
    private Repeater mTestCmdRepeater;

    private ScanResult mConnect2ScanResult;


    private EditText wifiPwdText;
    private TextView connectButton;
    private EditText ssidText;
    // checkBox
    private CheckBox wifiPwdCheck;

    private CustomProgressDialog progressDialog = null;

    private String mac = null;

    private boolean isRun = true;
    private boolean isLoading = true;
    private HailinDB db;
    private CheckBox mToggleButton;
    private TextView mWiFiStateTextView;
    private View contentView;
    private TextView mStatusTextView;
    private static final HandlerThread handlerThread = new HandlerThread("aotuconnect");

    static {
        handlerThread.start();
    }

    private View backBtn;

    public APSmartlinkFragment() {
        mNotifierHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 0:
                        Toast.makeText(getActivity().getApplicationContext(), msg.obj.toString(), Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        Toast.makeText(getActivity().getApplicationContext(), msg.arg1, msg.arg2 == 1 ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.default_dialog_title)
                                .setMessage(msg.arg1)
                                .setPositiveButton(R.string.ok, null)
                                .create().show();
                        break;
                    case 3:

                        new AlertDialog.Builder(getActivity())
                                .setTitle(R.string.default_dialog_title)
                                .setMessage(msg.obj.toString())
                                .setPositiveButton(R.string.ok, null)
                                .create().show();
                        break;
                    case 4:

                        Bundle data = msg.getData();
                        SimpleDialogListener listener = null;
                        if (data.getSerializable("listener") != null) {
                            listener = (SimpleDialogListener) data.getSerializable("listener");
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                                .setTitle(data.getString("title"))
                                .setMessage(data.getString("message"))
                                .setPositiveButton(R.string.ok, listener);
                        if (data.getBoolean("withCancelButton")) {
                            builder.setNegativeButton(R.string.no, null);
                        }
                        builder.create().show();
                        break;

                    default:
                        break;
                }
            }
        };


        mFilter = new IntentFilter();
        mFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        mFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        mFilter.addAction(WifiManager.NETWORK_IDS_CHANGED_ACTION);
        mFilter.addAction(WifiManager.SUPPLICANT_STATE_CHANGED_ACTION);
        mFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mFilter.addAction(WifiManager.RSSI_CHANGED_ACTION);

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleEvent(intent);
            }
        };

        mScanBroadcast = new UdpBroadcast() {

            @Override
            public void onReceived(List<DatagramPacket> packets) {

                mModules = Utils.decodePackets(getActivity(), packets);
                //save the ap module mid info into local
                if (mModules != null && mModules.size() > 0 && mModules.get(0) != null) {
                    Log.e(TAG, "ScanBroadcast: save the module info in local file:"+ mModules.get(0));
                    Utils.saveDevice(getActivity(), generateNetworkKey(), mModules.get(0));
                    mScanBroadcast.close();
                    //disconnect the current connection, so that the wifi automatic connector will connect it again;
                    //and it can get the module info from local, and enter cmd mode; it avoid send broadcast to get mid info
                    //each times when connected
                    if (isRun) {
//                        mNetworkHandler.removeMessages(MSG_RETRY_ENTER_CMD);
//                        mNetworkHandler.sendEmptyMessage(MSG_ENTER_CMD);
//                        hasEnterCMD = true;
                        if (!hasEnterCMD) {
                            mNetworkHandler.removeMessages(MSG_RETRY_ENTER_CMD);
                            mNetworkHandler.sendEmptyMessage(MSG_ENTER_CMD);
                            hasEnterCMD = true;
                        }
//                        mWifiManager.disconnect();
//                        Log.e(TAG, "onReceived: disconnect");
                    }

                } else {
                    logD("ScanBroadcast: not find any module info");
                    if (hasReceiveIP){
                        mScanBroadcast.open();
                        mScanBroadcast.send(Utils.getCMDScanModules(getActivity()));
                    }
                }
            }
        };

        mNetworkHandler = new Handler() {

            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);

                switch (msg.what) {
                    case MSG_ENTER_CMD:

                        logD("try to enter cmd mode");

                        mUdpUnicast.setIp(mModules.get(0).getIp());
                        mac = mModules.get(0).getMac();
                        Log.e(TAG, "mac: " + mModules.get(0).getMac());
                        mUdpUnicast.open();
                        mFailedTimes = 0;
                        new Thread(new Runnable() {

                            @Override
                            public void run() {

                                mATCommand.enterCMDMode();

                            }
                        }).start();
                        break;
                    case MSG_RETRY_ENTER_CMD:

                        getActivity().setProgressBarIndeterminateVisibility(true);
                        if (mFailedTimes > 3) {
                            showStatusText(R.color.material_blue_grey_800, getString(R.string.retry));
                        } else {
                            showStatusText(R.color.material_blue_grey_800, getString(R.string.waitting));
                        }
                        new Thread(new Runnable() {

                            @Override
                            public void run() {
                                //as there's no transparent transmission mode in this application,
                                //so just send AT+Q\r CMD to exit

                                if (mUdpUnicast.send(com.hf.smartlink.utils.Constants.CMD_EXIT_CMD_MODE)) {
                                    try {
                                        Thread.sleep(300);
                                    } catch (InterruptedException e) {
                                    }
                                    mATCommand.enterCMDMode();
                                } else {
                                    mWifiManager.setWifiEnabled(false);
                                    try {
                                        Thread.sleep(600);
                                    } catch (InterruptedException e) {

                                    }
                                    mWifiManager.setWifiEnabled(true);
                                    mWifiManager.reassociate();
                                }

                            }
                        }).start();
                        break;
                    case MSG_ENABLE_WIFI:
                        mToggleButton.setChecked(true);
                        break;

                    default:
                        break;
                }
            }
        };

        mLatestAccessPoints = new ArrayList<>();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        contentView = inflater.inflate(R.layout.ap_smartlink_fragment, container, false);

        //retrieve the current wifi connection if exist
        db = new HailinDB(getActivity());

        getActivity().setProgressBarIndeterminateVisibility(true);
        //scan now
        mWifiManager = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
        mWifiManager.startScan();
        List<ScanResult> scanResults = mWifiManager.getScanResults();
        //retrieve the scan result(wifi) which is connect in last time
        mConnect2ScanResult = Utils.getLastScanResult(getActivity());

        mLastWifiStatus =  WifiStatus.getInstanse(AppContext.getContext());

        String bssid = mLastWifiStatus.getBSSID();


        if (bssid != null) {//if the wifi is connected

            if (scanResults != null) {
                ScanResult scanResult = null;
                for (int i = 0; i < scanResults.size(); i++) {
                    if (Utils.removeDoubleQuotes(scanResults.get(i).BSSID).equals(Utils.removeDoubleQuotes(bssid))) {
                        scanResult = scanResults.get(i);
                        break;
                    }
                }

                if (scanResult != null) {
                    mConnect2ScanResult = scanResult;
                }
            }
        } else if (mConnect2ScanResult != null) {//if wifi is not connected

            if (scanResults != null) {
                for (int i = 0; i < scanResults.size(); i++) {
                    if (Utils.removeDoubleQuotes(scanResults.get(i).BSSID).equals(Utils.removeDoubleQuotes(mConnect2ScanResult.BSSID))) {
                        mConnect2ScanResult = scanResults.get(i);
                        break;
                    }
                }
            }
        }

        //setup views and listeners
        setupViews();
        //create an adapter for  AP choosing dialog
//        mAccessPointAdapter = new AccessPointAdapter(getActivity(), Utils.getSettingApSSID(getActivity())) {
//
//            @Override
//            public void onItemClicked(AccessPoint accessPoint, int position) {
//                super.onItemClicked(accessPoint, position);
//
//                if (mChooseApDialog != null && mChooseApDialog.isShowing()) {
//                    Button button = mChooseApDialog.getButton(Dialog.BUTTON_POSITIVE);
//                    if (!button.isEnabled()) {
//                        button.setEnabled(true);
//                    }
//                }
//            }
//        };
        progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
        mWifiEnabler = new WifiEnabler(getActivity(), progressDialog.getText());
        mScanner = new Scanner(getActivity());


        mWifiAutomaticConnecter = new WifiAutomaticConnecter(getActivity(), handlerThread.getLooper()) {

            @Override
            public void connectOpenNone(AccessPoint accessPoint, int networkId) {
                connect(networkId);
            }

            @Override
            public void onSsidNotFind() {
                if (isRun) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (isRun) {
                                addCount = 30;
                                isLoading = false;
//                                dialogDismiss();
                                SmartlinkFragment.setIsAPFail(true);
                                getFragmentManager().popBackStackImmediate();
                            }
                        }
                    });
                }
            }
        };

        mATCommandListener = new ATCommandListener() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse:" + response);

                response = response.trim();
                if ((response.equals("+ok") || response.startsWith(com.hf.smartlink.utils.Constants.RESPONSE_ERR))
                        && (System.currentTimeMillis() - mLastCMD < 2000)) {
                    synchronized (mAtResponse) {
                        mAtResponse.setLength(0);
                        mAtResponse.append(response);
                        mAtResponse.notifyAll();
                    }
                }
            }

            @Override
            public void onEnterCMDMode(boolean success) {


//                getActivity().setProgressBarIndeterminateVisibility(false);
                updateViews(success);
                synchronized (APSmartlinkFragment.class) {
                    if (!mIsCMDMode) {

                        if (success) {
                            Log.e(TAG, "onEnterCMDMode:" + success);
                            mStatusTextView.setText(null);
                            //start a test cmd repeater to send test cmd in periodic 50 seconds to keep connection
                            if (mTestCmdRepeater == null) {
                                mTestCmdRepeater = new Repeater(com.hf.smartlink.utils.Constants.TIMER_CHECK_CMD) {
                                    @Override
                                    public void repeateAction() {
                                        mATCommand.send(com.hf.smartlink.utils.Constants.CMD_VER);
                                        if (isLoading) {
//                                            dialogDismiss();
                                            isLoading = false;
                                            connectWifi();

                                        }

//
                                        Log.e(TAG, "onEnterCMDMode:" + "send:CMD_VER");
                                    }
                                };
                                mTestCmdRepeater.resume();
                            }


                        } else {

                            //show the error info about udp failed
                            mFailedTimes++;
                            if (mFailedTimes > 3) {
                                showStatusText(R.color.color_red, getString(R.string.enter_cmd_mode_failed));
                            }

                            if (!mIsExit) {
                                Log.e(TAG, "onEnterCMDMode: " + "Retry to enter CMD mode again for times");
                                mNetworkHandler.sendEmptyMessageDelayed(MSG_RETRY_ENTER_CMD, 1000);
                            }
                        }
                        mIsCMDMode = success;
                    }
                }
            }

            @Override
            public void onExitCMDMode(boolean success, NetworkProtocol protocol) {
                logD("onExitCMDMode:" + success);
            }

            @Override
            public void onReload(boolean success) {
            }

            @Override
            public void onReset(boolean success) {
            }

            @Override
            public void onSendFile(boolean success) {
            }

            @Override
            public void onResponseOfSendFile(String response) {
            }
        };
        mUdpUnicast = new UdpUnicast();
        mUdpUnicast.setPort(Utils.getUdpPort(getActivity()));
        mATCommand = new ATCommand(mUdpUnicast);
        mATCommand.setListener(mATCommandListener);

        return contentView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((OnPressBackListener)getActivity()).setSelectFragment(this);
        SmartlinkFragment.setIsAPFail(true);
    }


    void logD(String msg) {
        Log.d(TAG, msg == null ? "null" : msg);
    }

    void logW(String msg) {
        Log.w(TAG, msg == null ? "null" : msg);
    }

    void toast(String text) {
        text = text == null ? "null" : text;
        Message msg = mNotifierHandler.obtainMessage(0);
        msg.obj = text;
        mNotifierHandler.sendMessage(msg);
    }

    void toast(int resId) {
        toast(resId, false);
    }

    void toast(int resId, boolean longer) {
        Message msg = mNotifierHandler.obtainMessage(1);
        msg.arg1 = resId;
        msg.arg2 = longer ? 1 : 0;
        mNotifierHandler.sendMessage(msg);
    }


    void simpleDialog(String title, String message, boolean withCancelButton, SimpleDialogListener listener) {
        Message msg = mNotifierHandler.obtainMessage(4);
        Bundle data = new Bundle();
        data.putSerializable("listener", listener);
        data.putString("title", title);
        data.putString("message", message);
        data.putBoolean("withCancelButton", withCancelButton);
        msg.setData(data);
        mNotifierHandler.sendMessage(msg);
    }


    class SimpleDialogListener implements DialogInterface.OnClickListener, Serializable {

        private static final long serialVersionUID = 1L;

        @Override
        public void onClick(DialogInterface dialog, int which) {

        }
    }

    private boolean isFirstLoad = true;

    @Override
    public void onResume() {
        super.onResume();


        if (isLoading) {
            mWifiAutomaticConnecter.resume();
            Log.e(TAG, "onResume: " + "mWifiAutomaticConnecter.resume()");
        }
    }

    private void dialogDismiss() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onPause() {

        super.onPause();


    }

    @Override
    public void onStop() {
        super.onStop();
        mWifiAutomaticConnecter.pause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRun = false;
        progressDialog.setText("取消连接...");
        mWifiEnabler.pause();
        try {
            getActivity().unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        mScanner.pause();

        if (!mIsCMDMode) {
            mScanBroadcast.close();
        }

        handler.removeCallbacks(addDevRunnabel);
        mIsExit = true;
        mUdpUnicast.send(com.hf.smartlink.utils.Constants.CMD_EXIT_CMD_MODE);
        closeActions();
        mLastWifiStatus.reload(wifiPwdText.getText().toString());

        dialogDismiss();
        Log.e(TAG, "onDestroy: ");
    }


    @SuppressWarnings("serial")
    @Override
    public void onClick(View view) {
//        progressDialog.show();
//        mWifiAutomaticConnecter.resume();
//        isRun = true;
        if (isFirstLoad) {
              isFirstLoad = false;
//            progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialog) {
//                    mWifiAutomaticConnecter.pause();
//                    isRun = false;
//                    if (addCount > 0 && addCount < 30) {
//                        addCount = 30;
//                        handler.removeCallbacks(addDevRunnabel);
//                        getFragmentManager().popBackStackImmediate();
//                    } else if (isLoading) {
//                        SmartlinkFragment.setIsAPFail(false);
//                        getFragmentManager().popBackStackImmediate();
//                    }
//                }
//            });
            progressDialog.setCancelable(false);
            progressDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == event.KEYCODE_BACK)
                        Log.e(TAG, "onKey: " );
                    getFragmentManager().popBackStackImmediate();
//                    SmartlinkFragment.setIsAPFail(true);
                    return false;
                }
            });
            progressDialog.setMessage("");
            progressDialog.show();
            isRun = true;
            mWifiEnabler.resume();
            getActivity().registerReceiver(mReceiver, mFilter);

            if (!mWifiManager.isWifiEnabled()) {
                logD("Wifi is not enable, enable it in 2 seconds!");
                mNetworkHandler.sendEmptyMessageDelayed(MSG_ENABLE_WIFI, 2000);
            }
        }
        if (isLoading) {
            mWifiAutomaticConnecter.resume();
            Log.e(TAG, "onResume: " + "mWifiAutomaticConnecter.resume()");
        }

    }

    private void connectWifi() {
        //only the ssid is input, that means there's a scan result to connect
        Log.e(TAG, "connectWifi: "+ mConnect2ScanResult);
        if (mConnect2ScanResult != null) {

            //retrieve the latest wifi scan results
//            retrieveAccessPointsAdapter();
            List<AccessPoint> mAccessPoints = updateAccessPoints();
            //check whether there's the specific ssid in the list
//            mAccessPointAdapter.setSelected(mConnect2ScanResult);
            AccessPoint accessPoint = null;
            for (AccessPoint access : mAccessPoints) {
                if (scanResultEquals(access.getScanResult(), mConnect2ScanResult)) {
                    accessPoint = access;
                }
            }
//            AccessPoint accessPoint = mAccessPointAdapter.getSelected();

            //if exist
            if (accessPoint != null && accessPoint.getScanResult() != null) {
                final ScanResult scanResult = accessPoint.getScanResult();

                //check if the wifi network with ssid has changed
                boolean ssidChanged = mConnect2ScanResult.SSID != null
                        && !mConnect2ScanResult.SSID.equals(scanResult.SSID);
                boolean capabilitesChanged = mConnect2ScanResult.capabilities != null
                        && !mConnect2ScanResult.capabilities.equals(scanResult.capabilities);
                boolean openNone = Utils.SECURITY_OPEN_NONE.equals(Utils.parseSecurity(scanResult.capabilities));

                if (ssidChanged && capabilitesChanged && !openNone) {

                    simpleDialog(getString(R.string.network_changed_title, mConnect2ScanResult.SSID),
                            getString(R.string.network_changed_msg_ssid_capabilites, scanResult.SSID), true,
                            new SimpleDialogListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    ssidText.setText(scanResult.SSID);
                                    mConnect2ScanResult = scanResult;
                                    updateViews(true);
                                }
                            });

                    return;
                } else if ((ssidChanged && capabilitesChanged && openNone) || ssidChanged) {

                    simpleDialog(getString(R.string.network_changed_title, mConnect2ScanResult.SSID),
                            getString(R.string.network_changed_msg_ssid, scanResult.SSID), true,
                            new SimpleDialogListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    ssidText.setText(scanResult.SSID);
                                    mConnect2ScanResult = scanResult;

                                    if (checkSsidPasswordInput()) {
                                        switchModule2STA(mConnect2ScanResult, wifiPwdText.getText().toString());
                                    }
                                }
                            });
                    return;
                } else if (capabilitesChanged) {

                    if (openNone) {
                        mConnect2ScanResult = scanResult;
                    } else {
                        simpleDialog(getString(R.string.network_changed_title, mConnect2ScanResult.SSID),
                                getString(R.string.network_changed_msg_capabilites), true,
                                new SimpleDialogListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int which) {

                                        mConnect2ScanResult = scanResult;
                                        updateViews(true);
                                    }
                                });
                        return;
                    }
                }
            }

            if (checkSsidPasswordInput()) {
                switchModule2STA(mConnect2ScanResult, wifiPwdText.getText().toString());
            }
        }

    }

    /**
     * setup views and listeners
     */
    private void setupViews() {

        mWiFiStateTextView = (TextView) contentView.findViewById(R.id.mWiFiStateTextView);
        mToggleButton = (CheckBox) contentView.findViewById(R.id.togggle);
        mStatusTextView = (TextView) contentView.findViewById(R.id.mStatusTextView);
        Paint paint = mStatusTextView.getPaint();
        paint.setAntiAlias(true);
        paint.setUnderlineText(true);

        ssidText = (EditText) contentView.findViewById(R.id.ssidText);
        wifiPwdText = (EditText) contentView.findViewById(R.id.wifiPwdText);
//		mChooseButton = (Button)findViewById(R.id.button1);
        connectButton = (TextView) contentView.findViewById(R.id.connectButton);
//		mCancelButton = (Button)findViewById(R.id.button3);
        wifiPwdCheck = (CheckBox) contentView.findViewById(R.id.wifiPwdCheck);
        backBtn = contentView.findViewById(R.id.smartLinkBackButton);
        backBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStackImmediate();
            }
        });
        if (mConnect2ScanResult != null) {
            ssidText.setText(mConnect2ScanResult.SSID);
//            wifiPwdText.setText(Utils.getScanResultPassword(getActivity(), mConnect2ScanResult));


        }

        final TextWatcher passwordTextWatcher = new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateViews(true);
            }
        };
        wifiPwdText.addTextChangedListener(passwordTextWatcher);
        wifiPwdText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    wifiPwdText.setSelection(wifiPwdText.getText().length());
                }
            }
        });

        wifiPwdCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                wifiPwdText.removeTextChangedListener(passwordTextWatcher);
                wifiPwdText.setInputType(isChecked ? InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL :
                        InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                wifiPwdText.setSelection(wifiPwdText.getText().length());
                wifiPwdText.addTextChangedListener(passwordTextWatcher);
            }
        });

        connectButton.setOnClickListener(this);
    }


    /**
     * update the status of these views: {@link #ssidText},
     * {@link #wifiPwdText}, {@link #connectButton}
     *
     * @param LPBConnected true if the android device is connected to module HF-LPB, otherwise false
     */
    private void updateViews(boolean LPBConnected) {

        if (!LPBConnected && wifiPwdText.isEnabled()) {
            wifiPwdText.setEnabled(LPBConnected);
        }

        if (mConnect2ScanResult == null) {
            connectButton.setEnabled(false);
//            isEnableConnect = false;
            ssidText.requestFocus();
            return;
        }

        if (LPBConnected) {

            boolean needPassword = true;
            if (Utils.SECURITY_OPEN_NONE.equals(Utils.parseSecurity(mConnect2ScanResult.capabilities))) {
                needPassword = false;
            }

            if (needPassword) {
                connectButton.setEnabled(wifiPwdText.getText().length() != 0);
//                isEnableConnect = (wifiPwdText.getText().length() != 0);

                if (!wifiPwdText.isEnabled()) {
                    wifiPwdText.setEnabled(true);
                }
                wifiPwdText.requestFocus();
                wifiPwdText.setSelection(wifiPwdText.getText().length());
            } else {
                if (!connectButton.isEnabled()) {
                    connectButton.setEnabled(true);
//                    isEnableConnect = true;

                }
                if (wifiPwdText.isEnabled()) {
                    wifiPwdText.setEnabled(false);
                }
            }
        } else {
            if (connectButton.isEnabled()) {
                connectButton.setEnabled(false);
//                isEnableConnect = false;
            }
            ssidText.requestFocus();
        }
//        if (isEnableConnect){
//            connectWifi();
//        }


    }

    private boolean checkSsidPasswordInput() {

        Log.e(TAG, "checkSsidPasswordInput: ");

        String passwd = wifiPwdText.getText().toString();
        String security = Utils.parseSecurity(mConnect2ScanResult.capabilities);
        if (security != null && !security.equals(Utils.SECURITY_OPEN_NONE)) {

            if (passwd.length() == 0) {
                simpleDialog(getString(R.string.warning), getString(R.string.password_not_empty, mConnect2ScanResult.SSID), false, null);
                return false;
            } else if (security.equals(Utils.SECURITY_WEP)) {
                int wepType = Utils.checkWepType(passwd);
                System.out.println("wepType: " + wepType);
                if (wepType == Utils.WEP_INVALID) {
                    //prompt dialog that password invalid
                    simpleDialog(getString(R.string.warning), getString(R.string.wep_password_invalid), false, null);
                    return false;
                }
            }
        }

        Utils.saveLastScanResult(getActivity().getApplicationContext(), mConnect2ScanResult);
        Utils.saveScanResultPassword(getActivity().getApplicationContext(), mConnect2ScanResult, passwd);

        return true;
    }

    /**
     * switch the module to station mode
     *
     * @param scanResult the AP to connect to
     * @param password   the password
     */
    private void switchModule2STA(final ScanResult scanResult, final String password) {

        new AsyncTask<Void, Void, Integer>() {

            private static final int RESULT_CMD_MODE_FAILED = -1;
            private static final int RESULT_RESPONSE_ERROR = -2;
            private static final int RESULT_RESPONSE_TIME_OUT = -3;
            private static final int RESULT_SUCCESS = 0;

            @Override
            protected void onPreExecute() {
                getActivity().showDialog(1);
            }

            @Override
            protected Integer doInBackground(Void... params) {

                StringBuffer response = new StringBuffer();
                //send AT+ to test if it's in command mode
                if (!sendAtCmd(com.hf.smartlink.utils.Constants.CMD_TEST, response)) {
                    //not in command mode, try to enter into command mode
                    synchronized (mAtResponse) {
                        logD("Try to enter into cmd mode again");
                        mFailedTimes = 0;
                        if (mTestCmdRepeater != null) {
                            mTestCmdRepeater.pause();
                        }
                        mAtResponse.setLength(0);
                        mATCommand.enterCMDMode();
                        Log.e(TAG, "doInBackground: " + "enterCMDMode");
                        try {
                            mAtResponse.wait(15000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                    if (mAtResponse.toString().trim().equals("")) {
                        //enter into cmd mode failed
                        logW("Enter into cmd mode failed");
                        return RESULT_CMD_MODE_FAILED;
                    }
                }

                //send AT+WMODE=STA
                response.setLength(0);
                if (!sendAtCmd(com.hf.smartlink.utils.Constants.CMD_STA, response)) {
                    logW("Failed: send AT+WMODE=STA");
                    return response.toString().equals(com.hf.smartlink.utils.Constants.RESPONSE_ERR) ?
                            RESULT_RESPONSE_ERROR : RESULT_RESPONSE_TIME_OUT;
                }

                //send AT+WSSSID=%s
                response.setLength(0);
                if (!sendAtCmd(Utils.generateWsssid(scanResult.SSID), response)) {
                    logW("Failed: send AT+WSSSID=%s");
                    return response.toString().equals(com.hf.smartlink.utils.Constants.RESPONSE_ERR) ?
                            RESULT_RESPONSE_ERROR : RESULT_RESPONSE_TIME_OUT;
                }

                //send AT+WSKEY=%s
                response.setLength(0);
                if (!sendAtCmd(Utils.generateWskeyCmd(scanResult, password), response)) {
                    logW("Failed: send AT+WSKEY=%s");
                    return response.toString().equals(com.hf.smartlink.utils.Constants.RESPONSE_ERR) ?
                            RESULT_RESPONSE_ERROR : RESULT_RESPONSE_TIME_OUT;
                }

                //send AT+Z
                sendAtCmd(com.hf.smartlink.utils.Constants.CMD_RESET, null);
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return RESULT_SUCCESS;
            }

            @Override
            protected void onPostExecute(Integer result) {
                if (result != null) {
                    switch (result) {
                        case RESULT_CMD_MODE_FAILED:
//                            simpleDialog(R.string.enter_cmd_mode_failed);
                            progressDialog.setText(getString(R.string.enter_cmd_mode_failed));
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (isRun) {
//                                        progressDialog.setText("正在重新连接UDP");
//                                        connectWifi();
//                                    }
//                                }
//                            }, 5000);
                            SmartlinkFragment.setIsAPFail(true);
                            getFragmentManager().popBackStackImmediate();
                            break;
                        case RESULT_RESPONSE_ERROR:
                            progressDialog.setText(getString(R.string.response_failed));
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (isRun) {
//                                        progressDialog.setText("正在重新连接UDP");
//                                        connectWifi();
//
//                                    }
//                                }
//                            }, 5000);
//                            simpleDialog(R.string.response_failed);
                            SmartlinkFragment.setIsAPFail(true);
                            getFragmentManager().popBackStackImmediate();
                            break;
                        case RESULT_RESPONSE_TIME_OUT:
                            progressDialog.setText(getString(R.string.response_time_out));
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    if (isRun) {
                                        progressDialog.setText("正在重新连接UDP");
                                        connectWifi();

                                    }
                                }
                            }, 5000);
//                            simpleDialog(R.string.response_time_out);
                            break;
                        case RESULT_SUCCESS:
                            showStatusText(R.color.color_red, getString(R.string.ap_lost));
//                            toast(R.string.reset_wait, true);
                            progressDialog.setText(getString(R.string.reset_wait));
                            mWifiEnabler.pause();
                            try {
                                getActivity().unregisterReceiver(mReceiver);
                            } catch (Exception e) {
                            }
                            mScanner.pause();
                            mWifiAutomaticConnecter.pause();
                            if (!mIsCMDMode) {
                                mScanBroadcast.close();
                            }
                            mIsExit = true;
                            mUdpUnicast.send(com.hf.smartlink.utils.Constants.CMD_EXIT_CMD_MODE);
                            closeActions();
//                            mWifiManager.disconnect();

                            addCount = 0;
                            handler.post(addDevRunnabel);

//                            Toast.makeText(getActivity(), "正在添加设备...", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            break;
                    }
                }
//                getActivity().dismissDialog(1);
            }
        }.execute(null, null, null);
    }


    /**
     * 添加设备
     */

    private void addDevice() {
        String[] arg = {UserInfo.UserInfo.getUserName(), mac};
        ListItemBean listItemBean = db.getTDevice().findOne(arg);
        if (!(listItemBean != null && mac.equals(listItemBean.getMac()))) {
            sendHttp();
        } else {
            addCount = 30;
//            dialogDismiss();
            SmartlinkFragment.setIsAPFail(false);
            retPage(true);
        }
    }

    private int addCount = 0;
    private Runnable addDevRunnabel = new Runnable() {
        @Override
        public void run() {
            if (addCount == 0) {
                addCount++;
                mLastWifiStatus.reload(wifiPwdText.getText().toString());
                handler.postDelayed(this, 3000);
            } else {
                addDevice();
            }
        }
    };
    private Handler handler = new Handler();

    private void sendHttp() {
        String url = getResources().getText(R.string.url_add_device).toString();
        RequestParams paramMap = new RequestParams();
        paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
        HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                Log.e(TAG, "onSuccess: ");
                if (isRun){
                    BackgroundTask bt = new BackgroundTask(
                            getActivity(),
                            db,
                            mac,
                            "") {
                        @Override
                        public void exeTask() {
//                        retPage(true);
                        }
                    };
                    bt.getDataFromWeb();
                    addCount = 30;
//                dialogDismiss();
                    handler.removeCallbacks(addDevRunnabel);
                    SmartlinkFragment.setIsAPFail(false);

                    retPage(true);
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
//                Log.e(TAG, "onFailure: "+new String(responseString) );
                if (isRun){
                    if (getResources().getString(R.string.value_code_fourZeroZero).equals(
                            String.valueOf(statusCode))) {

                        handler.removeCallbacks(addDevRunnabel);
                        addCount = 30;
//                    dialogDismiss();
                        SmartlinkFragment.setIsAPFail(false);
                        retPage(true);
                    } else {
                        if (addCount < 30) {
                            handler.postDelayed(addDevRunnabel, 2000);
                            addCount++;
                            SmartlinkFragment.setIsAPFail(true);
                        } else {
                            addCount = 30;
//                        dialogDismiss();
                            getFragmentManager().popBackStackImmediate();
                        }
                        retPage(false);
                    }
                }
            }
        });
    }

    @SuppressLint("DefaultLocale")
    private void retPage(boolean bl) {
        if (bl) {

            Map<String, String> param = new HashMap<>();
            param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());

            db.getTUser().update(param);

            showToast(getResources().getString(R.string.lang_config_sucess));
            getFragmentManager().popBackStackImmediate();
        }
    }

    private void showToast(String text) {
        Toast t = Toast.makeText(getActivity(), null, Toast.LENGTH_LONG);
        LinearLayout layout = (LinearLayout) t.getView();
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setGravity(Gravity.CENTER);

        TextView tv = new TextView(getActivity());
        tv.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(20);
//    	tv.setPadding(10, 0, 0, 0);
        tv.setText(text);
        layout.addView(tv);
        t.show();
    }

    /**
     * send the AT command whose response is "+ok"
     *
     * @param cmd
     * @return
     */
    private boolean sendAtCmd(String cmd, StringBuffer response) {

        if (mTestCmdRepeater != null) {
            mTestCmdRepeater.pause();
        }

        boolean success = false;
        for (int i = 0; i < 2; i++) {//re-send it if the previous sending is failed

            synchronized (mAtResponse) {
                mAtResponse.setLength(0);

                if (mATCommand != null) {
                    mLastCMD = System.currentTimeMillis();
                    mATCommand.send(cmd);
                }

                if (!com.hf.smartlink.utils.Constants.CMD_RESET.equals(cmd)) {
                    try {
                        mAtResponse.wait(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    String atResp = mAtResponse.toString().trim();
                    Log.e(TAG, "response of test enter cmd mode : " + atResp);
                    if (atResp.equals(com.hf.smartlink.utils.Constants.RESPONSE_OK)) {
                        if (response != null) {
                            response.append(com.hf.smartlink.utils.Constants.RESPONSE_OK);
                        }
                        success = true;
                        break;
                    } else if (atResp.startsWith(com.hf.smartlink.utils.Constants.RESPONSE_ERR)) {
                        if (response != null) {
                            response.append(com.hf.smartlink.utils.Constants.RESPONSE_ERR);
                        }
                        success = false;
                        break;
                    }
                } else {
                    success = true;
                }
            }
        }

        if (mTestCmdRepeater != null) {
            mTestCmdRepeater.resumeWithDelay();
        }

        return success;
    }

    /**
     * Some actions need to be closed
     */
    private void closeActions() {

        mNetworkHandler.removeMessages(MSG_RETRY_ENTER_CMD);
        mScanBroadcast.close();
        mUdpUnicast.close();
        if (mTestCmdRepeater != null) {
            mTestCmdRepeater.pause();
        }
    }


    /**
     * Connect the configured network
     *
     * @param networkId
     */
    private void connect(int networkId) {
        if (networkId == -1) {
            return;
        }

        // Connect to network by disabling others.
        mWifiManager.enableNetwork(networkId, true);
        mWifiManager.saveConfiguration();
        mWifiManager.reconnect();
        updateAccessPoints();
    }

    private void enableNetworks() {
        for (int i = mLatestAccessPoints.size() - 1; i >= 0; --i) {
            WifiConfiguration config = mLatestAccessPoints.get(i).getConfig();
            if (config != null && config.status != WifiConfiguration.Status.ENABLED) {
                mWifiManager.enableNetwork(config.networkId, false);
            }
        }
    }


    private synchronized List<AccessPoint> updateAccessPoints() {

        List<AccessPoint> accessPoints = new ArrayList<AccessPoint>();

        List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
        if (configs != null) {
            for (WifiConfiguration config : configs) {

                // Shift the status to make enableNetworks() more efficient.
                if (config.status == WifiConfiguration.Status.CURRENT) {
                    config.status = WifiConfiguration.Status.ENABLED;
                } else if (mResetNetworks && config.status == WifiConfiguration.Status.DISABLED) {
                    config.status = WifiConfiguration.Status.CURRENT;
                }

                AccessPoint accessPoint = new AccessPoint(getActivity(), config);
                accessPoint.update(mLastInfo, mLastState);
                accessPoints.add(accessPoint);
            }
        }

        List<AccessPoint> scanAccessPoints = new ArrayList<AccessPoint>();
        List<ScanResult> results = mWifiManager.getScanResults();
        if (results != null) {
            for (ScanResult result : results) {
                // Ignore hidden and ad-hoc networks.
                if (result.SSID == null || result.SSID.length() == 0 ||
                        result.capabilities.contains("[IBSS]")) {
                    continue;
                }

                scanAccessPoints.add(new AccessPoint(getActivity(), result));
                for (AccessPoint accessPoint : accessPoints) {
                    accessPoint.update(result);
                }
            }
        }

        mLatestAccessPoints.clear();
        mLatestAccessPoints.addAll(scanAccessPoints);
        return scanAccessPoints;
    }

//    /**
//     * Update the access points using {@link #updateAccessPoints()}, and get the latest {@link #mAccessPointAdapter}
//     */
//    private void retrieveAccessPointsAdapter() {
//        mAccessPointAdapter.setDefaultSSID(Utils.getSettingApSSID(getActivity()));
//        mAccessPointAdapter.updateAccessPoints(updateAccessPoints());
//    }

    private boolean hasEnterCMD = false;
    private boolean hasReceiveIP = false;
    private void handleEvent(Intent intent) {
        String action = intent.getAction();

        if (WifiManager.WIFI_STATE_CHANGED_ACTION.equals(action)) {
            updateWifiState(intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                    WifiManager.WIFI_STATE_UNKNOWN));
        } else if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {

            updateAccessPoints();
        } else if (WifiManager.NETWORK_IDS_CHANGED_ACTION.equals(action)) {
            updateAccessPoints();
        } else if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION.equals(action)) {
            updateConnectionState(WifiInfo.getDetailedStateOf((SupplicantState)
                    intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE)));
        } else if (WifiManager.NETWORK_STATE_CHANGED_ACTION.equals(action)) {

            NetworkInfo.DetailedState state = ((NetworkInfo) intent.getParcelableExtra(
                    WifiManager.EXTRA_NETWORK_INFO)).getDetailedState();

            logD(state.name());
            String ssid = AccessPoint.removeDoubleQuotes(mWifiManager.getConnectionInfo().getSSID());
            if (state == NetworkInfo.DetailedState.CONNECTED && ssid != null) {

                logD(ssid + " is connected.");
                if (ssid.equals(Utils.getSettingApSSID(getActivity()))) {

                    if (!mIsCMDMode) {

                        Module module = Utils.getDevice(getActivity(), generateNetworkKey());
                        if (module != null) {

                            if (mModules == null) {
                                mModules = new ArrayList<Module>();
                                mModules.add(module);
                            } else {
                                mModules.add(0, module);
                            }
                            if (!hasEnterCMD) {
                                mNetworkHandler.removeMessages(MSG_RETRY_ENTER_CMD);
                                mNetworkHandler.sendEmptyMessage(MSG_ENTER_CMD);
                                hasEnterCMD = true;
                            }
                            Log.e(TAG, "handleEvent: " + "enterCMDMode");
                        } else {

                            if (!hasReceiveIP){
                                logD("Start to broadcast to find module info...");
                                hasReceiveIP = true;
                                mScanBroadcast.open();
                                mScanBroadcast.send(Utils.getCMDScanModules(getActivity()));
                            }
                        }
                        showStatusText(R.color.material_blue_grey_800, getString(R.string.connected_ap, Utils.getSettingApSSID(getActivity())));
                    }
                } else {
//                    logD("Disconnect it.");
//                    WifiInfo connectionInfo = mWifiManager.getConnectionInfo();
//                    if (isRun) {
//                        if (connectionInfo.getSupplicantState().compareTo(SupplicantState.COMPLETED) > 0) {
//                            if (mLastWifiStatus != null)
//                                mLastWifiStatus.load();
//                        }
//                        mWifiManager.disconnect();
//                        Log.e(TAG, "handleEvent: disconnect");
//                    }
                }
            } else if (state == NetworkInfo.DetailedState.DISCONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                mIsCMDMode = false;
                Log.e(TAG, "handleEvent: " + "mIsCMDMode = false");
                closeActions();
                updateViews(false);
                showStatusText(R.color.color_red, getString(R.string.ap_lost));
                getActivity().setProgressBarIndeterminateVisibility(true);
                hasEnterCMD = false;
                hasReceiveIP = false;
            }
            updateConnectionState(state);
        } else if (WifiManager.RSSI_CHANGED_ACTION.equals(action)) {
            updateConnectionState(null);
        }
    }

    private void updateConnectionState(NetworkInfo.DetailedState state) {
        /* sticky broadcasts can call this when wifi is disabled */
        if (!mWifiManager.isWifiEnabled()) {
            mScanner.pause();
            return;
        }

        if (state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
            mScanner.pause();
        } else {
            mScanner.resume();
        }

        mLastInfo = mWifiManager.getConnectionInfo();
        if (state != null) {
            mLastState = state;
        }

        for (int i = mLatestAccessPoints.size() - 1; i >= 0; --i) {
            mLatestAccessPoints.get(i).update(mLastInfo, mLastState);
        }

        if (mResetNetworks && (state == NetworkInfo.DetailedState.CONNECTED ||
                state == NetworkInfo.DetailedState.DISCONNECTED || state == NetworkInfo.DetailedState.FAILED)) {
            updateAccessPoints();
            enableNetworks();
        }
    }

    private void updateWifiState(int state) {
        if (state == WifiManager.WIFI_STATE_ENABLED) {
            mScanner.resume();
            updateAccessPoints();
        } else {
            mScanner.pause();
            mLatestAccessPoints.clear();
        }
    }


    private String generateNetworkKey() {

        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
        if (wifiInfo != null && wifiInfo.getBSSID() != null) {
            return wifiInfo.getBSSID();
        } else {
            return Utils.getSettingApSSID(getActivity());
        }
    }

    private void showStatusText(int color, String text) {
        if (color == -1) {
            mStatusTextView.setTextColor(Color.GRAY);
        } else {
            mStatusTextView.setTextColor(getResources().getColor(color));
        }
        mStatusTextView.setText(text);
//        mStatusTextView.setVisibility(View.VISIBLE);
    }


    //    public final boolean ping(String ip) {
//
//        String result = null;
//        try {
//
//            Process p = Runtime.getRuntime().exec("ping -c 3 -w 100 " + ip);// ping3次
//            // 读取ping的内容，可不加。
//            InputStream input = p.getInputStream();
//            BufferedReader in = new BufferedReader(new InputStreamReader(input));
//            StringBuffer stringBuffer = new StringBuffer();
//            String content = "";
//            while ((content = in.readLine()) != null) {
//                stringBuffer.append(content);
//            }
//            Log.i("TTT", "result content : " + stringBuffer.toString());
//            // PING的状态
//            int status = p.waitFor();
//            if (status == 0) {
//                result = "successful~";
//                return true;
//            } else {
//                result = "failed~ cannot reach the IP address";
//            }
//        } catch (IOException e) {
//            result = "failed~ IOException";
//        } catch (InterruptedException e) {
//            result = "failed~ InterruptedException";
//        } finally {
//            Log.i("TTT", "result = " + result);
//        }
//        return false;
//    }
    private boolean scanResultEquals(ScanResult one, ScanResult other) {

        if (one != null && one.BSSID != null && other != null && other.BSSID != null
                && one.BSSID.trim().equals(other.BSSID.trim())) {
            return true;
        }

        return false;
    }
}