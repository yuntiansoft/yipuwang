package cn.com.yuntian.wojia.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.activity.MipcaActivityCapture;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.Util;


/**
 * AddDeviceFragment
 * 
 * @author chenwh
 *
 */
public class AddDeviceFragment extends Fragment {
	
	// ��������������ı���
	private EditText etId;
	
	private TextView messageText, addDevBackButton;
	
	private Button bnConfirm, scanCodeBtn;
	
	private ImageView loadingAddDevImage1;
	
	private LinearLayout addDevLayoutView;
	
//	private MainFrameTask mMainFrameTask = null;
	
	private CustomProgressDialog progressDialog= null;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	private final static int SCANNIN_GREQUEST_CODE = 1;
	private final int CAMERA_PERMMISSIONE_REQUEST_CODE = 0;
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.add_device_fragment, container, false);
	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop(); 
        
        // ȡ��progressDialog
        progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
        
        // ��ȡ�����е�ȷ����ť
        bnConfirm = (Button) getView().findViewById(R.id.devAddBtnConfirm);
        scanCodeBtn = (Button) getView().findViewById(R.id.scanCodeBtn);
        messageText = (TextView) getView().findViewById(R.id.devAddMessageText);
        etId = (EditText) getView().findViewById(R.id.deviceIdAdd);
        addDevBackButton = (TextView) getView().findViewById(R.id.addDevBackButton);
        addDevLayoutView = (LinearLayout) getView().findViewById(R.id.addDevLayoutView);
        
        loadingAddDevImage1 = (ImageView) getView().findViewById(R.id.loadingAddDevImage1);
//        loadingAddDevImage2 = (ImageView) getView().findViewById(R.id.loadingAddDevImage2);
//        loadingAddDevImage3 = (ImageView) getView().findViewById(R.id.loadingAddDevImage3);
        AnimationDrawable animationDrawable1 = (AnimationDrawable) loadingAddDevImage1.getBackground();
//        AnimationDrawable animationDrawable2 = (AnimationDrawable) loadingAddDevImage2.getBackground();
//        AnimationDrawable animationDrawable3 = (AnimationDrawable) loadingAddDevImage3.getBackground();
        animationDrawable1.start();
//        animationDrawable2.start();
//        animationDrawable3.start();
				
        messageText.setText("");
        
        bnConfirm.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (requireCheck()) {
					messageText.setText("");
					if (progressDialog != null) {
						progressDialog.show();
					}
					sendHttp();
//					 new Thread() {
//		                    public void run() {
//		                    	String url = getResources().getText(R.string.url_base).toString() 
//		                    			+ getResources().getText(R.string.url_add_device).toString();
//		                    	Map<String, String> paramMap = new HashMap<String, String>();
//		                    	paramMap.put(getResources().getText(R.string.param_device_id).toString(),
//		                    			etId.getText().toString());
//		                    	try {
//									AsyncHttpClient.getResponse(getRequest.getPostRequest(url, paramMap));
//
//								} catch (Exception e) {
//									e.printStackTrace();
//									Log.e(this.getClass().getName(), e.getMessage());
//								}
//		                    }
//					 
//		                }.start();
				}	
			}
		});
        
        scanCodeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
						// TODO: Consider calling
						//    ActivityCompat#requestPermissions
						// here to request the missing permissions, and then overriding
						//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
						//                                          int[] grantResults)
						// to handle the case where the user grants the permission. See the documentation
						// for ActivityCompat#requestPermissions for more details.
//                                        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),Manifest.permission.CAMERA)){
//                                            Toast.makeText(getActivity(), "Some permissions need to be reopened.", Toast.LENGTH_SHORT).show();
//                                        }else {
						requestPermissions( new String[]{Manifest.permission.CAMERA}, CAMERA_PERMMISSIONE_REQUEST_CODE);
//                                        }
						return;
					}else {
						Intent intent = new Intent();
						intent.setClass(getActivity(), MipcaActivityCapture.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
					}
				}else {
					Intent intent = new Intent();
					intent.setClass(getActivity(), MipcaActivityCapture.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
				}

			}
		});
        
        addDevBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				getFragmentManager().popBackStackImmediate();
 			}
        });
        
        addDevLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {	
					downX = (int) event.getX();
					isLeftMove = false;
					isRightMove = false;
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					if ((event.getX() - downX) > mTouchSlop) {
						isRightMove = true;
					} else if ((event.getX() - downX) < -mTouchSlop) {
						isLeftMove = true;
					}
					break;
				}
				case MotionEvent.ACTION_UP:
					if (isRightMove) {
						// ����һ������
						getFragmentManager().popBackStackImmediate();
					} else if (isLeftMove) {
	
					}
					break;
				}

				return true;
			}
 		});	
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(etId.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_dev_id_req));
    		return false;
    	}
    	return true;
    }
    
    private void sendHttp() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_add_device).toString();
//    	Map<String, String> paramMap = new HashMap<String, String>();
//    	paramMap.put(getResources().getText(R.string.param_mac).toString(),
//    			etId.getText().toString());
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT) {
//    		@Override
//    		protected void onSuccess(JSONObject response) {
////    			stopProgressDialog();
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}
//    			// ����ʱ��
////    			String result;
////				try {
////					result = response.getString(getResources().getText(R.string.param_result).toString());
////				} catch (NotFoundException e) {
////					Log.e(this.getClass().getName(), e.getMessage());
////					result = getResources().getText(R.string.value_result_four).toString();
////				} catch (JSONException e) {
////					Log.e(this.getClass().getName(), e.getMessage());
////					result = getResources().getText(R.string.value_result_four).toString();
////				}
////    			if (getResources().getText(R.string.value_result_ok).toString().equals(result)) {
//				ListFragment newFragment = new ListFragment();
//				FragmentTransaction transaction = getFragmentManager().beginTransaction();
//				transaction.replace(R.id.fragment_container, newFragment);
//				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//				transaction.addToBackStack(this.getClass().getName());
//
//				// Commit the transaction
//				transaction.commit();		
////    			} else if (getResources().getText(R.string.value_result_one).toString().equals(result)) {
////    				// ���豸ID�����ڣ�������ȷ��
////    				DialogUtil.showDialog(getActivity(), 
////    						getResources().getText(R.string.device_id_notexist).toString(), false);
////    				
////    			} else if (getResources().getText(R.string.value_result_two).toString().equals(result)) {
////    				// �Ѿ���ӹ����豸�����ظ����
////    				DialogUtil.showDialog(getActivity(), 
////    						getResources().getText(R.string.device_id_repeat).toString(), false);	
////    			} else if (getResources().getText(R.string.value_result_three).toString().equals(result)) {
////    				// �Ѿ���ӹ����豸�����ظ����
////    				DialogUtil.showDialog(getActivity(), 
////    						getResources().getText(R.string.device_nm_repeat).toString(), false);	
////    			} else {
////    				// ϵͳ����
////    				DialogUtil.showDialog(getActivity(), 
////    						getResources().getText(R.string.server_exception).toString(), false);
////    			}
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
////    			stopProgressDialog();
//    			if (e instanceof HttpResponseException) {
//    				HttpResponseException he = (HttpResponseException) e;
//    				if (getResources().getString(R.string.value_code_noexist).equals(
//    						String.valueOf(he.getStatusCode()))) {
//    					messageText.setText(getResources().getString(
//    							R.string.lang_mess_device_id_notexist));
//    				} else if (getResources().getString(R.string.value_code_exist).equals(
//    					String.valueOf(he.getStatusCode()))) {
//    					messageText.setText(getResources().getString(
//    							R.string.lang_mess_device_id_repeat));
//    				} else {
//    					messageText.setText(getResources().getString(R.string.lang_mess_exception));
//    				}
//    			} else {	
//    				messageText.setText(getResources().getString(R.string.lang_mess_exception));
//    			}
//    			
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}		
//    		}
//    	};
//    	
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, paramMap), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//			}	
//		}
    	
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_add_device).toString();
    	String url = getResources().getText(R.string.url_add_device).toString();
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(),
    			etId.getText().toString());
//		paramMap.put("pair_code","123456");
//    	HttpClientUtil.getHttpClient().post(url, paramMap, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] response) {     					
//            	if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}
////				ListFragment newFragment = new ListFragment();
////				FragmentTransaction transaction = getFragmentManager().beginTransaction();
////				transaction.replace(R.id.fragment_container, newFragment);
//				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
////				transaction.addToBackStack(this.getClass().getName());
//
//				// Commit the transaction
////				transaction.commit();	
//            	getFragmentManager().popBackStackImmediate();
//            }
//            
//            
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
//            	Log.e(this.getClass().getName(), e.getMessage(), e);
//
//				if (getResources().getString(R.string.value_code_fourZeroFour).equals(
//						String.valueOf(statusCode))) {
//					messageText.setText(getResources().getString(
//							R.string.lang_mess_device_id_notexist));
//				} else if (getResources().getString(R.string.value_code_fourZeroZero).equals(
//					String.valueOf(statusCode))) {
//					messageText.setText(getResources().getString(
//							R.string.lang_mess_device_id_repeat));
//				} else {
//					messageText.setText(getResources().getString(R.string.lang_mess_exception));
//				}
//    			
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}
//            }
//        });
    	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
//				ListFragment newFragment = new ListFragment();
//				FragmentTransaction transaction = getFragmentManager().beginTransaction();
//				transaction.replace(R.id.fragment_container, newFragment);
				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//				transaction.addToBackStack(this.getClass().getName());

				// Commit the transaction
//				transaction.commit();	
            	getFragmentManager().popBackStackImmediate();
            }
            
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
				Log.e(this.getClass().getName(), "onFailure: "+new String(responseString) );
				if (getResources().getString(R.string.value_code_fourZeroFour).equals(
						String.valueOf(statusCode))) {
					messageText.setText(getResources().getString(
							R.string.lang_mess_device_id_notexist));
				} else if (getResources().getString(R.string.value_code_fourZeroZero).equals(
					String.valueOf(statusCode))) {
					messageText.setText(getResources().getString(
							R.string.lang_mess_device_id_repeat));
				} else {
					messageText.setText(getResources().getString(R.string.lang_mess_exception));
				}
    			
    			if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            }
        };
        HttpClientUtil.post(url, paramMap, responseHandler);
    }
    
    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
		case SCANNIN_GREQUEST_CODE:
			if(resultCode == Activity.RESULT_OK){
				Bundle bundle = data.getExtras();
				//��ʾɨ�赽������
				etId.setText(bundle.getString("result"));
			}
			break;
		}
    }	
    
//    private void startProgressDialog(){
//		if (progressDialog == null) {
//			CustomProgressDialog pd= new CustomProgressDialog(getActivity());	 
//			progressDialog = pd.createDialog();
//			progressDialog.setMessage("������...");
//			progressDialog.show();
//		}
//
//	}
//	
//	private void stopProgressDialog(){
//		if (progressDialog != null){
//			progressDialog.dismiss();
//			progressDialog = null;
//		}
//	}
@Override
public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
	super.onRequestPermissionsResult(requestCode, permissions, grantResults);
	if (requestCode == CAMERA_PERMMISSIONE_REQUEST_CODE) {
		if (grantResults.length >= 1) {
			int cameraResult = grantResults[0];//相机权限
			boolean cameraGranted = cameraResult == PackageManager.PERMISSION_GRANTED;//拍照权限
			if (cameraGranted) {
				Intent intent = new Intent();
				intent.setClass(getActivity(), MipcaActivityCapture.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
			}
		}
	}
}
}