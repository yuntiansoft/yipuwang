package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * AmerFanFragment
 * 
 * @author chenwh
 *
 */
public class AmerFanFragment extends Fragment {
	
	private TextView newAmerBackButton, fanOnBtnImg, fanAutoBtnImg, fanCircBtnImg, fanOkBtn;
	
	private TextView newAmerBackText, titleText;
	
	private LinearLayout newAmerLayoutView;
	
	private String mac;
	
	private String fan;
	
	private HailinDB db;
	
	private boolean isForeRun = true;
	
	private AmerTempBean amerTempBean;
	
	private BackgroundTask bTask;
	
	private Handler handler = new Handler();
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private CustomProgressDialog progressDialog = null;
	
	private static boolean pageIsUpd = false;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.amer_fan_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();  
        
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity(); 
        
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				Constants.AMER_FAN_FRAGMENT) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;
		handler.post(runnable);	
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void initItem() {
    	
    	newAmerBackButton = (TextView) getView().findViewById(R.id.newAmerBackButton);
    	fanOnBtnImg = (TextView) getView().findViewById(R.id.fanOnBtnImg);
    	fanAutoBtnImg = (TextView) getView().findViewById(R.id.fanAutoBtnImg);
    	fanCircBtnImg = (TextView) getView().findViewById(R.id.fanCircBtnImg);
    	fanOkBtn = (TextView) getView().findViewById(R.id.fanOkBtn);
    	newAmerBackText = (TextView) getView().findViewById(R.id.newAmerBackText);
    	titleText = (TextView) getView().findViewById(R.id.titleText);
    	
    	newAmerLayoutView =  (LinearLayout) getView().findViewById(R.id.newAmerLayoutView);
    	
        // ȡ��progressDialog
        progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
    }
    
    private void itemEvent() {
    	buttonEvent();
    	backEvent();
    }
    
    private void buttonEvent() {
    	
    	fanOnBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
	        	if (!getResources().getString(R.string.value_fan_mod_one).equals(fan)) {
	        		fan = getResources().getString(R.string.value_fan_mod_one);
	        		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on_sel);
	        		fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto);
	        		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ);
	        	}
			}
        });
    	
    	fanAutoBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!getResources().getString(R.string.value_fan_mod_two).equals(fan)) {
	        		fan = getResources().getString(R.string.value_fan_mod_two);
	        		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on);
	        		fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto_sel);
	        		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ);
	        	}
			}
        });
    	
    	fanCircBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
				if (!getResources().getString(R.string.value_fan_mod_zero).equals(fan)) {
	        		fan = getResources().getString(R.string.value_fan_mod_zero);
	        		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on);
	        		fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto);
	        		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ_sel);
	        	}
			}
        });
    	
    	fanOkBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
				if (progressDialog != null) {
					progressDialog.show();
				}
				updWeb();
			}
        });
    }
    
    private void updateDb() {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, mac);
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.FAN_MOD, fan);
		db.getTAmerTemp().update(params);
    }
    
    private void updWeb() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	RequestParams paramMap = new RequestParams();
//    	String mac = "abcde11223344edcba2";
//    	String mac = "0050C2D8E7A1";
//    	String mac = "0050C2D8E79B";
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	paramMap.put(getResources().getString(R.string.param_fan_mod), fan);
//    	HttpClientUtil.getHttpClient().post(url, paramMap, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
//            	updateDb();
//            	pageIsUpd = false;
//            	getFragmentManager().popBackStackImmediate();
//            	if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}
//            }
//            
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
//            	if (progressDialog != null) {
//    				progressDialog.dismiss();
//				}
//            	Toast.makeText(getActivity().getApplicationContext(), 
//    					getResources().getString(R.string.lang_mess_exception),  
//    			        Toast.LENGTH_SHORT).show();
//            	pageIsUpd = false;
//            }  
//        });
    	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
            	updateDb();
            	pageIsUpd = false;
            	getFragmentManager().popBackStackImmediate();
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            	Toast.makeText(getActivity().getApplicationContext(), 
    					getResources().getString(R.string.lang_mess_exception),  
    			        Toast.LENGTH_SHORT).show();
            	pageIsUpd = false;
            }  
        };
        HttpClientUtil.post(url, paramMap, responseHandler);
    }
    
    private void backEvent() {
    	
    	newAmerBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerBackText.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ������ҳ����
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    }
    
    private void setDataToPage() {
           
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	amerTempBean = db.getTAmerTemp().findOne(arg);
    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
        fan = amerTempBean.getFanMod();
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		if (getResources().getString(R.string.value_fan_mod_two).equals(fan)) {
    			fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto_sel);
        	} else if (getResources().getString(R.string.value_fan_mod_one).equals(fan)) {
        		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on_sel);
        	} else if (getResources().getString(R.string.value_fan_mod_zero).equals(fan)) {
        		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ_sel);
        	}
    	} else {
    		if (getResources().getString(R.string.value_fan_mod_two).equals(fan)) {
    			fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto_offline_sel);
        	} else if (getResources().getString(R.string.value_fan_mod_one).equals(fan)) {
        		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on_offline_sel);
        	} else if (getResources().getString(R.string.value_fan_mod_zero).equals(fan)) {
        		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ_offline_sel);
        	}
    	}
    }
     
    private void setOnline(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on);
    		fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto);
    		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ);
    		fanOkBtn.setBackgroundResource(R.drawable.new_amer_ok);
    		newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow);
    		titleText.setTextColor(getResources().getColor(R.color.color_green));
    	} else {
    		setDisEnable();
    		fanOnBtnImg.setBackgroundResource(R.drawable.new_amer_fan_on_offline);
    		fanAutoBtnImg.setBackgroundResource(R.drawable.new_amer_fan_auto_offline);
    		fanCircBtnImg.setBackgroundResource(R.drawable.new_amer_fan_circ_offline);
    		fanOkBtn.setBackgroundResource(R.drawable.new_amer_ok_offline);
    		newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow_offline);
    		titleText.setTextColor(getResources().getColor(R.color.color_grey));
    	}
    }
    
    private void setEnable() {
    	fanOnBtnImg.setEnabled(true);
    	fanAutoBtnImg.setEnabled(true);
    	fanCircBtnImg.setEnabled(true);
    	fanOkBtn.setEnabled(true);
    }

	private void setDisEnable() {
		fanOnBtnImg.setEnabled(false);
		fanAutoBtnImg.setEnabled(false);
		fanCircBtnImg.setEnabled(false);
		fanOkBtn.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
	
	public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
}