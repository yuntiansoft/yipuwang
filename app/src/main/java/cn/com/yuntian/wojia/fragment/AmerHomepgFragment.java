package cn.com.yuntian.wojia.fragment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * AmerHomepgFragment
 * 
 * @author chenwh
 *
 */
public class AmerHomepgFragment extends Fragment {
	
	private TextView newAmerTempDisText, newAmerTempUnitText, newAmerBackButton, newAmerTitleText;
	
	private TextView newAmerTempSetText, newAmerTempSetColdText, newAmerTempSetHeatText, newAmerBackText; 
	
	private TextView newAmerStatusBtnImg, newAmerFanBtnImg, newAmerModBtnImg, newAmerModText, newAmerCerPoiText;
	
	private TextView setTempCerPoiText, setTempCerColdPoiText, setTempCerHeatPoiText; 
	
	private TextView tempSetUnitText, tempSetHeatUnitText, tempSetColdUnitText;
	
	private ImageView amerTempSetCir, amerTempDisCir, amerTempSetBg, amerTempDisBg;
	
	private FrameLayout newAmerTempSetBg, newAmerTempSetColdBg, newAmerTempSetHeatBg, newAmerTempBg;
	
	private LinearLayout newAmerLayoutView, newAmerAutoTempSetBg;
	
	private int maxCoolTemp = 37;
	
	private int minCoolTemp = 10;
	
	private int minHeatTemp = 4;
	
	private int maxHeatTemp = 32;
	
	private int minAutoTemp = 4;
	
	private int maxAutoTemp = 37;
	
	private int maxCoolTempFah = 99;
	
	private int minCoolTempFah = 50;
	
	private int minHeatTempFah = 40;
	
	private int maxHeatTempFah = 90;
	
	private int minAutoTempFah = 40;
	
	private int maxAutoTempFah = 99;
	
	private String mac;
	
	private String mod;
	
	private String devNm;
	
	private HailinDB db;
	
	private Bitmap disTempBitmap = null;
	
	private Bitmap setTempBitmap = null;
	
	private Bitmap setTempBgBitmap = null;
	
	private Bitmap disTempBgBitmap = null;
	
	private Map<String, String> webParam;
	
	private boolean isForeRun = true;
	
	private AmerTempBean amerTempBean;
	
	private BackgroundTask bTask;
	
	private Handler handler = new Handler();
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private static boolean pageIsUpd = false;
	
	private boolean reLoadPageBl = true;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.amer_homepg_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();  
        
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        reLoadPageBl = true;
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity(); 
        
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		devNm = bundle.getString(Constants.DIS_DEV_NAME);

		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		maxCoolTemp = maxCoolTempFah;
    		minCoolTemp = minCoolTempFah;
    		maxHeatTemp = maxHeatTempFah;
    		minHeatTemp = minHeatTempFah;
    		maxAutoTemp = maxAutoTempFah;
    		minAutoTemp = minAutoTempFah;
    	} 
		
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				Constants.AMER_HOMEPG_FRAGMENT) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		handler.post(runnable);	
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	reLoadPageBl = false;
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void initItem() {
    	
    	newAmerTempDisText = (TextView) getView().findViewById(R.id.newAmerTempDisText);
    	newAmerTempUnitText  = (TextView) getView().findViewById(R.id.newAmerTempUnitText);
    	newAmerBackButton = (TextView) getView().findViewById(R.id.newAmerBackButton);
    	newAmerTitleText = (TextView) getView().findViewById(R.id.newAmerTitleText);
    	newAmerTempSetText = (TextView) getView().findViewById(R.id.newAmerTempSetText);
    	newAmerTempSetColdText = (TextView) getView().findViewById(R.id.newAmerTempSetColdText);
    	newAmerTempSetHeatText = (TextView) getView().findViewById(R.id.newAmerTempSetHeatText);
    	newAmerStatusBtnImg = (TextView) getView().findViewById(R.id.newAmerStatusBtnImg);
    	newAmerFanBtnImg = (TextView) getView().findViewById(R.id.newAmerFanBtnImg);
    	newAmerModBtnImg = (TextView) getView().findViewById(R.id.newAmerModBtnImg);
    	newAmerModText = (TextView) getView().findViewById(R.id.newAmerModText);
    	newAmerBackText = (TextView) getView().findViewById(R.id.newAmerBackText);
    	newAmerCerPoiText = (TextView) getView().findViewById(R.id.newAmerCerPoiText);
    	setTempCerPoiText = (TextView) getView().findViewById(R.id.setTempCerPoiText);
    	setTempCerColdPoiText = (TextView) getView().findViewById(R.id.setTempCerColdPoiText);
    	setTempCerHeatPoiText = (TextView) getView().findViewById(R.id.setTempCerHeatPoiText);
    	tempSetUnitText = (TextView) getView().findViewById(R.id.tempSetUnitText);
    	tempSetHeatUnitText = (TextView) getView().findViewById(R.id.tempSetHeatUnitText);
    	tempSetColdUnitText = (TextView) getView().findViewById(R.id.tempSetColdUnitText);

    	amerTempSetCir = (ImageView) getView().findViewById(R.id.amerTempSetCir);
    	amerTempDisCir = (ImageView) getView().findViewById(R.id.amerTempDisCir); 
    	amerTempSetBg = (ImageView) getView().findViewById(R.id.amerTempSetBg); 
    	amerTempDisBg = (ImageView) getView().findViewById(R.id.amerTempDisBg); 
    	
    	newAmerTempSetBg = (FrameLayout) getView().findViewById(R.id.newAmerTempSetBg); 
    	newAmerTempSetColdBg = (FrameLayout) getView().findViewById(R.id.newAmerTempSetColdBg); 
    	newAmerTempSetHeatBg = (FrameLayout) getView().findViewById(R.id.newAmerTempSetHeatBg); 
    	newAmerTempBg = (FrameLayout) getView().findViewById(R.id.newAmerTempBg);
    	
    	newAmerLayoutView =  (LinearLayout) getView().findViewById(R.id.newAmerLayoutView);
    	newAmerAutoTempSetBg =  (LinearLayout) getView().findViewById(R.id.newAmerAutoTempSetBg);
        
    	newAmerTitleText.setText(devNm);
    }
    
    private void itemEvent() {
    	buttonEvent();
    	tempEvent();
    	backEvent();
    }
    
    private void buttonEvent() {
    	
    	newAmerFanBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
				Fragment fragment = new AmerFanFragment();
				redirectPage(fragment);
			}
        });
    	
    	newAmerStatusBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Fragment fragment = new AmerStatusFragment();
				redirectPage(fragment);
			}
        });
    	
    	newAmerModBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
				pageIsUpd = true;
				String oldMod = "";
				webParam = new HashMap<String, String>();
				if (getResources().getString(R.string.value_mod_perm).equals(mod)) {
					oldMod = amerTempBean.getOriMod();
					if (CheckUtil.requireCheck(oldMod)) {
						mod = oldMod;
					} else {
						mod = getResources().getString(R.string.value_mod_temp);
					}
					setModText();
					newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod);
					webParam.put(Constants.PERM_STATUS, getResources().getString(R.string.value_perm_status_off));
		    	} else {
		    		oldMod = mod;
		    		mod = getResources().getString(R.string.value_mod_perm);
		    		setModText();
					newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod_sel);
					webParam.put(Constants.ORI_MOD, oldMod);
					webParam.put(Constants.PERM_STATUS, getResources().getString(R.string.value_perm_status_on));
		    	}
				webParam.put(Constants.MOD, mod);
				updWeb();
			}
        });
    }
    
    private void updateDb() {
    	webParam.put(Constants.MAC, mac);
    	webParam.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
		db.getTAmerTemp().update(webParam);
    }
    
    private void updWeb() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	paramMap.put(getResources().getString(R.string.param_mod), mod);
    	if (getResources().getString(R.string.value_mod_perm).equals(mod)) {
    		paramMap.put(getResources().getString(R.string.param_perm_status), 
    				getResources().getString(R.string.value_perm_status_on));
    	} else {
    		paramMap.put(getResources().getString(R.string.param_perm_status), 
    				getResources().getString(R.string.value_perm_status_off));
    	}
//    	HttpClientUtil.getHttpClient().post(url, paramMap, new AsyncHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
//            	updateDb();
//            	pageIsUpd = false;
//            }
//            
//            @Override
//            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
//            	if (reLoadPageBl) {
//            		setDataToPage();
//            	}
//            	pageIsUpd = false;
//            }  
//        });
    	AsyncHttpResponseHandler responseHandler = new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
            	updateDb();
            	pageIsUpd = false;
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (reLoadPageBl) {
            		setDataToPage();
            	}
            	pageIsUpd = false;
            }  
        };
        HttpClientUtil.post(url, paramMap, responseHandler);
    }
    
    private void redirectPage(Fragment fragment) {
    	FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.setCustomAnimations(
				R.anim.push_left_in, R.anim.push_left_out);
		Bundle bundle = new Bundle();
		bundle.putString(Constants.MAC, mac);
		fragment.setArguments(bundle);
		//�滻fragment
		transaction.replace(R.id.fragment_container, fragment);
		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
    }
    
    private void tempEvent() {
    	newAmerTempBg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
				String status = amerTempBean.getStatus();
				if (!getResources().getString(R.string.value_status_four).equals(status)) {
					Fragment fragment = new AmerTempFragment();
					redirectPage(fragment);
					releaseVelocityTracker();
				}
			}
        });
    	
    	newAmerTempBg.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
								return true;
							} else {
//								String status = amerTempBean.getStatus();
//								if (!getResources().getString(R.string.value_status_four).equals(status)) {
//									Fragment fragment = new AmerTempFragment();
//									redirectPage(fragment);
//								}
//								releaseVelocityTracker();
								return false;
							}
						} else {
							if ((Math.abs(event.getY() - downY) <= mTouchSlop) 
									&& (Math.abs(event.getX() - downX) <= mTouchSlop)){
//								String status = amerTempBean.getStatus();
//								if (!getResources().getString(R.string.value_status_four).equals(status)) {
//									Fragment fragment = new AmerTempFragment();
//									redirectPage(fragment);
//								}
//								releaseVelocityTracker();
								return false;
							} else {
								return true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
							return true;
						}
						break;
				}

				return false;
			}
 		});	
    }
    
    private void backEvent() {
    	
    	newAmerBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerBackText.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    }
    
    private void setDataToPage() {
           
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	amerTempBean = db.getTAmerTemp().findOne(arg);
    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
    	
    	String disTemp = Util.getTempDegree(amerTempBean.getDisTemp());
//    	LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tempUnitFrame.getLayoutParams(); 
//    	FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) newAmerCerPoiText.getLayoutParams(); 
    	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    		String[] cerTemp = disTemp.split("\\.");
    		newAmerTempDisText.setText(cerTemp[0]);
    		newAmerTempUnitText.setText(
					getResources().getString(R.string.value_temp_cen));
    		if (cerTemp.length > 1) {
    			newAmerCerPoiText.setText("." + cerTemp[1]);
    		}
//    		lp.setMargins(0, 0, 0, 0);  
//    		tempUnitFrame.setLayoutParams(lp);
//    		flp.setMargins(0, 105, 0, 0);
//    		newAmerCerPoiText.setLayoutParams(flp);
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			newAmerTempDisText.setText(disTemp);
			newAmerTempUnitText.setText(
					getResources().getString(R.string.value_temp_fah));
			newAmerCerPoiText.setText("");
//			lp.setMargins(-10, 0, 0, 0);  
//			tempUnitFrame.setLayoutParams(lp);
		}
    	
    	mod = amerTempBean.getMod();
    	setModText();
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		 if (getResources().getString(R.string.value_mod_perm).equals(mod)) {
    			 newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod_sel);
    		 } else {
    			 newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod);
    		 }
    	} else {
    		if (getResources().getString(R.string.value_mod_perm).equals(mod)) {
   			 	newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod_offline_sel);
	   		} else {
	   			newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod_offline);
	   		}
    	}
    	
    	String fanMod = amerTempBean.getFanMod();
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		if (getResources().getString(R.string.value_fan_mod_zero).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_circ);
    		} else if (getResources().getString(R.string.value_fan_mod_one).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_on);
    		} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_auto);
	   		} 
	   	} else {
	   		if (getResources().getString(R.string.value_fan_mod_zero).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_circ_offline);
    		} else if (getResources().getString(R.string.value_fan_mod_one).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_on_offline);
    		} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
    			newAmerFanBtnImg.setBackgroundResource(R.drawable.home_fan_auto_offline);
	   		} 
	   	}
    	
        double coolTemp;
        if (!CheckUtil.requireCheck(amerTempBean.getTempCool())) {
        	coolTemp = (double) maxCoolTemp;
    	} else {
    		coolTemp = Double.valueOf(Util.getTempDegree(amerTempBean.getTempCool())).doubleValue();
    		if (coolTemp > maxCoolTemp) {
    			coolTemp = maxCoolTemp;
    		} else if (coolTemp < minCoolTemp) {
    			coolTemp = minCoolTemp;
    		}
    	}
        
        double heatTemp;
        if (!CheckUtil.requireCheck(amerTempBean.getTempHeat())) {
        	heatTemp  = (double) minHeatTemp;;
    	} else {
    		heatTemp =  Double.valueOf(Util.getTempDegree(amerTempBean.getTempHeat())).doubleValue();
    		if (heatTemp > maxHeatTemp) {
    			heatTemp = maxHeatTemp;
    		} else if (heatTemp < minHeatTemp) {
    			heatTemp = minHeatTemp;
    		}
    	}  

        String setTempHeatValue = "";
        String setTempCoolValue = "";
        boolean offBl = false;
        String status = amerTempBean.getStatus();
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		if (getResources().getString(R.string.value_status_zero).equals(status)) {
    			newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_auto);
    			newAmerTempSetBg.setVisibility(View.GONE);
    			newAmerAutoTempSetBg.setVisibility(View.VISIBLE);
    			newAmerTempSetColdBg.setBackgroundResource(R.drawable.amer_temp_set_cold);
    			newAmerTempSetHeatBg.setBackgroundResource(R.drawable.amer_temp_set_heat);
    			setTempHeatValue = getTempValue(heatTemp);
    			setTempCoolValue = getTempValue(coolTemp);
        	} else {
        		newAmerTempSetBg.setVisibility(View.VISIBLE);
    			newAmerAutoTempSetBg.setVisibility(View.GONE);
    			if (getResources().getString(R.string.value_status_one).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_heat);
    				newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_heat);
    				setTempHeatValue = getTempValue(heatTemp);
    			} else if (getResources().getString(R.string.value_status_three).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_emer);
    				newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_emer);
    				setTempHeatValue = getTempValue(heatTemp);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_cool);
    				newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_cold);
    				setTempCoolValue = getTempValue(coolTemp);
    			} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_off);
    				String oldStatus = amerTempBean.getOriStatus();
    				if (getResources().getString(R.string.value_status_zero).equals(oldStatus)) {
    					newAmerTempSetBg.setVisibility(View.GONE);
    	    			newAmerAutoTempSetBg.setVisibility(View.VISIBLE);
    	    			newAmerTempSetColdBg.setBackgroundResource(R.drawable.amer_temp_set_off);
    	    			newAmerTempSetHeatBg.setBackgroundResource(R.drawable.amer_temp_set_off);
    	    			setTempHeatValue = getTempValue(heatTemp);
    	    			setTempCoolValue = getTempValue(coolTemp);
    				} else if (getResources().getString(R.string.value_status_one).equals(oldStatus) 
        					|| getResources().getString(R.string.value_status_three).equals(oldStatus) ) {
    					newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_off);
    					setTempHeatValue = getTempValue(heatTemp);
    				} else if (getResources().getString(R.string.value_status_two).equals(oldStatus)) {
    					newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_off);
    					setTempCoolValue = getTempValue(coolTemp);
    				} else {
    					newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_off);
    					offBl = true;
    				}
    			}
        	} 
    	} else {
    		if (getResources().getString(R.string.value_status_zero).equals(status)) {
    			newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_auto_offline);
    			newAmerTempSetBg.setVisibility(View.GONE);
    			newAmerAutoTempSetBg.setVisibility(View.VISIBLE);
    			setTempHeatValue = getTempValue(heatTemp);
    			setTempCoolValue = getTempValue(coolTemp);
    		} else {
    			newAmerTempSetBg.setVisibility(View.VISIBLE);
    			newAmerAutoTempSetBg.setVisibility(View.GONE);
    			if (getResources().getString(R.string.value_status_one).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_heat_offline);
    				setTempHeatValue = getTempValue(heatTemp);
    			} else if (getResources().getString(R.string.value_status_three).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_emer_offline);
    				setTempHeatValue = getTempValue(heatTemp);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_cool_offline);
    				setTempCoolValue = getTempValue(coolTemp);
	    		} else if (getResources().getString(R.string.value_status_four).equals(status)) {
	    			newAmerStatusBtnImg.setBackgroundResource(R.drawable.home_status_off_offline);
	    			String oldStatus = amerTempBean.getOriStatus();
					if (getResources().getString(R.string.value_status_zero).equals(oldStatus)) {
						newAmerTempSetBg.setVisibility(View.GONE);
    	    			newAmerAutoTempSetBg.setVisibility(View.VISIBLE);
    	    			setTempHeatValue = getTempValue(heatTemp);
    	    			setTempCoolValue = getTempValue(coolTemp);
					} else if (getResources().getString(R.string.value_status_one).equals(oldStatus) 
	    					|| getResources().getString(R.string.value_status_three).equals(oldStatus) ) {
						setTempHeatValue = getTempValue(heatTemp);
					} else if (getResources().getString(R.string.value_status_two).equals(oldStatus)) {
						setTempCoolValue = getTempValue(coolTemp);
					} else {
						offBl = true;
					}
	    		} 
    		}
    	}
        
        if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
        	if (!offBl) {
        		tempSetUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
        		tempSetHeatUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
        		tempSetColdUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
	    		if (CheckUtil.requireCheck(setTempHeatValue) && CheckUtil.requireCheck(setTempCoolValue)) {
	    			String[] cerHeatTemp = setTempHeatValue.split("\\.");
	    			String[] cerCoolTemp = setTempCoolValue.split("\\.");
	    			newAmerTempSetHeatText.setText(cerHeatTemp[0]);
	    			newAmerTempSetColdText.setText(cerCoolTemp[0]);
	        		if (cerHeatTemp.length > 1) {
	        			setTempCerHeatPoiText.setText("." + cerHeatTemp[1]);
	        		}
	        		if (cerCoolTemp.length > 1) {
	        			setTempCerColdPoiText.setText("." + cerCoolTemp[1]);
	        		}
				} else if (CheckUtil.requireCheck(setTempHeatValue)) {
	    			String[] cerTemp = setTempHeatValue.split("\\.");
	    			newAmerTempSetText.setText(cerTemp[0]);
	        		if (cerTemp.length > 1) {
	        			setTempCerPoiText.setText("." + cerTemp[1]);
	        		}
				} else if (CheckUtil.requireCheck(setTempCoolValue)) {
					String[] cerTemp = setTempCoolValue.split("\\.");
	    			newAmerTempSetText.setText(cerTemp[0]);
	        		if (cerTemp.length > 1) {
	        			setTempCerPoiText.setText("." + cerTemp[1]);
	        		}
				} else {
					newAmerTempSetColdText.setText("");
	    			newAmerTempSetHeatText.setText("");
	    			setTempCerColdPoiText.setText("");
	    			setTempCerHeatPoiText.setText("");
	    			newAmerTempSetText.setText("");
					setTempCerPoiText.setText("");
				}
        	} else {
        		newAmerTempSetColdText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    			newAmerTempSetHeatText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    			setTempCerColdPoiText.setText("");
    			setTempCerHeatPoiText.setText("");
    			newAmerTempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
				setTempCerPoiText.setText("");
				tempSetUnitText.setText("");
        		tempSetHeatUnitText.setText("");
        		tempSetColdUnitText.setText("");
        	}
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			if (!offBl) { 
				tempSetUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
        		tempSetHeatUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
        		tempSetColdUnitText.setText(getResources().getString(R.string.lang_txt_temp_unit));
				if (CheckUtil.requireCheck(setTempHeatValue) && CheckUtil.requireCheck(setTempCoolValue)) {
					newAmerTempSetHeatText.setText(setTempHeatValue);
					newAmerTempSetColdText.setText(setTempCoolValue);
	    			setTempCerColdPoiText.setText("");
	    			setTempCerHeatPoiText.setText("");
				} else if (CheckUtil.requireCheck(setTempHeatValue)) {
					newAmerTempSetText.setText(setTempHeatValue);
					setTempCerPoiText.setText("");
				} else if (CheckUtil.requireCheck(setTempCoolValue)) {
					newAmerTempSetText.setText(setTempCoolValue);
					setTempCerPoiText.setText("");
				} else {
					newAmerTempSetColdText.setText("");
	    			newAmerTempSetHeatText.setText("");
	    			setTempCerColdPoiText.setText("");
	    			setTempCerHeatPoiText.setText("");
	    			newAmerTempSetText.setText("");
					setTempCerPoiText.setText("");
				}
			} else {
				newAmerTempSetColdText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    			newAmerTempSetHeatText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    			setTempCerColdPoiText.setText("");
    			setTempCerHeatPoiText.setText("");
    			newAmerTempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
				setTempCerPoiText.setText("");
				tempSetUnitText.setText("");
        		tempSetHeatUnitText.setText("");
        		tempSetColdUnitText.setText("");
			}
		}
     
        int offset = Util.dip2px(getActivity(), 1);
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	amerTempSetCir.setVisibility(View.VISIBLE);
	        amerTempDisCir.setVisibility(View.VISIBLE);
        	
	        disTempBgBitmap = BitmapFactory.decodeResource(
	    			getResources(), R.drawable.amer_temp_dis_cir_bg).copy(Bitmap.Config.ARGB_8888, true); 
	        Canvas canvas = new Canvas(disTempBgBitmap);
	        Paint paint = new Paint();
	        RectF rectf = new RectF(0 - offset, 0 - offset, disTempBgBitmap.getWidth() + offset,  disTempBgBitmap.getHeight() + offset);
	        paint.setAntiAlias(true);
		    paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
	        canvas.drawArc(rectf, 120, -60, true, paint);
	        amerTempDisBg.setImageBitmap(disTempBgBitmap);
	       
	        if (offBl) {
		        setTempBgBitmap = BitmapFactory.decodeResource(
		    			getResources(), R.drawable.amer_temp_set_cir_bg_offline).copy(Bitmap.Config.ARGB_8888, true); 
	        } else {
	        	setTempBgBitmap = BitmapFactory.decodeResource(
		    			getResources(), R.drawable.amer_temp_set_cir_bg).copy(Bitmap.Config.ARGB_8888, true);
	        }
	        canvas = new Canvas(setTempBgBitmap);
		    rectf = new RectF(0 - offset, 0 - offset, setTempBgBitmap.getWidth() + offset,  setTempBgBitmap.getHeight() + offset);
		    canvas.drawArc(rectf, 130, -80, true, paint);
	        amerTempSetBg.setImageBitmap(setTempBgBitmap);
	        
	        int angle = 0;
	        int temp = minAutoTemp;
	        double iDisTemp = Double.valueOf(disTemp).doubleValue();
	        if (getResources().getString(R.string.value_status_one).equals(status)
	        		|| getResources().getString(R.string.value_status_three).equals(status)) {
	        	temp = (int) heatTemp;
	        } else if (getResources().getString(R.string.value_status_two).equals(status)) {
	        	temp = (int) coolTemp;
	        } else if (getResources().getString(R.string.value_status_zero).equals(status)) {
	        	if (heatTemp >= iDisTemp) {
	        		temp = (int) heatTemp;
	        	} else if (coolTemp <= iDisTemp) {
	        		temp = (int) coolTemp;
	        	} else {
	        		temp = 0;
	        	}
	        } else if (getResources().getString(R.string.value_status_four).equals(status)) {
	        	String oldStatus = amerTempBean.getOriStatus();
	        	if (getResources().getString(R.string.value_status_one).equals(oldStatus)
	             		|| getResources().getString(R.string.value_status_three).equals(oldStatus)) {
	        		temp = (int) heatTemp;
				} else if (getResources().getString(R.string.value_status_two).equals(oldStatus)) {
					temp = (int) coolTemp;
				} else if (getResources().getString(R.string.value_status_zero).equals(oldStatus)) {
		        	if (heatTemp >= iDisTemp) {
		        		temp = (int) heatTemp;
		        	} else if (coolTemp <= iDisTemp) {
		        		temp = (int) coolTemp;
		        	} else {
		        		temp = 0;
		        	}
				} else {
					temp = (int) heatTemp;
				}
	        } 
	        
	        int draw = 0;
	        if (temp == 0 || offBl) {
	//        	angle = -80;
	//        	draw = R.drawable.amer_temp_set_cir_bg;
	        	amerTempSetCir.setVisibility(View.GONE);
	        } else {
	        	amerTempSetCir.setVisibility(View.VISIBLE);
	        	angle = -360 + (360 - 80)*(temp - minAutoTemp)/(maxAutoTemp - minAutoTemp);
	        	draw = R.drawable.amer_temp_set_cir;
	        	setTempBitmap = BitmapFactory.decodeResource(
	        				getResources(), draw).copy(Bitmap.Config.ARGB_8888, true); 
	            canvas = new Canvas(setTempBitmap);
	            
	            rectf = new RectF(0 - offset, 0 - offset, setTempBitmap.getWidth() + offset,  setTempBitmap.getHeight() + offset);
	            canvas.drawArc(rectf, 130, angle, true, paint);
	            amerTempSetCir.setImageBitmap(setTempBitmap);
	        }
	//        setTempBitmap = BitmapFactory.decodeResource(
	//    			getResources(), draw).copy(Bitmap.Config.ARGB_8888, true); 
	//        canvas = new Canvas(setTempBitmap);
	//        rectf = new RectF(0, 0, setTempBitmap.getWidth() + 5,  setTempBitmap.getHeight() + 5);
	//        canvas.drawArc(rectf, 130, angle, true, paint);
	//        amerTempSetCir.setImageBitmap(setTempBitmap);
	
	        if (iDisTemp > maxAutoTemp) {
	        	iDisTemp = maxAutoTemp;
	        } else if (iDisTemp < minAutoTemp) {
	        	iDisTemp = minAutoTemp;
	        }
	        angle = (int) (-360 + (360 - 60)*(iDisTemp - minAutoTemp)/(maxAutoTemp - minAutoTemp));
	        disTempBitmap = BitmapFactory.decodeResource(
	    			getResources(), R.drawable.amer_temp_dis_cir).copy(Bitmap.Config.ARGB_8888, true); 
	        canvas = new Canvas(disTempBitmap);
	        rectf = new RectF(0 - offset, 0 - offset, disTempBitmap.getWidth() + offset,  disTempBitmap.getHeight() + offset);
	        canvas.drawArc(rectf, 120, angle, true, paint);
	        amerTempDisCir.setImageBitmap(disTempBitmap);
        } else {
        	setTempBgBitmap = BitmapFactory.decodeResource(
	    			getResources(), R.drawable.amer_temp_set_cir_bg_offline).copy(Bitmap.Config.ARGB_8888, true); 
	        Canvas canvas = new Canvas(setTempBgBitmap);
		    Paint paint = new Paint();  
		    RectF rectf = new RectF(0 - offset, 0 - offset, setTempBgBitmap.getWidth() + offset,  setTempBgBitmap.getHeight() + offset);
		    paint.setAntiAlias(true);
		    paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
		    canvas.drawArc(rectf, 130, -80, true, paint);
	        amerTempSetBg.setImageBitmap(setTempBgBitmap);	
	        
	        disTempBgBitmap = BitmapFactory.decodeResource(
	    			getResources(), R.drawable.amer_temp_dis_cir_bg_offline).copy(Bitmap.Config.ARGB_8888, true); 
	        canvas = new Canvas(disTempBgBitmap);
	        rectf = new RectF(0 - offset, 0 - offset, disTempBgBitmap.getWidth() + offset,  disTempBgBitmap.getHeight() + offset);
	        canvas.drawArc(rectf, 120, -60, true, paint);
	        amerTempDisBg.setImageBitmap(disTempBgBitmap);
	        
	        amerTempSetCir.setVisibility(View.GONE);
	        amerTempDisCir.setVisibility(View.GONE);
        }
    }
    
    private void setModText() {
    	if (getResources().getString(R.string.value_mod_sche).equals(mod)) {
    		newAmerModText.setText(getResources().getString(R.string.lang_txt_mod_sche));
    	} else if (getResources().getString(R.string.value_mod_perm).equals(mod)) {
    		newAmerModText.setText(getResources().getString(R.string.lang_txt_mod_perm));
    	} else if (getResources().getString(R.string.value_mod_holi).equals(mod)) {
    		newAmerModText.setText(getResources().getString(R.string.lang_txt_mod_holi));
    	} else if (getResources().getString(R.string.value_mod_temp).equals(mod)) {
    		newAmerModText.setText(getResources().getString(R.string.lang_txt_mod_temp));
    	} else {
    		newAmerModText.setText(getResources().getString(R.string.lang_txt_mod_temp));
    	}
    }
    
    private String getTempValue(double value) {
    	String ret = "";
        if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		ret = Util.getValue(String.valueOf(value), 0); 		
		} else {
			ret = String.valueOf(new BigDecimal(value).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue());
		}
        return ret;
    }
     
    private void setOnline(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
        	newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod);
        	newAmerLayoutView.setBackgroundResource(R.drawable.new_amer_home_bg);
    	} else {
    		setDisEnable();
        	newAmerModBtnImg.setBackgroundResource(R.drawable.new_amer_mod_offline);
        	newAmerTempSetBg.setBackgroundResource(R.drawable.amer_temp_set_offline);
        	newAmerTempSetColdBg.setBackgroundResource(R.drawable.amer_temp_set_offline);
        	newAmerTempSetHeatBg.setBackgroundResource(R.drawable.amer_temp_set_offline);
        	newAmerLayoutView.setBackgroundResource(R.drawable.new_amer_home_bg_offline);
    	}
    }
    
    private void setEnable() {
    	newAmerModBtnImg.setEnabled(true);
    }

	private void setDisEnable() {
    	newAmerModBtnImg.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
	
	public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
}