package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * AmerStatusFragment
 * 
 * @author chenwh
 *
 */
public class AmerStatusFragment extends Fragment {
	
	private TextView staColdBtnImg, staHeatBtnImg, staOffBtnImg, staAutoBtnImg, staEmerBtnImg;
	
	private TextView newAmerBackButton, statusOkBtn, heatBtnImg, coldBtnImg, autoBtnImg, offBtnImg;
	
	private TextView newAmerBackText, titleText;
	
	private LinearLayout newAmerLayoutView;
	
	private RelativeLayout emerExist, emerNotExist;
	
	private String mac;
	
	private String status;
	
//	private String oldStatus;
	
	private HailinDB db;
	
	private boolean isForeRun = true;
	
	private AmerTempBean amerTempBean;
	
	private BackgroundTask bTask;
	
	private Handler handler = new Handler();
	
	private int coolDefaultTemp = 29;
	
	private int heatDefaultTemp = 20;
	
	private int heatDefaultTempFah = 68;
	
	private int coolDefaultTempFah = 84;
	
	private String coolTemp;
	
	private String heatTemp;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private CustomProgressDialog progressDialog = null;
	
	private static boolean pageIsUpd = false;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.amer_status_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();  
        
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity(); 
        
        if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
        	coolTemp = Constants.FAH_START_KEY + String.valueOf(coolDefaultTempFah);
        	heatTemp = Constants.FAH_START_KEY + String.valueOf(heatDefaultTempFah);
    	} else {
    		coolTemp = Constants.CEN_START_KEY + String.valueOf(coolDefaultTemp);
    		heatTemp = Constants.CEN_START_KEY + String.valueOf(heatDefaultTemp);
    	}
        
        
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);	
		
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				Constants.AMER_STATUS_FRAGMENT) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;
		handler.post(runnable);	
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void initItem() {
    	
    	newAmerBackButton = (TextView) getView().findViewById(R.id.newAmerBackButton);
    	staColdBtnImg = (TextView) getView().findViewById(R.id.staColdBtnImg);
    	staHeatBtnImg = (TextView) getView().findViewById(R.id.staHeatBtnImg);
    	staOffBtnImg = (TextView) getView().findViewById(R.id.staOffBtnImg);
    	staAutoBtnImg = (TextView) getView().findViewById(R.id.staAutoBtnImg);
    	staEmerBtnImg = (TextView) getView().findViewById(R.id.staEmerBtnImg);
    	statusOkBtn = (TextView) getView().findViewById(R.id.statusOkBtn);
    	heatBtnImg = (TextView) getView().findViewById(R.id.heatBtnImg);
    	coldBtnImg = (TextView) getView().findViewById(R.id.coldBtnImg);
    	autoBtnImg = (TextView) getView().findViewById(R.id.autoBtnImg);
    	offBtnImg = (TextView) getView().findViewById(R.id.offBtnImg);
    	newAmerBackText = (TextView) getView().findViewById(R.id.newAmerBackText);
    	titleText = (TextView) getView().findViewById(R.id.titleText);

    	emerExist =  (RelativeLayout) getView().findViewById(R.id.emerExist);
    	emerNotExist =  (RelativeLayout) getView().findViewById(R.id.emerNotExist);
    	
    	newAmerLayoutView =  (LinearLayout) getView().findViewById(R.id.newAmerLayoutView);
    	
        // ȡ��progressDialog
        progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
    }
    
    private void itemEvent() {
    	buttonEvent();
    	backEvent();
    }
    
    private void buttonEvent() {
    	
    	staColdBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
	        	if (!getResources().getString(R.string.value_status_two).equals(status)) {
	        		setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_two);
	        		staColdBtnImg.setBackgroundResource(R.drawable.new_amer_cold_sel);
	        	}
			}
        });
    	
    	staHeatBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			if (!getResources().getString(R.string.value_status_one).equals(status)) {
    				setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_one);
	        		staHeatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_sel);
    			} 
			}
        });
    	
    	staAutoBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!getResources().getString(R.string.value_status_zero).equals(status)) {
					setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_zero);
    				staAutoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_sel);
    			} 
			}
        });
    	
    	staEmerBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			if (!getResources().getString(R.string.value_status_three).equals(status)) {
    				setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_three);
    				staEmerBtnImg.setBackgroundResource(R.drawable.new_amer_emer_sel);
    			}
			}
        });
    	
    	staOffBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			if (!getResources().getString(R.string.value_status_four).equals(status)) {
    				setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_four);
    				staOffBtnImg.setBackgroundResource(R.drawable.new_amer_off_sel);
    			}
			}
        });
    	
    	coldBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
	        	if (!getResources().getString(R.string.value_status_two).equals(status)) {
	        		setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_two);
	        		coldBtnImg.setBackgroundResource(R.drawable.new_amer_cold_sel);
	        	}
			}
        });
    	
    	heatBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			if (!getResources().getString(R.string.value_status_one).equals(status)) {
    				setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_one);
	        		heatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_sel);
    			} 
			}
        });
    	
    	autoBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (!getResources().getString(R.string.value_status_zero).equals(status)) {
					setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_zero);
    				autoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_sel);
    			} 
			}
        });
    	
    	offBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			if (!getResources().getString(R.string.value_status_four).equals(status)) {
    				setButtonDrawable();
	        		status = getResources().getString(R.string.value_status_four);
    				offBtnImg.setBackgroundResource(R.drawable.new_amer_off_sel);
    			}
			}
        });
    	
    	statusOkBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
				if (progressDialog != null) {
					progressDialog.show();
				}
				updWeb();
			}
        });
    }
    
    private void updateDb() {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, mac);
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.STATUS, status);
		if (!getResources().getString(R.string.value_status_four).equals(status)) {
			params.put(Constants.ORI_STATUS, status);
		}
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		params.put(Constants.TEMP_HEAT, heatTemp);
    		params.put(Constants.TEMP_COOL, coolTemp);
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		params.put(Constants.TEMP_COOL, coolTemp);
    	} else if (getResources().getString(R.string.value_status_one).equals(status)
    			|| getResources().getString(R.string.value_status_three).equals(status)) {
    		params.put(Constants.TEMP_HEAT, heatTemp);
    	}
		db.getTAmerTemp().update(params);
    }
    
    private void updWeb() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	RequestParams paramMap = new RequestParams();
//    	String mac = "abcde11223344edcba2";
//    	String mac = "0050C2D8E7A1";
//    	String mac = "0050C2D8E79B";
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	paramMap.put(getResources().getString(R.string.param_status), status);
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		paramMap.put(getResources().getString(R.string.param_temp_heat), heatTemp);
    		paramMap.put(getResources().getString(R.string.param_temp_cool), coolTemp);
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		paramMap.put(getResources().getString(R.string.param_temp_cool), coolTemp);
    	} else if (getResources().getString(R.string.value_status_one).equals(status)
    			|| getResources().getString(R.string.value_status_three).equals(status)) {
    		paramMap.put(getResources().getString(R.string.param_temp_heat), heatTemp);
    	}
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
            	updateDb();
            	pageIsUpd = false;
            	getFragmentManager().popBackStackImmediate();
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            	Toast.makeText(getActivity().getApplicationContext(), 
    					getResources().getString(R.string.lang_mess_exception),  
    			        Toast.LENGTH_SHORT).show();
            	pageIsUpd = false;
            }  
        });
    }
    
    private void backEvent() {
    	
    	newAmerBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerBackText.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ������ҳ����
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    }
    
    private void setDataToPage() {
           
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	amerTempBean = db.getTAmerTemp().findOne(arg);
    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
        status = amerTempBean.getStatus();
//        oldStatus = amerTempBean.getOriStatus();
        String emerStatus = amerTempBean.getEmerStatus();
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	if (getResources().getString(R.string.value_emer_status_on).equals(emerStatus)) {
        		emerExist.setVisibility(View.VISIBLE);
    			emerNotExist.setVisibility(View.GONE);
    			if (getResources().getString(R.string.value_status_zero).equals(status)) {
    				staAutoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_sel);
    			} else if (getResources().getString(R.string.value_status_one).equals(status)) {
    				staHeatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_sel);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				staColdBtnImg.setBackgroundResource(R.drawable.new_amer_cold_sel);
    			} else if (getResources().getString(R.string.value_status_three).equals(status)) {
    				staEmerBtnImg.setBackgroundResource(R.drawable.new_amer_emer_sel);
    			} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    				staOffBtnImg.setBackgroundResource(R.drawable.new_amer_off_sel);
    			}
        	} else {
        		emerExist.setVisibility(View.GONE);
        		emerNotExist.setVisibility(View.VISIBLE);
        		if (getResources().getString(R.string.value_status_zero).equals(status)) {
    				autoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_sel);
    			} else if (getResources().getString(R.string.value_status_one).equals(status)) {
    				heatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_sel);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				coldBtnImg.setBackgroundResource(R.drawable.new_amer_cold_sel);
    			} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    				offBtnImg.setBackgroundResource(R.drawable.new_amer_off_sel);
    			}
        	}
    	} else {
    		if (getResources().getString(R.string.value_emer_status_on).equals(emerStatus)) {
    			emerExist.setVisibility(View.VISIBLE);
    			emerNotExist.setVisibility(View.GONE);
    			if (getResources().getString(R.string.value_status_zero).equals(status)) {
    				staAutoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_offline_sel);
    			} else if (getResources().getString(R.string.value_status_one).equals(status)) {
    				staHeatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_offline_sel);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				staColdBtnImg.setBackgroundResource(R.drawable.new_amer_cold_offline_sel);
    			} else if (getResources().getString(R.string.value_status_three).equals(status)) {
    				staEmerBtnImg.setBackgroundResource(R.drawable.new_amer_emer_offline_sel);
    			} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    				staOffBtnImg.setBackgroundResource(R.drawable.new_amer_off_offline_sel);
    			}
        	} else {
        		emerExist.setVisibility(View.GONE);
        		emerNotExist.setVisibility(View.VISIBLE);
        		if (getResources().getString(R.string.value_status_zero).equals(status)) {
    				autoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_offline_sel);
    			} else if (getResources().getString(R.string.value_status_one).equals(status)) {
    				heatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_offline_sel);
    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    				coldBtnImg.setBackgroundResource(R.drawable.new_amer_cold_offline_sel);
    			} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    				offBtnImg.setBackgroundResource(R.drawable.new_amer_off_offline_sel);
    			}
        	}
    	}
    }
     
    private void setOnline(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		setButtonDrawable();
        	statusOkBtn.setBackgroundResource(R.drawable.new_amer_ok);
        	newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow);
    		titleText.setTextColor(getResources().getColor(R.color.color_green));
    	} else {
    		setDisEnable();
    		staColdBtnImg.setBackgroundResource(R.drawable.new_amer_cold_offline);
        	staHeatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_offline);
        	staOffBtnImg.setBackgroundResource(R.drawable.new_amer_off_offline);
        	staAutoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_offline);
        	staEmerBtnImg.setBackgroundResource(R.drawable.new_amer_emer_offline);
        	heatBtnImg.setBackgroundResource(R.drawable.new_amer_heat_offline);
        	coldBtnImg.setBackgroundResource(R.drawable.new_amer_cold_offline);
        	autoBtnImg.setBackgroundResource(R.drawable.new_amer_auto_offline);
        	offBtnImg.setBackgroundResource(R.drawable.new_amer_off_offline);
        	statusOkBtn.setBackgroundResource(R.drawable.new_amer_ok_offline);
        	newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow_offline);
    		titleText.setTextColor(getResources().getColor(R.color.color_grey));
    	}
    }
    
    private void setButtonDrawable() {
    	staColdBtnImg.setBackgroundResource(R.drawable.new_amer_cold);
    	staHeatBtnImg.setBackgroundResource(R.drawable.new_amer_heat);
    	staOffBtnImg.setBackgroundResource(R.drawable.new_amer_off);
    	staAutoBtnImg.setBackgroundResource(R.drawable.new_amer_auto);
    	staEmerBtnImg.setBackgroundResource(R.drawable.new_amer_emer);
    	heatBtnImg.setBackgroundResource(R.drawable.new_amer_heat);
    	coldBtnImg.setBackgroundResource(R.drawable.new_amer_cold);
    	autoBtnImg.setBackgroundResource(R.drawable.new_amer_auto);
    	offBtnImg.setBackgroundResource(R.drawable.new_amer_off);
    }
    
    private void setEnable() {
    	staColdBtnImg.setEnabled(true);
    	staHeatBtnImg.setEnabled(true);
    	staOffBtnImg.setEnabled(true);
    	staAutoBtnImg.setEnabled(true);
    	staEmerBtnImg.setEnabled(true);
    	heatBtnImg.setEnabled(true);
    	coldBtnImg.setEnabled(true);
    	autoBtnImg.setEnabled(true);
    	offBtnImg.setEnabled(true);
    	statusOkBtn.setEnabled(true);
    }

	private void setDisEnable() {
		staColdBtnImg.setEnabled(false);
    	staHeatBtnImg.setEnabled(false);
    	staOffBtnImg.setEnabled(false);
    	staAutoBtnImg.setEnabled(false);
    	staEmerBtnImg.setEnabled(false);
    	heatBtnImg.setEnabled(false);
    	coldBtnImg.setEnabled(false);
    	autoBtnImg.setEnabled(false);
    	offBtnImg.setEnabled(false);
    	statusOkBtn.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
	
	public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
}