package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.Map;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;
import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextPaint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * AmerTempFragment
 * 
 * @author chenwh
 *
 */
public class AmerTempFragment extends Fragment {
	
	private TextView newAmerBackButton, amerTempOkBtn, tempSetText, tempHeatSetText, tempCoolSetText;
	
	private TextView newAmerBackText, titleText, tempTextSumm;
	
	private LinearLayout newAmerLayoutView, wheelBg, wheelAutoBg;
	
	private WheelView amerTempWheel, amerTempCoolWheel, amerTempHeatWheel;
	
	private RelativeLayout wheelRelLayout, wheelAutoRelLayout;
	
	private FrameLayout tempSetBg, tempSetCoolBg, tempSetHeatBg;
	
	private String mac;
	
	private String status;
	
	private HailinDB db;
	
	private boolean isForeRun = true;
	
	private AmerTempBean amerTempBean;
	
	private BackgroundTask bTask;
	
	private Handler handler = new Handler();
	
	private int maxCoolTemp = 37;
	
	private int minCoolTemp = 10;
	
	private int minHeatTemp = 4;
	
	private int maxHeatTemp = 32;
	
//	private int minAutoTemp = 4;
//	
//	private int maxAutoTemp = 37;
	
	private int maxCoolTempFah = 99;
	
	private int minCoolTempFah = 50;
	
	private int minHeatTempFah = 40;
	
	private int maxHeatTempFah = 90;
	
//	private int minAutoTempFah = 40;
//	
//	private int maxAutoTempFah = 99;
	
	private int minValue = 0;
	
	private int maxValue = 0;
	
	private int minHeatValue = 0;
	
	private int maxHeatValue = 0;
	
	private int minCoolValue = 0;
	
	private int maxCoolValue = 0;
	
	private int currentTemp = 0;
	
	private int currentCoolTemp = 0;
	
	private int currentHeatTemp = 0;
	
	private boolean scrolling = false; 
	
	private boolean heatScrolling = false; 
	
	private boolean coolScrolling = false; 
	
	private String heatTemp;
	
	private String coolTemp;
	
	private static boolean pageIsUpd = false;
	
	private String tempCool;
	
	private String tempHeat;
	
	private String temp;
	
	private String online;
	
	private boolean wheelInit = false;
	
	private int deadTemp = 0;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private CustomProgressDialog progressDialog = null;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.amer_temp_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();  
        
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity(); 
        
        if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		maxCoolTemp = maxCoolTempFah;
    		minCoolTemp = minCoolTempFah;
    		maxHeatTemp = maxHeatTempFah;
    		minHeatTemp = minHeatTempFah;
    	} 
        
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);	
		
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				Constants.AMER_TEMP_FRAGMENT) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		handler.post(runnable);	
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void initItem() {
    	
    	newAmerBackButton = (TextView) getView().findViewById(R.id.newAmerBackButton);
    	amerTempOkBtn = (TextView) getView().findViewById(R.id.amerTempOkBtn);
    	tempSetText = (TextView) getView().findViewById(R.id.tempSetText); 
//    	tempMsgText = (TextView) getView().findViewById(R.id.tempMsgText); 
    	tempHeatSetText = (TextView) getView().findViewById(R.id.tempHeatSetText); 
    	tempCoolSetText = (TextView) getView().findViewById(R.id.tempCoolSetText); 
    	newAmerBackText = (TextView) getView().findViewById(R.id.newAmerBackText);
    	titleText = (TextView) getView().findViewById(R.id.titleText);
    	tempTextSumm = (TextView) getView().findViewById(R.id.tempTextSumm);
    	
    	amerTempWheel =  (WheelView) getView().findViewById(R.id.amerTempWheel);
    	amerTempCoolWheel =  (WheelView) getView().findViewById(R.id.amerTempCoolWheel);
    	amerTempHeatWheel =  (WheelView) getView().findViewById(R.id.amerTempHeatWheel);
    	
    	wheelRelLayout = (RelativeLayout) getView().findViewById(R.id.wheelRelLayout);
    	wheelAutoRelLayout = (RelativeLayout) getView().findViewById(R.id.wheelAutoRelLayout);
    	
    	newAmerLayoutView =  (LinearLayout) getView().findViewById(R.id.newAmerLayoutView);
    	wheelBg =  (LinearLayout) getView().findViewById(R.id.wheelBg);
    	wheelAutoBg =  (LinearLayout) getView().findViewById(R.id.wheelAutoBg);
    	
    	tempSetBg =  (FrameLayout) getView().findViewById(R.id.tempSetBg);
    	tempSetCoolBg =  (FrameLayout) getView().findViewById(R.id.tempSetCoolBg);
    	tempSetHeatBg =  (FrameLayout) getView().findViewById(R.id.tempSetHeatBg);
    	
        // ȡ��progressDialog
        progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
        
//        tempMsgText.setText("");
        
        if (Constants.LANG_DEFALUT_ENG.equals(getResources().getString(R.string.lang_temp_default))) {
        	tempTextSumm.setVisibility(View.VISIBLE);
		} else {
			tempTextSumm.setVisibility(View.GONE);
		}
    }
    
    private void itemEvent() {
    	wheelEvent();
    	buttonEvent();
    	backEvent();
    }
    
    private void wheelEvent() {
    	
    	amerTempWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if (!scrolling) {
					pageIsUpd = false;
					TempAdapter tAdapter = new TempAdapter(getActivity(), minValue, maxValue, 1);
					currentTemp = newValue;
			        amerTempWheel.setViewAdapter(tAdapter);
			        amerTempWheel.setCurrentItem(currentTemp);
				}
				
			}
		});
    	
    	amerTempWheel.addScrollingListener( new OnWheelScrollListener() {
			@Override
			public void onScrollingStarted(WheelView wheel) {
				scrolling = true;
				pageIsUpd = true;
				wheelInit = true;
			}
			@Override
			public void onScrollingFinished(WheelView wheel) {
				scrolling = false;
				pageIsUpd = false;
				currentTemp = wheel.getCurrentItem();
				TempAdapter tAdapter = new TempAdapter(getActivity(), minValue, maxValue, 1);
		        amerTempWheel.setViewAdapter(tAdapter);
		        amerTempWheel.setCurrentItem(currentTemp);
			}
		});
    	
    	amerTempCoolWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if (!coolScrolling) {
					if (!heatScrolling) {
						pageIsUpd = false;
					}
					TempAdapter tAdapter = new TempAdapter(getActivity(), minCoolValue, maxCoolValue, 2);
					currentCoolTemp = newValue;
					amerTempCoolWheel.setViewAdapter(tAdapter);
					amerTempCoolWheel.setCurrentItem(currentCoolTemp);
				}
				
			}
		});
    	
    	amerTempCoolWheel.addScrollingListener( new OnWheelScrollListener() {
			@Override
			public void onScrollingStarted(WheelView wheel) {
				coolScrolling = true;
				pageIsUpd = true;
			}
			@Override
			public void onScrollingFinished(WheelView wheel) {
				if (!heatScrolling) {
					pageIsUpd = false;
				}
				coolScrolling = false;
				currentCoolTemp = wheel.getCurrentItem();
				TempAdapter tAdapter = new TempAdapter(getActivity(), minCoolValue, maxCoolValue, 2);
				amerTempCoolWheel.setViewAdapter(tAdapter);
				amerTempCoolWheel.setCurrentItem(currentCoolTemp);
				
				int heatCurTemp = amerTempHeatWheel.getCurrentItem() + minHeatValue;
	        	if (currentCoolTemp + minCoolValue < heatCurTemp + deadTemp) {
	        		currentHeatTemp = currentCoolTemp + minCoolValue - deadTemp - minHeatValue;
	        		amerTempHeatWheel.setCurrentItem(currentHeatTemp);
	        	}
			}
		});
    	
    	amerTempHeatWheel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if (!heatScrolling) {
					if (!coolScrolling) {
						pageIsUpd = false;
					}
					TempAdapter tAdapter = new TempAdapter(getActivity(), minHeatValue, maxHeatValue, 3);
					currentHeatTemp = newValue;
					amerTempHeatWheel.setViewAdapter(tAdapter);
					amerTempHeatWheel.setCurrentItem(currentHeatTemp);
				}
				
			}
		});
    	
    	amerTempHeatWheel.addScrollingListener( new OnWheelScrollListener() {
			@Override
			public void onScrollingStarted(WheelView wheel) {
				heatScrolling = true;
				pageIsUpd = true;
			}
			@Override
			public void onScrollingFinished(WheelView wheel) {
				if (!coolScrolling) {
					pageIsUpd = false;
				}
				heatScrolling = false;
				currentHeatTemp = wheel.getCurrentItem();
				TempAdapter tAdapter = new TempAdapter(getActivity(), minHeatValue, maxHeatValue, 3);
				amerTempHeatWheel.setViewAdapter(tAdapter);
				amerTempHeatWheel.setCurrentItem(currentHeatTemp);
				
				int coolCurTemp = amerTempCoolWheel.getCurrentItem() + minCoolValue;
	        	if (currentHeatTemp  + minHeatValue > coolCurTemp - deadTemp) {
	        		currentCoolTemp = currentHeatTemp + minHeatValue + deadTemp  - minCoolValue;
	        		amerTempCoolWheel.setCurrentItem(currentCoolTemp);
	        	}
			}
		});
    }
    
    private void buttonEvent() {
    	amerTempOkBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				tempMsgText.setText("");
				if (scrolling || heatScrolling || coolScrolling) {
					return;
				}
				pageIsUpd = true;
//				if (!checkTemp()) {
//					pageIsUpd = false;
//					return;
//				}
				
				setTemp();
				if (progressDialog != null) {
					progressDialog.show();
				}
				
				updWeb();
			}
        });
    }
    
    private void setTemp() {
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {

        	int heatCurTemp = amerTempHeatWheel.getCurrentItem() + minHeatValue;
        	int coolCurTemp = amerTempCoolWheel.getCurrentItem() + minCoolValue;
        	
        	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
        		heatTemp = Constants.FAH_START_KEY + String.valueOf(heatCurTemp);
        		coolTemp = Constants.FAH_START_KEY + String.valueOf(coolCurTemp); 
    		} else {
    			heatTemp = Constants.CEN_START_KEY + String.valueOf(heatCurTemp);
        		coolTemp = Constants.CEN_START_KEY + String.valueOf(coolCurTemp);
    		}
        	
//        	if ((heatCurTemp + deadTemp) > coolCurTemp) {
//        		String msg = getResources().getString(R.string.lang_mess_temp_set);
//        		tempMsgText.setText(msg.replace("{0}", String.valueOf(deadTemp)));
//        	}
        	
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			coolTemp = Constants.FAH_START_KEY + String.valueOf(
    					amerTempWheel.getCurrentItem() + minValue);
    		} else {
    			coolTemp = Constants.CEN_START_KEY + String.valueOf(
    					amerTempWheel.getCurrentItem() + minValue);
    		}
    	} else {
    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			heatTemp = Constants.FAH_START_KEY + String.valueOf(
    					amerTempWheel.getCurrentItem() + minValue);
    		} else {
    			heatTemp = Constants.CEN_START_KEY + String.valueOf(
    					amerTempWheel.getCurrentItem() + minValue);
    		}
    	}
    	
    }
    
    private void updateDb() {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, mac);
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.MOD, getResources().getString(R.string.value_mod_temp));
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		params.put(Constants.TEMP_HEAT, heatTemp);
    		params.put(Constants.TEMP_COOL, coolTemp);
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		params.put(Constants.TEMP_COOL, coolTemp);
    	} else {
    		params.put(Constants.TEMP_HEAT, heatTemp);
    	}
    	
		db.getTAmerTemp().update(params);
    }
    
    private void updWeb() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	RequestParams paramMap = new RequestParams();
//    	String mac = "abcde11223344edcba2";
//    	String mac = "0050C2D8E7A1";
//    	String mac = "0050C2D8E79B";
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		paramMap.put(getResources().getString(R.string.param_temp_heat), heatTemp);
    		paramMap.put(getResources().getString(R.string.param_temp_cool), coolTemp);
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		paramMap.put(getResources().getString(R.string.param_temp_cool), coolTemp);
    	} else {
    		paramMap.put(getResources().getString(R.string.param_temp_heat), heatTemp);
    	}
    	paramMap.put(getResources().getString(R.string.param_mod), 
    			getResources().getString(R.string.value_mod_temp));
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
            	updateDb();
            	pageIsUpd = false;
            	getFragmentManager().popBackStackImmediate();
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (progressDialog != null) {
    				progressDialog.dismiss();
				}
            	Toast.makeText(getActivity().getApplicationContext(), 
    					getResources().getString(R.string.lang_mess_exception),  
    			        Toast.LENGTH_SHORT).show();
            	pageIsUpd = false;
            }  
        });
    }
    
    private void backEvent() {
    	
    	newAmerBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerBackText.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	newAmerLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ������ҳ����
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    }
    
    private void setDataToPage() {
           
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	amerTempBean = db.getTAmerTemp().findOne(arg);
    	online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	
    	String deadZoneTemp = amerTempBean.getDeadZoneTemp();
    	if (!CheckUtil.requireCheck(deadZoneTemp)) {
    		deadTemp = 2;
    	}
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			if (deadTemp == 0 && deadZoneTemp.startsWith(Constants.FAH_START_KEY)) {
				deadTemp = Integer.valueOf(deadZoneTemp.substring(1));
			} else {
				deadTemp = 2;
			}
		} else {
			if (deadTemp == 0 && deadZoneTemp.startsWith(Constants.CEN_START_KEY)) {
				deadTemp = Integer.valueOf(deadZoneTemp.substring(1));
			} else {
				deadTemp = 2;
			}
		}
    	
    	setOnline();
        status = amerTempBean.getStatus();
        if (getResources().getString(R.string.value_status_four).equals(status)) {
        	setTempValue(amerTempBean.getOriStatus());
        } else {
        	setTempValue(status);
        }
        
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	if (getResources().getString(R.string.value_status_two).equals(status)) {
        		tempSetBg.setBackgroundResource(R.drawable.set_temp_cool);
        	} else if (getResources().getString(R.string.value_status_one).equals(status)) {
        		tempSetBg.setBackgroundResource(R.drawable.set_temp_heat);
        	} else if (getResources().getString(R.string.value_status_three).equals(status)) {
        		tempSetBg.setBackgroundResource(R.drawable.set_temp_emer);
        	} else if (getResources().getString(R.string.value_status_four).equals(status)) {
        		tempSetBg.setBackgroundResource(R.drawable.set_temp_off);
        		tempSetCoolBg.setBackgroundResource(R.drawable.set_temp_off);
        		tempSetHeatBg.setBackgroundResource(R.drawable.set_temp_off);
        		setDisEnable();
        		amerTempOkBtn.setBackgroundResource(R.drawable.new_amer_ok_offline);
        	}
        } 
        
        if (getResources().getString(R.string.value_status_two).equals(status)) {
        	tempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_cool));
        } else if (getResources().getString(R.string.value_status_three).equals(status)) {
        	tempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_emer));
    	} else if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		tempHeatSetText.setText(getResources().getString(R.string.lang_txt_temp_set_heat));
    		tempCoolSetText.setText(getResources().getString(R.string.lang_txt_temp_set_cool));
    	} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    		tempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    		tempHeatSetText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    		tempCoolSetText.setText(getResources().getString(R.string.lang_txt_temp_set_off));
    	} else {
    		tempSetText.setText(getResources().getString(R.string.lang_txt_temp_set_heat));
    	}
    		
    	
    }
    
    private void setTempValue(String status) {
        if (getResources().getString(R.string.value_status_two).equals(status)) {
        	temp = amerTempBean.getTempCool();
        	minValue = minCoolTemp;
        	maxValue = maxCoolTemp;
        } else if ((getResources().getString(R.string.value_status_one).equals(status)) 
    			|| (getResources().getString(R.string.value_status_three).equals(status))) {
        	temp = amerTempBean.getTempHeat();
        	minValue = minHeatTemp;
        	maxValue = maxHeatTemp;
        } else if (getResources().getString(R.string.value_status_zero).equals(status)) {
        	tempCool = amerTempBean.getTempCool();
        	tempHeat = amerTempBean.getTempHeat();
        	minHeatValue = minHeatTemp;
        	maxHeatValue = maxHeatTemp;
        	minCoolValue = minCoolTemp;
        	maxCoolValue = maxCoolTemp;
        } 
        
        setWheel(status);

    }
    
    private void setWheel(String status) {
    	 if (getResources().getString(R.string.value_status_zero).equals(status)) {
         	 setAutoWheel();
         } else {
        	 setCoolHeatWheel();
         }
    }
    
    private void setAutoWheel() {
    	wheelRelLayout.setVisibility(View.GONE);
    	wheelAutoRelLayout.setVisibility(View.VISIBLE);

    	tempCool = Util.getValue(Util.getTempDegree(tempCool), 0);
    	tempHeat = Util.getValue(Util.getTempDegree(tempHeat), 0); 
    	int tCoolVal = Integer.valueOf(tempCool).intValue();
    	int tHeatVal = Integer.valueOf(tempHeat).intValue();
    	if (tCoolVal < minCoolValue) {
    		tCoolVal = minCoolValue;
    	} else if (tCoolVal > maxCoolValue) {
    		tCoolVal = maxCoolValue;
    	}
    	currentCoolTemp = tCoolVal - minCoolValue;
    	
    	if (tHeatVal < minHeatValue) {
    		tHeatVal = minHeatValue;
    	} else if (tHeatVal > maxHeatValue) {
    		tHeatVal = maxHeatValue;
    	}
    	currentHeatTemp = tHeatVal - minHeatValue;

        TempAdapter tCoolAdapter = new TempAdapter(getActivity(), minCoolValue, maxCoolValue, 2);
        amerTempCoolWheel.setViewAdapter(tCoolAdapter);
        amerTempCoolWheel.setDrawShadows(false);
        amerTempCoolWheel.setBackFlg(false);
        amerTempCoolWheel.setCurrentItem(currentCoolTemp);
        
        TempAdapter tHeatAdapter = new TempAdapter(getActivity(), minHeatValue, maxHeatValue, 3);
        amerTempHeatWheel.setViewAdapter(tHeatAdapter);
        amerTempHeatWheel.setDrawShadows(false);
        amerTempHeatWheel.setBackFlg(false);
        amerTempHeatWheel.setCurrentItem(currentHeatTemp);
        
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	amerTempCoolWheel.setWheelForeground(R.drawable.wheel_amer_cool_val);
        	amerTempHeatWheel.setWheelForeground(R.drawable.wheel_amer_heat_val);
        } else {
        	amerTempCoolWheel.setWheelForeground(R.drawable.wheel_amer_cool_val_offline);
        	amerTempHeatWheel.setWheelForeground(R.drawable.wheel_amer_heat_val_offline);
        }
    }
    
    private void setCoolHeatWheel() {
    	wheelRelLayout.setVisibility(View.VISIBLE);
    	wheelAutoRelLayout.setVisibility(View.GONE);
    	temp = Util.getValue(Util.getTempDegree(temp), 0); 
    	int tVal = Integer.valueOf(temp).intValue();
    	if (tVal < minValue) {
    		tVal = minValue;
    	} else if (tVal > maxValue) {
    		tVal = maxValue;
    	}
    	currentTemp = tVal - minValue;

        TempAdapter tAdapter = new TempAdapter(getActivity(), minValue, maxValue, 1);
        amerTempWheel.setViewAdapter(tAdapter);
        amerTempWheel.setDrawShadows(false);
        amerTempWheel.setBackFlg(false);
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	amerTempWheel.setWheelForeground(R.drawable.wheel_amer_val);
        } else {
        	amerTempWheel.setWheelForeground(R.drawable.wheel_amer_val_offline);
        }
        
        amerTempWheel.setCurrentItem(currentTemp);
    }
     
    private void setOnline() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		amerTempOkBtn.setBackgroundResource(R.drawable.new_amer_ok);
    		tempSetCoolBg.setBackgroundResource(R.drawable.set_temp_cool);
    		tempSetHeatBg.setBackgroundResource(R.drawable.set_temp_heat);
    		newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow);
    		titleText.setTextColor(getResources().getColor(R.color.color_green));
    		wheelBg.setBackgroundResource(R.drawable.wheel_amer_bg);
    		wheelAutoBg.setBackgroundResource(R.drawable.wheel_amer_bg);
    	} else {
    		setDisEnable();
    		amerTempOkBtn.setBackgroundResource(R.drawable.new_amer_ok_offline);
    		tempSetBg.setBackgroundResource(R.drawable.set_temp_offline);
    		tempSetCoolBg.setBackgroundResource(R.drawable.set_temp_offline);
    		tempSetHeatBg.setBackgroundResource(R.drawable.set_temp_offline);
    		newAmerBackButton.setBackgroundResource(R.drawable.new_amer_back_arrow_offline);
    		titleText.setTextColor(getResources().getColor(R.color.color_grey));
    		wheelBg.setBackgroundResource(R.drawable.wheel_amer_bg_offline);
    		wheelAutoBg.setBackgroundResource(R.drawable.wheel_amer_bg_offline);
    	}
    }
    
   
    
    private void setEnable() {
    	amerTempWheel.setEnabled(true);
    	amerTempHeatWheel.setEnabled(true);
    	amerTempCoolWheel.setEnabled(true);
    	amerTempOkBtn.setEnabled(true);
    }

	private void setDisEnable() {
		amerTempWheel.setEnabled(false);
		amerTempHeatWheel.setEnabled(false);
    	amerTempCoolWheel.setEnabled(false);
    	amerTempOkBtn.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
	
	/**
	 * Adapter for TempAdapter
	 */
	private class TempAdapter extends AbstractWheelTextAdapter {

		private int minVal;
		
		private int maxVal;
		
		private int wheelType;
		
		private Context context;
		
		/**
		 * Constructor
		 */
		protected TempAdapter(Context context, int minVal, int maxVal, int wheelType) {
			super(context, R.layout.wheel_temp, NO_RESOURCE);
			setItemTextResource(R.id.setTemp);
			this.minVal = minVal;
			this.maxVal = maxVal;
			this.wheelType = wheelType;
			this.context = context;
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = null;
			if (index >= 0 && index < getItemsCount()) {
	            if (cachedView == null) {
	            	cachedView = getView(itemResourceId, parent);
	            }
	            TextView textView = getTextView(cachedView, itemTextResourceId);
	            TextView poiTextView = getTextView(cachedView, R.id.setTempPoiText);
	            if (textView != null) {
	                CharSequence text = getItemText(index);
	                if (text == null) {
	                    text = "";
	                }
	                textView.setText(text);
	                boolean bl = scrolling;
	                int cTemp = currentTemp;
	                if (wheelType == 2) {
	                	bl = coolScrolling;
	                	cTemp = currentCoolTemp;
	                } else if (wheelType == 3) {
	                	bl = heatScrolling;
	                	cTemp = currentHeatTemp;
	                }
	                boolean cBl = false;
                	if ((minVal + cTemp) == (maxVal - 1)) {
                		cBl = true;
                	}
                	boolean hBl = false;
                	if (cTemp == 1) {
                		hBl = true;
                	}
	                if (!bl) {
		                if (index == cTemp) {
		                	TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
		                	setCenterText(textView, unitText, poiTextView, cBl);
		                } else if (index == (cTemp - 1) 
		                		|| index == (cTemp + 1)) {
		                	int topMargin = 4;
		                	if (index == (cTemp - 1)) {
		                		topMargin = -4;
		                	}
		                	TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
		                	setSecondText(textView, unitText, poiTextView, topMargin);
		                } else if (index == (cTemp - 2) 
		                		|| index == (cTemp + 2)) {
		                	TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
		                	setThirdText(textView, unitText, poiTextView, cBl, hBl);
		                }
	                } else {
//	                	if (parent.getChildCount() == 0) {
//	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		setThirdText(textView, unitText, poiTextView);
//	                	} else if (parent.getChildCount() == 1) {
//	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		setSecondText(textView, unitText, poiTextView, -8);
//	                	} else if (parent.getChildCount() == 2) {
//	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		setCenterText(textView, unitText, poiTextView);
//	                	} else if (parent.getChildCount() == 3) {
//	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		setSecondText(textView, unitText, poiTextView, 8);
//	                	} else if (parent.getChildCount() == 4) {
//	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		setThirdText(textView, unitText, poiTextView);
//	                	} else 
	                	if (parent.getChildCount() == 5) {
	                		TextView txZero = (TextView) parent.getChildAt(0).findViewById(R.id.setTemp);
	                		TextView txOne = (TextView) parent.getChildAt(1).findViewById(R.id.setTemp);
	                		TextView txTwo = (TextView) parent.getChildAt(2).findViewById(R.id.setTemp);
	                		TextView txThree = (TextView) parent.getChildAt(3).findViewById(R.id.setTemp);
	                		TextView txFour = (TextView) parent.getChildAt(4).findViewById(R.id.setTemp);
	                		TextView txUnitZero = (TextView) parent.getChildAt(0).findViewById(R.id.setTempUnit);
	                		TextView txUnitOne = (TextView) parent.getChildAt(1).findViewById(R.id.setTempUnit);
	                		TextView txUnitTwo = (TextView) parent.getChildAt(2).findViewById(R.id.setTempUnit);
	                		TextView txUnitThree = (TextView) parent.getChildAt(3).findViewById(R.id.setTempUnit);
	                		TextView txUnitFour = (TextView) parent.getChildAt(4).findViewById(R.id.setTempUnit);
	                		
	                		TextView txPoiZero = (TextView) parent.getChildAt(0).findViewById(R.id.setTempPoiText);
	                		TextView txPoiOne = (TextView) parent.getChildAt(1).findViewById(R.id.setTempPoiText);
	                		TextView txPoiTwo = (TextView) parent.getChildAt(2).findViewById(R.id.setTempPoiText);
	                		TextView txPoiThree = (TextView) parent.getChildAt(3).findViewById(R.id.setTempPoiText);
	                		TextView txPoiFour = (TextView) parent.getChildAt(4).findViewById(R.id.setTempPoiText);
	                		
	                		TextView unitText = (TextView) cachedView.findViewById(R.id.setTempUnit);
//	                		int iValue = Integer.valueOf(txZero.getText().toString());
//	                		if (iValue > index + minValue) {
//	                			setThirdText(textView, unitText);
//		                		setThirdText(txThree, txUnitThree);
//		                		setSecondText(txZero, txUnitZero, -8);
//		                		setSecondText(txTwo, txUnitTwo, 8);
//		                		setCenterText(txOne, txUnitOne);
//	                		} else {
//	                			setThirdText(textView, unitText);
//		                		setThirdText(txOne, txUnitOne);
//		                		setSecondText(txTwo, txUnitTwo, -8);
//		                		setSecondText(txFour, txUnitFour, 8);
//		                		setCenterText(txThree, txUnitThree);
//	                		}
	                		setText(txZero, txUnitZero, txPoiZero);
	                		setText(txOne, txUnitOne, txPoiOne);
	                		setText(txTwo, txUnitTwo, txPoiTwo);
	                		setText(txThree, txUnitThree, txPoiThree);
	                		setText(txFour, txUnitFour, txPoiFour);
	                		setText(textView, unitText, poiTextView);
	                	}
	                }
	    
	                if (itemResourceId == TEXT_VIEW_ITEM_RESOURCE) {
	                    configureTextView(textView);
	                }
	            }
	            
	            view = cachedView;
	        }
	    		    	
			return view;
		}

		@Override
		public CharSequence getItemText(int index) {
	        if (index >= 0 && index < getItemsCount()) {
//	            int value = minVal + index;
//	            if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
//	            	return String.valueOf(value);
//	            } else {
//	            	return String.valueOf((double) value);
//	            }
	            
	            return String.valueOf(minVal + index);
	        }
	        return null;
	    }
		
		@Override
	    public int getItemsCount() {
	        return maxVal - minVal + 1;
	    } 
		
		private View getView(int resource, ViewGroup parent) {
	        switch (resource) {
	        case NO_RESOURCE:
	            return null;
	        case TEXT_VIEW_ITEM_RESOURCE:
	            return new TextView(context);
	        default:
	            return inflater.inflate(resource, parent, false);    
	        }
	    }
		
		private TextView getTextView(View view, int textResource) {
	    	TextView text = null;
	    	try {
	            if (textResource == NO_RESOURCE && view instanceof TextView) {
	                text = (TextView) view;
	            } else if (textResource != NO_RESOURCE) {
	                text = (TextView) view.findViewById(textResource);
	            }
	        } catch (ClassCastException e) {
	            Log.e("AbstractWheelAdapter", "You must supply a resource ID for a TextView");
	            throw new IllegalStateException(
	                    "AbstractWheelAdapter requires the resource ID to be a TextView", e);
	        }
	        
	        return text;
	    }
		
		private void setCenterText(TextView textView, TextView unitText, TextView poiTextView, boolean cBl) {
			textView.setTextSize(33);
        	TextPaint tp = textView.getPaint(); 
        	tp.setFakeBoldText(true);
        	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
        		unitText.setText(
    					getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			unitText.setText(
    					getResources().getString(R.string.value_temp_fah));
    		}
//        	LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
//        			LinearLayout.LayoutParams.WRAP_CONTENT);  
//        	lp.setMargins(dip2px(context, 1), 0, 0, 0);  
//        	textView.setLayoutParams(lp); 
        	
        	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
        		poiTextView.setText("");
        		LinearLayout.LayoutParams ulp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
            			LinearLayout.LayoutParams.WRAP_CONTENT);
        		ulp.setMargins(0, Util.dip2px(context, 16), 0, 0); 
        		unitText.setLayoutParams(ulp); 
        		
            	LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
    			LinearLayout.LayoutParams.WRAP_CONTENT);  
		    	lp.setMargins(Util.dip2px(context, 4), 0, 0, 0);  
		    	textView.setLayoutParams(lp); 
        	} else {	
        		if (wheelInit) {
        			poiTextView.setTextSize(18);
                	TextPaint ptp = poiTextView.getPaint(); 
                	ptp.setFakeBoldText(true);
            		poiTextView.setText(".0");
            		LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
                			LinearLayout.LayoutParams.WRAP_CONTENT);  
            		plp.setMargins(0, Util.dip2px(context, -2), 0, 0); 
            		poiTextView.setLayoutParams(plp);
            	} else {
	        		String val = (String) textView.getText();
	        		if (cBl && CheckUtil.requireCheck(val) && Integer.valueOf(val) == maxVal - 1) {
	        			poiTextView.setTextSize(18);
	                	TextPaint ptp = poiTextView.getPaint(); 
	                	ptp.setFakeBoldText(true);
	            		poiTextView.setText(".0");
	            		LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	                			LinearLayout.LayoutParams.WRAP_CONTENT);  
	            		plp.setMargins(0, Util.dip2px(context, -7), 0, 0); 
	            		poiTextView.setLayoutParams(plp);
	            		
	            		LinearLayout.LayoutParams ulp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	                			LinearLayout.LayoutParams.WRAP_CONTENT);
	            		ulp.setMargins(0, Util.dip2px(context, -5), 0, 0); 
	            		unitText.setLayoutParams(ulp);
	        		} else {
	        			poiTextView.setTextSize(18);
	                	TextPaint ptp = poiTextView.getPaint(); 
	                	ptp.setFakeBoldText(true);
	            		poiTextView.setText(".0");
	            		LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	                			LinearLayout.LayoutParams.WRAP_CONTENT);  
	            		plp.setMargins(0, Util.dip2px(context, -2), 0, 0); 
	            		poiTextView.setLayoutParams(plp);
	        		}
            	}
        	}
		}
		
		private void setSecondText(TextView textView, TextView unitText, TextView poiTextView, int topMargin) {
			textView.setTextSize(23);
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
        			LinearLayout.LayoutParams.WRAP_CONTENT);  
        	lp.setMargins(0, Util.dip2px(context, topMargin), 0, 0);  
        	textView.setLayoutParams(lp);  
        	unitText.setText("");
        	
        	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
        		poiTextView.setText("");
        	} else {
        		poiTextView.setTextSize(14);
        		poiTextView.setText(".0");
//        		if (test) {
//	        		if (topMargin < 0) {
//	        			LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
//	                			LinearLayout.LayoutParams.WRAP_CONTENT);  
//	        			plp.setMargins(0, dip2px(context, -18), 0, 0);  
//	        			poiTextView.setLayoutParams(plp);
//	        		}
//        		} else {
        			if (topMargin < 0) {
	        			LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	                			LinearLayout.LayoutParams.WRAP_CONTENT);  
	        			plp.setMargins(0, Util.dip2px(context, -18), 0, 0);  
	        			poiTextView.setLayoutParams(plp);
	        		} else {
	        			LinearLayout.LayoutParams plp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	                			LinearLayout.LayoutParams.WRAP_CONTENT);  
	        			plp.setMargins(0, Util.dip2px(context, 0), 0, 0);  
	        			poiTextView.setLayoutParams(plp);
	        		}
//        		}
        	}
		}
		
		private void setThirdText(TextView textView, TextView unitText, TextView poiTextView, boolean cBl, boolean hBl) {
			textView.setTextSize(16);
        	unitText.setText("");
        	if (wheelInit) {
	        	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
	        		poiTextView.setText("");
	        	} else {
	        		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	            			LinearLayout.LayoutParams.WRAP_CONTENT);  
	            	lp.setMargins(0, Util.dip2px(context, -13), 0, 0); 
	        		poiTextView.setTextSize(10);
	        		poiTextView.setText(".0");
	        		poiTextView.setLayoutParams(lp);
	        	}
        	} else {
        		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
	        		poiTextView.setText("");
	        	} else {
//	        		String val = (String) textView.getText();

//	        		if (cBl && CheckUtil.requireCheck(val) && Integer.valueOf(val) == maxVal - 3) {
	        		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
	            			LinearLayout.LayoutParams.WRAP_CONTENT);  
	        		if (cBl) {
	        			lp.setMargins(0, Util.dip2px(context, -5), 0, 0); 
	        		} else if (hBl) {
	        			lp.setMargins(0, Util.dip2px(context, -18), 0, 0); 
	        		} else {
	        			lp.setMargins(0, Util.dip2px(context, -13), 0, 0); 
	        		}
	        		poiTextView.setTextSize(10);
	        		poiTextView.setText(".0");
	        		poiTextView.setLayoutParams(lp);
	        	}
        	}
		}
		
		private void setText(TextView textView, TextView unitText, TextView poiTextView) {
			String value = (String) textView.getText().toString();
			((LinearLayout.LayoutParams) textView.getLayoutParams()).setMargins(0, 0, 0, 0);
			TextPaint tp = textView.getPaint(); 
        	tp.setFakeBoldText(false);
			textView.setTextSize(20);
        	unitText.setText("");
        	poiTextView.setText("");
        	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
        		if (CheckUtil.requireCheck(value) && !value.contains(".0")) {
        			textView.setText(value + ".0");
        		}
        	}
//        	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
//        		poiTextView.setText("");
//        	} else {
//        		poiTextView.setTextSize(15);
//        		poiTextView.setText(".0");
//        		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 
//            			LinearLayout.LayoutParams.WRAP_CONTENT);  
//            	lp.setMargins(0, -10, 0, 0);
//            	poiTextView.setLayoutParams(lp);
//        	}
		}
		
	}
	
	public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
}