package cn.com.yuntian.wojia.fragment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.RangeSeekBar;
import cn.com.yuntian.wojia.logic.AmerTempBean;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class AmericanTempFragment extends Fragment {
	
	private TextView aTempDisTempText, aTempDisTempUnitText, aTempFanOn, amerBackButton, amerTitleText;
	
	private TextView aTempColdBtn, aTempAutoBtn, aTempHeatBtn, aTempMergenBtn, aTempOffBtn; 
	
//	private TextView amerRangeLeftText, amerRangeRightText;
	
	private TextView aTempCoolText, aTempCoolValue, aTempCoolUnit, aTempHumiValue, aTempFanAuto;
	
	private TextView aTempHeatText, aTempHeatValue, aTempHeatUnit, aTempDisHumiText, aTempFancirc;
	
	private ImageView aTempAnimBtn, aTempDisHumiMiddleBg, aTempDisTempImg;
	
	private SeekBar amerHeatSeekBar, amerCoolSeekBar, amerHumiSeekBar; 
	
	private SeekBar amerHumiSeekBarOffline, amerSeekBarOffline;
	
	private FrameLayout amerTitleBg, tempBg;
	
	private TextView aTempDisHumiBottomBg, aTempDisHumiTopBg;
	
	private LinearLayout aTempBottomBg, amerHumiBg, amerHumiBgOffline, amerLayoutView;
	
	private int maxCoolTemp = 37;
	
	private int minCoolTemp = 10;
	
	private int minHeatTemp = 4;
	
	private int maxHeatTemp = 32;
	
	private int minAutoTemp = 4;
	
	private int maxAutoTemp = 37;
	
	private int maxCoolTempFah = 99;
	
	private int minCoolTempFah = 50;
	
	private int minHeatTempFah = 40;
	
	private int maxHeatTempFah = 90;
	
	private int minAutoTempFah = 40;
	
	private int maxAutoTempFah = 99;
	
	int screenWidth;
	
	int screenHeight;
	
	private String mac;
	
	private String devType;
	
	private String devNm;
	
	private HailinDB db;
	
	private String status;
	
	private String fanMod;
	
	private double deadTemp;
	
	private static boolean pageIsUpd = false;
	
	private Bitmap mBitmap;
	
	private boolean isForeRun = true;
	
	private AmerTempBean amerTempBean;
	
	private AmerTempBean bakBean;
	
	private boolean animBl = true; 
	
	private BackgroundTask bTask;
	
	private Handler handler = new Handler();
	
	Map<String, String> webParam;
	
	private Animation operatingAnim;
	
	private LinearLayout amerRangeSeekBar, amerRangeSeekBarOffline;
	
	private RangeSeekBar<Integer> seekBar;
	
	private RangeSeekBar<Integer> seekBarOffline;
	
	private int autoHeatMinValue = 0;
	
	private int autoCoolMaxValue = 0;
	
	private int coolProgress = 0;
	
	private int heatProgress = 0;

	private String coolSaveTemp;
	
	private String heatSaveTemp;
	
	private String autoHeatTemp;
	
	private String autoCoolTemp;
	
	private boolean heatProBl = false;
	
	private boolean coolProBl = false;
	
	private boolean humiProBl = false;
	
	private int coolDefaultTemp = 29;
	
	private int heatDefaultTemp = 20;
	
	private int autoHeatDefaultTemp = 20;
	
	private int autoCoolDefaultTemp = 29;
	
	private int coolDefaultTempFah = 84;
	
	private int heatDefaultTempFah = 68;
	
	private int autoHeatDefaultTempFah = 68;
	
	private int autoCoolDefaultTempFah = 84;
	
	private boolean reLoadPageBl = true;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
//	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);	
		devType = bundle.getString(Constants.DEV_TYPE);	
		devNm = bundle.getString(Constants.DIS_DEV_NAME);
		
		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
			return inflater.inflate(R.layout.lake_temp_fragment, container, false);
		} else {
			return inflater.inflate(R.layout.american_temp_fragment, container, false);
		}
		
		
	}

    @Override
    public void onStart() {
        super.onStart();  
        
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        reLoadPageBl = true;
        db = new HailinDB(getActivity());
        DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity(); 

		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		maxCoolTemp = maxCoolTempFah;
    		minCoolTemp = minCoolTempFah;
    		maxHeatTemp = maxHeatTempFah;
    		minHeatTemp = minHeatTempFah;
    		maxAutoTemp = maxAutoTempFah;
    		minAutoTemp = minAutoTempFah;
    		coolDefaultTemp = coolDefaultTempFah;
    		heatDefaultTemp = heatDefaultTempFah;
    		autoCoolDefaultTemp = autoCoolDefaultTempFah;
    		autoHeatDefaultTemp = autoHeatDefaultTempFah;
    	} 
		
//		Bundle bundle = this.getArguments();
//		mac  = bundle.getString(Constants.MAC);	
//		devType = bundle.getString(Constants.DEV_TYPE);	
//		devNm = bundle.getString(Constants.DIS_DEV_NAME);
		
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_atemp)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;
		handler.post(runnable);	
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	reLoadPageBl = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void initItem() {
    	
        aTempDisTempText = (TextView) getView().findViewById(R.id.aTempDisTempText);
        aTempDisTempUnitText  = (TextView) getView().findViewById(R.id.aTempDisTempUnitText);
        aTempColdBtn = (TextView) getView().findViewById(R.id.aTempColdBtn);
        aTempAutoBtn = (TextView) getView().findViewById(R.id.aTempAutoBtn);
        aTempHeatBtn = (TextView) getView().findViewById(R.id.aTempHeatBtn);
        aTempCoolText = (TextView) getView().findViewById(R.id.aTempCoolText);
        aTempCoolValue = (TextView) getView().findViewById(R.id.aTempCoolValue);
        aTempCoolUnit = (TextView) getView().findViewById(R.id.aTempCoolUnit);
        aTempHeatText = (TextView) getView().findViewById(R.id.aTempHeatText);
        aTempHeatValue = (TextView) getView().findViewById(R.id.aTempHeatValue);
        aTempHeatUnit = (TextView) getView().findViewById(R.id.aTempHeatUnit);
        aTempMergenBtn = (TextView) getView().findViewById(R.id.aTempMergenBtn);
        aTempHumiValue = (TextView) getView().findViewById(R.id.aTempHumiValue);
        aTempDisHumiText = (TextView) getView().findViewById(R.id.aTempDisHumiText);
        aTempOffBtn = (TextView) getView().findViewById(R.id.aTempOffBtn);
        aTempFanOn = (TextView) getView().findViewById(R.id.aTempFanOn);
        aTempFanAuto = (TextView) getView().findViewById(R.id.aTempFanAuto);
        aTempFancirc = (TextView) getView().findViewById(R.id.aTempFancirc);
        amerBackButton = (TextView) getView().findViewById(R.id.amerBackButton);
        amerTitleText = (TextView) getView().findViewById(R.id.amerTitleText); 
//        amerRangeLeftText =  (TextView) getView().findViewById(R.id.amerRangeLeftText);
//        amerRangeRightText =  (TextView) getView().findViewById(R.id.amerRangeRightText);
        aTempAnimBtn = (ImageView) getView().findViewById(R.id.aTempAnimBtn);
        aTempDisHumiMiddleBg = (ImageView) getView().findViewById(R.id.aTempDisHumiMiddleBg); 
        aTempDisTempImg = (ImageView) getView().findViewById(R.id.aTempDisTempImg); 
        amerRangeSeekBar = (LinearLayout) getView().findViewById(R.id.amerRangeSeekBar);
        amerRangeSeekBarOffline = (LinearLayout) getView().findViewById(R.id.amerRangeSeekBarOffline);
        amerHeatSeekBar = (SeekBar) getView().findViewById(R.id.amerHeatSeekBar);
        amerCoolSeekBar = (SeekBar) getView().findViewById(R.id.amerCoolSeekBar);
        amerHumiSeekBar = (SeekBar) getView().findViewById(R.id.amerHumiSeekBar);
        amerSeekBarOffline = (SeekBar) getView().findViewById(R.id.amerSeekBarOffline);
        amerHumiSeekBarOffline = (SeekBar) getView().findViewById(R.id.amerHumiSeekBarOffline);
        amerTitleBg = (FrameLayout) getView().findViewById(R.id.amerTitleBg); 
        tempBg = (FrameLayout) getView().findViewById(R.id.tempBg); 
        aTempDisHumiBottomBg = (TextView) getView().findViewById(R.id.aTempDisHumiBottomBg); 
        aTempDisHumiTopBg = (TextView) getView().findViewById(R.id.aTempDisHumiTopBg);
        aTempBottomBg = (LinearLayout) getView().findViewById(R.id.aTempBottomBg);
        amerHumiBg = (LinearLayout) getView().findViewById(R.id.amerHumiBg);
        amerHumiBgOffline = (LinearLayout) getView().findViewById(R.id.amerHumiBgOffline);
        amerLayoutView =  (LinearLayout) getView().findViewById(R.id.amerLayoutView);
        
        amerTitleText.setText(devNm);
        
        operatingAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.amer_temp_anima);  
        LinearInterpolator lin = new LinearInterpolator();  
        operatingAnim.setInterpolator(lin);  
        
        seekBar = new RangeSeekBar<Integer>(
				0, 
				100, 
				getActivity(), 
				Color.parseColor("#B5D745"), 
				Color.parseColor("#4FB243"), 
				Color.parseColor("#DE6135"), 
				R.drawable.temp_btn, R.drawable.temp_btn,
				18,
				50);
        amerRangeSeekBar.addView(seekBar);
        
        seekBarOffline = new RangeSeekBar<Integer>(
				0, 
				100, 
				getActivity(), 
				Color.parseColor("#8E8E8E"), 
				Color.parseColor("#686868"), 
				Color.parseColor("#898989"), 
				R.drawable.temp_btn, R.drawable.temp_btn,
				18,
				50);
        amerRangeSeekBarOffline.addView(seekBarOffline);
		
            
//        if (screenHeight > 1000 ) {
//        	aTempDisTempText.setTextSize(45);
//        	aTempDisTempUnitText.setTextSize(35);
//        } else if (screenHeight > 900 ) {
//        	aTempDisTempText.setTextSize(35);
//        	aTempDisTempUnitText.setTextSize(25);
//        } else if (screenHeight > 800 ) {
//        	aTempDisTempText.setTextSize(30);
//        	aTempDisTempUnitText.setTextSize(20);
//        } else {
//        	aTempDisTempText.setTextSize(25);
//        	aTempDisTempUnitText.setTextSize(15);
//        }
        
        if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        	amerHumiSeekBar.setEnabled(false);
        	aTempFancirc.setEnabled(false);
        	aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        }
        
    }
    
    private void itemEvent() {
    	seekBarEvent();
    	fanModEvent();
    	statusEvent();
    	backEvent();
    }
    

	private void seekBarEvent() {
    	amerHeatSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
    		String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (heatProBl) {
					double temp = minHeatTemp + progress * (maxHeatTemp - minHeatTemp)/100;
					heatProgress = progress;
					setText = setTempValue(aTempHeatValue, temp, true);	
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				heatProBl = true;
				pageIsUpd = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				heatProBl = false;
				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
            		setText = Constants.FAH_START_KEY + setText;
        		} else { 
        			setText = Constants.CEN_START_KEY + setText;
        		}
				Map<String, String> param = new HashMap<String, String>();
				param.put(Constants.STATUS, status);
				param.put(Constants.TEMP_HEAT, setText);
				bakBean.setTempHeat(setText);
					
				RequestParams paramMap = new RequestParams();
				paramMap.put(getResources().getString(R.string.param_temp_heat), setText);
				paramMap.put(getResources().getString(R.string.param_status), status);
	    		updateWeb(paramMap, param);
			}
    		
    	});
    	
    	amerCoolSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
    		String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (coolProBl) {
					double temp = minCoolTemp + progress * (maxCoolTemp - minCoolTemp)/100;
					coolProgress = progress;
					setText = setTempValue(aTempCoolValue, temp, false);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				coolProBl = true;
				pageIsUpd = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				coolProBl = false;
				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
            		setText = Constants.FAH_START_KEY + setText;
        		} else { 
        			setText = Constants.CEN_START_KEY + setText;
        		}
				Map<String, String> param = new HashMap<String, String>();
				param.put(Constants.STATUS, getResources().getString(R.string.value_status_two));
				param.put(Constants.TEMP_COOL, setText);
				bakBean.setTempCool(setText);
					
				RequestParams paramMap = new RequestParams();
				paramMap.put(getResources().getString(R.string.param_temp_cool), setText);
				paramMap.put(getResources().getString(R.string.param_status), 
						getResources().getString(R.string.value_status_two));
	    		updateWeb(paramMap, param);
			}	
    	});
    	
    	seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
    		String setheatText = null;
    		String setcoolText = null;
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
            	pageIsUpd = true;
            	int deatTempValue = Integer.valueOf(Util.getValue(String.valueOf(deadTemp), 0)).intValue();
            	boolean deadBl = false;
            	if (autoHeatMinValue == minValue && autoCoolMaxValue != maxValue) {
            		if (maxValue < minValue + deadTemp*100/(maxAutoTemp - minAutoTemp)) {
            			maxValue = (int) (minValue + deadTemp*100/(maxAutoTemp - minAutoTemp));
            			seekBar.setSelectedMaxValue(maxValue);
            			deadBl = true;
            		}
            		int coolTemp = minAutoTemp + maxValue * (maxAutoTemp - minAutoTemp)/100;
            		int heatTemp = Integer.valueOf(Util.getValue(aTempHeatValue.getText().toString(), 0)).intValue();
            		if (deadBl) {
            			coolTemp = heatTemp + deatTempValue;
            			deadBl = false;
            		}
            		setcoolText = setTempValue(aTempCoolValue, coolTemp, false);
            		setheatText = null;
            		autoCoolMaxValue = maxValue;
            	} else if (autoCoolMaxValue == maxValue && autoHeatMinValue != minValue) {
            		if (minValue > maxValue - deadTemp*100/(maxAutoTemp - minAutoTemp)) {
            			minValue = (int) (maxValue - deadTemp*100/(maxAutoTemp - minAutoTemp));
            			seekBar.setSelectedMinValue(minValue);
            			deadBl = true;
            		}
            		int heatTemp = minAutoTemp + minValue * (maxAutoTemp - minAutoTemp)/100;
            		int coolTemp = Integer.valueOf(Util.getValue(aTempCoolValue.getText().toString(), 0)).intValue();
            		if (deadBl) {
            			heatTemp = coolTemp - deatTempValue;
            			deadBl = false;
            		}
            		setheatText = setTempValue(aTempHeatValue, heatTemp, true);
            		setcoolText = null;
            		autoHeatMinValue = minValue;
            	}
            }
             
            @Override
 			public void onStopTrackingTouch() {
            	if (setheatText == null) {
            		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	            		setcoolText = Constants.FAH_START_KEY + setcoolText;
	        		} else { 
	        			setcoolText = Constants.CEN_START_KEY + setcoolText;
	        		}
            		Map<String, String> param = new HashMap<String, String>();
					param.put(Constants.STATUS, getResources().getString(R.string.value_status_zero));
					param.put(Constants.TEMP_COOL, setcoolText);
					bakBean.setTempCool(setcoolText);
						
					RequestParams paramMap = new RequestParams();
					paramMap.put(getResources().getString(R.string.param_temp_cool), setcoolText);
					paramMap.put(getResources().getString(R.string.param_status), 
							getResources().getString(R.string.value_status_zero));
		    		updateWeb(paramMap, param);
            	} else if (setcoolText == null) {
            		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	            		setheatText = Constants.FAH_START_KEY + setheatText;
	        		} else { 
	        			setheatText = Constants.CEN_START_KEY + setheatText;
	        		}
					Map<String, String> param = new HashMap<String, String>();
					param.put(Constants.STATUS, getResources().getString(R.string.value_status_zero));
					param.put(Constants.TEMP_HEAT, setheatText);
					bakBean.setTempHeat(setheatText);
						
					RequestParams paramMap = new RequestParams();
					paramMap.put(getResources().getString(R.string.param_temp_heat), setheatText);
					paramMap.put(getResources().getString(R.string.param_status), 
							getResources().getString(R.string.value_status_zero));
		    		updateWeb(paramMap, param);
            	}
//            	if (setheatText != null && setcoolText != null) {
//	            	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
//	            		setheatText = Constants.FAH_START_KEY + setheatText;
//	            		setcoolText = Constants.FAH_START_KEY + setcoolText;
//	        		} else { 
//	        			setheatText = Constants.CEN_START_KEY + setheatText;
//	        			setcoolText = Constants.CEN_START_KEY + setcoolText;
//	        		}
//					Map<String, String> param = new HashMap<String, String>();
//					param.put(Constants.STATUS, getResources().getString(R.string.value_status_zero));
//					param.put(Constants.TEMP_COOL, setcoolText);
//					param.put(Constants.TEMP_HEAT, setheatText);
//					bakBean.setTempCool(setcoolText);
//					bakBean.setTempHeat(setheatText);
//						
//					RequestParams paramMap = new RequestParams();
//					paramMap.put(getResources().getString(R.string.param_temp_cool), setcoolText);
//					paramMap.put(getResources().getString(R.string.param_temp_heat), setheatText);
//					paramMap.put(getResources().getString(R.string.param_status), 
//							getResources().getString(R.string.value_status_zero));
//		    		updateWeb(paramMap, param);
//            	}
 			}   
    	 });
    	
    	amerHumiSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
    		String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (humiProBl) {
					aTempHumiValue.setText(progress + "%");	
					setText = String.valueOf(progress);
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				pageIsUpd = true;
				humiProBl = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				humiProBl = false;
				Map<String, String> param = new HashMap<String, String>();
	    		param.put(Constants.HUMI, setText);
	    		bakBean.setHumi(setText);
	    		
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_humi), setText);
	    		updateWeb(paramMap, param);
			}
    		
    	});
    }
    
    private void fanModEvent() {
    	
    	aTempFanOn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
	    		if (!getResources().getString(R.string.value_fan_mod_one).equals(fanMod)) {	
	    			aTempFanOn.setBackgroundResource(R.drawable.fan_on_sel);
		    		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto);
		    		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
		    		} else {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ);
		    		}
		    		
	    			pageIsUpd = true;
	    			Map<String, String> param = new HashMap<String, String>();
	    			fanMod = getResources().getString(R.string.value_fan_mod_one);
    	    		param.put(Constants.FAN_MOD, fanMod);
    	    		bakBean.setFanMod(fanMod);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), fanMod);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempFanAuto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {				
	    		if (!getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
	    			aTempFanOn.setBackgroundResource(R.drawable.fan_on);
		    		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto_sel);
		    		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
		    		} else {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ);
		    		}
		    		
	    			pageIsUpd = true;
	    			Map<String, String> param = new HashMap<String, String>();
	    			fanMod = getResources().getString(R.string.value_fan_mod_two);
    	    		param.put(Constants.FAN_MOD, fanMod);
    	    		bakBean.setFanMod(fanMod);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), fanMod);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempFancirc.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				
	    		if (!getResources().getString(R.string.value_fan_mod_zero).equals(fanMod)) {		
	    			aTempFanOn.setBackgroundResource(R.drawable.fan_on);
		    		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto);
		    		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
		    		} else {
		    			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_sel);
		    		}
		    		
	    			pageIsUpd = true;
	    			Map<String, String> param = new HashMap<String, String>();
	    			fanMod = getResources().getString(R.string.value_fan_mod_zero);
    	    		param.put(Constants.FAN_MOD, fanMod);
    	    		bakBean.setFanMod(fanMod);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), fanMod);
    	    		updateWeb(paramMap, param);
	    		}
			}
        });
    }
    
    private void statusEvent() {
    	aTempColdBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
	    		if (!getResources().getString(R.string.value_status_two).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_two);
	    			if (!animBl) {
	    				aTempAnimBtn.startAnimation(operatingAnim);
	    				animBl = true;
	    			}
	    			
	    			setStatus(getResources().getString(R.string.value_status_two),
	    					Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_two), 
	    					Constants.VALUE_ONLINE_ONE);
	    			
//	    			double coolTemp;
//    				if (aTempCoolValue.getText() == null) {
//    					coolTemp = maxCoolTemp;
//    				} else {
//    					coolTemp = Double.valueOf(aTempCoolValue.getText().toString());
//    				}
//    				
//    				setTempMove(0, coolTemp);
	    			
	    			String tempValue;
	    			if (coolProgress == 0) {
	    				setTempMove(0, coolDefaultTemp);
	    				setTempValue(aTempCoolValue, coolDefaultTemp, false);
	    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	            			tempValue = Constants.FAH_START_KEY + Util.getValue(String.valueOf(coolDefaultTemp), 0);
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + Util.getValue(String.valueOf(coolDefaultTemp), 1);
	            		}
	    			} else {
	    				amerCoolSeekBar.setProgress(coolProgress);
	    				aTempCoolValue.setText(coolSaveTemp);
	 
	    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	            			tempValue = Constants.FAH_START_KEY + coolSaveTemp;
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + coolSaveTemp;
	            		}
	    			}
	    			
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, getResources().getString(R.string.value_status_two));
    	    		bakBean.setStatus(getResources().getString(R.string.value_status_two));
    	    		bakBean.setTempCool(tempValue);
    	    		param.put(Constants.TEMP_COOL, tempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), 
    	    				getResources().getString(R.string.value_status_two));
    	    		paramMap.put(getResources().getString(R.string.param_temp_cool), tempValue);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempAutoBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_status_zero).equals(
	    				bakBean.getStatus())) {	    			
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_zero);
	    			if (!animBl) {
	    				aTempAnimBtn.startAnimation(operatingAnim);
	    				animBl = true;
	    			}
	    			
	    			setStatus(getResources().getString(R.string.value_status_zero), 
	    					Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_zero), 
	    					Constants.VALUE_ONLINE_ONE);

	    			String heatTempValue;
	    			String coolTempValue;
    				if (autoHeatMinValue != 0 && autoCoolMaxValue != 0) {
    					seekBar.setSelectedMaxValue(autoCoolMaxValue);
    					seekBar.setSelectedMinValue(autoHeatMinValue);
    					
    					aTempHeatValue.setText(autoHeatTemp);
    					aTempCoolValue.setText(autoCoolTemp);
	    				
	    				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    					heatTempValue = Constants.FAH_START_KEY + autoHeatTemp;
	    					coolTempValue = Constants.FAH_START_KEY + autoCoolTemp;
	            		} else {	                			
	            			heatTempValue = Constants.CEN_START_KEY + autoHeatTemp;
	    					coolTempValue = Constants.CEN_START_KEY + autoCoolTemp;
	            		}
    				} else {
        				setTempMove(autoHeatDefaultTemp, autoCoolDefaultTemp);
        				setTempValue(aTempCoolValue, autoCoolDefaultTemp, false);
        				setTempValue(aTempHeatValue, autoHeatDefaultTemp, true);
	    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    	    			heatTempValue = Constants.FAH_START_KEY + Util.getValue(String.valueOf(autoHeatDefaultTemp), 0);
	    	    			coolTempValue = Constants.FAH_START_KEY + Util.getValue(String.valueOf(autoCoolDefaultTemp), 0);
	            		} else {	                			
	            			heatTempValue = Constants.CEN_START_KEY + Util.getValue(String.valueOf(autoHeatDefaultTemp), 1);
	    	    			coolTempValue = Constants.CEN_START_KEY + Util.getValue(String.valueOf(autoCoolDefaultTemp), 1);
	            		}
    				}
    				
    				Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, status);
    	    		bakBean.setStatus(status);
    	    		bakBean.setTempCool(coolTempValue);
    	    		bakBean.setTempHeat(heatTempValue);
    	    		param.put(Constants.TEMP_COOL, coolTempValue);
    	    		param.put(Constants.TEMP_HEAT, heatTempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), status);
    	    		paramMap.put(getResources().getString(R.string.param_temp_cool), coolTempValue);
    	    		paramMap.put(getResources().getString(R.string.param_temp_heat), heatTempValue);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempHeatBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_status_one).equals(
	    				bakBean.getStatus())) {    			
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_one);
	    			if (!animBl) {
	    				aTempAnimBtn.startAnimation(operatingAnim);
	    				animBl = true;
	    			}

	    			setStatus(getResources().getString(R.string.value_status_one), 
	    					Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_one), 
	    					Constants.VALUE_ONLINE_ONE);
	    			
//	    			double heatTemp;
//    				if (aTempHeatValue.getText() == null) {
//    					heatTemp = minHeatTemp;
//    				} else {
//    					heatTemp = Double.valueOf(aTempHeatValue.getText().toString());
//    				}
//    				setTempMove(heatTemp, 0);
	    			String tempValue;
	    			if (heatProgress == 0) {
	    				setTempMove(heatDefaultTemp, 0);
	    				setTempValue(aTempHeatValue, heatDefaultTemp, true);

	    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    	    			tempValue = Constants.FAH_START_KEY + Util.getValue(String.valueOf(heatDefaultTemp), 0);
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + Util.getValue(String.valueOf(heatDefaultTemp), 1);;
	            		}
	    	    		
	    			} else {
	    				amerHeatSeekBar.setProgress(heatProgress);
	    				aTempHeatValue.setText(heatSaveTemp);
	    				
	    				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    	    			tempValue = Constants.FAH_START_KEY + heatSaveTemp;
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + heatSaveTemp;
	            		}
	    			}
	    			
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, getResources().getString(R.string.value_status_one));
    	    		bakBean.setStatus(getResources().getString(R.string.value_status_one));
    	    		bakBean.setTempHeat(tempValue);
    	    		param.put(Constants.TEMP_HEAT, tempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), 
    	    				getResources().getString(R.string.value_status_one));
    	    		paramMap.put(getResources().getString(R.string.param_temp_heat), tempValue);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempMergenBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {    		
	    		if (!getResources().getString(R.string.value_status_three).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_three);
	    			if (!animBl) {
	    				aTempAnimBtn.startAnimation(operatingAnim);
	    				animBl = true;
	    			}
	    			
	    			setStatus(getResources().getString(R.string.value_status_three), 
	    					Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_three), 
	    					Constants.VALUE_ONLINE_ONE);
	    			
//	    			double heatTemp;
//    				if (aTempHeatValue.getText() == null) {
//    					heatTemp = minHeatTemp;
//    				} else {
//    					heatTemp = Double.valueOf(aTempHeatValue.getText().toString());
//    				}
//    				setTempMove(heatTemp, 0);
	    			
	    			String tempValue;
	    			if (heatProgress == 0) {
	    				setTempMove(heatDefaultTemp, 0);
	    				setTempValue(aTempHeatValue, heatDefaultTemp, true);

	    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    	    			tempValue = Constants.FAH_START_KEY + Util.getValue(String.valueOf(heatDefaultTemp), 0);
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + Util.getValue(String.valueOf(heatDefaultTemp), 1);;
	            		}
	    			} else {
	    				amerHeatSeekBar.setProgress(heatProgress);
	    				aTempHeatValue.setText(heatSaveTemp);
	    				
	    				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
	    	    			tempValue = Constants.FAH_START_KEY + heatSaveTemp;
	            		} else {	                			
	            			tempValue = Constants.CEN_START_KEY + heatSaveTemp;
	            		}
	    			}
	    			
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, status);
    	    		bakBean.setStatus(status);
    	    		bakBean.setTempHeat(tempValue);
    	    		param.put(Constants.TEMP_HEAT, tempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), status);
    	    		paramMap.put(getResources().getString(R.string.param_temp_heat), tempValue);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	aTempOffBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_status_four).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_four);
	    			aTempAnimBtn.clearAnimation();
	    			animBl = false;
	    			
	    			setStatus(getResources().getString(R.string.value_status_four), 
	    					Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_four), 
	    					Constants.VALUE_ONLINE_ONE);
	    			
	    			
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS,getResources().getString(R.string.value_status_four));
    	    		bakBean.setStatus(getResources().getString(R.string.value_status_four));
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), 
    	    				getResources().getString(R.string.value_status_four));
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    }
    
    private void backEvent() {
    	
    	amerBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	amerLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    }
    
    private void setDataToPage() {
           
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	amerTempBean = db.getTAmerTemp().findOne(arg);
    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
    	
    	status = amerTempBean.getStatus();
    	fanMod = amerTempBean.getFanMod();
    	
    	String deadZoneTemp = amerTempBean.getDeadZoneTemp();
    	if (!CheckUtil.requireCheck(deadZoneTemp)) {
        	deadZoneTemp = "f2";
    	} 
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			if (deadZoneTemp.startsWith(Constants.FAH_START_KEY)) {
				deadTemp = Double.valueOf(deadZoneTemp.substring(1)).doubleValue();
			} else {
				deadTemp = Double.valueOf(Util.getTempDegree(deadZoneTemp)).doubleValue() 
    					- Double.valueOf(Util.getTempDegree("c0")).doubleValue();
			}
		} else {
			if (deadZoneTemp.startsWith(Constants.CEN_START_KEY)) {
				deadTemp = Double.valueOf(deadZoneTemp.substring(1)).doubleValue();
			} else {
				deadTemp = Double.valueOf(Util.getTempDegree(deadZoneTemp)).doubleValue() 
    					- Double.valueOf(Util.getTempDegree("f0")).doubleValue();
			}
		}
    	
    	aTempDisTempText.setText(Util.getTempDegree(amerTempBean.getDisTemp()));
    	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
			aTempDisTempUnitText.setText(
					getResources().getString(R.string.value_temp_cen));
			aTempCoolUnit.setText(
					getResources().getString(R.string.value_temp_cen));
			aTempHeatUnit.setText(
					getResources().getString(R.string.value_temp_cen));
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			aTempDisTempUnitText.setText(
					getResources().getString(R.string.value_temp_fah));
			aTempCoolUnit.setText(
					getResources().getString(R.string.value_temp_fah));
			aTempHeatUnit.setText(
					getResources().getString(R.string.value_temp_fah));
		}
    	
//    	if (bakBean == null || (bakBean.getHumi() != null 
//    			&& !bakBean.getHumi().equals(amerTempBean.getHumi()))) {
    		String humi = amerTempBean.getHumi();
	    	if (!CheckUtil.requireCheck(humi)) {
	    		humi = "0";
	    	}
	    	aTempHumiValue.setText(humi + "%");
	    	amerHumiSeekBar.setProgress(Integer.valueOf(humi).intValue());
	    	amerHumiSeekBarOffline.setProgress(Integer.valueOf(humi).intValue());
//    	}

    	
    	String disHumi =  amerTempBean.getDisHumi();
    	if (!CheckUtil.requireCheck(disHumi)) {
    		disHumi = "0";
    	}
    	aTempDisHumiText.setText(disHumi + "%");
    	
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		if (getResources().getString(R.string.value_fan_mod_zero).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_sel);
        		}
        	} else if (getResources().getString(R.string.value_fan_mod_one).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on_sel);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ);
        		}
        	} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto_sel);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ);
        		}
        	}
    	} else {
    		if (getResources().getString(R.string.value_fan_mod_zero).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on_offline);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto_offline);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_sel_offline);
        		}
        	} else if (getResources().getString(R.string.value_fan_mod_one).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on_sel_offline);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto_offline);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_offline);
        		}
        	} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
        		aTempFanOn.setBackgroundResource(R.drawable.fan_on_offline);
        		aTempFanAuto.setBackgroundResource(R.drawable.fan_auto_sel_offline);
        		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_disable);
        		} else {
        			aTempFancirc.setBackgroundResource(R.drawable.fan_circ_offline);
        		}
        	}
    	}

        double coolTemp;
        if (!CheckUtil.requireCheck(amerTempBean.getTempCool())) {
        	coolTemp = (double) maxCoolTemp;
    	} else {
    		coolTemp = Double.valueOf(Util.getTempDegree(amerTempBean.getTempCool())).doubleValue();
    		if (coolTemp > maxCoolTemp) {
    			coolTemp = maxCoolTemp;
    		} else if (coolTemp < minCoolTemp) {
    			coolTemp = minCoolTemp;
    		}
    	}
        
        double heatTemp;
        if (!CheckUtil.requireCheck(amerTempBean.getTempHeat())) {
        	heatTemp  = (double) minHeatTemp;;
    	} else {
    		heatTemp =  Double.valueOf(Util.getTempDegree(amerTempBean.getTempHeat())).doubleValue();
    		if (heatTemp > maxHeatTemp) {
    			heatTemp = maxHeatTemp;
    		} else if (heatTemp < minHeatTemp) {
    			heatTemp = minHeatTemp;
    		}
    	}  
        
    	setTempValue(aTempCoolValue, coolTemp, false);
    	setTempValue(aTempHeatValue, heatTemp, true);
       	
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		
    		if (bakBean == null || !animBl) {
		        aTempAnimBtn.startAnimation(operatingAnim);  
		        animBl = true;
			} 
    		
			setVisible(status, online);
			
    		if (coolTemp < heatTemp) {
    			coolTemp = heatTemp + Double.valueOf(Util.getValue(String.valueOf(deadTemp), 0)).doubleValue();
    			setTempValue(aTempCoolValue, coolTemp, false);
    		}
			
//    		if (bakBean == null || (bakBean.getTempCool() != null 
//        			&& !bakBean.getTempCool().equals(amerTempBean.getTempCool()))
//        			|| (bakBean.getTempHeat() != null 
//    	        			&& !bakBean.getTempHeat().equals(amerTempBean.getTempHeat()))
//    	        			|| (!bakBean.getStatus().equals(status))) {
    			
        		setTempMove(heatTemp, coolTemp);
//    		}
    		
    		setStatus(status, online);
    		
    	} else if ((getResources().getString(R.string.value_status_one).equals(status)) 
    			|| (getResources().getString(R.string.value_status_three).equals(status))) {
    		
    		if (bakBean == null || !animBl) {
		        aTempAnimBtn.startAnimation(operatingAnim);  
		        animBl = true;
			} 
    		
			setVisible(status, online);
    		
//			if (bakBean == null || (bakBean.getTempHeat() != null 
//        			&& !bakBean.getTempHeat().equals(amerTempBean.getTempHeat()))
//        			|| (!bakBean.getStatus().equals(status))) {  
				
				setTempMove(heatTemp, coolTemp);	
//			}
    		setStatus(status, online);

    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		
    		if (bakBean == null || !animBl) {
		        aTempAnimBtn.startAnimation(operatingAnim);  
		        animBl = true;
			} 
    		
			setVisible(status, online);
    		
//			if (bakBean == null || (bakBean.getTempCool() != null 
//        			&& !bakBean.getTempCool().equals(amerTempBean.getTempCool()))
//        			|| (!bakBean.getStatus().equals(status))) {
				
				setTempMove(heatTemp, coolTemp);
//			}
    		setStatus(status, online);

    	} else if (getResources().getString(R.string.value_status_four).equals(status)) {
    		if (bakBean != null && animBl) {
		        aTempAnimBtn.clearAnimation();  
			} 
    		animBl = false;
			setVisible(status, online);
			setStatus(status, online);
    	}
    	
    	if (!Constants.VALUE_ONLINE_ONE.equals(online) && animBl) {
    		aTempAnimBtn.clearAnimation();  
    		animBl = false;
    	}
    	
    	int angle = (Integer.valueOf(disHumi) - 100)*360/100;
    	int drawable = 0;
		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			drawable = R.drawable.humi_round_middle;
    	} else {
    		drawable = R.drawable.humi_round_middle_offline;
    	}
    	mBitmap = BitmapFactory.decodeResource(
    			getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true); 
        Canvas canvas = new Canvas(mBitmap);
        Paint paint = new Paint();  
        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        canvas.drawArc(rectf, 270, angle, true, paint);
        aTempDisHumiMiddleBg.setImageBitmap(mBitmap);	
        
        bakBean = (AmerTempBean) amerTempBean.clone();
    }
    
    private String setTempValue(TextView view, double value, boolean bl) {
    	String ret;
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		ret = Util.getValue(String.valueOf(value), 0); 		
		} else {
			ret = String.valueOf(new BigDecimal(value).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue());
//			ret = Util.getValue(String.valueOf(value), 1);
		}
    	
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		if (bl) {
    			autoHeatTemp = ret;
    		} else {
    			autoCoolTemp = ret;
    		}
    	} else  {
    		if (bl) {
    			heatSaveTemp = ret;
    		} else {
    			coolSaveTemp = ret;
    		}
    	} 
    	view.setText(ret);
    	return ret;
     }
    
	private void setTempMove(double heatTemp, double coolTemp) {
    	if (getResources().getString(R.string.value_status_zero).equals(status)) {
    		autoHeatMinValue = (int) (heatTemp-minAutoTemp) * 100/(maxAutoTemp - minAutoTemp);
    		autoCoolMaxValue = (int) (coolTemp-minAutoTemp) * 100/(maxAutoTemp - minAutoTemp);
    		seekBar.setSelectedMinValue(autoHeatMinValue);
    		seekBar.setSelectedMaxValue(autoCoolMaxValue);
    		seekBarOffline.setSelectedMinValue(autoHeatMinValue);
    		seekBarOffline.setSelectedMaxValue(autoCoolMaxValue);
    	} else if (getResources().getString(R.string.value_status_one).equals(status) || 
    			getResources().getString(R.string.value_status_three).equals(status)) {
    		heatProgress = (int) (heatTemp-minHeatTemp) * 100/(maxHeatTemp - minHeatTemp);
    		amerHeatSeekBar.setProgress(heatProgress);
    		amerSeekBarOffline.setProgress(heatProgress);
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
    		coolProgress = (int) (coolTemp-minCoolTemp) * 100/(maxCoolTemp - minCoolTemp);   		
    		amerCoolSeekBar.setProgress(coolProgress);
    		amerSeekBarOffline.setProgress(coolProgress);
    	}
    }
    
    private void setVisible (String statusValue, String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
//    	if (bakBean == null || (bakBean.getStatus() != null 
//    			&& !bakBean.getStatus().equals(statusValue))) {
    		amerSeekBarOffline.setVisibility(View.INVISIBLE);
    		amerRangeSeekBarOffline.setVisibility(View.INVISIBLE);
    		if (getResources().getString(R.string.value_status_zero).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.VISIBLE);
        		aTempCoolValue.setVisibility(View.VISIBLE);
        		aTempCoolUnit.setVisibility(View.VISIBLE);
        		aTempHeatText.setVisibility(View.VISIBLE);
        		aTempHeatValue.setVisibility(View.VISIBLE);
        		aTempHeatUnit.setVisibility(View.VISIBLE);
//        		amerRangeLeftText.setVisibility(View.VISIBLE);
//        		amerRangeRightText.setVisibility(View.VISIBLE);
        		amerRangeSeekBar.setVisibility(View.VISIBLE);
        		amerHeatSeekBar.setVisibility(View.INVISIBLE);
        		amerCoolSeekBar.setVisibility(View.INVISIBLE);
        	} else if ((getResources().getString(R.string.value_status_one).equals(statusValue)) 
        			|| (getResources().getString(R.string.value_status_three).equals(statusValue))) {
        		aTempCoolText.setVisibility(View.INVISIBLE);
        		aTempCoolValue.setVisibility(View.INVISIBLE);
        		aTempCoolUnit.setVisibility(View.INVISIBLE);
        		aTempHeatText.setVisibility(View.VISIBLE);
        		aTempHeatValue.setVisibility(View.VISIBLE);
        		aTempHeatUnit.setVisibility(View.VISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		amerRangeSeekBar.setVisibility(View.INVISIBLE);
        		amerHeatSeekBar.setVisibility(View.VISIBLE);
        		amerCoolSeekBar.setVisibility(View.INVISIBLE);
        	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.VISIBLE);
        		aTempCoolValue.setVisibility(View.VISIBLE);
        		aTempCoolUnit.setVisibility(View.VISIBLE);
        		aTempHeatText.setVisibility(View.INVISIBLE);
        		aTempHeatValue.setVisibility(View.INVISIBLE);
        		aTempHeatUnit.setVisibility(View.INVISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		amerRangeSeekBar.setVisibility(View.INVISIBLE);
        		amerHeatSeekBar.setVisibility(View.INVISIBLE);
        		amerCoolSeekBar.setVisibility(View.VISIBLE);
        	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.INVISIBLE);
        		aTempCoolValue.setVisibility(View.INVISIBLE);
        		aTempCoolUnit.setVisibility(View.INVISIBLE);
        		aTempHeatText.setVisibility(View.INVISIBLE);
        		aTempHeatValue.setVisibility(View.INVISIBLE);
        		aTempHeatUnit.setVisibility(View.INVISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		amerRangeSeekBar.setVisibility(View.INVISIBLE);
        		amerHeatSeekBar.setVisibility(View.INVISIBLE);
        		amerCoolSeekBar.setVisibility(View.INVISIBLE);
        	}
//    	}
    	} else {
    		amerRangeSeekBar.setVisibility(View.INVISIBLE);
    		amerHeatSeekBar.setVisibility(View.INVISIBLE);
    		amerCoolSeekBar.setVisibility(View.INVISIBLE);
    		if (getResources().getString(R.string.value_status_zero).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.VISIBLE);
        		aTempCoolValue.setVisibility(View.VISIBLE);
        		aTempCoolUnit.setVisibility(View.VISIBLE);
        		aTempHeatText.setVisibility(View.VISIBLE);
        		aTempHeatValue.setVisibility(View.VISIBLE);
        		aTempHeatUnit.setVisibility(View.VISIBLE);
//        		amerRangeLeftText.setVisibility(View.VISIBLE);
//        		amerRangeRightText.setVisibility(View.VISIBLE);
        		amerSeekBarOffline.setVisibility(View.INVISIBLE);
        		amerRangeSeekBarOffline.setVisibility(View.VISIBLE);
        		
        	} else if ((getResources().getString(R.string.value_status_one).equals(statusValue)) 
        			|| (getResources().getString(R.string.value_status_three).equals(statusValue))) {
        		aTempCoolText.setVisibility(View.INVISIBLE);
        		aTempCoolValue.setVisibility(View.INVISIBLE);
        		aTempCoolUnit.setVisibility(View.INVISIBLE);
        		aTempHeatText.setVisibility(View.VISIBLE);
        		aTempHeatValue.setVisibility(View.VISIBLE);
        		aTempHeatUnit.setVisibility(View.VISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		
        		amerSeekBarOffline.setVisibility(View.VISIBLE);
        		amerRangeSeekBarOffline.setVisibility(View.INVISIBLE);
        	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.VISIBLE);
        		aTempCoolValue.setVisibility(View.VISIBLE);
        		aTempCoolUnit.setVisibility(View.VISIBLE);
        		aTempHeatText.setVisibility(View.INVISIBLE);
        		aTempHeatValue.setVisibility(View.INVISIBLE);
        		aTempHeatUnit.setVisibility(View.INVISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		
        		amerSeekBarOffline.setVisibility(View.VISIBLE);
        		amerRangeSeekBarOffline.setVisibility(View.INVISIBLE);
        	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
        		aTempCoolText.setVisibility(View.INVISIBLE);
        		aTempCoolValue.setVisibility(View.INVISIBLE);
        		aTempCoolUnit.setVisibility(View.INVISIBLE);
        		aTempHeatText.setVisibility(View.INVISIBLE);
        		aTempHeatValue.setVisibility(View.INVISIBLE);
        		aTempHeatUnit.setVisibility(View.INVISIBLE);
//        		amerRangeLeftText.setVisibility(View.INVISIBLE);
//        		amerRangeRightText.setVisibility(View.INVISIBLE);
        		
        		amerSeekBarOffline.setVisibility(View.INVISIBLE);
        		amerRangeSeekBarOffline.setVisibility(View.INVISIBLE);
        	}
    	}
    	
    }
    
    private void setStatus(String statusValue, String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
//	    	if (bakBean == null || (bakBean.getStatus() != null 
//	    			&& !bakBean.getStatus().equals(statusValue))) {
		    	if (getResources().getString(R.string.value_status_zero).equals(statusValue)) {
		    		if (screenWidth >= 620) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_sel);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off);
		    		} else if (screenWidth >= 450 ) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_sel90);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90);
			        } else {
			        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_sel80);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80);
			        }
		    	} else if (getResources().getString(R.string.value_status_one).equals(statusValue)) {
		    		if (screenWidth >= 620) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_sel);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off);
		    		} else if (screenWidth >= 450 ) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_sel90);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90);
			        } else {
			        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_sel80);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80);
			        }	
		    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
		    		if (screenWidth >= 620) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_sel);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off);
		    		} else if (screenWidth >= 450 ) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_sel90);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90);
			        } else {
			        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_sel80);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80);
			        }		
		    	} else if (getResources().getString(R.string.value_status_three).equals(statusValue)) {
		    		if (screenWidth >= 620) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_sel);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off);
		    		} else if (screenWidth >= 450 ) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_sel90);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90);
			        } else {
			        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_sel80);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80);
			        }	
		    	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
		    		if (screenWidth >= 620) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_sel);
		    		} else if (screenWidth >= 450 ) {
		    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_sel90);
			        } else {
			        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80);
			    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80);
			    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80);
			    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80);
			    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_sel80);
			        }	
		    	}
//	    	}
    	} else {
    		if (getResources().getString(R.string.value_status_zero).equals(statusValue)) {
	    		if (screenWidth >= 620) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_sel_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_offline);
	    		} else if (screenWidth >= 450 ) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90_sel_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90_offline);
		        } else {
		        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80_sel_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80_offline);
		        }
	    	} else if (getResources().getString(R.string.value_status_one).equals(statusValue)) {
	    		if (screenWidth >= 620) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_sel_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_offline);
	    		} else if (screenWidth >= 450 ) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90_sel_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90_offline);
		        } else {
		        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80_sel_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80_offline);
		        }	
	    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
	    		if (screenWidth >= 620) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_sel_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_offline);
	    		} else if (screenWidth >= 450 ) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90_sel_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90_offline);
		        } else {
		        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80_sel_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80_offline);
		        }		
	    	} else if (getResources().getString(R.string.value_status_three).equals(statusValue)) {
	    		if (screenWidth >= 620) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_sel_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_offline);
	    		} else if (screenWidth >= 450 ) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90_sel_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90_offline);
		        } else {
		        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80_sel_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80_offline);
		        }	
	    	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
	    		if (screenWidth >= 620) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off_sel_offline);
	    		} else if (screenWidth >= 450 ) {
	    			aTempColdBtn.setBackgroundResource(R.drawable.btn_cold90_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto90_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat90_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen90_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off90_sel_offline);
		        } else {
		        	aTempColdBtn.setBackgroundResource(R.drawable.btn_cold80_offline);
		    		aTempAutoBtn.setBackgroundResource(R.drawable.btn_auto80_offline);
		    		aTempHeatBtn.setBackgroundResource(R.drawable.btn_heat80_offline);
		    		aTempMergenBtn.setBackgroundResource(R.drawable.btn_mergen80_offline);
		    		aTempOffBtn.setBackgroundResource(R.drawable.btn_off80_sel_offline);
		        }	
	    	}
    	}
    }
    
    private void updateDb(Map<String, String> param) {
    	param.put(Constants.MAC, mac);
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	db.getTAmerTemp().update(param);
    }
    
    private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url =  getResources().getText(R.string.url_upd_device).toString();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
    	webParam = param;
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) { 
            	updateDb(webParam);
            	pageIsUpd = false;
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (reLoadPageBl) {
            		setDataToPage();
            	}
            	pageIsUpd = false;
            }  
        });
    }
    
    public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
    
    private void setOnline(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		if (getResources().getString(R.string.value_deviceid_atemp).equals(devType)) {
            	amerHumiSeekBar.setEnabled(false);
            	aTempFancirc.setEnabled(false);
            }
    		amerTitleBg.setBackgroundResource(R.drawable.pm25_title);
    		amerBackButton.setBackgroundResource(R.drawable.back_arrow);
    		tempBg.setBackgroundResource(R.drawable.temp_bg);
    		aTempDisHumiBottomBg.setBackgroundResource(R.drawable.humi_round_bottom);
    		aTempDisHumiTopBg.setBackgroundResource(R.drawable.humi_round_top);
    		aTempBottomBg.setBackgroundResource(R.drawable.amer_temp_bottom_bg);
    		aTempAnimBtn.setBackgroundResource(R.drawable.green_wind_sel);
    		aTempDisTempImg.setBackgroundResource(R.drawable.list_temp);
    		amerHumiBg.setVisibility(View.VISIBLE);
    		amerHeatSeekBar.setVisibility(View.VISIBLE);
    		amerCoolSeekBar.setVisibility(View.VISIBLE);
    		amerHumiBgOffline.setVisibility(View.INVISIBLE);
    		amerSeekBarOffline.setVisibility(View.INVISIBLE);
//    		greenSeekBarOffline.setVisibility(View.INVISIBLE);
//    		greenSeekBar.setVisibility(View.VISIBLE);
    		amerHumiSeekBarOffline.setEnabled(false);
    		amerSeekBarOffline.setEnabled(false);
    		seekBarOffline.setEnabled(false);
    	} else {
    		setDisEnable();
    		amerTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
    		amerBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		tempBg.setBackgroundResource(R.drawable.temp_bg_offline);
    		aTempDisHumiBottomBg.setBackgroundResource(R.drawable.humi_round_bottom_offline);
    		aTempDisHumiTopBg.setBackgroundResource(R.drawable.humi_round_top_offline);
    		aTempBottomBg.setBackgroundResource(R.drawable.amer_temp_bottom_bg_offline);
    		aTempAnimBtn.setBackgroundResource(R.drawable.green_wind_offline);
    		aTempDisTempImg.setBackgroundResource(R.drawable.list_temp_offline);
    		amerHumiBg.setVisibility(View.INVISIBLE);
    		amerHeatSeekBar.setVisibility(View.INVISIBLE);
    		amerCoolSeekBar.setVisibility(View.INVISIBLE);
    		amerHumiBgOffline.setVisibility(View.VISIBLE);
    		amerSeekBarOffline.setVisibility(View.VISIBLE);
//    		greenSeekBarOffline.setVisibility(View.VISIBLE);
//    		greenSeekBar.setVisibility(View.INVISIBLE);
    	}
    }
    
    private void setEnable() {
    	aTempColdBtn.setEnabled(true);
    	aTempAutoBtn.setEnabled(true);
    	aTempHeatBtn.setEnabled(true);
    	aTempMergenBtn.setEnabled(true);
    	aTempOffBtn.setEnabled(true);
    	aTempFanOn.setEnabled(true);
    	aTempFanAuto.setEnabled(true);
    	aTempFancirc.setEnabled(true);
    	amerHumiSeekBar.setEnabled(true);
    	amerHeatSeekBar.setEnabled(true);
    	amerCoolSeekBar.setEnabled(true);
    	seekBar.setEnabled(true);
    }

	private void setDisEnable() {
		aTempColdBtn.setEnabled(false);
    	aTempAutoBtn.setEnabled(false);
    	aTempHeatBtn.setEnabled(false);
    	aTempMergenBtn.setEnabled(false);
    	aTempOffBtn.setEnabled(false);
    	aTempFanOn.setEnabled(false);
    	aTempFanAuto.setEnabled(false);
    	aTempFancirc.setEnabled(false);
    	amerHumiSeekBar.setEnabled(false);
    	amerHeatSeekBar.setEnabled(false);
    	amerCoolSeekBar.setEnabled(false);
    	amerHumiSeekBarOffline.setEnabled(false);
    	amerSeekBarOffline.setEnabled(false);
    	seekBar.setEnabled(false);
    	seekBarOffline.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
}