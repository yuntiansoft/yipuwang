package cn.com.yuntian.wojia.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.Co2VocBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;


/**
 * Co2VocFragment
 * 
 * @author chenwh
 *
 */
public class Co2VocFragment extends Fragment {
	
	private TextView co2VocBackButton, co2VocTitleText, co2VocValue, co2VocTips;
	
	private TextView vocTipTextLeft, vocTipTextMiddle, vocTipTextRight;
	
	private ImageView co2VocColCirBg, co2VocValImg;
	
	private LinearLayout co2VocLayoutView, co2VocPicDisplay;
	
	private FrameLayout co2VocTitleBg;
	
	private HailinDB db;
	
	private String mac;
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private Bitmap mBitmap;
	
	private int maxCo2Value = 2000;
	
	private int minCo2Value = 0;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
//	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private int co2 = 0;
	
	private int voc = 0;
	
	int screenWidth = 0;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.co2_voc_fragment, container, false);
	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
        
        db = new HailinDB(getActivity());
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();  
        
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		String devNm = bundle.getString(Constants.DIS_DEV_NAME);
		
		initItem();
		setDataToPage();
        setLayoutEvent();
        
        co2VocTitleText.setText(devNm);
        
		bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_co2_voc)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
            	handler.postDelayed(runnable, 5000);
        	}
        };
		isForeRun = true;
		handler.post(runnable);	
    }
    
    private void initItem() {
    	 co2VocBackButton = (TextView) getView().findViewById(R.id.co2VocBackButton);
         co2VocTitleText = (TextView) getView().findViewById(R.id.co2VocTitleText);
         co2VocValue = (TextView) getView().findViewById(R.id.co2VocValue);
         co2VocTips =  (TextView) getView().findViewById(R.id.co2VocTips);
         vocTipTextLeft =  (TextView) getView().findViewById(R.id.vocTipTextLeft);
         vocTipTextMiddle =  (TextView) getView().findViewById(R.id.vocTipTextMiddle);
         vocTipTextRight =  (TextView) getView().findViewById(R.id.vocTipTextRight);
         co2VocColCirBg = (ImageView) getView().findViewById(R.id.co2VocColCirBg);
         co2VocValImg = (ImageView) getView().findViewById(R.id.co2VocValImg);
         co2VocLayoutView = (LinearLayout) getView().findViewById(R.id.co2VocLayoutView);
         co2VocPicDisplay = (LinearLayout) getView().findViewById(R.id.co2VocPicDisplay);
         co2VocTitleBg = (FrameLayout) getView().findViewById(R.id.co2VocTitleBg);
    }
    
    private void setLayoutEvent() {
    	
    	co2VocBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	co2VocLayoutView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});		
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
        
    private void setDataToPage() {
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	Co2VocBean Co2VocBean = db.getTCo2Voc().findOne(arg);
    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
    	
    	if (Co2VocBean != null) {   				
    		if (CheckUtil.requireCheck(Co2VocBean.getDisCo2())) {
    			co2 = Integer.valueOf(Co2VocBean.getDisCo2()).intValue();
    		}
    		if (CheckUtil.requireCheck(Co2VocBean.getDisVoc())) {
    			voc = Integer.valueOf(Co2VocBean.getDisVoc()).intValue();
    		}
    		co2VocValue.setText(String.valueOf(co2));

    		setCo2Tips();
    		setVocPic(online);

    		int tempCo2Value = co2;
        	if (co2 > maxCo2Value) {
        		tempCo2Value = maxCo2Value;
        	}
        	int angle = (tempCo2Value - maxCo2Value)*360/(maxCo2Value-minCo2Value);
    		int drawable = 0;
    		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    			drawable = R.drawable.co2_voc_cir_top;
        	} else {
        		drawable = R.drawable.co2_voc_cir_top_offline;
        	}	
			mBitmap = BitmapFactory.decodeResource(
					getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true); 
	        Canvas canvas = new Canvas(mBitmap);
	        Paint paint = new Paint();  
	        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
	        paint.setAntiAlias(true);
	        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
	        canvas.drawArc(rectf, 270, angle, true, paint);
	        co2VocColCirBg.setImageBitmap(mBitmap);	
    	} 
    }
    
    private void setOnline(String online) {
    	co2VocValue.setText("0");
        co2VocTips.setText(getResources().getString(R.string.lang_txt_voc_error));
        vocTipTextLeft.setVisibility(View.INVISIBLE);
        vocTipTextMiddle.setVisibility(View.VISIBLE);
        vocTipTextRight.setVisibility(View.INVISIBLE);
        vocTipTextMiddle.setText(getResources().getString(R.string.lang_txt_voc_error));
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
            co2VocLayoutView.setBackgroundResource(R.drawable.co2_voc_bg);
            co2VocBackButton.setBackgroundResource(R.drawable.back_arrow);
            co2VocTitleBg.setBackgroundResource(R.drawable.pm25_title);
            co2VocPicDisplay.setBackgroundResource(R.drawable.co2_bla_cir_bg);
            co2VocValImg.setBackgroundResource(R.drawable.co2_voc_00);
    	} else {
    		co2VocLayoutView.setBackgroundResource(R.drawable.co2_voc_bg_offline);
            co2VocBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
            co2VocTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
            co2VocPicDisplay.setBackgroundResource(R.drawable.co2_bla_cir_bg_offline);
            co2VocValImg.setBackgroundResource(R.drawable.co2_voc_00_offline);
    	}
    }
    
    private void setCo2Tips() {
    	co2VocTips.setTextSize(20);
    	if (co2 == 0) {
    		co2VocTips.setText(R.string.lang_txt_voc_error);
    	} else if (co2 < 450) {
    		co2VocTips.setText(R.string.lang_txt_co2_one);
    		if (Constants.LANG_DEFALUT_ENG.equals(getResources().getString(R.string.lang_temp_default))) {
    			co2VocTips.setTextSize(15);
    		}
    	} else if (co2 < 1000) {
    		co2VocTips.setText(R.string.lang_txt_co2_two);
    	} else { 
    		co2VocTips.setText(R.string.lang_txt_co2_three);
    	}
    }
    
    private void setVocPic(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	switch (voc) {
		        case 1:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_01);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		        	break;   
		        case 2:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_02);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		        	break;   
		        case 3:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_03);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		            break;  
		        case 4:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_04);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break; 
		        case 5:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_05);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break;  
		        case 6:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_06);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break; 
		        case 7:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_07);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        case 8:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_08);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        case 9:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_09);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        default: 
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_00);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_error);
		            break;  
	    	} 
    	} else {
    		switch (voc) {
	    		case 1:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_01_offline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		        	break;   
		        case 2:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_02_offline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		        	break;   
		        case 3:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_03_offline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		            break;  
		        case 4:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_04_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break; 
		        case 5:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_05_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break;  
		        case 6:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_06_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_Exc);
		            break; 
		        case 7:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_07_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        case 8:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_08_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        case 9:
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_09_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.INVISIBLE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		            break;
		        default: 
		        	co2VocValImg.setBackgroundResource(R.drawable.co2_voc_00_offline);
		    		vocTipTextLeft.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.INVISIBLE);
		    		vocTipTextMiddle.setText(R.string.lang_txt_voc_error);
		            break;  
	    	}
    	}
    }
    
    private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
}