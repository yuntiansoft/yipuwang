package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
//import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;


/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class DeviceRenameFragment extends Fragment {

	private EditText deviceName;
	
	private TextView messageText, devRenmBackButton, deviceIdText;
	
	private Button btnConfirm;
	
	private HailinDB db;
	
	private String mac;
	
	private LinearLayout devRenmLayoutView;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.device_rename, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop(); 
        
        deviceName = (EditText) getView().findViewById(R.id.deviceRenameText);
        btnConfirm = (Button) getView().findViewById(R.id.devRenBtnConfirm);
        messageText = (TextView) getView().findViewById(R.id.devRenMessageText);
        devRenmBackButton = (TextView) getView().findViewById(R.id.devRenmBackButton);
        devRenmLayoutView = (LinearLayout) getView().findViewById(R.id.devRenmLayoutView);
        deviceIdText = (TextView) getView().findViewById(R.id.deviceIdText);
        
        db = new HailinDB(getActivity());
        Bundle args = this.getArguments();	
    	mac = args.getString(Constants.MAC);
    	deviceIdText.setText(mac);
    	deviceName.setText(args.getString(Constants.DIS_DEV_NAME));
    	
        btnConfirm.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
//				if (requireCheck()) {
					messageText.setText("");
					// ����DB
					updateDb();
//					redirectLogin();
					updWeb();
//				}			
			}				
		});
        
        devRenmBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				getFragmentManager().popBackStackImmediate();
 			}
        });
        
        devRenmLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {	
					downX = (int) event.getX();
					isLeftMove = false;
					isRightMove = false;
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					if ((event.getX() - downX) > mTouchSlop) {
						isRightMove = true;
					} else if ((event.getX() - downX) < -mTouchSlop) {
						isLeftMove = true;
					}
					break;
				}
				case MotionEvent.ACTION_UP:
					if (isRightMove) {
						// ����һ������
						getFragmentManager().popBackStackImmediate();
					} else if (isLeftMove) {
	
					}
					break;
				}

				return true;
			}
 		});	
    }
    
    private void updateDb() {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.DIS_DEV_NAME, deviceName.getText().toString());
    	params.put(Constants.MAC, mac);
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
		db.getTDevice().update(params);
    }
    
//    private void redirectLogin() {
//    	ListFragment listFragment = new ListFragment();
//		FragmentTransaction transaction = getFragmentManager().beginTransaction();
//		transaction.replace(R.id.fragment_container, listFragment);
//		// ��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		transaction.addToBackStack(this.getClass().getName());
//		// Commit the transaction
//		transaction.commit();
//    }
    
//    private boolean requireCheck() {
//    	if (TextUtils.isEmpty(deviceName.getText())) {
//    		messageText.setText(getResources().getString(R.string.lang_mess_dev_nm_req));
//    		return false;
//    	}
//    	return true;
//    }
    
    
    
    private void updWeb() {
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_dev_rename);
//    	Map<String, String> rawParams = new HashMap<String, String>();
//    	rawParams.put(getResources().getString(R.string.param_device_nm), 
//    			deviceName.getText().toString());
//    	rawParams.put(getResources().getString(R.string.param_mac), mac);
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT)
//    	{
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    			
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    		} 
//    	};
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, rawParams), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//		}
    	
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_dev_rename);
    	String url = getResources().getString(R.string.url_dev_rename);
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_device_nm), 
    			deviceName.getText().toString());
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) {     					
            	getFragmentManager().popBackStackImmediate();					    					
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
            	getFragmentManager().popBackStackImmediate();
            }   
        });
    }

}