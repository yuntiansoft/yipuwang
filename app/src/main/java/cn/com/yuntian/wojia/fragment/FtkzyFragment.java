package cn.com.yuntian.wojia.fragment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
;import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.FtkzyBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * FtkzyFragment
 * 
 * @author chenwh
 *
 */
public class FtkzyFragment extends Fragment {
	
	private TextView backButton, titleText, collTempText, collTempUnit, tankTempUpText, tankTempUpUnit;
	
	private TextView  tankTempDownText, tankTempDownUnit, backTempText, backTempUnit, collTempImg, backTempImg;
	
	private TextView collPumpButton, pipePumpButton, auxiHeatButton, tankTempUpImg, tankTempDownImg;
	
	private TextView collWheelImg, backWheelImg, auxiAnimaImg, collPumpText, pipePumpText, auxiHeatText;
	
	private LinearLayout collTempLinearLayout, tankTempUpLinearLayout, tankTempDownLinearLayout, auxiAnimaLinearLayout;
	
	private LinearLayout backTempLinearLayout, ftkzyLayoutView, collWheelLinearLayout, backWheelLinearLayout; 
	
	private FrameLayout devFrameLayout, titleFrameLayout, collPumpImg, pipePumpImg, auxiHeatImg;
	
	private HailinDB db;
	
	private String mac;
	
	private String devNm;
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private static boolean pageIsUpd = false;
	
	private FtkzyBean ftkzyBean = null;
	
	private FtkzyBean bakBean = null;
	
	private Map<String, String> webParam;
	
	private String online = "";
	
	private boolean reLoadPageBl = true;
	
	private int mTouchSlop;
	
	private int downX;
	
	private int downY;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private int wheelWidth = 0;
	
	private int devBgWidth = 0;
	
	private int devBgHeight = 0;
	
	private AnimationDrawable auxiAnimation;
	
	private Animation collAnima;
	
	private Animation backAnima;
	
	private String collPumpOnDev;
	
	private String pipePumpOnDev;
	
	private String auxiHeatOnDev;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		return inflater.inflate(R.layout.ftkzy_fragment, container, false);
	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        reLoadPageBl = true;
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20;
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
		
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		devNm = bundle.getString(Constants.DIS_DEV_NAME);	
        
		initItem();
        setDataToPage();
        itemEvent(); 
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_ftkzy)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;

		handler.post(runnable);	
		
    }
    
    private void initItem() {
    	
		backButton = (TextView) getView().findViewById(R.id.backButton);
    	titleText = (TextView) getView().findViewById(R.id.titleText);
    	collTempText = (TextView) getView().findViewById(R.id.collTempText);
    	collTempUnit = (TextView) getView().findViewById(R.id.collTempUnit);
    	tankTempUpText = (TextView) getView().findViewById(R.id.tankTempUpText);
    	tankTempUpUnit = (TextView) getView().findViewById(R.id.tankTempUpUnit);
    	tankTempDownText = (TextView) getView().findViewById(R.id.tankTempDownText);
    	tankTempDownUnit = (TextView) getView().findViewById(R.id.tankTempDownUnit);
    	backTempText = (TextView) getView().findViewById(R.id.backTempText);
    	backTempUnit = (TextView) getView().findViewById(R.id.backTempUnit);
    	collPumpButton = (TextView) getView().findViewById(R.id.collPumpButton);
    	pipePumpButton = (TextView) getView().findViewById(R.id.pipePumpButton);
    	auxiHeatButton = (TextView) getView().findViewById(R.id.auxiHeatButton);
    	collTempImg = (TextView) getView().findViewById(R.id.collTempImg);
    	tankTempUpImg = (TextView) getView().findViewById(R.id.tankTempUpImg);
    	tankTempDownImg = (TextView) getView().findViewById(R.id.tankTempDownImg);
    	backTempImg = (TextView) getView().findViewById(R.id.backTempImg);
    	collWheelImg = (TextView) getView().findViewById(R.id.collWheelImg);
    	backWheelImg = (TextView) getView().findViewById(R.id.backWheelImg);
    	auxiAnimaImg = (TextView) getView().findViewById(R.id.auxiAnimaImg);
    	collPumpText = (TextView) getView().findViewById(R.id.collPumpText);
    	pipePumpText = (TextView) getView().findViewById(R.id.pipePumpText);
    	auxiHeatText = (TextView) getView().findViewById(R.id.auxiHeatText);
    	
    	ftkzyLayoutView = (LinearLayout) getView().findViewById(R.id.ftkzyLayoutView);
    	collTempLinearLayout = (LinearLayout) getView().findViewById(R.id.collTempLinearLayout);
    	tankTempUpLinearLayout = (LinearLayout) getView().findViewById(R.id.tankTempUpLinearLayout);
    	tankTempDownLinearLayout = (LinearLayout) getView().findViewById(R.id.tankTempDownLinearLayout);
    	backTempLinearLayout = (LinearLayout) getView().findViewById(R.id.backTempLinearLayout);
    	collWheelLinearLayout = (LinearLayout) getView().findViewById(R.id.collWheelLinearLayout);
    	backWheelLinearLayout = (LinearLayout) getView().findViewById(R.id.backWheelLinearLayout);
    	auxiAnimaLinearLayout = (LinearLayout) getView().findViewById(R.id.auxiAnimaLinearLayout);
    	
    	devFrameLayout = (FrameLayout) getView().findViewById(R.id.devFrameLayout);
    	titleFrameLayout = (FrameLayout) getView().findViewById(R.id.titleFrameLayout);
    	collPumpImg = (FrameLayout) getView().findViewById(R.id.collPumpImg);
    	pipePumpImg = (FrameLayout) getView().findViewById(R.id.pipePumpImg);
    	auxiHeatImg = (FrameLayout) getView().findViewById(R.id.auxiHeatImg);
    	
        collAnima = AnimationUtils.loadAnimation(getActivity(), R.anim.green_wind_anima);  
        collAnima.setInterpolator(new LinearInterpolator()); 
        collAnima.setDuration(750);
        
        backAnima = AnimationUtils.loadAnimation(getActivity(), R.anim.green_wind_anima);  
        backAnima.setInterpolator(new LinearInterpolator()); 
        backAnima.setDuration(350);
    	
    	titleText.setText(devNm);
    	
    	ViewTreeObserver tempImgVto = backWheelImg.getViewTreeObserver();   
    	tempImgVto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() { 
			@SuppressWarnings("deprecation")
			@Override  
            public void onGlobalLayout() { 
            	backWheelImg.getViewTreeObserver().removeGlobalOnLayoutListener(this); 
            	wheelWidth = backWheelImg.getWidth();
            	if (devBgWidth != 0) {
            		backWheelImg.setWidth(new BigDecimal(21*(devBgWidth/640)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                	backWheelImg.setHeight(new BigDecimal(21*(devBgHeight/385)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
            	}
            }   
        });
        
        ViewTreeObserver devFVto = devFrameLayout.getViewTreeObserver();   
        devFVto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() { 
            @SuppressWarnings("deprecation")
			@Override  
            public void onGlobalLayout() { 
            	devFrameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this); 
            	devBgHeight = devFrameLayout.getHeight();
            	devBgWidth = devFrameLayout.getWidth();
            	setImageMargin();
            	if (wheelWidth != 0) {
            		backWheelImg.setWidth(new BigDecimal(21*(devBgWidth/640)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
                	backWheelImg.setHeight(new BigDecimal(21*(devBgHeight/385)).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
            	}
            }   
        });
    }
    
    private void setImageMargin() {
    	int wCollDis = new BigDecimal(20*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hCollDis = new BigDecimal(135*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams collLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT); 
    	collLp.gravity = Gravity.TOP|Gravity.LEFT;  
    	collLp.setMargins(wCollDis, hCollDis, 0, 0); 
    	collTempLinearLayout.setLayoutParams(collLp);
    	
    	int wTankUpDis = new BigDecimal(333*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hTankUpDis = new BigDecimal(291*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams tankUpLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	tankUpLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	tankUpLp.setMargins(wTankUpDis, hTankUpDis, 0, 0); 
    	tankTempUpLinearLayout.setLayoutParams(tankUpLp);
    	
    	int wTankDownDis = new BigDecimal(333*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hTankDownDis = new BigDecimal(42*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams tankDownLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	tankDownLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	tankDownLp.setMargins(wTankDownDis, hTankDownDis, 0, 0); 
    	tankTempDownLinearLayout.setLayoutParams(tankDownLp);
    	
    	int wBackDis = new BigDecimal(476*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hBackDis = new BigDecimal(266*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams backLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	backLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	backLp.setMargins(wBackDis, hBackDis, 0, 0); 
    	backTempLinearLayout.setLayoutParams(backLp);
    	
    	int wCollWheelDis = new BigDecimal(285*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hCollWheelDis = new BigDecimal(235*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams collWheelLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	collWheelLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	collWheelLp.setMargins(wCollWheelDis, hCollWheelDis, 0, 0); 
    	collWheelLinearLayout.setLayoutParams(collWheelLp);
    	
    	int wBackWheelDis = new BigDecimal(540*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hBackWheelDis = new BigDecimal(247*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams backWheelLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	backWheelLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	backWheelLp.setMargins(wBackWheelDis, hBackWheelDis, 0, 0); 
    	backWheelLinearLayout.setLayoutParams(backWheelLp);
    	
    	int wAuxiAnimaDis = new BigDecimal(365*devBgWidth).divide(new BigDecimal(640), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	int hAuxiAnimaDis = new BigDecimal(163*devBgHeight).divide(new BigDecimal(385), 0, BigDecimal.ROUND_HALF_UP).intValue();
    	FrameLayout.LayoutParams auxiAnimaLp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, 
    			FrameLayout.LayoutParams.WRAP_CONTENT);  
    	auxiAnimaLp.gravity = Gravity.TOP|Gravity.LEFT; 
    	auxiAnimaLp.setMargins(wAuxiAnimaDis, hAuxiAnimaDis, 0, 0); 
    	auxiAnimaLinearLayout.setLayoutParams(auxiAnimaLp);
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	reLoadPageBl = false;

    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () { 
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };

    private void itemEvent() {
    	btnEvent();
    	backEvent();
    }
    
    private void btnEvent() {
    	
    	collPumpButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String oldCollPump = bakBean.getCollPump();
				Boolean collPumpFlg = false; 
				
				if (Constants.VALUE_ONLINE_ONE.equals(oldCollPump)) {
					if (getResources().getString(R.string.value_coll_pump_app).equals(collPumpOnDev)) {
						pageIsUpd = true;
						bakBean.setCollPump(Constants.VALUE_ONLINE_ZERO);
	    				collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_off);
	    				collWheelImg.clearAnimation();
					} else {
						Builder builder = new Builder(getActivity());
				    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
				    	builder.setMessage(getResources().getString(R.string.lang_ftkzy_control_mess));  
						builder.setNegativeButton(getResources().getString(R.string.lang_dialog_confirm), 
								new DialogInterface.OnClickListener() {   
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});  
					
						builder.create().show();
						return;
					}
				} else {
					pageIsUpd = true;
    				bakBean.setCollPump(Constants.VALUE_ONLINE_ONE);
    				collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
    				collPumpFlg = true;
    				collWheelImg.clearAnimation();
    				collWheelImg.startAnimation(collAnima);
    				collPumpOnDev = getResources().getString(R.string.value_coll_pump_app);
    				bakBean.setCollPumpOnDev(collPumpOnDev);
				}
				
				Map<String, String> param = new HashMap<String, String>();
    			param.put(Constants.COLL_PUMP, bakBean.getCollPump());
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_coll_pump), 
	    				collPumpFlg);
	    		if(collPumpFlg) {
	    			param.put(Constants.COLL_PUMP_ON_DEV, collPumpOnDev);
	    			paramMap.put(getResources().getString(R.string.param_coll_pump_on_dev), collPumpOnDev);
	    		}
	    		updateWeb(paramMap, param);	
    		}
        });
    	
    	pipePumpButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	    		
	    		String oldPipePump = bakBean.getPipePump();
				Boolean pipePumpFlg = false; 
				
				if (Constants.VALUE_ONLINE_ONE.equals(oldPipePump)) {
					if (getResources().getString(R.string.value_pipe_pump_app).equals(pipePumpOnDev)) {
						pageIsUpd = true;
						bakBean.setPipePump(Constants.VALUE_ONLINE_ZERO);
						pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_off);
	    				backWheelImg.clearAnimation();
					} else {
						Builder builder = new Builder(getActivity());
				    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
				    	builder.setMessage(getResources().getString(R.string.lang_ftkzy_control_mess));  
						builder.setNegativeButton(getResources().getString(R.string.lang_dialog_confirm), 
								new DialogInterface.OnClickListener() {   
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});  
					
						builder.create().show();
						return;
					}
				} else {
					pageIsUpd = true;
    				
    				bakBean.setPipePump(Constants.VALUE_ONLINE_ONE);
    				pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
    				pipePumpFlg = true;
    				backWheelImg.clearAnimation();
    				backWheelImg.startAnimation(backAnima);
    				pipePumpOnDev = getResources().getString(R.string.value_pipe_pump_app);
    				bakBean.setPipePumpOnDev(pipePumpOnDev);
				}
				
				Map<String, String> param = new HashMap<String, String>();
    			param.put(Constants.PIPE_PUMP, bakBean.getPipePump());
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_pipe_pump), 
	    				pipePumpFlg);
	    		if(pipePumpFlg) {
	    			param.put(Constants.PIPE_PUMP_ON_DEV, pipePumpOnDev);
	    			paramMap.put(getResources().getString(R.string.param_pipe_pump_on_dev), pipePumpOnDev);
	    		}
	    		updateWeb(paramMap, param);	
    		}
        });
    	
    	auxiHeatButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
    			
	    		String oldAuxiHeat = bakBean.getAuxiHeat();
	    		Boolean auxiHeatFlg = false; 
				
				if (Constants.VALUE_ONLINE_ONE.equals(oldAuxiHeat)) {
					if (getResources().getString(R.string.value_auxi_heat_app).equals(auxiHeatOnDev)) {
						pageIsUpd = true;
	    				
	    				bakBean.setAuxiHeat(Constants.VALUE_ONLINE_ZERO);
	    				auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_off);
	    				if (auxiAnimation != null && auxiAnimation.isRunning()) {
	            			auxiAnimation.stop();
	            			auxiAnimaImg.setBackgroundResource(R.drawable.ftkzy_auxi_anim1);
	            		}
					} else {
						Builder builder = new Builder(getActivity());
				    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
				    	builder.setMessage(getResources().getString(R.string.lang_ftkzy_control_mess));  
						builder.setNegativeButton(getResources().getString(R.string.lang_dialog_confirm), 
								new DialogInterface.OnClickListener() {   
							@Override
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
							}
						});  
					
						builder.create().show();
						return;
					}
				} else {
					pageIsUpd = true;
    				
    				bakBean.setAuxiHeat(Constants.VALUE_ONLINE_ONE);
    				auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_on);
    				auxiHeatFlg = true;
    				auxiAnimaImg.setBackgroundResource(R.drawable.ftkzy_auxi_anima);
            		auxiAnimation = (AnimationDrawable) auxiAnimaImg.getBackground();
            		if (!auxiAnimation.isRunning()) {
            			auxiAnimation.start();
            		}	
            		auxiHeatOnDev = getResources().getString(R.string.value_auxi_heat_app);
    				bakBean.setAuxiHeatOnDev(auxiHeatOnDev);
				}
				
				Map<String, String> param = new HashMap<String, String>();
    			param.put(Constants.AUXI_HEAT, bakBean.getAuxiHeat());
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_auxi_heat), 
	    				auxiHeatFlg);
	    		
	    		if(auxiHeatFlg) {
	    			param.put(Constants.AUXI_HEAT_ON_DEV, auxiHeatOnDev);
	    			paramMap.put(getResources().getString(R.string.param_auxi_heat_on_dev), auxiHeatOnDev);
	    		}
	    		updateWeb(paramMap, param);	
    		}
        });
    }
    
    private void backEvent() {
    	
		backButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
		
		ftkzyLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// 
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    }
        
    private void setDataToPage() {

    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	ftkzyBean = db.getTFtkzy().findOne(arg);
    	
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline();
    	
    	collPumpOnDev = ftkzyBean.getCollPumpOnDev();
    	pipePumpOnDev = ftkzyBean.getPipePumpOnDev();
    	auxiHeatOnDev = ftkzyBean.getAuxiHeatOnDev();
    	
    	boolean collFlg = true;
    	boolean tankDownFlg = true;
    	boolean tankUpFlg = true;
    	boolean backFlg = true;
    	String collTemp = ftkzyBean.getCollTempOne();
    	if (!CheckUtil.requireCheck(collTemp)) {
    		collTemp = "0";
    	} else if (collTemp.startsWith("c") || collTemp.startsWith("f")
    			|| collTemp.startsWith("C") || collTemp.startsWith("F")) {
    		collTemp = Util.getValue(Util.getTempDegree(collTemp), 0);
    	} else {
    		collFlg = false;
    	}
    	
    	String tankDownTemp = ftkzyBean.getTankTempThree();
    	if (!CheckUtil.requireCheck(tankDownTemp)) {
    		tankDownTemp = "0";
    	} else if (tankDownTemp.startsWith("c") || tankDownTemp.startsWith("f")
    			|| tankDownTemp.startsWith("C") || tankDownTemp.startsWith("F")) {
    		tankDownTemp = Util.getValue(Util.getTempDegree(tankDownTemp), 0);
    	} else {
    		tankDownFlg = false;
    	}
    	
    	String tankUpTemp = ftkzyBean.getTankTempTwo();
    	if (!CheckUtil.requireCheck(tankUpTemp)) {
    		tankUpTemp = "0";
    	} else if (tankUpTemp.startsWith("c") || tankUpTemp.startsWith("f")
    			|| tankUpTemp.startsWith("C") || tankUpTemp.startsWith("F")) {
    		tankUpTemp = Util.getValue(Util.getTempDegree(tankUpTemp), 0);
    	} else {
    		tankUpFlg = false;
    	}
    	
    	String backTemp = ftkzyBean.getBackTempFour();
    	if (!CheckUtil.requireCheck(backTemp)) {
    		backTemp = "0";
    	} else if (backTemp.startsWith("c") || backTemp.startsWith("f")
    			|| backTemp.startsWith("C") || backTemp.startsWith("F")) {
    		backTemp = Util.getValue(Util.getTempDegree(backTemp), 0);
    	} else {
    		backFlg = false;
    	}

    	collTempText.setText(collTemp);
    	tankTempUpText.setText(tankUpTemp);
    	tankTempDownText.setText(tankDownTemp);
    	backTempText.setText(backTemp);
    	
    	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    		if (collFlg) {
	    		collTempUnit.setText(
						getResources().getString(R.string.value_temp_cen));
    		} else {
    			collTempUnit.setText("");
    		}
    		
    		if (tankUpFlg) {
    			tankTempUpUnit.setText(
    					getResources().getString(R.string.value_temp_cen));
    		} else {
    			tankTempUpUnit.setText("");
    		}
    		
    		if (tankDownFlg) {
    			tankTempDownUnit.setText(
    					getResources().getString(R.string.value_temp_cen));
    		} else {
    			tankTempDownUnit.setText("");
    		}
    		
    		if (backFlg) {
    			backTempUnit.setText(
    					getResources().getString(R.string.value_temp_cen));
    		} else {
    			backTempUnit.setText("");
    		}
    		
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			if (collFlg) {
	    		collTempUnit.setText(
						getResources().getString(R.string.value_temp_fah));
    		} else {
    			collTempUnit.setText("");
    		}
    		
    		if (tankUpFlg) {
    			tankTempUpUnit.setText(
    					getResources().getString(R.string.value_temp_fah));
    		} else {
    			tankTempUpUnit.setText("");
    		}
    		
    		if (tankDownFlg) {
    			tankTempDownUnit.setText(
    					getResources().getString(R.string.value_temp_fah));
    		} else {
    			tankTempDownUnit.setText("");
    		}
    		
    		if (backFlg) {
    			backTempUnit.setText(
    					getResources().getString(R.string.value_temp_fah));
    		} else {
    			backTempUnit.setText("");
    		}
		}
        
    	String tankTempFlg = ftkzyBean.getTankTempFlg();
    	if (getResources().getString(R.string.value_no_display).equals(tankTempFlg)) {
    		backTempLinearLayout.setVisibility(View.INVISIBLE);
    		tankTempUpLinearLayout.setVisibility(View.INVISIBLE);
    	} else if (getResources().getString(R.string.value_T4_display).equals(tankTempFlg)) {
    		backTempLinearLayout.setVisibility(View.VISIBLE);
    		tankTempUpLinearLayout.setVisibility(View.INVISIBLE);
    	} else if (getResources().getString(R.string.value_T2_display).equals(tankTempFlg)) {
    		backTempLinearLayout.setVisibility(View.INVISIBLE);
    		tankTempUpLinearLayout.setVisibility(View.VISIBLE);
    	} else {
    		backTempLinearLayout.setVisibility(View.VISIBLE);
    		tankTempUpLinearLayout.setVisibility(View.VISIBLE);
    	}
        
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	collWheelImg.setBackgroundResource(R.drawable.ftkzy_coll_wheel);
        	backWheelImg.setBackgroundResource(R.drawable.ftkzy_back_wheel);
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getCollPump())) {
				collWheelImg.clearAnimation();
        		collWheelImg.startAnimation(collAnima);
        		collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
        	} else {
        		collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_off);
				collWheelImg.clearAnimation();
        	}
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getPipePump())) {
        		backWheelImg.clearAnimation();
        		backWheelImg.startAnimation(backAnima);
        		pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
        	} else {
        		pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_off);
        		backWheelImg.clearAnimation();
        	}
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getAuxiHeat())) {
        		auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_on);
    			auxiAnimaImg.setBackgroundResource(R.drawable.ftkzy_auxi_anima);
        		auxiAnimation = (AnimationDrawable) auxiAnimaImg.getBackground();
        		
        		if (!auxiAnimation.isRunning()) {
        			auxiAnimation.start();
        		}	
        	} else {
        		auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_off);
        		if (auxiAnimation != null && auxiAnimation.isRunning()) {
        			auxiAnimation.stop();
        			auxiAnimaImg.setBackgroundResource(R.drawable.ftkzy_auxi_anim1);
        		}
        	}
        } else {
        	collWheelImg.setBackgroundResource(R.drawable.ftkzy_coll_wheel_offline);
        	collWheelImg.clearAnimation();
        	backWheelImg.setBackgroundResource(R.drawable.ftkzy_back_wheel_offline);
        	backWheelImg.clearAnimation();
        	auxiAnimaImg.setBackgroundResource(R.drawable.ftkzy_auxi_anim1_offline);
        	if (auxiAnimation != null && auxiAnimation.isRunning()) {
    			auxiAnimation.stop();
    		}
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getCollPump())) {
        		collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	} else {
        		collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_off_offline);
        	}
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getPipePump())) {
        		pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	} else {
        		pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_off_offline);
        	}
        	if (Constants.VALUE_ONLINE_ONE.equals(ftkzyBean.getAuxiHeat())) {
        		auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	} else {
        		auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_off_offline);
        	}
        }	
        
        bakBean = (FtkzyBean) ftkzyBean.clone();
    }
    
    private void updateDb(Map<String, String> param) {
    	param.put(Constants.MAC, mac);
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	db.getTFtkzy().update(param);
    }
    
    private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
    	webParam = param;
    	
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) { 
            	updateDb(webParam);
            	pageIsUpd = false;
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (reLoadPageBl) {
            		setDataToPage();
            	}
            	pageIsUpd = false;
            }   
        });
    }
    
    public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
    
    private void setOnline() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		titleFrameLayout.setBackgroundResource(R.drawable.ftkzy_title);
    		backButton.setBackgroundResource(R.drawable.back_arrow);
    		devFrameLayout.setBackgroundResource(R.drawable.ftkzy_dev_bg);
        	collTempImg.setBackgroundResource(R.drawable.ftkzy_coll_temp);
        	tankTempUpImg.setBackgroundResource(R.drawable.ftkzy_tank_temp);
        	tankTempDownImg.setBackgroundResource(R.drawable.ftkzy_tank_temp);
        	backTempImg.setBackgroundResource(R.drawable.ftkzy_back_temp);
        	collPumpImg.setBackgroundResource(R.drawable.ftkzy_coll_pump_bg);
        	pipePumpImg.setBackgroundResource(R.drawable.ftkzy_pipe_pump_bg);
        	auxiHeatImg.setBackgroundResource(R.drawable.ftkzy_auxi_heat_bg);
        	collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
        	pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_on);
        	auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_on);
        	collPumpText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_on));
        	pipePumpText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_on));
        	auxiHeatText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_on));
    	} else {
    		setDisEnable();
    		titleFrameLayout.setBackgroundResource(R.drawable.ftkzy_title_offline);
    		backButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		devFrameLayout.setBackgroundResource(R.drawable.ftkzy_dev_bg_offline);
        	collTempImg.setBackgroundResource(R.drawable.ftkzy_coll_temp_offline);
        	tankTempUpImg.setBackgroundResource(R.drawable.ftkzy_tank_temp_offline);
        	tankTempDownImg.setBackgroundResource(R.drawable.ftkzy_tank_temp_offline);
        	backTempImg.setBackgroundResource(R.drawable.ftkzy_back_temp_offline);
        	collPumpImg.setBackgroundResource(R.drawable.ftkzy_coll_pump_bg_offline);
        	pipePumpImg.setBackgroundResource(R.drawable.ftkzy_pipe_pump_bg_offline);
        	auxiHeatImg.setBackgroundResource(R.drawable.ftkzy_auxi_heat_bg_offline);
        	collPumpButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	pipePumpButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	auxiHeatButton.setBackgroundResource(R.drawable.ftkzy_button_on_offline);
        	collPumpText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_off));
        	pipePumpText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_off));
        	auxiHeatText.setTextColor(getResources().getColor(R.color.color_ftkzy_ctrl_off));
    	}
    }
    
    private void setEnable() {
    	collPumpButton.setEnabled(true);
    	pipePumpButton.setEnabled(true);
    	auxiHeatButton.setEnabled(true);
    }

	private void setDisEnable() {
    	collPumpButton.setEnabled(false);
    	pipePumpButton.setEnabled(false);
    	auxiHeatButton.setEnabled(false);
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
}