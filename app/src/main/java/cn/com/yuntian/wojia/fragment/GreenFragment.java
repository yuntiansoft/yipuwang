package cn.com.yuntian.wojia.fragment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.GreenTempBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class GreenFragment extends Fragment {
	
	private TextView greenBackButton, greenTempValueText, greenTempUnitText, greenTempText;
//	greenTempImg;
	
	private ImageView greenColCirBg, greEnergyImg;
	
	private TextView greenColdBtnImg, greenHeatBtnImg, greenWindBtnImg, greenOffBtnImg, greenWindAnim, greenOnOffText;
	
	private TextView greenWindLowImg, greenWindMiddleImg, greenWindHighImg, greenAutoWindImg, greenTitleText, coverText;
	
//	private ImageView greenTempCoolBtn, greenTempHeatBtn; 
	
//	private LinearLayout greenLayoutView, greenTempCoolMoveLinear, greenTempHeatMoveLinear;
	
	private LinearLayout greenTempBg, greenLayoutView, greenPicDisplay, greenWindBg, greenAutoWindBg; 
	
//	private LinearLayout greenColdBtnLinearLayout, greenColdTextLinearLayout, greenHeatTextLinearLayout, greenWindTextLinearLayout, greenOffTextLinearLayout;
	
	private FrameLayout greEnergyBgImg, greenTitleBg;
	
	private SeekBar greenSeekBar, greenSeekBarOffline;
	
	private HailinDB db;
	
	private String mac;
	
	private String devNm;
	
	private String devType;
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private Bitmap mBitmap;
	
	private final int maxCTemp = 35;
	
	private final int minCTemp = 5;
	
	private final int maxFTemp = 95;
	
	private final int minFTemp = 41;
	
	private int coolDefaultTemp = 29;
	
	private int heatDefaultTemp = 20;
	
	private int coolSaveEnergy = 26;
	
	private int heatSaveEnergy = 18;
	
	private double coolDefaultSaveEnergy = 0;
	
	private double heatDefaultSaveEnergy = 0;
	
	private int maxTemp;
	
	private int minTemp;
	
//	private int tempBtnwidth;
	
	private int screenWidth;
	
//	private int screenHeight;
	
	private Animation operatingAnim;
	
	private double tempStand;
	
	private static boolean pageIsUpd = false;
	
	private GreenTempBean greenTempBean;
	
	private GreenTempBean bakBean;
	
	private Map<String, String> webParam;
	
	private boolean animBl = true; 
	
	private String status;
	
	private String statusOnOff = "";
	
	private int greenTempWidth;
	
	private boolean proBl = false;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
//	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;
	
	private boolean reLoadPageBl = true;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
//		return inflater.inflate(R.layout.green_fragment, container, false);
		
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		devNm = bundle.getString(Constants.DIS_DEV_NAME);	
		devType = bundle.getString(Constants.DEV_TYPE);
		
		if (getResources().getString(R.string.value_deviceid_lingdong_ac).equals(devType)) {
			return inflater.inflate(R.layout.lingdong_ac_fragment, container, false);
		} else {
			return inflater.inflate(R.layout.green_fragment, container, false);
		}

	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        reLoadPageBl = true;
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20;
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
//		screenHeight = dm.heightPixels;
		

		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		maxTemp = maxFTemp;
    		minTemp = minFTemp;
    		coolDefaultTemp = Integer.valueOf(Util.centigrade2Fahrenheit(coolDefaultTemp, 0)).intValue();
    		heatDefaultTemp = Integer.valueOf(Util.centigrade2Fahrenheit(heatDefaultTemp, 0)).intValue();
    		coolSaveEnergy = Integer.valueOf(Util.centigrade2Fahrenheit(coolSaveEnergy, 0)).intValue();
    		heatSaveEnergy = Integer.valueOf(Util.centigrade2Fahrenheit(heatSaveEnergy, 0)).intValue();
    	} else {
    		maxTemp = maxCTemp;
    		minTemp = minCTemp;
    	}
		
//		Bundle bundle = this.getArguments();
//		mac  = bundle.getString(Constants.MAC);	
//		devNm = bundle.getString(Constants.DIS_DEV_NAME);	
//		devType = bundle.getString(Constants.DEV_TYPE);
        
		initItem();
        setDataToPage();
        itemEvent(); 
        
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_green)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;
		handler.post(runnable);	
		
    }
    
    private void initItem() {
    	
		greenBackButton = (TextView) getView().findViewById(R.id.greenBackButton);
    	greenTempValueText = (TextView) getView().findViewById(R.id.greenTempValueText);
    	greenTempUnitText = (TextView) getView().findViewById(R.id.greenTempUnitText);
    	greenTempText = (TextView) getView().findViewById(R.id.greenTempText);
    	
    	greenColCirBg = (ImageView) getView().findViewById(R.id.greenColCirBg);	
    	greEnergyImg = (ImageView) getView().findViewById(R.id.greEnergyImg);	
    	greenWindAnim = (TextView) getView().findViewById(R.id.greenWindAnim);	
    	greenWindLowImg = (TextView) getView().findViewById(R.id.greenWindLowImg);	
    	greenWindMiddleImg = (TextView) getView().findViewById(R.id.greenWindMiddleImg);	
    	greenWindHighImg = (TextView) getView().findViewById(R.id.greenWindHighImg);	
    	greenAutoWindImg = (TextView) getView().findViewById(R.id.greenAutoWindImg);
    	greenColdBtnImg = (TextView) getView().findViewById(R.id.greenColdBtnImg);	
    	greenWindBtnImg = (TextView) getView().findViewById(R.id.greenWindBtnImg);	
    	greenHeatBtnImg = (TextView) getView().findViewById(R.id.greenHeatBtnImg);	
    	greenOffBtnImg = (TextView) getView().findViewById(R.id.greenOffBtnImg);
    	greenTitleText = (TextView) getView().findViewById(R.id.greenTitleText);
    	greenOnOffText = (TextView) getView().findViewById(R.id.greenOnOffText);
    	greenLayoutView = (LinearLayout) getView().findViewById(R.id.greenLayoutView);
    	greenPicDisplay = (LinearLayout) getView().findViewById(R.id.greenPicDisplay);
    	greenWindBg = (LinearLayout) getView().findViewById(R.id.greenWindBg);
    	greenAutoWindBg = (LinearLayout) getView().findViewById(R.id.greenAutoWindBg);
    	coverText = (TextView) getView().findViewById(R.id.coverText);
    	
//    	greenColdBtnLinearLayout = (LinearLayout) getView().findViewById(R.id.greenColdBtnLinearLayout);
//    	greenColdTextLinearLayout = (LinearLayout) getView().findViewById(R.id.greenColdTextLinearLayout);
//    	greenHeatTextLinearLayout = (LinearLayout) getView().findViewById(R.id.greenHeatTextLinearLayout);
//    	greenWindTextLinearLayout = (LinearLayout) getView().findViewById(R.id.greenWindTextLinearLayout);
//    	greenOffTextLinearLayout = (LinearLayout) getView().findViewById(R.id.greenOffTextLinearLayout);

    	greenTitleText.setText(devNm);

    	greenTempBg = (LinearLayout) getView().findViewById(R.id.greenTempBg);
    	greEnergyBgImg = (FrameLayout) getView().findViewById(R.id.greEnergyBgImg);
    	greenTitleBg =  (FrameLayout) getView().findViewById(R.id.greenTitleBg);
    	
    	greenSeekBar = (SeekBar) getView().findViewById(R.id.greenSeekBar);
    	greenSeekBarOffline = (SeekBar) getView().findViewById(R.id.greenSeekBarOffline);
    	
        operatingAnim = AnimationUtils.loadAnimation(getActivity(), R.anim.green_wind_anima);  
        LinearInterpolator lin = new LinearInterpolator();  
        operatingAnim.setInterpolator(lin);  
        
        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED); 
    	int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED); 
//    	Drawable draw = getResources().getDrawable(R.drawable.temp_btn);
//    	tempBtnwidth = draw.getMinimumWidth(); 
    	
    	greenTempValueText.measure(w, h); 
    	greenTempWidth = greenTempValueText.getMeasuredWidth(); 
    	
//    	greenColdBtnLinearLayout.measure(w, h); 
//    	int linearWidth = greenColdBtnLinearLayout.getMeasuredWidth();
//    	greenColdTextLinearLayout.getLayoutParams().width = linearWidth;
//    	greenHeatTextLinearLayout.getLayoutParams().width = linearWidth;
//    	greenWindTextLinearLayout.getLayoutParams().width = linearWidth;
//    	greenOffTextLinearLayout.getLayoutParams().width = linearWidth;
    	
    	// ������Ч���ȣ���Ļ����  - button���  + button�Ŀհ�(2)
//    	tempValidLength = screenWidth - tempBtnwidth + 24;
//		tempStand = ((double)screenWidth)/(maxTemp - minTemp);
    }
    
//    private void redirectPage() {
//    	Pm25LineFragment lineFragment = new Pm25LineFragment();
//		FragmentTransaction transaction = getFragmentManager().beginTransaction();
//		transaction.replace(R.id.fragment_container, lineFragment);
//		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		transaction.addToBackStack(this.getClass().getName());
//		// Commit the transaction
//		transaction.commit();
//    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	reLoadPageBl = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
    
    private void itemEvent() {
    	seekbarEvent();
    	fanModEvent();
    	statusEvent();
    	energyEvent();
    	backEvent();
    }
    
    private void seekbarEvent() {
    	greenSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
    		String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (proBl) {
					setTempTextLeftMoving(progress);
					double temp = minTemp + ((double) progress*screenWidth/100)/tempStand;
					if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
			    		greenTempValueText.setText(setText + getResources().getString(R.string.value_temp_fah));		
					} else {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue());
						greenTempValueText.setText(setText + getResources().getString(R.string.value_temp_cen));
					}
					
				}
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				proBl = true;
				pageIsUpd = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				proBl = false;
				if (setText == null || setText.startsWith(Constants.FAH_START_KEY) 
						|| setText.startsWith(Constants.CEN_START_KEY)) {
					return;
				}
				
				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
            		setText = Constants.FAH_START_KEY + setText;
        		} else { 
        			setText = Constants.CEN_START_KEY + setText;
        		}
				Map<String, String> param = new HashMap<String, String>();
				RequestParams paramMap = new RequestParams();
//				if (getResources().getString(R.string.value_status_one).equals(status)) {
//					param.put(Constants.TEMP_HEAT, setText);
//					
//					bakBean.setTempHeat(setText);
//					paramMap.put(getResources().getString(R.string.param_temp_heat), setText);
//				} else if (getResources().getString(R.string.value_status_two).equals(status)) {
//					param.put(Constants.TEMP_COOL, setText);
//					bakBean.setTempCool(setText);
//					paramMap.put(getResources().getString(R.string.param_temp_cool), setText);
//					
//				}
//				param.put(Constants.STATUS, status);
//				paramMap.put(getResources().getString(R.string.param_status), status);
				
				param.put(Constants.TEMP_HEAT, setText);
				paramMap.put(getResources().getString(R.string.param_temp_heat), setText);
				param.put(Constants.TEMP_COOL, setText);
				paramMap.put(getResources().getString(R.string.param_temp_cool), setText);
				bakBean.setTempHeat(setText);
				bakBean.setTempCool(setText);

				setSaveEnergyForWind(bakBean.getFanMod(), false, param, paramMap);
	    		updateWeb(paramMap, param);
			}
    		
    	});
    }
    
    private void statusEvent() {
    	
    	greenHeatBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_status_one).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_one);
	    			if (!animBl) {
	    				animBl = true;
	    				if (getResources().getString(R.string.value_fan_mod_three).equals(
	    	    				bakBean.getFanMod())) {
	    	    			operatingAnim.setDuration(1000);
	    				} else if (getResources().getString(R.string.value_fan_mod_four).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(750);
	    				} else if (getResources().getString(R.string.value_fan_mod_five).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(500);
	    				} else {
	    					operatingAnim.setDuration(750);
	    				}
    	    			greenWindAnim.startAnimation(operatingAnim);	
	    			}

	    			setStatus(status, Constants.VALUE_ONLINE_ONE);
	    			setVisible(status, Constants.VALUE_ONLINE_ONE);

    				double heatTemp;
//			        if (!CheckUtil.requireCheck(bakBean.getTempHeat())) {
//			        	heatTemp  = (double) heatDefaultTemp;
//			    	} else {
//			    		heatTemp =  Double.valueOf(Util.getTempDegree(bakBean.getTempHeat())).doubleValue();
//			    		if (heatTemp > maxTemp) {
//			    			heatTemp = maxTemp;
//			    		} else if (heatTemp < minTemp) {
//			    			heatTemp = minTemp;
//			    		}
//			    	}
//			        
//			        int progress = (int) (heatTemp - minTemp) * 100/(maxTemp - minTemp);
//		    		greenSeekBar.setProgress(progress);
//		    		setTempTextLeftMoving(progress);
//			        
//    	    		String tempValue = null;
// 			        String setText = null;
//     	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
//     	    			setText = String.valueOf(new BigDecimal(
//     	    					heatTemp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
//     	    			tempValue = Constants.FAH_START_KEY +  setText;
//     	    			setText = setText + getResources().getString(R.string.value_temp_fah);
//             		} else {	
//             			setText = String.valueOf(new BigDecimal(
//     	    					heatTemp).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
//             			tempValue = Constants.CEN_START_KEY + setText;
//             			setText = setText + getResources().getString(R.string.value_temp_cen);
//             		}
//     	    		greenTempValueText.setText(setText);
     	    		
     	            if (Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy())) {
     	            	heatTemp = heatDefaultSaveEnergy;
     	            	setTempValue(heatTemp);
        				setTempMove(heatTemp);	
     	            } else if (getResources().getString(R.string.value_status_five).equals(
    	    				bakBean.getStatus())) {
     	            	heatTemp = getTempHeat(bakBean.getTempHeat());
     	            	setTempValue(heatTemp);
        				setTempMove(heatTemp);	
     	            }
    				
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, status);
//    	    		param.put(Constants.TEMP_HEAT, tempValue);
    	    		bakBean.setStatus(status);
//    	    		bakBean.setTempHeat(tempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
//    	    		paramMap.put(getResources().getString(R.string.param_temp_heat), tempValue);
    	    		paramMap.put(getResources().getString(R.string.param_status), status);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	greenColdBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {	
				if (!getResources().getString(R.string.value_status_two).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_two);
	    			if (!animBl) {
	    				animBl = true;
	    				if (getResources().getString(R.string.value_fan_mod_three).equals(
	    	    				bakBean.getFanMod())) {
	    	    			operatingAnim.setDuration(1000);
	    				} else if (getResources().getString(R.string.value_fan_mod_four).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(750);
	    				} else if (getResources().getString(R.string.value_fan_mod_five).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(500);
	    				} else {
	    					operatingAnim.setDuration(750);
	    				}
    	    			greenWindAnim.startAnimation(operatingAnim);	
	    			}

	    			setStatus(status, Constants.VALUE_ONLINE_ONE);
	    			setVisible(status, Constants.VALUE_ONLINE_ONE);

	    			double coolTemp;
//	    	        if (!CheckUtil.requireCheck(bakBean.getTempCool())) {
//	    	        	coolTemp = (double) coolDefaultTemp;
//	    	    	} else {
//	    	    		coolTemp = Double.valueOf(Util.getTempDegree(bakBean.getTempCool())).doubleValue();
//	    	    		if (coolTemp > maxTemp) {
//	    	    			coolTemp = maxTemp;
//	    	    		} else if (coolTemp < minTemp) {
//	    	    			coolTemp = minTemp;
//	    	    		}
//	    	    	}
//	    	        
//	    	        int progress = (int) (coolTemp - minTemp) * 100/(maxTemp - minTemp);
//		    		greenSeekBar.setProgress(progress);
//		    		setTempTextLeftMoving(progress);
//		        			        
//			        String tempValue = null;
//			        String setText = null;
//    	    		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
//    	    			setText = String.valueOf(new BigDecimal(
//    	    					coolTemp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
//    	    			tempValue = Constants.FAH_START_KEY +  setText;
//    	    			setText = setText + getResources().getString(R.string.value_temp_fah);
//            		} else {	
//            			setText = String.valueOf(new BigDecimal(
//    	    					coolTemp).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
//            			tempValue = Constants.CEN_START_KEY + setText;
//            			setText = setText + getResources().getString(R.string.value_temp_cen);
//            		}
//    	    		greenTempValueText.setText(setText);
	    			if (Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy())) {
	    				coolTemp = coolDefaultSaveEnergy;
	    				setTempValue(coolTemp);
	    				setTempMove(coolTemp);
     	            } else if (getResources().getString(R.string.value_status_five).equals(
    	    				bakBean.getStatus())) {
     	            	coolTemp = getTempHeat(bakBean.getTempCool());
     	            	setTempValue(coolTemp);
        				setTempMove(coolTemp);
     	            }

	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, status);
//    	    		param.put(Constants.TEMP_COOL, tempValue);
    	    		bakBean.setStatus(status);
//    	    		bakBean.setTempCool(tempValue);
    	    		
    	    		RequestParams paramMap = new RequestParams();
//    	    		paramMap.put(getResources().getString(R.string.param_temp_cool), tempValue);
    	    		paramMap.put(getResources().getString(R.string.param_status), status);
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	greenOffBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
	    		if (getResources().getString(R.string.value_status_off).equals(
	    				bakBean.getStatusOnOff())) {
	    			statusOnOff = getResources().getString(R.string.value_status_on);
	    			setFanMod(bakBean.getFanMod(), Constants.VALUE_ONLINE_ONE, bakBean.getSaveEnergy());
	    			setEnable();
	    			setStatus(getResources().getString(R.string.value_status_six), Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_six), Constants.VALUE_ONLINE_ONE);
	    			greenOnOffText.setText(getResources().getString(R.string.lang_txt_off));
	    			if (!CheckUtil.requireCheck(bakBean.getSaveEnergy())) {
		    			greEnergyImg.setBackgroundResource(R.drawable.green_energy);
		    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
		    		} else if (Constants.VALUE_ENERGY_ZERO.equals(bakBean.getSaveEnergy())) {
		    			greEnergyImg.setBackgroundResource(R.drawable.green_energy);
		    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
		    		} else {
		    			greEnergyImg.setBackgroundResource(R.drawable.green_energy_sel);
		    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_light_bg);
		    		}
	    		} else {
	    			statusOnOff = getResources().getString(R.string.value_status_off);
	    			if (animBl) {
	    				animBl = false;
    	    			greenWindAnim.clearAnimation();	
	    			}
	    			setStatus(getResources().getString(R.string.value_status_four), Constants.VALUE_ONLINE_ONE);
	    			setVisible(getResources().getString(R.string.value_status_four), Constants.VALUE_ONLINE_ONE);
	    			greenOnOffText.setText(getResources().getString(R.string.lang_txt_on));
	    		}
    			
    			Map<String, String> param = new HashMap<String, String>();
	    		param.put(Constants.STATUS_ON_OFF, statusOnOff);
	    		bakBean.setStatusOnOff(statusOnOff);
	    		
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_status_onoff), statusOnOff);
	    		updateWeb(paramMap, param);
			}
        });
    	
    	greenWindBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_status_five).equals(
	    				bakBean.getStatus())) {
	    			pageIsUpd = true;
	    			status = getResources().getString(R.string.value_status_five);
	    			
	    			if (!animBl) {
	    				animBl = true;
	    				if (getResources().getString(R.string.value_fan_mod_three).equals(
	    	    				bakBean.getFanMod())) {
	    	    			operatingAnim.setDuration(1000);
	    				} else if (getResources().getString(R.string.value_fan_mod_four).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(750);
	    				} else if (getResources().getString(R.string.value_fan_mod_five).equals(
	    	    				bakBean.getFanMod())) {
	    					operatingAnim.setDuration(500);
	    				} else {
	    					operatingAnim.setDuration(750);
	    				}
    	    			greenWindAnim.startAnimation(operatingAnim);	
	    			}
	    			
	    			setStatus(status, Constants.VALUE_ONLINE_ONE);
	    			setVisible(status, Constants.VALUE_ONLINE_ONE);

	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.STATUS, status);
    	    		bakBean.setStatus(status);
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_status), status);
    	    		
    	    		if (getResources().getString(R.string.value_fan_mod_two).equals(bakBean.getFanMod())
    	    				&& Constants.VALUE_ENERGY_ZERO.equals(bakBean.getSaveEnergy())) {
    	    			setFanAutoToHigh(param, paramMap);
    	    		}
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    }
    
    private void fanModEvent() {
    	
    	greenWindLowImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
//				setWindImage(Constants.VALUE_ONLINE_ONE, 1);
//				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
//				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
	    		
	    		if (!getResources().getString(R.string.value_fan_mod_three).equals(
	    				bakBean.getFanMod()) && !Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy())) {
	    			pageIsUpd = true;
	    			
    				animBl = true;
    				greenWindAnim.clearAnimation();
    				operatingAnim.setDuration(1000);
    				greenWindAnim.startAnimation(operatingAnim);
    				
    				setWindImage(Constants.VALUE_ONLINE_ONE, 1);
    				if (!getResources().getString(R.string.value_status_five).equals(bakBean.getStatus())) {
	    				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
	    				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
    				}
	    			
	    			Map<String, String> param = new HashMap<String, String>();
    	    		param.put(Constants.FAN_MOD, getResources().getString(R.string.value_fan_mod_three));
    	    		bakBean.setFanMod(getResources().getString(R.string.value_fan_mod_three));
    	    		
    	    		RequestParams paramMap = new RequestParams();
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), 
    	    				getResources().getString(R.string.value_fan_mod_three));
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	greenWindMiddleImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
//				setWindImage(Constants.VALUE_ONLINE_ONE, 2);
//				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
//				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
	    		
	    		if (!getResources().getString(R.string.value_fan_mod_four).equals(
	    				bakBean.getFanMod()) || (getResources().getString(R.string.value_fan_mod_four).equals(
	    	    				bakBean.getFanMod()) && Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy()))) {
	    			pageIsUpd = true;
	    			
//	    			animBl = true;
//    				greenWindAnim.clearAnimation();
//    				operatingAnim.setDuration(750);
//    				greenWindAnim.startAnimation(operatingAnim);
    				
	    			Map<String, String> param = new HashMap<String, String>();
	    			RequestParams paramMap = new RequestParams();
	    			setSaveEnergyForWind(getResources().getString(R.string.value_fan_mod_four), true, param, paramMap);
	    			
    	    		param.put(Constants.FAN_MOD, getResources().getString(R.string.value_fan_mod_four));
    	    		bakBean.setFanMod(getResources().getString(R.string.value_fan_mod_four));
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), 
    	    				getResources().getString(R.string.value_fan_mod_four));

    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	greenWindHighImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel);
//				setWindImage(Constants.VALUE_ONLINE_ONE, 3);
//				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
//				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
	    		
	    		if (!getResources().getString(R.string.value_fan_mod_five).equals(
	    				bakBean.getFanMod()) || (getResources().getString(R.string.value_fan_mod_five).equals(
	    	    				bakBean.getFanMod()) && Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy()))) {
	    			pageIsUpd = true;
	    			
//	    			animBl = true;
//	    			greenWindAnim.clearAnimation();
//    				operatingAnim.setDuration(500);
//    				greenWindAnim.startAnimation(operatingAnim);
	    			
	    			Map<String, String> param = new HashMap<String, String>();
	    			RequestParams paramMap = new RequestParams();
	    			setSaveEnergyForWind(getResources().getString(R.string.value_fan_mod_five), true, param, paramMap);
	    			
    	    		param.put(Constants.FAN_MOD, getResources().getString(R.string.value_fan_mod_five));
    	    		bakBean.setFanMod(getResources().getString(R.string.value_fan_mod_five));
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), 
    	    				getResources().getString(R.string.value_fan_mod_five));
    	    		
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    	
    	greenAutoWindImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
//				greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
//				setWindImage(Constants.VALUE_ONLINE_ONE, 0);
//				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_sel);
//				greenWindAnim.setBackgroundResource(R.drawable.green_wind);
	    		
	    		if (!getResources().getString(R.string.value_fan_mod_two).equals(
	    				bakBean.getFanMod()) || (getResources().getString(R.string.value_fan_mod_two).equals(
	    	    				bakBean.getFanMod()) && Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy()))) {
	    			pageIsUpd = true;
	    			
//	    			animBl = true;
//	    			greenWindAnim.clearAnimation();
//    				operatingAnim.setDuration(750);
//    				greenWindAnim.startAnimation(operatingAnim);
	    			
	    			Map<String, String> param = new HashMap<String, String>();
	    			RequestParams paramMap = new RequestParams();
	    			setSaveEnergyForWind(getResources().getString(R.string.value_fan_mod_two), true, param, paramMap);
	    			
    	    		param.put(Constants.FAN_MOD, getResources().getString(R.string.value_fan_mod_two));
    	    		bakBean.setFanMod(getResources().getString(R.string.value_fan_mod_two));	
    	    		paramMap.put(getResources().getString(R.string.param_fan_mod), 
    	    				getResources().getString(R.string.value_fan_mod_two));
    	    		
    	    		updateWeb(paramMap, param);
	    		}	
			}
        });
    }
    
    private void energyEvent() {
    	greEnergyImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
				
				boolean energyBl;
//				double coolTemp;
//		        double heatTemp;
//		        if (Constants.VALUE_ENERGY_ZERO.equals(bakBean.getSaveEnergy())) {
//		        	coolTemp = coolDefaultSaveEnergy;
//		        	heatTemp = heatDefaultSaveEnergy;
//		        	energyBl = true;
//		        } else {
//		        	coolTemp = getTempCool(bakBean.getTempCool());
//		        	heatTemp = getTempHeat(bakBean.getTempHeat());
//		        	energyBl = false;
//		        }
//		        if (getResources().getString(R.string.value_status_one).equals(bakBean.getStatus())) {
//			        setTempValue(heatTemp);
//					setTempMove(heatTemp);	
//		        } else if (getResources().getString(R.string.value_status_two).equals(bakBean.getStatus())) {
//		        	setTempValue(coolTemp);
//					setTempMove(coolTemp);
//		        }
		        if (Constants.VALUE_ENERGY_ZERO.equals(bakBean.getSaveEnergy())) {
		        	energyBl = true;
		        } else {
		        	energyBl = false;
		        }
		        
		        setTempRestore(bakBean.getSaveEnergy());
		        
		        Map<String, String> param = new HashMap<String, String>();
		        RequestParams paramMap = new RequestParams();
		        
				if (energyBl) {
					greEnergyImg.setBackgroundResource(R.drawable.green_energy_sel);
					greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_light_bg);
					if (!getResources().getString(R.string.value_fan_mod_three).equals(bakBean.getFanMod())) {
						setFanMod(bakBean.getFanMod(), Constants.VALUE_ONLINE_ONE, Constants.VALUE_ENERGY_ONE);
					}
					param.put(Constants.SAVE_ENERGY, Constants.VALUE_ENERGY_ONE);
    				bakBean.setSaveEnergy(Constants.VALUE_ENERGY_ONE);
				} else {
					greEnergyImg.setBackgroundResource(R.drawable.green_energy);
					greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
					if (!getResources().getString(R.string.value_fan_mod_three).equals(bakBean.getFanMod())) {
						if (getResources().getString(R.string.value_fan_mod_two).equals(bakBean.getFanMod())
								&& getResources().getString(R.string.value_status_five).equals(bakBean.getStatus())) {
	    	    			setFanAutoToHigh(param, paramMap);
						} else {
							setFanMod(bakBean.getFanMod(), Constants.VALUE_ONLINE_ONE, Constants.VALUE_ENERGY_ZERO);
						}
					}
					
					param.put(Constants.SAVE_ENERGY, Constants.VALUE_ENERGY_ZERO);
    				bakBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
				}

	    		paramMap.put(getResources().getString(R.string.param_save_energy), String.valueOf(energyBl));
	    		updateWeb(paramMap, param);	
			}
        });
    }
    
    private void backEvent() {
    	
    	coverText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			}
        });
    	
		greenBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
		
		greenLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    }
        
    private void setDataToPage() {

    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	greenTempBean = db.getTGreenTemp().findOne(arg);
    	
    	if (listItemBean != null) {
    		String adminOnly = listItemBean.getAdminOnly();
    		if (CheckUtil.requireCheck(adminOnly) && getResources().getString(R.string.value_admin_only_pc).equals(adminOnly)) {
    			coverText.setVisibility(View.VISIBLE);
    			greenTitleText.setText(devNm + getResources().getString(R.string.lang_txt_auth));
    		} else {
    			coverText.setVisibility(View.GONE);
    			greenTitleText.setText(devNm);
    		}
    	}
		int tempDefaultMin = -100;
		int tempDefaultMax = -100;
		try {
			String tempHeatDefaultMax = Util.getTempDegree(greenTempBean.getTempHeatDefaultMAx());
			if (CheckUtil.requireCheck(tempHeatDefaultMax)) {
				tempDefaultMax = Double.valueOf(tempHeatDefaultMax).intValue();
			}
		} catch (Exception e) {

		}

		try {
			String tempHeatDefaultMin = Util.getTempDegree(greenTempBean.getTempHeatDefaultMin());
			if (CheckUtil.requireCheck(tempHeatDefaultMin)) {
				tempDefaultMin = Double.valueOf(tempHeatDefaultMin).intValue();
			}
		} catch (Exception e) {

		}

		if (tempDefaultMin != -100 && tempDefaultMax != -100) {
			if (tempDefaultMin < tempDefaultMax) {
				minTemp = tempDefaultMin;
				maxTemp = tempDefaultMax;
			}
		} else if (tempDefaultMin == -100 && tempDefaultMax != -100 ) {
			if (minTemp < tempDefaultMax) {
				maxTemp = tempDefaultMax;
			}
		} else if (tempDefaultMax == -100 && tempDefaultMin != -100) {
			if (tempDefaultMin < maxTemp) {
				minTemp = tempDefaultMin;
			}
		}

		tempStand = ((double)screenWidth)/(maxTemp - minTemp);

    	try {
//	    	if (coolDefaultSaveEnergy == 0) {
	    		String coolTempSaveEnergy = Util.getTempDegree(greenTempBean.getTempCoolSaveEnergy());
	    		if (CheckUtil.requireCheck(coolTempSaveEnergy)) {
	    			coolDefaultSaveEnergy = Double.valueOf(coolTempSaveEnergy).doubleValue();
	    		} else {
	    			coolDefaultSaveEnergy = coolSaveEnergy;
	    		}
//	    	}
    	} catch (Exception e) {
    		if (coolDefaultSaveEnergy == 0) {
    			coolDefaultSaveEnergy = coolSaveEnergy;
    		}
    	}
    	
    	try {
//	    	if (heatDefaultSaveEnergy == 0) {
	    		String heatTempSaveEnergy = Util.getTempDegree(greenTempBean.getTempHeatSaveEnergy());
	    		if (CheckUtil.requireCheck(heatTempSaveEnergy)) {
	    			heatDefaultSaveEnergy = Double.valueOf(heatTempSaveEnergy).doubleValue();
	    		} else {
	    			heatDefaultSaveEnergy = heatSaveEnergy;
	    		}
//	    	}
    	} catch (Exception e) {
    		if (heatDefaultSaveEnergy == 0) {
    			heatDefaultSaveEnergy = heatSaveEnergy;
    		}
    	}
    	
    	if (coolDefaultSaveEnergy > maxTemp) {
    		coolDefaultSaveEnergy = maxTemp;
		} else if (coolDefaultSaveEnergy < minTemp) {
			coolDefaultSaveEnergy = minTemp;
		}
    	if (heatDefaultSaveEnergy > maxTemp) {
    		heatDefaultSaveEnergy = maxTemp;
		} else if (heatDefaultSaveEnergy < minTemp) {
			heatDefaultSaveEnergy = minTemp;
		}



    	String online = "";
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline(online);
    	
    	status = greenTempBean.getStatus();
    	statusOnOff = greenTempBean.getStatusOnOff();
    	if (!CheckUtil.requireCheck(statusOnOff)) {
    		statusOnOff = getResources().getString(R.string.value_status_on);
    	}
    	String disTemp = Util.getTempDegree(greenTempBean.getDisTemp());
    	if (!CheckUtil.requireCheck(disTemp)) {
    		disTemp = "0";
    	}
    	greenTempText.setText(disTemp);
    	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    		greenTempUnitText.setText(
					getResources().getString(R.string.value_temp_cen));
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			greenTempUnitText.setText(
					getResources().getString(R.string.value_temp_fah));
		}
    	
//    	if (bakBean == null || (bakBean.getSaveEnergy() != null 
//    			&& !bakBean.getSaveEnergy().equals(greenTempBean.getSaveEnergy()))) {
    		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    		if (!CheckUtil.requireCheck(greenTempBean.getSaveEnergy())) {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy);
	    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
	    		} else if (Constants.VALUE_ENERGY_ZERO.equals(greenTempBean.getSaveEnergy())) {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy);
	    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
	    		} else {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy_sel);
	    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_light_bg);
	    			
	    		}
    		} else {
    			if (!CheckUtil.requireCheck(greenTempBean.getSaveEnergy())) {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy_offline);
	    		} else if (Constants.VALUE_ENERGY_ZERO.equals(greenTempBean.getSaveEnergy())) {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy_offline);
	    		} else {
	    			greEnergyImg.setBackgroundResource(R.drawable.green_energy_sel_offline);
	    		}
    			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
    		}
//    	}
    	
    	
//    	if (bakBean == null || (bakBean.getFanMod() != null 
//    			&& !bakBean.getFanMod().equals(greenTempBean.getFanMod()))) {
//	    	setFanMod(fanMod);
//    	}
 
        double coolTemp;
        double heatTemp;
        if (Constants.VALUE_ENERGY_ONE.equals(greenTempBean.getSaveEnergy())) {
        	coolTemp = coolDefaultSaveEnergy;
        	heatTemp = heatDefaultSaveEnergy;
        } else {
        	coolTemp = getTempCool(greenTempBean.getTempCool());
        	heatTemp = getTempHeat(greenTempBean.getTempHeat());
        }

    	if ((getResources().getString(R.string.value_status_one).equals(status))) {
//			if (bakBean == null || (bakBean.getTempHeat() != null 
//        			&& !bakBean.getTempHeat().equals(greenTempBean.getTempHeat()))
//        			|| (!bakBean.getStatus().equals(status)) || !Constants.VALUE_ONLINE_ONE.equals(online)) {  
				
//				setVisible(status);
				setTempValue(heatTemp);
//				setStatus(status);
				setTempMove(heatTemp);	
//			}
    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {	
//    		if (bakBean == null || (bakBean.getTempCool() != null 
//        			&& !bakBean.getTempCool().equals(greenTempBean.getTempCool()))
//        			|| (!bakBean.getStatus().equals(status)) || !Constants.VALUE_ONLINE_ONE.equals(online)) {  
//				setVisible(status);
				setTempValue(coolTemp);
//				setStatus(status);
				setTempMove(coolTemp);	
//			}
    	} else if (getResources().getString(R.string.value_status_five).equals(status)) {
//			setVisible(status);
//			setStatus(status);
    	}
    	
    	if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
    		setVisible(getResources().getString(R.string.value_status_six), online);
    		setFanMod(greenTempBean.getFanMod(), online, greenTempBean.getSaveEnergy());
			setStatus(getResources().getString(R.string.value_status_six), online);
			greenOnOffText.setText(getResources().getString(R.string.lang_txt_off));
    	} else if (getResources().getString(R.string.value_status_off).equals(statusOnOff)) {
//    		if (animBl) {
//    			greenWindAnim.clearAnimation();  
//			} 
//    		animBl = false;
			setVisible(getResources().getString(R.string.value_status_four), online);
			setStatus(getResources().getString(R.string.value_status_four), online);
			greenOnOffText.setText(getResources().getString(R.string.lang_txt_on));
    	}
    	
    	int currentTemp = Double.valueOf(disTemp).intValue();
    	if (currentTemp > maxTemp) {
    		currentTemp = maxTemp;
    	}
    	int angle = (currentTemp - maxTemp)*360/(maxTemp-minTemp);
    	int drawable = 0;
		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			drawable = R.drawable.green_temp_cir_top;
    	} else {
    		drawable = R.drawable.green_temp_cir_top_offline;
    	}
    	mBitmap = BitmapFactory.decodeResource(
    			getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true); 
        Canvas canvas = new Canvas(mBitmap);
        Paint paint = new Paint();  
        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        canvas.drawArc(rectf, 270, angle, true, paint);
        greenColCirBg.setImageBitmap(mBitmap);	
        
        bakBean = (GreenTempBean) greenTempBean.clone();
    	
    }
    
    private double getTempCool(String tempCool) {
    	double coolTemp;
    	if (!CheckUtil.requireCheck(tempCool)) {
        	coolTemp = (double) coolDefaultTemp;
    	} else {
    		coolTemp = Double.valueOf(Util.getTempDegree(tempCool)).doubleValue();
    		if (coolTemp > maxTemp) {
    			coolTemp = maxTemp;
    		} else if (coolTemp < minTemp) {
    			coolTemp = minTemp;
    		}
    	}
    	return coolTemp;
    }
    
    private double getTempHeat(String tempHeat) {
    	double heatTemp;
    	if (!CheckUtil.requireCheck(tempHeat)) {
        	heatTemp  = (double) heatDefaultTemp;
    	} else {
    		heatTemp =  Double.valueOf(Util.getTempDegree(tempHeat)).doubleValue();
    		if (heatTemp > maxTemp) {
    			heatTemp = maxTemp;
    		} else if (heatTemp < minTemp) {
    			heatTemp = minTemp;
    		}
    	}  
    	return heatTemp;
    }
    
    private void setSaveEnergyForWind(String fanMod, boolean tempBl, Map<String, String> param, RequestParams paramMap) {
		if (Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy())) {
			greEnergyImg.setBackgroundResource(R.drawable.green_energy);
			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
			
			setFanMod(fanMod, 
					Constants.VALUE_ONLINE_ONE, 
					Constants.VALUE_ENERGY_ZERO);

			bakBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
			param.put(Constants.SAVE_ENERGY, Constants.VALUE_ENERGY_ZERO);
			paramMap.put(getResources().getString(R.string.param_save_energy), String.valueOf(false));
			
			if (tempBl) {
				setTempRestore(Constants.VALUE_ENERGY_ONE);
			} 
		} else if (tempBl) {
			setFanMod(fanMod, 
					Constants.VALUE_ONLINE_ONE, 
					Constants.VALUE_ENERGY_ZERO);
		}
    }
    
    private void setTempRestore(String saveEnergy) {
    	double coolTemp;
        double heatTemp;
        if (Constants.VALUE_ENERGY_ZERO.equals(saveEnergy)) {
        	coolTemp = coolDefaultSaveEnergy;
        	heatTemp = heatDefaultSaveEnergy;
        } else {
        	coolTemp = getTempCool(bakBean.getTempCool());
        	heatTemp = getTempHeat(bakBean.getTempHeat());
        }
        if (getResources().getString(R.string.value_status_one).equals(bakBean.getStatus())) {
	        setTempValue(heatTemp);
			setTempMove(heatTemp);	
        } else if (getResources().getString(R.string.value_status_two).equals(bakBean.getStatus())) {
        	setTempValue(coolTemp);
			setTempMove(coolTemp);
        }
    }
    
    private void setFanAutoToHigh(Map<String, String> param, RequestParams paramMap) {
    	setFanMod(getResources().getString(R.string.value_fan_mod_five), 
				Constants.VALUE_ONLINE_ONE, 
				Constants.VALUE_ENERGY_ZERO); 
		bakBean.setFanMod(getResources().getString(R.string.value_fan_mod_five));
		param.put(Constants.FAN_MOD, getResources().getString(R.string.value_fan_mod_five));
		paramMap.put(getResources().getString(R.string.param_fan_mod), 
				getResources().getString(R.string.value_fan_mod_five));
    }
    
    private void setTempMove(double temp) {
//    	if (getResources().getString(R.string.value_status_one).equals(status)) {  	
    		
    		int progress = (int) (temp-minTemp) * 100/(maxTemp - minTemp);
    		greenSeekBar.setProgress(progress);
    		greenSeekBarOffline.setProgress(progress);
    		setTempTextLeftMoving(progress); 
  
//    	} else if (getResources().getString(R.string.value_status_two).equals(status)) {
//    		
//    		int progress = (int) (temp-minTemp) * 100/(maxTemp - minTemp);
//    		greenSeekBar.setProgress(progress);
//    		greenSeekBarOffline.setProgress(progress);
//    		setTempTextLeftMoving(progress);  	
//    	}
    }
    
    private void setTempTextLeftMoving(int progress) {
    	LinearLayout.LayoutParams tempValueParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT); 
		int leftValueMovex = progress*screenWidth/100 - greenTempWidth/2;
		int i = (int)((40 - progress)/2.6);
		leftValueMovex = leftValueMovex + i;
    	if (leftValueMovex < 0) {
    		leftValueMovex = 0;
    	} else if (leftValueMovex + greenTempWidth > screenWidth) {
    		leftValueMovex = screenWidth - greenTempWidth;
    	}
    	tempValueParams.leftMargin = leftValueMovex;
    	greenTempValueText.setLayoutParams(tempValueParams);
    }
    
    private void setVisible(String statusValue, String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	if (getResources().getString(R.string.value_status_four).equals(statusValue) ||
	    			getResources().getString(R.string.value_status_five).equals(statusValue)) {
	    		greenTempValueText.setVisibility(View.INVISIBLE);
	    		greenSeekBar.setVisibility(View.INVISIBLE);
	    		greenTempBg.setVisibility(View.VISIBLE);
	    	} else if (getResources().getString(R.string.value_status_one).equals(statusValue)){
	    		greenTempValueText.setVisibility(View.VISIBLE);
	    		greenSeekBar.setVisibility(View.VISIBLE);
	    		greenTempBg.setVisibility(View.INVISIBLE);
	    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)){
	    		greenTempValueText.setVisibility(View.VISIBLE);
	    		greenSeekBar.setVisibility(View.VISIBLE);
	    		greenTempBg.setVisibility(View.INVISIBLE);
	    	}  else if (getResources().getString(R.string.value_status_six).equals(statusValue)) {
	    		if (CheckUtil.requireCheck(status)) {
	    			if (getResources().getString(R.string.value_status_five).equals(status)) {
	    				greenTempValueText.setVisibility(View.INVISIBLE);
	            		greenSeekBar.setVisibility(View.INVISIBLE);
	            		greenTempBg.setVisibility(View.VISIBLE);
	    			} else {
	    				greenTempValueText.setVisibility(View.VISIBLE);
	            		greenSeekBar.setVisibility(View.VISIBLE);
	            		greenTempBg.setVisibility(View.INVISIBLE);
	    			}
	    		} else {
	    			greenTempValueText.setVisibility(View.INVISIBLE);
	        		greenSeekBar.setVisibility(View.INVISIBLE);
	        		greenTempBg.setVisibility(View.VISIBLE);
	    		}
	    	}
	    	
	    	greenSeekBarOffline.setVisibility(View.INVISIBLE);
    	} else {
    		if (getResources().getString(R.string.value_status_four).equals(statusValue) ||
	    			getResources().getString(R.string.value_status_five).equals(statusValue)) {
	    		greenTempValueText.setVisibility(View.INVISIBLE);
	    		greenSeekBarOffline.setVisibility(View.INVISIBLE);
	    		greenTempBg.setVisibility(View.VISIBLE);
	    	} else if (getResources().getString(R.string.value_status_one).equals(statusValue)){
	    		greenTempValueText.setVisibility(View.VISIBLE);
	    		greenSeekBarOffline.setVisibility(View.VISIBLE);
	    		greenTempBg.setVisibility(View.INVISIBLE);
	    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)){
	    		greenTempValueText.setVisibility(View.VISIBLE);
	    		greenSeekBarOffline.setVisibility(View.VISIBLE);
	    		greenTempBg.setVisibility(View.INVISIBLE);
	    	}  else if (getResources().getString(R.string.value_status_six).equals(statusValue)) {
	    		if (CheckUtil.requireCheck(status)) {
	    			if (getResources().getString(R.string.value_status_five).equals(status)) {
	    				greenTempValueText.setVisibility(View.INVISIBLE);
	    				greenSeekBarOffline.setVisibility(View.INVISIBLE);
	            		greenTempBg.setVisibility(View.VISIBLE);
	    			} else {
	    				greenTempValueText.setVisibility(View.VISIBLE);
	    				greenSeekBarOffline.setVisibility(View.VISIBLE);
	            		greenTempBg.setVisibility(View.INVISIBLE);
	    			}
	    		} else {
	    			greenTempValueText.setVisibility(View.INVISIBLE);
	    			greenSeekBarOffline.setVisibility(View.INVISIBLE);
	        		greenTempBg.setVisibility(View.VISIBLE);
	    		}
	    	}
    		
    		greenSeekBar.setVisibility(View.INVISIBLE);
    	}
    }
    
    private void setTempValue(double temp) {
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		String value = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
    		greenTempValueText.setText(value + getResources().getString(R.string.value_temp_fah));
    		
		} else {
			String value = String.valueOf(new BigDecimal(temp).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
			greenTempValueText.setText(value + getResources().getString(R.string.value_temp_cen));
		}
    }
    
    private void setStatus(String statusValue, String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	if (bakBean == null || (bakBean.getStatus() != null 
	    			&& !bakBean.getStatus().equals(statusValue))) {
	    		if (getResources().getString(R.string.value_status_one).equals(statusValue)) {
	    			greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_sel);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off);    		
		    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_sel);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off); 
		    	} else if (getResources().getString(R.string.value_status_five).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_sel);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off); 	
		    	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_sel); 
		    	} else if (getResources().getString(R.string.value_status_six).equals(statusValue)) {
		    		greenOffBtnImg.setBackgroundResource(R.drawable.btn_off); 
		    		if (CheckUtil.requireCheck(status)) {
		    			if (getResources().getString(R.string.value_status_one).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_sel);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan);
		    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_sel);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan);
		    			} else if (getResources().getString(R.string.value_status_five).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_sel);
		    			}
		    		} else {
		    			greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold);
		    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat);
		    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan);
		    		}
		    		
		    		greenWindBg.setBackgroundResource(R.drawable.green_wind_bg);
					greenTempBg.setBackgroundResource(R.drawable.green_temp_bg);
		    	}
	    	}
	    	
	    	if (getResources().getString(R.string.value_status_five).equals(status)) {
	    		greenAutoWindBg.setBackgroundResource(R.drawable.green_auto_wind_bg_offline);
	    		greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
	    		greenAutoWindImg.setEnabled(false);
	    	} else if (bakBean != null && getResources().getString(R.string.value_status_five).equals(bakBean.getStatus())) {
	    		greenAutoWindBg.setBackgroundResource(R.drawable.green_auto_wind_bg);
	    		greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
	    		greenAutoWindImg.setEnabled(true);
	    	}
    	} else {
    		if (bakBean == null || (bakBean.getStatus() != null 
	    			&& !bakBean.getStatus().equals(statusValue))) {
	    		if (getResources().getString(R.string.value_status_one).equals(statusValue)) {
	    			greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_sel_offline);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_offline);    		
		    	} else if (getResources().getString(R.string.value_status_two).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_sel_offline);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_offline); 
		    	} else if (getResources().getString(R.string.value_status_five).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_sel_offline);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_offline); 	
		    	} else if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
		    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
	    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
	    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
	    			greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_sel_offline); 
		    	} else if (getResources().getString(R.string.value_status_six).equals(statusValue)) {
		    		greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_offline); 
		    		if (CheckUtil.requireCheck(status)) {
		    			if (getResources().getString(R.string.value_status_one).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_sel_offline);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
		    			} else if (getResources().getString(R.string.value_status_two).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_sel_offline);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
		    			} else if (getResources().getString(R.string.value_status_five).equals(status)) {
		    				greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
		        			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
		        			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_sel_offline);
		    			}
		    		} else {
		    			greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
		    			greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
		    			greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
		    		}
		    	}
	    	}
    	}
    	
    	if (getResources().getString(R.string.value_status_four).equals(statusValue)) {
//    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
//			greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
//			greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
			setWindImage(Constants.VALUE_ONLINE_ZERO, 0);
			greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
			greenWindBg.setBackgroundResource(R.drawable.green_wind_bg_offline);
			greenTempBg.setBackgroundResource(R.drawable.green_temp_bg_offline);
			greenWindAnim.setBackgroundResource(R.drawable.green_wind_offline);
			greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
			greEnergyImg.setBackgroundResource(R.drawable.green_energy_offline);
			animBl = false;
			greenWindAnim.clearAnimation();
			setoffBtnEnable(statusValue, online);
    	} 
    }
    
    private void setFanMod(String fanMod, String online, String saveEnergy) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	if (Constants.VALUE_ENERGY_ONE.equals(saveEnergy) 
	    			|| getResources().getString(R.string.value_fan_mod_three).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				setWindImage(Constants.VALUE_ONLINE_ONE, 1);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
				
				animBl = true;
				greenWindAnim.clearAnimation();
				operatingAnim.setDuration(1000);
				greenWindAnim.startAnimation(operatingAnim);
	    	} else if (getResources().getString(R.string.value_fan_mod_four).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				setWindImage(Constants.VALUE_ONLINE_ONE, 2);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
	
				animBl = true;
				greenWindAnim.clearAnimation();
				operatingAnim.setDuration(750);
				greenWindAnim.startAnimation(operatingAnim);
	    	} else if (getResources().getString(R.string.value_fan_mod_five).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel);
				setWindImage(Constants.VALUE_ONLINE_ONE, 3);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel);
	
				animBl = true;
				greenWindAnim.clearAnimation();
				operatingAnim.setDuration(500);
				greenWindAnim.startAnimation(operatingAnim);
	    	} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				setWindImage(Constants.VALUE_ONLINE_ONE, 0);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_sel);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind);
	
				animBl = true;
				greenWindAnim.clearAnimation();
				operatingAnim.setDuration(750);
				greenWindAnim.startAnimation(operatingAnim);
	    	}
	    	if (getResources().getString(R.string.value_status_five).equals(status)) {
	    		greenAutoWindBg.setBackgroundResource(R.drawable.green_auto_wind_bg_offline);
	    		greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
	    	}
    	} else {
    		if (Constants.VALUE_ENERGY_ONE.equals(saveEnergy) 
    				|| getResources().getString(R.string.value_fan_mod_three).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel_offline);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				setWindImage(Constants.VALUE_ONLINE_ZERO, 1);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel_offline);	
	    	} else if (getResources().getString(R.string.value_fan_mod_four).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel_offline);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				setWindImage(Constants.VALUE_ONLINE_ZERO, 2);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel_offline);
	    	} else if (getResources().getString(R.string.value_fan_mod_five).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel_offline);
				setWindImage(Constants.VALUE_ONLINE_ZERO, 3);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_sel_offline);
	    	} else if (getResources().getString(R.string.value_fan_mod_two).equals(fanMod)) {
//	    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
//				greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
//				greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				setWindImage(Constants.VALUE_ONLINE_ZERO, 0);
				greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_sel_offline);
				greenWindAnim.setBackgroundResource(R.drawable.green_wind_offline);
	    	}
    		
    		animBl = false;
			greenWindAnim.clearAnimation();
    	}
    }
    
    private void setoffBtnEnable(String statusValue, String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setDisEnable();
	    	greenOffBtnImg.setEnabled(true);
    	}
    }
    
    private void updateDb(Map<String, String> param) {
    	param.put(Constants.MAC, mac);
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	db.getTGreenTemp().update(param);
    }
    
    private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
    	webParam = param;
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) { 
            	updateDb(webParam);
            	pageIsUpd = false;
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (reLoadPageBl) {
            		setDataToPage();
            	}
            	pageIsUpd = false;
            }   
        });
    }
    
    public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
    
    private void setOnline(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		greenLayoutView.setBackgroundResource(R.drawable.green_bg);
    		greenTitleBg.setBackgroundResource(R.drawable.pm25_title);
    		greenBackButton.setBackgroundResource(R.drawable.back_arrow);
    		greenPicDisplay.setBackgroundResource(R.drawable.green_bla_cir_bg);
    		greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
    		greEnergyImg.setBackgroundResource(R.drawable.green_energy);
    		greenTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg);
    		greenTempBg.setBackgroundResource(R.drawable.green_temp_bg);
    		greenWindBg.setBackgroundResource(R.drawable.green_wind_bg);
    		greenAutoWindBg.setBackgroundResource(R.drawable.green_auto_wind_bg);
    		
    		greenSeekBarOffline.setVisibility(View.INVISIBLE);
    		greenSeekBar.setVisibility(View.VISIBLE);
    	} else {
    		setDisEnable();
    		greenLayoutView.setBackgroundResource(R.drawable.green_bg_offline);
    		greenTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
    		greenBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		greenPicDisplay.setBackgroundResource(R.drawable.green_bla_cir_bg_offline);
    		greEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
    		greEnergyImg.setBackgroundResource(R.drawable.green_energy_offline);
    		greenTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg_offline);
    		greenTempBg.setBackgroundResource(R.drawable.green_temp_bg_offline);
    		greenWindBg.setBackgroundResource(R.drawable.green_wind_bg_offline);
    		greenAutoWindBg.setBackgroundResource(R.drawable.green_auto_wind_bg_offline);
    		
    		greenWindAnim.setBackgroundResource(R.drawable.green_wind_offline);
//    		greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
//    		greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
//    		greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
    		setWindImage(Constants.VALUE_ONLINE_ZERO, 0);
    		greenAutoWindImg.setBackgroundResource(R.drawable.green_auto_wind_offline);
    		greenColdBtnImg.setBackgroundResource(R.drawable.btn_cold_offline);
    		greenHeatBtnImg.setBackgroundResource(R.drawable.btn_heat_offline);
    		greenWindBtnImg.setBackgroundResource(R.drawable.btn_fan_offline);
    		greenOffBtnImg.setBackgroundResource(R.drawable.btn_off_offline);
    		
    		greenSeekBarOffline.setVisibility(View.VISIBLE);
    		greenSeekBar.setVisibility(View.INVISIBLE);
    	}
    }
    
    private void setEnable() {
		greenColdBtnImg.setEnabled(true);
		greenHeatBtnImg.setEnabled(true);
		greenWindBtnImg.setEnabled(true);
		greenOffBtnImg.setEnabled(true);
		greenWindLowImg.setEnabled(true);
		greenWindMiddleImg.setEnabled(true);
		greenWindHighImg.setEnabled(true);
		greenAutoWindImg.setEnabled(true);
		greEnergyImg.setEnabled(true);
		greenSeekBar.setEnabled(true);
    }

	private void setDisEnable() {
		greenColdBtnImg.setEnabled(false);
		greenHeatBtnImg.setEnabled(false);
		greenWindBtnImg.setEnabled(false);
		greenOffBtnImg.setEnabled(false);
		greenWindLowImg.setEnabled(false);
		greenWindMiddleImg.setEnabled(false);
		greenWindHighImg.setEnabled(false);
		greenAutoWindImg.setEnabled(false);
		greEnergyImg.setEnabled(false);
		greenSeekBar.setEnabled(false);
		greenSeekBarOffline.setEnabled(false);
	}
	
	private void setWindImage(String online, int selFlg) {
		if (!Constants.TEMP_VALUE_ONE.equals(getResources().getString(R.string.lang_temp_default))) {
			if (Constants.VALUE_ONLINE_ONE.equals(online)) {
				if (selFlg == 1) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				} else if (selFlg == 2) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				} else if (selFlg == 3) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel);
				} else {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high);
				}
			} else {
				if (selFlg == 1) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				} else if (selFlg == 2) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				} else if (selFlg == 3) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel_offline);
				} else {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_offline);
				}
			}
		} else {
			if (Constants.VALUE_ONLINE_ONE.equals(online)) {
				if (selFlg == 1) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel_eng);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng);
				} else if (selFlg == 2) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel_eng);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng);
				} else if (selFlg == 3) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel_eng);
				} else {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng);
				}
			} else {
				if (selFlg == 1) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_sel_eng_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng_offline);
				} else if (selFlg == 2) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_sel_eng_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng_offline);
				} else if (selFlg == 3) {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_sel_eng_offline);
				} else {
					greenWindLowImg.setBackgroundResource(R.drawable.green_wind_low_eng_offline);
					greenWindMiddleImg.setBackgroundResource(R.drawable.green_wind_middle_eng_offline);
					greenWindHighImg.setBackgroundResource(R.drawable.green_wind_high_eng_offline);
				}
			}
		}
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
}