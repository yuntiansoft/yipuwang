package cn.com.yuntian.wojia.fragment;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
;import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.LingDongTempBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * LingDongTempFragment
 * 
 * @author chenwh
 *
 */
public class LingDongTempFragmentForWoci extends Fragment {
	
	private TextView ldBackButton, ldTempValueText, ldTempUnitText, ldTempText, linkageInputBtn, linkageOutputBtn;
	
	private TextView ldTitleText, ldOutBtnImg, ldEnergyBtnImg, ldComfortBtnImg, ldOffBtnImg, ldValveBtnImg;
	
	private TextView ldOutBtnText, ldEnergyBtnText, ldComfortBtnText, ldOffBtnText, ldValveBtnText, scheduleButton;
	
	private LinearLayout ldTempBg, ldLayoutView, ldPicDisplay; 
	
	private FrameLayout ldTitleBg;
	
	private SeekBar ldSeekBar, ldSeekBarOffline;
	
	private HailinDB db;
	
	private String mac;
	
	private String devNm;
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private Bitmap mBitmap;
	
	private final int maxCTemp = 35;
	
	private final int minCTemp = 5;
	
	private final int maxFTemp = 95;
	
	private final int minFTemp = 41;
	
	private int heatDefaultTemp = 20;
	
	private int maxTemp;
	
	private int minTemp;
	
	private int screenWidth;
	
	private double tempStand;
	
	private static boolean pageIsUpd = false;
	
	private LingDongTempBean ldTempBean = null;
	
	private LingDongTempBean bakBean = null;
	
	private Map<String, String> webParam;
	
	private String statusOnOff = "";
	
	private String status = "";
	
	private int ldTempWidth;
	
	private boolean proBl = false;
	
	private int ldOut = 10;
	
	private int ldEnergy = 18;
	
	private int ldComfort = 22;
	
	private double ldDefaultOut = 0;
	
	private double ldDefaultEnergy = 0;
	
	private double ldDefaultComfort = 0;
	
	private String online = "";
	
	private boolean reLoadPageBl = true;
	
//	int angle = 0;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
//	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private boolean bTaskBl = true;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		return inflater.inflate(R.layout.lingdong_temp_fragment_forwoci, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        reLoadPageBl = true;
        db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20;
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		
		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		maxTemp = maxFTemp;
    		minTemp = minFTemp;
    		heatDefaultTemp = Integer.valueOf(Util.centigrade2Fahrenheit(heatDefaultTemp, 0)).intValue();
    		ldOut = Integer.valueOf(Util.centigrade2Fahrenheit(ldOut, 0)).intValue();
    		ldEnergy = Integer.valueOf(Util.centigrade2Fahrenheit(ldEnergy, 0)).intValue();
    		ldComfort = Integer.valueOf(Util.centigrade2Fahrenheit(ldComfort, 0)).intValue();
    	} else {
    		maxTemp = maxCTemp;
    		minTemp = minCTemp;
    	}
		
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);	
		devNm = bundle.getString(Constants.DIS_DEV_NAME);	
        
		initItem();
        setDataToPage();
        itemEvent(); 
        bTaskBl = true;
        bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_lingdong_temp)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
        		
        		if (isForeRun) {
        			if (!bTaskBl) {
        				bTaskBl = true;
        				handler.postDelayed(runnable, 5000);
        			}
        		}
        	}
        };
		isForeRun = true;
		pageIsUpd = false;

		handler.post(runnable);	
		
    }
    
    private void initItem() {
    	
		ldBackButton = (TextView) getView().findViewById(R.id.ldBackButton);
    	ldTempValueText = (TextView) getView().findViewById(R.id.ldTempValueText);
    	ldTempUnitText = (TextView) getView().findViewById(R.id.ldTempUnitText);
    	ldTempText = (TextView) getView().findViewById(R.id.ldTempText);
    	ldTitleText = (TextView) getView().findViewById(R.id.ldTitleText);
    	ldOutBtnImg = (TextView) getView().findViewById(R.id.ldOutBtnImg);
    	ldEnergyBtnImg = (TextView) getView().findViewById(R.id.ldEnergyBtnImg);
    	ldComfortBtnImg = (TextView) getView().findViewById(R.id.ldComfortBtnImg);
    	ldOffBtnImg = (TextView) getView().findViewById(R.id.ldOffBtnImg);
    	ldOutBtnText = (TextView) getView().findViewById(R.id.ldOutBtnText);
    	ldEnergyBtnText = (TextView) getView().findViewById(R.id.ldEnergyBtnText);
    	ldComfortBtnText = (TextView) getView().findViewById(R.id.ldComfortBtnText);
    	ldOffBtnText = (TextView) getView().findViewById(R.id.ldOffBtnText);
    	scheduleButton = (TextView) getView().findViewById(R.id.scheduleButton);
    	linkageInputBtn = (TextView) getView().findViewById(R.id.linkageInputBtn);
    	linkageOutputBtn = (TextView) getView().findViewById(R.id.linkageOutputBtn);
    	ldValveBtnImg = (TextView) getView().findViewById(R.id.ldValveBtnImg);
    	ldValveBtnText = (TextView) getView().findViewById(R.id.ldValveBtnText);
    	
    	ldLayoutView = (LinearLayout) getView().findViewById(R.id.ldLayoutView);
    	ldTempBg = (LinearLayout) getView().findViewById(R.id.ldTempBg);
    	ldPicDisplay = (LinearLayout) getView().findViewById(R.id.ldPicDisplay);

    	ldTitleBg =  (FrameLayout) getView().findViewById(R.id.ldTitleBg);
    	
    	ldSeekBar = (SeekBar) getView().findViewById(R.id.ldSeekBar);
    	ldSeekBarOffline = (SeekBar) getView().findViewById(R.id.ldSeekBarOffline);
    	
    	ldTitleText.setText(devNm);
        
        int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED); 
    	int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED); 
    	
    	ldTempValueText.measure(w, h); 
    	ldTempWidth = ldTempValueText.getMeasuredWidth(); 
    	
    	// ������Ч���ȣ���Ļ����  - button���  + button�Ŀհ�(2)
		tempStand = ((double)screenWidth)/(maxTemp - minTemp);
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	reLoadPageBl = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () { 
        	bTaskBl = false;
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };

    private void itemEvent() {
    	btnEvent();
    	seekbarEvent();
    	backEvent();
    }
    
    private void seekbarEvent() {
    	ldSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
    		String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (proBl) {
					setTempTextLeftMoving(progress);
					double temp = minTemp + ((double) progress*screenWidth/100)/tempStand;
					if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
			    		ldTempValueText.setText(setText + getResources().getString(R.string.value_temp_fah));		
					} else {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue());
						ldTempValueText.setText(setText + getResources().getString(R.string.value_temp_cen));
					}
					
				}
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				proBl = true;
				pageIsUpd = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				proBl = false;
				
				if (setText == null || setText.startsWith(Constants.FAH_START_KEY) 
						|| setText.startsWith(Constants.CEN_START_KEY)) {
					return;
				}
				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {  
            		setText = Constants.FAH_START_KEY + setText;
        		} else { 
        			setText = Constants.CEN_START_KEY + setText;
        		}
				Map<String, String> param = new HashMap<String, String>();
				RequestParams paramMap = new RequestParams();
				param.put(Constants.TEMP_HEAT, setText);
				bakBean.setTempHeat(setText);
				paramMap.put(getResources().getString(R.string.param_temp_heat), setText);
				
				if (getResources().getString(R.string.value_mod_out).equals(bakBean.getHeatMode())) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_notrun));
					paramMap.put(getResources().getString(R.string.param_heat_mode), 
							getResources().getString(R.string.value_mod_notrun));
    				bakBean.setHeatMode(getResources().getString(R.string.value_mod_notrun));
				} else if (getResources().getString(R.string.value_mod_energy).equals(bakBean.getHeatMode())) {
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_small);
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_notrun));
					paramMap.put(getResources().getString(R.string.param_heat_mode), 
							getResources().getString(R.string.value_mod_notrun));
    				bakBean.setHeatMode(getResources().getString(R.string.value_mod_notrun));
				} else if (getResources().getString(R.string.value_mod_comfort).equals(bakBean.getHeatMode())) {
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_small);
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_notrun));
					paramMap.put(getResources().getString(R.string.param_heat_mode), 
							getResources().getString(R.string.value_mod_notrun));
    				bakBean.setHeatMode(getResources().getString(R.string.value_mod_notrun));
				}

	    		updateWeb(paramMap, param);			
			}
    	});
    }
    
    private void btnEvent() {
    	
    	ldOutBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_mod_out).equals(
	    				bakBean.getHeatMode())) {
	    			pageIsUpd = true;
	    			bakBean.setHeatMode(getResources().getString(R.string.value_mod_out));
	    			setModeBtnImg(false);
			        setTempValue(ldDefaultOut);
					setTempMove(ldDefaultOut);	
					
					Map<String, String> param = new HashMap<String, String>();
	    			param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_out));
		    		RequestParams paramMap = new RequestParams();
		    		paramMap.put(getResources().getString(R.string.param_heat_mode), 
		    				getResources().getString(R.string.value_mod_out));
		    		updateWeb(paramMap, param);	
	    		}
			}
        });
    	
    	ldEnergyBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_mod_energy).equals(
	    				bakBean.getHeatMode())) {
	    			pageIsUpd = true;
	    			bakBean.setHeatMode(getResources().getString(R.string.value_mod_energy));
	    			setModeBtnImg(false);
			        setTempValue(ldDefaultEnergy);
					setTempMove(ldDefaultEnergy);	
					
					Map<String, String> param = new HashMap<String, String>();
	    			param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_energy));
		    		RequestParams paramMap = new RequestParams();
		    		paramMap.put(getResources().getString(R.string.param_heat_mode), 
		    				getResources().getString(R.string.value_mod_energy));
		    		updateWeb(paramMap, param);	
	    		}
			}
        });
    	
    	ldComfortBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
	    		if (!getResources().getString(R.string.value_mod_comfort).equals(
	    				bakBean.getHeatMode())) {
	    			pageIsUpd = true;
	    			bakBean.setHeatMode(getResources().getString(R.string.value_mod_comfort));
	    			setModeBtnImg(false);
			        setTempValue(ldDefaultComfort);
					setTempMove(ldDefaultComfort);	
					
					Map<String, String> param = new HashMap<String, String>();
	    			param.put(Constants.HEAT_MODE, getResources().getString(R.string.value_mod_comfort));
		    		RequestParams paramMap = new RequestParams();
		    		paramMap.put(getResources().getString(R.string.param_heat_mode), 
		    				getResources().getString(R.string.value_mod_comfort));
		    		updateWeb(paramMap, param);	
	    		}
			}
        });
    	
    	ldOffBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
	    		if (getResources().getString(R.string.value_status_off).equals(
	    				bakBean.getStatusOnOff())) {
	    			statusOnOff = getResources().getString(R.string.value_status_on);
	    		} else {
	    			statusOnOff = getResources().getString(R.string.value_status_off);
	    		}
	    		setStatusOnOffInfo(false); 
    			Map<String, String> param = new HashMap<String, String>();
	    		param.put(Constants.STATUS_ON_OFF, statusOnOff);
	    		bakBean.setStatusOnOff(statusOnOff);
	    		
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_status_onoff), statusOnOff);
	    		updateWeb(paramMap, param);
			}
        });
    	
    	ldValveBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
				String closeBoiler = "";
	    		if (getResources().getString(R.string.value_close_boiler_on).equals(
	    				bakBean.getCloseBoiler())) {
	    			closeBoiler = getResources().getString(R.string.value_close_boiler_off);
            		ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_small);
            		ldValveBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
	    		} else {
	    			closeBoiler = getResources().getString(R.string.value_close_boiler_on);
	    			ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_sel_small);
            		ldValveBtnText.setTextColor(getResources().getColor(R.color.color_white));
	    		}
	    		
    			Map<String, String> param = new HashMap<String, String>();
	    		param.put(Constants.CLOSE_BOILER, closeBoiler);
	    		bakBean.setCloseBoiler(closeBoiler);
	    		
	    		RequestParams paramMap = new RequestParams();
	    		paramMap.put(getResources().getString(R.string.param_close_boiler), closeBoiler);
	    		updateWeb(paramMap, param);
			}
        });
    	
    	scheduleButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				FragmentTransaction transaction = getFragmentManager().beginTransaction();
				transaction.setCustomAnimations(
						R.anim.push_left_in, R.anim.push_left_out);
				//�滻fragment
				LingDongTempScheduleFragment fragment = new LingDongTempScheduleFragment();
				Bundle nBundle = new Bundle();  	
    			nBundle.putString(Constants.MAC, mac);	
    			fragment.setArguments(nBundle);
				transaction.replace(R.id.fragment_container, fragment);
				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
				transaction.addToBackStack(null);
				
				// Commit the transaction
				transaction.commit();
			}
        });
    }
    
    private void backEvent() {
    	
		ldBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
		
		ldLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    }
        
    private void setDataToPage() {
//    	Map<String, String> param = new HashMap<String, String>();
//    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
//    	param.put(Constants.MAC, mac);
//    	param.put(Constants.IS_ONLINE, "1");
//    	db.getTDevice().update(param);
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	ldTempBean = db.getTLingDongTemp().findOne(arg);
    	
    	try {
    		String ldTempOut = Util.getTempDegree(ldTempBean.getTempOut());
    		if (CheckUtil.requireCheck(ldTempOut) && !"0".equals(ldTempOut)) {
    			ldDefaultOut = Double.valueOf(ldTempOut).doubleValue();
    		} else {
    			ldDefaultOut = ldOut;
    		}	
    	} catch (Exception e) {
    		if (ldDefaultOut == 0) {
    			ldDefaultOut = ldOut;
    		}
    	}
    	
    	try {
    		String ldTempEnergy = Util.getTempDegree(ldTempBean.getTempEnergy());
    		if (CheckUtil.requireCheck(ldTempEnergy) && !"0".equals(ldTempEnergy)) {
    			ldDefaultEnergy = Double.valueOf(ldTempEnergy).doubleValue();
    		} else {
    			ldDefaultEnergy = ldEnergy;
    		}
    	} catch (Exception e) {
    		if (ldDefaultEnergy == 0) {
    			ldDefaultEnergy = ldEnergy;
    		}
    	}
    	
    	try {
    		String ldTempComfort = Util.getTempDegree(ldTempBean.getTempComfort());
    		if (CheckUtil.requireCheck(ldTempComfort) && !"0".equals(ldTempComfort)) {
    			ldDefaultComfort = Double.valueOf(ldTempComfort).doubleValue();
    		} else {
    			ldDefaultComfort = ldComfort;
    		}
    	} catch (Exception e) {
    		if (ldDefaultComfort == 0) {
    			ldDefaultComfort = ldComfort;
    		}
    	}
    	
    	if (ldDefaultOut == 0) {
    		ldDefaultOut = ldOut;
    	}
    	if (ldDefaultEnergy == 0) {
    		ldDefaultEnergy = ldEnergy;
    	}
    	if (ldDefaultComfort == 0) {
    		ldDefaultComfort = ldComfort;
    	}
    	
    	
    	statusOnOff = ldTempBean.getStatusOnOff();
    	if (!CheckUtil.requireCheck(statusOnOff)) {
    		statusOnOff = getResources().getString(R.string.value_status_on);
    	}
    	status = ldTempBean.getStatus();
    	
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	setOnline();

    	String disTemp = Util.getTempDegree(ldTempBean.getDisTemp());
    	if (!CheckUtil.requireCheck(disTemp)) {
    		disTemp = "0";
    	}
    	ldTempText.setText(disTemp);
    	if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    		ldTempUnitText.setText(
					getResources().getString(R.string.value_temp_cen));
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			ldTempUnitText.setText(
					getResources().getString(R.string.value_temp_fah));
		}
        
        double heatTemp;
        if (getResources().getString(R.string.value_mod_out).equals(
				ldTempBean.getHeatMode())) {
        	heatTemp = ldDefaultOut;
        } else if (getResources().getString(R.string.value_mod_energy).equals(
				ldTempBean.getHeatMode())) {
        	heatTemp = ldDefaultEnergy;
        } else if (getResources().getString(R.string.value_mod_comfort).equals(
				ldTempBean.getHeatMode())) {
        	heatTemp = ldDefaultComfort;
        } else {
        	heatTemp = getTempHeat(ldTempBean.getTempHeat());
        }
        
        setTempValue(heatTemp);
        setTempMove(heatTemp);
        
        String linkageInput = ldTempBean.getLinkageInput();
        String linkageOutput = ldTempBean.getLinkageOutput();
        String closeBoiler = ldTempBean.getCloseBoiler();
        if (Constants.VALUE_ONLINE_ONE.equals(online)) {
        	setStatusOnOffInfo(true);
        	if (getResources().getString(R.string.value_linkage_input_on).equals(linkageInput)) {
        		linkageInputBtn.setBackgroundResource(R.drawable.linkage_input);
        	} else {
        		linkageInputBtn.setBackgroundResource(R.drawable.linkage_input_offline);
        	}
        	if (getResources().getString(R.string.value_linkage_output_on).equals(linkageOutput)) {
        		linkageOutputBtn.setBackgroundResource(R.drawable.linkage_output);
        	} else {
        		linkageOutputBtn.setBackgroundResource(R.drawable.linkage_output_offline);
        	}
        	if (getResources().getString(R.string.value_close_boiler_on).equals(closeBoiler)) {
        		ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_sel_small);
        		ldValveBtnText.setTextColor(getResources().getColor(R.color.color_white));
        	} else {
        		ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_small);
        		ldValveBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
        	}
        } else {
//        	setButtonValue(false);
        	linkageInputBtn.setBackgroundResource(R.drawable.linkage_input_offline);
        	linkageOutputBtn.setBackgroundResource(R.drawable.linkage_output_offline);
        	setModeBtnImg(true);
        	ldValveBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
        	if (getResources().getString(R.string.value_close_boiler_on).equals(closeBoiler)) {
        		ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_sel_offline_small);
        	} else {
        		ldValveBtnImg.setBackgroundResource(R.drawable.ld_btn_valve_offline_small);
        	}
        }	
        
        bakBean = (LingDongTempBean) ldTempBean.clone();
    }
    
    private double getTempHeat(String tempHeat) {
    	double heatTemp;
    	if (!CheckUtil.requireCheck(tempHeat)) {
        	heatTemp  = (double) heatDefaultTemp;
    	} else {
    		heatTemp =  Double.valueOf(Util.getTempDegree(tempHeat)).doubleValue();
    		if (heatTemp > maxTemp) {
    			heatTemp = maxTemp;
    		} else if (heatTemp < minTemp) {
    			heatTemp = minTemp;
    		}
    	}  
    	return heatTemp;
    }
    
    private void setTempMove(double temp) {
		int progress = (int) (temp-minTemp) * 100/(maxTemp - minTemp);
		ldSeekBar.setProgress(progress);
		ldSeekBarOffline.setProgress(progress);
		setTempTextLeftMoving(progress); 
    }
    
    private void setTempTextLeftMoving(int progress) {
    	LinearLayout.LayoutParams tempValueParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT); 
		int leftValueMovex = progress*screenWidth/100 - ldTempWidth/2;
		int i = (int)((40 - progress)/2.6);
		leftValueMovex = leftValueMovex + i;
    	if (leftValueMovex < 0) {
    		leftValueMovex = 0;
    	} else if (leftValueMovex + ldTempWidth > screenWidth) {
    		leftValueMovex = screenWidth - ldTempWidth;
    	}
    	tempValueParams.leftMargin = leftValueMovex;
    	ldTempValueText.setLayoutParams(tempValueParams);
    }
    
    private void setVisible() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		ldSeekBar.setVisibility(View.VISIBLE);
	    	ldSeekBarOffline.setVisibility(View.INVISIBLE);
    	} else {
    		ldSeekBar.setVisibility(View.INVISIBLE);
	    	ldSeekBarOffline.setVisibility(View.VISIBLE);
    	}
    }
    
    private void setTempValue(double temp) {
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		String value = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
    		ldTempValueText.setText(value + getResources().getString(R.string.value_temp_fah));
    		
		} else {
			String value = String.valueOf(new BigDecimal(temp).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
			ldTempValueText.setText(value + getResources().getString(R.string.value_temp_cen));
		}
    }
    
    private void updateDb(Map<String, String> param) {
    	param.put(Constants.MAC, mac);
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	db.getTLingDongTemp().update(param);
    }
    
    private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
    	webParam = param;
    	
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) { 
            	updateDb(webParam);
            	pageIsUpd = false;
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	if (reLoadPageBl) {
            		setDataToPage();
            	}
            	pageIsUpd = false;
            }   
        });
    }
    
    public static boolean getPageIsUpd () {
    	return pageIsUpd;
    }
    
    private void setOnline() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setEnable();
    		setVisible();
    		ldTitleBg.setBackgroundResource(R.drawable.pm25_title);
    		ldBackButton.setBackgroundResource(R.drawable.back_arrow);
    		ldTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg);
    		ldTempBg.setBackgroundResource(R.drawable.green_temp_bg);
    	} else {
    		setDisEnable();
    		setVisible();
    		ldTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
    		ldBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		ldPicDisplay.setBackgroundResource(R.drawable.heating_cir_offline);
    		ldTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg_offline);
    		ldTempBg.setBackgroundResource(R.drawable.green_temp_bg_offline);
    	}
    }
    
    private void setCirBg() {
    	if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
			if (getResources().getString(R.string.value_status_one).equals(status)) {
				ldPicDisplay.setBackgroundResource(R.drawable.heating_cir_start_on);
			} else {
				ldPicDisplay.setBackgroundResource(R.drawable.heating_cir_start_off);
			}
		} else {
			ldPicDisplay.setBackgroundResource(R.drawable.heating_cir_close);
		}
    }
    
    private void setEnable() {
    	setItemEnable();
    	ldOffBtnImg.setEnabled(true);
    	ldValveBtnImg.setEnabled(true);
    }

	private void setDisEnable() {
		setItemDisEnable();
		ldOffBtnImg.setEnabled(false);	
		ldValveBtnImg.setEnabled(false);
	}
	
	private void setItemEnable() {
		ldOutBtnImg.setEnabled(true);
		ldEnergyBtnImg.setEnabled(true);
		ldComfortBtnImg.setEnabled(true);
		ldSeekBar.setEnabled(true);
	}
	
	private void setItemDisEnable() {
		ldOutBtnImg.setEnabled(false);
		ldEnergyBtnImg.setEnabled(false);
		ldComfortBtnImg.setEnabled(false);
		ldSeekBar.setEnabled(false);
		ldSeekBarOffline.setEnabled(false);
	}
	
	private void setStatusOnOffInfo(boolean bl) {
//		setButtonValue(true);
		setCirBg();
		setModeBtnImg(bl);
		if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
			setItemEnable();
			ldSeekBar.setVisibility(View.VISIBLE);
			ldTempValueText.setVisibility(View.VISIBLE);
    	} else {
    		setItemDisEnable();
    		ldSeekBar.setVisibility(View.INVISIBLE);
    		ldTempValueText.setVisibility(View.INVISIBLE);
    	}
	}
	
//	private void setButtonValue(boolean bl) {
//		if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
//			ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_off));
//			ldOffBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
//			if (bl) {
//				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off);
//				
//			} else {
//				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_offline);
//			}
//    	} else {
//    		ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_on));
//    		ldOffBtnText.setTextColor(getResources().getColor(R.color.color_white));
//    		if (bl) {
//    			ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_sel);
//			} else {
//				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_offline_sel);
//			}
//    	}
//	}
	
	private void setModeBtnImg(boolean bl) {
		String heatMode = "";
		if (bl) {
			heatMode = ldTempBean.getHeatMode();
		} else {
			heatMode = bakBean.getHeatMode();
		}
		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_small);
				ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_off));
				ldOffBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				if (getResources().getString(R.string.value_mod_out).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_sel_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.color_white));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				} else if (getResources().getString(R.string.value_mod_energy).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_sel_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.color_white));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				} else if (getResources().getString(R.string.value_mod_comfort).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_sel_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_white));
				} else {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_grey));
				}
			} else {
				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_sel_small);
				ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_on));
				ldOffBtnText.setTextColor(getResources().getColor(R.color.color_white));
				ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_small);
				ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_small);
				ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_small);
				ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_grey));
			}
		} else {
			if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_offline_small);
				ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_off));
				ldOffBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				if (getResources().getString(R.string.value_mod_out).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_sel_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.color_white));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				} else if (getResources().getString(R.string.value_mod_energy).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_sel_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.color_white));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				} else if (getResources().getString(R.string.value_mod_comfort).equals(
						heatMode)) {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_sel_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_white));
				} else {
					ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_small);
					ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_small);
					ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_small);
					ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
					ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_grey));
				}
			} else {
				ldOffBtnImg.setBackgroundResource(R.drawable.ld_btn_off_offline_sel_small);
				ldOffBtnText.setText(getResources().getString(R.string.lang_heating_btn_value_on));
				ldOffBtnText.setTextColor(getResources().getColor(R.color.color_white));
				ldOutBtnImg.setBackgroundResource(R.drawable.ld_btn_out_offline_small);
				ldEnergyBtnImg.setBackgroundResource(R.drawable.ld_btn_energy_offline_small);
				ldComfortBtnImg.setBackgroundResource(R.drawable.ld_btn_comfort_offline_small);
				ldOutBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				ldEnergyBtnText.setTextColor(getResources().getColor(R.color.lingdong_grey));
				ldComfortBtnText.setTextColor(getResources().getColor(R.color.color_grey));
			}
		}
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
}