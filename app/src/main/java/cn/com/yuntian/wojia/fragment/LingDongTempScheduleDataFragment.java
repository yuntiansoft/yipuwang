package cn.com.yuntian.wojia.fragment;

import java.util.Arrays;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;


import com.wangjie.wheelview.WheelView;
;import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.LingDongTempBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * LingDongTempScheduleDataFragment
 * 
 * @author chenwh
 *
 */
public class LingDongTempScheduleDataFragment extends Fragment {
	
	private TextView timeStartText, timeLeaveText, timeReturnText, timeSleepText;
	
	private TextView tempStartText, tempLeaveText, tempReturnText, tempSleepText;
	
	private ImageView timeStartImg, timeLeaveImg, timeReturnImg, timeSleepImg;
	
	private ImageView tempStartImg, tempLeaveImg, tempReturnImg, tempSleepImg;
	
	private LinearLayout timeStartLayout, timeLeaveLayout, timeReturnLayout, timeSleepLayout;
	
	private LinearLayout tempStartLayout, tempLeaveLayout, tempReturnLayout, tempSleepLayout;
	
	private String mac;
	
	private String week;
	
	private HailinDB db;
	
	private int hour;
	
	private int minute;
	
	private String[] tempData;
	
	private int selected = 0;
	
	private String tempSchedule = "";
	
	private String defalutStartTime = "07:00";
	
	private String defalutLeaveTime = "08:00";
	
	private String defalutReturnTime = "18:00";
	
	private String defalutSleepTime = "21:00";
	
	private String defalutTempCen = "25.0";
	
	private String defalutTempFah = "77";

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		return inflater.inflate(R.layout.lingdong_temp_schedule_data_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();   

        Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);	
		week = bundle.getString(Constants.WEEK);
		
		db = new HailinDB(getActivity());
		
		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			tempData = getResources().getStringArray(R.array.lingdong_defalut_temp_fah);
		} else {
			tempData = getResources().getStringArray(R.array.lingdong_defalut_temp_cen);
		}
        
		initItem();
		setDataToPage();
		itemEvent();
		
    }
    
    private void initItem() {
    	timeStartText = (TextView) getView().findViewById(R.id.timeStartText);
    	timeLeaveText = (TextView) getView().findViewById(R.id.timeLeaveText);
    	timeReturnText = (TextView) getView().findViewById(R.id.timeReturnText);
    	timeSleepText = (TextView) getView().findViewById(R.id.timeSleepText);
    	tempStartText = (TextView) getView().findViewById(R.id.tempStartText);
    	tempLeaveText = (TextView) getView().findViewById(R.id.tempLeaveText);
    	tempReturnText = (TextView) getView().findViewById(R.id.tempReturnText);
    	tempSleepText = (TextView) getView().findViewById(R.id.tempSleepText);
    	
    	timeStartImg = (ImageView) getView().findViewById(R.id.timeStartImg);
    	timeLeaveImg = (ImageView) getView().findViewById(R.id.timeLeaveImg);
    	timeReturnImg = (ImageView) getView().findViewById(R.id.timeReturnImg);
    	timeSleepImg = (ImageView) getView().findViewById(R.id.timeSleepImg);
    	tempStartImg = (ImageView) getView().findViewById(R.id.tempStartImg);
    	tempLeaveImg = (ImageView) getView().findViewById(R.id.tempLeaveImg);
    	tempReturnImg = (ImageView) getView().findViewById(R.id.tempReturnImg);
    	tempSleepImg = (ImageView) getView().findViewById(R.id.tempSleepImg);
    	
    	timeStartLayout = (LinearLayout) getView().findViewById(R.id.timeStartLayout);
    	timeLeaveLayout = (LinearLayout) getView().findViewById(R.id.timeLeaveLayout);
    	timeReturnLayout = (LinearLayout) getView().findViewById(R.id.timeReturnLayout);
    	timeSleepLayout = (LinearLayout) getView().findViewById(R.id.timeSleepLayout);
    	tempStartLayout = (LinearLayout) getView().findViewById(R.id.tempStartLayout);
    	tempLeaveLayout = (LinearLayout) getView().findViewById(R.id.tempLeaveLayout);
    	tempReturnLayout = (LinearLayout) getView().findViewById(R.id.tempReturnLayout);
    	tempSleepLayout = (LinearLayout) getView().findViewById(R.id.tempSleepLayout);
    }
    
    private void setDataToPage() {
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	LingDongTempBean ldTempBean = db.getTLingDongTemp().findOne(arg);
    	String[] scheData = null;
    	String online = listItemBean.getIsOnline();
    	setOnline(online);
    	
    	if (ldTempBean == null) {
    		scheData = getScheduleData(null);
    	} else {
    		tempSchedule = ldTempBean.getSchedule();
    		scheData = getScheduleData(tempSchedule);
    	}
    	
    	setText(scheData);
    }
    
    private void setText(String[] scheData) {
    	int i = -1;
    	if (getResources().getString(R.string.value_week_mon).equals(week)) {
    		i = 0;
    	} else if (getResources().getString(R.string.value_week_tues).equals(week)) {
    		i = 1;
    	} else if (getResources().getString(R.string.value_week_wed).equals(week)) {
    		i = 2;
    	} else if (getResources().getString(R.string.value_week_thur).equals(week)) {
    		i = 3;
    	} else if (getResources().getString(R.string.value_week_fri).equals(week)) {
    		i = 4;
    	} else if (getResources().getString(R.string.value_week_sat).equals(week)) {
    		i = 5;
    	} else if (getResources().getString(R.string.value_week_sun).equals(week)) {
    		i = 6;
    	}
    	
    	timeStartText.setText(scheData[i*8]);
		timeLeaveText.setText(scheData[i*8 + 2]);
		timeReturnText.setText(scheData[i*8 + 4]);
		timeSleepText.setText(scheData[i*8 + 6]);
		tempStartText.setText(scheData[i*8 + 1]);
		tempLeaveText.setText(scheData[i*8 + 3]);
		tempReturnText.setText(scheData[i*8 + 5]);
		tempSleepText.setText(scheData[i*8 + 7]);
    }
    
    private String[] getScheduleData(String schedule) {
    	String[] retStr = new String[56];
    	int times = 0;
    	if (CheckUtil.requireCheck(schedule) && schedule.length() == 168) {
    		String hourVal = "";
    		String minVal = "";
    		String tempVal = "";
    		for (int i = 0; i < 168; i++) {
    			String value = schedule.substring(i, i+1);
    			if (i%6 == 0) {
    				hourVal = value;
    			} else if (i%6 == 1) {
    				hourVal = hourVal + value;
    			} else if (i%6 == 2) {
    				minVal = value;
    			} else if (i%6 == 3) {
    				minVal = minVal + value;
    				retStr[times] = hourVal + ":" + minVal;
    				hourVal = "";
    				minVal = "";
    				times++;
    			} else if (i%6 == 4) {
    				tempVal = value;
    			} else if (i%6 == 5) {
    				tempVal = tempVal + value;
    				try {
    					int tempInteger = Integer.valueOf(tempVal);
    					if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    						tempVal = Util.centigrade2Fahrenheit(tempInteger, 0) + getResources().getString(R.string.value_temp_fah);
    					} else {
    						tempVal = tempInteger + ".0" + getResources().getString(R.string.value_temp_cen);
    					}
    				} catch (Exception e) {
    					if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    						tempVal = defalutTempFah + getResources().getString(R.string.value_temp_fah);		
    					} else {
    						tempVal = defalutTempCen + getResources().getString(R.string.value_temp_cen);	
    					}
    				}
    				retStr[times] = tempVal;
    				times++;
    				tempVal = "";
    			} 
    		}
    	} else {
    		tempSchedule = "";
    		for (int i = 1; i <= 56; i++) {
    			if (i%8 == 1) {
    				String[] strTime = defalutStartTime.split(":");
    				tempSchedule = tempSchedule + strTime[0] + strTime[1];
    				retStr[i - 1] = defalutStartTime;	
    			} else if (i%8 == 3) {
    				String[] strTime = defalutLeaveTime.split(":");
    				tempSchedule = tempSchedule + strTime[0] + strTime[1];
    				retStr[i - 1] = defalutLeaveTime;
    			} else if (i%8 == 5) {
    				String[] strTime = defalutReturnTime.split(":");
    				tempSchedule = tempSchedule + strTime[0] + strTime[1];
    				retStr[i - 1] = defalutReturnTime;
    			} else if (i%8 == 7) {
    				String[] strTime = defalutSleepTime.split(":");
    				tempSchedule = tempSchedule + strTime[0] + strTime[1];
    				retStr[i - 1] = defalutSleepTime;
    			} else {
    				tempSchedule = tempSchedule + defalutTempCen.substring(0, 2);
    				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    					retStr[i - 1] = defalutTempFah + getResources().getString(R.string.value_temp_fah);		
					} else {
						retStr[i - 1] = defalutTempCen + getResources().getString(R.string.value_temp_cen);	
					}
    			}
    		}
    	}
    	
    	return retStr;
    }
    
    private void setOnline(String online) {
    	setItemBackground(online);
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		timeStartImg.setBackgroundResource(R.drawable.schedule_time);
    		timeLeaveImg.setBackgroundResource(R.drawable.schedule_time);
    		timeReturnImg.setBackgroundResource(R.drawable.schedule_time);
    		timeSleepImg.setBackgroundResource(R.drawable.schedule_time);
    		
    		tempStartImg.setBackgroundResource(R.drawable.schedule_temp);
    		tempLeaveImg.setBackgroundResource(R.drawable.schedule_temp);
    		tempReturnImg.setBackgroundResource(R.drawable.schedule_temp);
    		tempSleepImg.setBackgroundResource(R.drawable.schedule_temp);
    	} else {
    		timeStartImg.setBackgroundResource(R.drawable.schedule_time_offline);
    		timeLeaveImg.setBackgroundResource(R.drawable.schedule_time_offline);
    		timeReturnImg.setBackgroundResource(R.drawable.schedule_time_offline);
    		timeSleepImg.setBackgroundResource(R.drawable.schedule_time_offline);
    		
    		tempStartImg.setBackgroundResource(R.drawable.schedule_temp_offline);
    		tempLeaveImg.setBackgroundResource(R.drawable.schedule_temp_offline);
    		tempReturnImg.setBackgroundResource(R.drawable.schedule_temp_offline);
    		tempSleepImg.setBackgroundResource(R.drawable.schedule_temp_offline);
    	}
    }
    
    private void setItemBackground(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		setItemEnable(true);
    	} else {
    		setItemEnable(false);
    	}
    }
    
    private void setItemEnable(boolean bl) {
    	timeStartText.setEnabled(bl);
    	timeLeaveText.setEnabled(bl);
    	timeReturnText.setEnabled(bl);
    	timeSleepText.setEnabled(bl);
    	
    	tempStartText.setEnabled(bl);
    	tempLeaveText.setEnabled(bl);
    	tempReturnText.setEnabled(bl);
    	tempSleepText.setEnabled(bl);
    }
    
    private void itemEvent() {
    	timeStartLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String strTime = timeStartText.getText().toString();
				hour = Integer.valueOf(strTime.substring(0,2));
				minute = Integer.valueOf(strTime.substring(3));
//				new TimePickerFragment() {
//					 @Override
//					 public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//						 String disHour = String.valueOf(hourOfDay);
//						 if (hourOfDay < 10) {
//							 disHour = "0" + disHour;
//						 }
//						 String disMin = String.valueOf(minute);
//						 if (minute < 10) {
//							 disMin = "0" + minute;
//						 }
//						 String disTime = disHour + ":" + disMin;
//						 timeStartText.setText(disTime);
//					 }
//				}.show(getFragmentManager(), "timePicker");
			}
        });
    	
    	timeLeaveLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String strTime = timeLeaveText.getText().toString();
				hour = Integer.valueOf(strTime.substring(0,2));
				minute = Integer.valueOf(strTime.substring(3));
//				new TimePickerFragment() {
//					 @Override
//					 public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//						 String disHour = String.valueOf(hourOfDay);
//						 if (hourOfDay < 10) {
//							 disHour = "0" + disHour;
//						 }
//						 String disMin = String.valueOf(minute);
//						 if (minute < 10) {
//							 disMin = "0" + minute;
//						 }
//						 String disTime = disHour + ":" + disMin;
//						 timeLeaveText.setText(disTime);
//					 }
//				}.show(getFragmentManager(), "timePicker");
			}
        });
    	
    	timeReturnLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String strTime = timeReturnText.getText().toString();
				hour = Integer.valueOf(strTime.substring(0,2));
				minute = Integer.valueOf(strTime.substring(3));
//				new TimePickerFragment() {
//					 @Override
//					 public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//						 String disHour = String.valueOf(hourOfDay);
//						 if (hourOfDay < 10) {
//							 disHour = "0" + disHour;
//						 }
//						 String disMin = String.valueOf(minute);
//						 if (minute < 10) {
//							 disMin = "0" + minute;
//						 }
//						 String disTime = disHour + ":" + disMin;
//						 timeReturnText.setText(disTime);
//					 }
//				}.show(getFragmentManager(), "timePicker");
			}
        });
    	
    	timeSleepLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				String strTime = timeSleepText.getText().toString();
				hour = Integer.valueOf(strTime.substring(0,2));
				minute = Integer.valueOf(strTime.substring(3));
//				new TimePickerFragment() {
//					 @Override
//					 public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//						 String disHour = String.valueOf(hourOfDay);
//						 if (hourOfDay < 10) {
//							 disHour = "0" + disHour;
//						 }
//						 String disMin = String.valueOf(minute);
//						 if (minute < 10) {
//							 disMin = "0" + minute;
//						 }
//						 String disTime = disHour + ":" + disMin;
//						 timeSleepText.setText(disTime);
//					 }
//				}.show(getFragmentManager(), "timePicker");
			}
        });
    	
    	tempStartLayout.setOnClickListener(new OnClickListener() {
 			@SuppressLint("InflateParams")
			@Override
 			public void onClick(View v) {	
 				 View outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
 				 WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                 wv.setOffset(2);
                 wv.setItems(Arrays.asList(tempData));
                 selected = getSelectedIndex(tempStartText);
                 wv.setSeletion(selected);
                 selected = selected + 2;
                 wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                     @Override
                     public void onSelected(int selectedIndex, String item) {
                    	 selected = selectedIndex;
                     }
                 });

                 new AlertDialog.Builder(getActivity())
                         .setTitle(getResources().getString(R.string.lang_txt_temp_select))
                         .setView(outerView)
                         .setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
                             new DialogInterface.OnClickListener() {   
		                 			@Override
		                 			public void onClick(DialogInterface dialog, int which) {
		                 				dialog.dismiss();
		                 				tempStartText.setText(tempData[selected - 2]);
		                 			}
                             })
                         .setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null)
                         .show();
 				
 			}
 		});	
    	
    	tempLeaveLayout.setOnClickListener(new OnClickListener() {
 			@SuppressLint("InflateParams")
			@Override
 			public void onClick(View v) {	
 				 View outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
 				 WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                 wv.setOffset(2);
                 wv.setItems(Arrays.asList(tempData));
                 selected = getSelectedIndex(tempLeaveText);
                 wv.setSeletion(selected);
                 selected = selected + 2;
                 wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                     @Override
                     public void onSelected(int selectedIndex, String item) {
                    	 selected = selectedIndex;
                     }
                 });

                 new AlertDialog.Builder(getActivity())
                         .setTitle(getResources().getString(R.string.lang_txt_temp_select))
                         .setView(outerView)
                         .setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
                             new DialogInterface.OnClickListener() {   
		                 			@Override
		                 			public void onClick(DialogInterface dialog, int which) {
		                 				dialog.dismiss();
		                 				tempLeaveText.setText(tempData[selected - 2]);
		                 			}
                             })
                         .setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null)
                         .show();
 				
 			}
 		});	
    	
    	tempReturnLayout.setOnClickListener(new OnClickListener() {
 			@SuppressLint("InflateParams")
			@Override
 			public void onClick(View v) {	
 				 View outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
 				 WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                 wv.setOffset(2);
                 wv.setItems(Arrays.asList(tempData));
                 selected = getSelectedIndex(tempReturnText);
                 wv.setSeletion(selected);
                 selected = selected + 2;
                 wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                     @Override
                     public void onSelected(int selectedIndex, String item) {
                    	 selected = selectedIndex;
                     }
                 });

                 new AlertDialog.Builder(getActivity())
                         .setTitle(getResources().getString(R.string.lang_txt_temp_select))
                         .setView(outerView)
                         .setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
                             new DialogInterface.OnClickListener() {   
		                 			@Override
		                 			public void onClick(DialogInterface dialog, int which) {
		                 				dialog.dismiss();
		                 				tempReturnText.setText(tempData[selected - 2]);
		                 			}
                             })
                         .setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null)
                         .show();
 				
 			}
 		});
    	
    	tempSleepLayout.setOnClickListener(new OnClickListener() {
 			@SuppressLint("InflateParams")
			@Override
 			public void onClick(View v) {	
 				 View outerView = LayoutInflater.from(getActivity()).inflate(R.layout.wheel_view, null);
 				 WheelView wv = (WheelView) outerView.findViewById(R.id.wheel_view_wv);
                 wv.setOffset(2);
                 wv.setItems(Arrays.asList(tempData));
                 selected = getSelectedIndex(tempSleepText);
                 wv.setSeletion(selected);
                 selected = selected + 2;
                 wv.setOnWheelViewListener(new WheelView.OnWheelViewListener() {
                     @Override
                     public void onSelected(int selectedIndex, String item) {
                    	 selected = selectedIndex;
                     }
                 });

                 new AlertDialog.Builder(getActivity())
                         .setTitle(getResources().getString(R.string.lang_txt_temp_select))
                         .setView(outerView)
                         .setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
                             new DialogInterface.OnClickListener() {   
		                 			@Override
		                 			public void onClick(DialogInterface dialog, int which) {
		                 				dialog.dismiss();
		                 				tempSleepText.setText(tempData[selected - 2]);
		                 			}
                             })
                         .setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null)
                         .show();
 				
 			}
 		});
    }
    
    private int getSelectedIndex(TextView v) {
    	int index = 0;
    	String str = v.getText().toString();
    	for (int i = 0; i < tempData.length; i++) {
    		if (tempData[i].equals(str)) {
    			index = i;
    			break;
    		}
    	}
    	
    	return index;
    }
    
//    public class TimePickerFragment extends DialogFragment implements
//			TimePickerDialog.OnTimeSetListener {
//
//		@Override
//		public Dialog onCreateDialog(Bundle savedInstanceState) {
//			TimePickerDialog dialog = new TimePickerDialog(getActivity(), this, hour, minute,
//					DateFormat.is24HourFormat(getActivity()));
//			dialog.setTitle(getResources().getString(R.string.lang_txt_date_select));
//			return dialog;
//
//	//		return new TimePickerDialog(getActivity(), this, hour, minute,
//	//			DateFormat.is24HourFormat(getActivity()));
//		}
//
//		@Override
//		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//
//		}
//	}
    
}