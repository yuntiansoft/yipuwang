package cn.com.yuntian.wojia.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.http.Header;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
;import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.LingDongTempBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * LingDongTempFragment
 * 
 * @author chenwh
 *
 */
public class LingDongTempScheduleFragment extends Fragment {
	
//	private LinearLayout ldLayoutView;
	
	private FrameLayout ldTitleBg;
	
	private TextView ldBackButton, ldSaveButton;
	
	private TextView firstText, secondText, thirdText, fourthText, fifthText, sixthText, seventhText;
	
	private View firstLineView, secondLineView, thirdLineView, fourthLineView, fifthLineView, sixthLineView, seventhLineView;
	
//	private View lineView;
	
//	private ViewPager vPager = null;
	
	private ViewPager vPager = null;
	
	private ArrayList<Fragment> fragmentList = null;
	
	private HailinDB db;
	
	private String mac;
	
	private MyFragmentPagerAdapter mAdapter;
	
	private Map<String, String> webParam;
	
	private boolean[] weekSelected;
	
	private String[] weekData;

	private String tempSchedule = "";
	
	private int currentPage = 1;
	
	private int week;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		return inflater.inflate(R.layout.lingdong_temp_schedule_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        db = new HailinDB(getActivity());
		
		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		
		weekData = getResources().getStringArray(R.array.lingdong_week);
        
		initItem();
		InitViewPager();
        setDataToPage();
        itemEvent(); 
//        bTaskBl = true;
//        bTask = new BackgroundTask(
//				getActivity(), 
//				db,
//				mac,
//				Constants.LINGDONG_TEMP_SCHEDULE_FRAGMENT) {
//        	@Override
//        	public void exeTask() {
//        		if (bTask.isTaskContinue() && isForeRun) {
//        			setDataToPage(); 
//            	} 
//        		
//        		if (isForeRun) {
//        			if (!bTaskBl) {
//        				bTaskBl = true;
//        				handler.postDelayed(runnable, 5000);
//        			}
//        		}
//        	}
//        };
//		isForeRun = true;
//
//		handler.post(runnable);	
		
    }
    
    private void initItem() {
    	ldTitleBg = (FrameLayout) getView().findViewById(R.id.ldTitleBg);
    	ldBackButton = (TextView) getView().findViewById(R.id.ldBackButton);
    	ldSaveButton = (TextView) getView().findViewById(R.id.ldSaveButton);
    	firstText = (TextView) getView().findViewById(R.id.firstText);
    	secondText = (TextView) getView().findViewById(R.id.secondText);
    	thirdText = (TextView) getView().findViewById(R.id.thirdText);
    	fourthText = (TextView) getView().findViewById(R.id.fourthText);
    	fifthText = (TextView) getView().findViewById(R.id.fifthText);
    	sixthText = (TextView) getView().findViewById(R.id.sixthText);
    	seventhText = (TextView) getView().findViewById(R.id.seventhText);
    	firstLineView = (View) getView().findViewById(R.id.firstLineView);
    	secondLineView = (View) getView().findViewById(R.id.secondLineView);
    	thirdLineView = (View) getView().findViewById(R.id.thirdLineView);
    	fourthLineView = (View) getView().findViewById(R.id.fourthLineView);
    	fifthLineView = (View) getView().findViewById(R.id.fifthLineView);
    	sixthLineView = (View) getView().findViewById(R.id.sixthLineView);
    	seventhLineView = (View) getView().findViewById(R.id.seventhLineView);
    	
    	vPager = (ViewPager) getView().findViewById(R.id.vPager);
//    	lineView =  (View) getView().findViewById(R.id.lineView);
    	
    	ldTitleBg = (FrameLayout) getView().findViewById(R.id.ldTitleBg);
    }
    
    /*
	 * ��ʼ��ViewPager
	 */
	private void InitViewPager(){
		fragmentList = new ArrayList<Fragment>();
		Fragment leftFragment = new LingDongTempScheduleDataFragment();
		Bundle leftArgs = new Bundle();
		leftArgs.putString(Constants.MAC, mac);
		leftArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_sun));
		leftFragment.setArguments(leftArgs);
		fragmentList.add(leftFragment);
		
		Fragment monFragment = new LingDongTempScheduleDataFragment();
		Bundle monArgs = new Bundle();
		monArgs.putString(Constants.MAC, mac);
		monArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_mon));
		monFragment.setArguments(monArgs);
		fragmentList.add(monFragment);
		
		Fragment tuesFragment = new LingDongTempScheduleDataFragment();
		Bundle tuesArgs = new Bundle();
		tuesArgs.putString(Constants.MAC, mac);
		tuesArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_tues));
		tuesFragment.setArguments(tuesArgs);
		fragmentList.add(tuesFragment);
		
		Fragment wedFragment = new LingDongTempScheduleDataFragment();
		Bundle wedArgs = new Bundle();
		wedArgs.putString(Constants.MAC, mac);
		wedArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_wed));
		wedFragment.setArguments(wedArgs);
		fragmentList.add(wedFragment);
		
		Fragment thurFragment = new LingDongTempScheduleDataFragment();
		Bundle thurArgs = new Bundle();
		thurArgs.putString(Constants.MAC, mac);
		thurArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_thur));
		thurFragment.setArguments(thurArgs);
		fragmentList.add(thurFragment);
		
		Fragment friFragment = new LingDongTempScheduleDataFragment();
		Bundle friArgs = new Bundle();
		friArgs.putString(Constants.MAC, mac);
		friArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_fri));
		friFragment.setArguments(friArgs);
		fragmentList.add(friFragment);
		
		Fragment satFragment = new LingDongTempScheduleDataFragment();
		Bundle satArgs = new Bundle();
		satArgs.putString(Constants.MAC, mac);
		satArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_sat));
		satFragment.setArguments(satArgs);
		fragmentList.add(satFragment);
		
		Fragment sunFragment = new LingDongTempScheduleDataFragment();
		Bundle sunArgs = new Bundle();
		sunArgs.putString(Constants.MAC, mac);
		sunArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_sun));
		sunFragment.setArguments(sunArgs);
		fragmentList.add(sunFragment);
		
		Fragment rightFragment = new LingDongTempScheduleDataFragment();
		Bundle rightArgs = new Bundle();
		rightArgs.putString(Constants.MAC, mac);
		rightArgs.putString(Constants.WEEK, getResources().getString(R.string.value_week_mon));
		rightFragment.setArguments(rightArgs);
		fragmentList.add(rightFragment);
		
		Calendar cal = Calendar.getInstance(Locale.CHINA);
		week = cal.get(Calendar.DAY_OF_WEEK);
		week = week - 1;
		if (week == 0) {
			week = 7;
		}
		
		//��ViewPager����������
		mAdapter = new MyFragmentPagerAdapter(getChildFragmentManager(), fragmentList);
		vPager.setAdapter(mAdapter);
		currentPage = week;
		vPager.setCurrentItem(currentPage);//���õ�ǰ��ʾ��ǩҳΪ��һҳ
		vPager.setOnPageChangeListener(new MyOnPageChangeListener());//ҳ��仯ʱ�ļ�����	
		vPager.setOffscreenPageLimit(8);
	}
    
//    @Override
//    public void onStop() {
//    	isForeRun = false;
//    	super.onStop();
//    }
    
//    private Runnable runnable = new Runnable() {
//        public void run () { 
//        	bTaskBl = false;
//        	if (isForeRun) {
//        		bTask.getDataFromWeb();
//        	} 
//        }
//    };

    private void itemEvent() {
    	btnEvent();
    	backEvent();
    }
    
   
    
    private void btnEvent() {
    	firstText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 1) {
					week = 1;
					setWeekIndex();
				}
			}
    	});
    	
    	secondText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 2) {
					week = 2;
					setWeekIndex();
				}
			}
    	});
    	
    	thirdText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 3) {
					week = 3;
					setWeekIndex();
				}
			}
    	});
    	
    	fourthText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 4) {
					week = 4;
					setWeekIndex();
				}
			}
    	});
    	
    	fifthText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 5) {
					week = 5;
					setWeekIndex();
				}
			}
    	});
    	
    	sixthText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 6) {
					week = 6;
					setWeekIndex();
				}
			}
    	});
    	
    	seventhText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (week != 7) {
					week = 7;
					setWeekIndex();
				}
			}
    	});
    	
    	firstText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 1) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	secondText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 2) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	thirdText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 3) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	fourthText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 4) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	fifthText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 5) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	sixthText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 6) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
    	
    	seventhText.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				if (week != 7) {
					return false;
				}
				copyExec();
				
				return false;
			}
    	});
//    	fourthText.setOnClickListener(new OnClickListener() {
//    		@Override
// 			public void onClick(View v) {
//    			weekSelected = new boolean[]{false,false,false,false,false,false};
//    			final String[] dataSource = getWeekData().get(0);
//    			String curWeek =  getWeekData().get(1)[0];
//    			
//    			Builder builder = new AlertDialog.Builder(getActivity());  
//    			String title = getResources().getString(R.string.lang_txt_week_copy);
//    			title = title.replace("{0}", curWeek);
//                builder.setTitle(title);   
//                DialogInterface.OnMultiChoiceClickListener mutiListener =   
//                    new DialogInterface.OnMultiChoiceClickListener() {  
//                          
//                        @Override  
//                        public void onClick(DialogInterface dialogInterface,   
//                                int which, boolean isChecked) {  
//                        	weekSelected[which] = isChecked;  
//                        }  
//                    };  
//                builder.setMultiChoiceItems(dataSource, weekSelected, mutiListener);  
//                DialogInterface.OnClickListener btnListener =   
//                    new DialogInterface.OnClickListener() {  
//                        @Override  
//                        public void onClick(DialogInterface dialogInterface, int which) {    
//                            for(int i = 0; i < weekSelected.length; i++) {  
//                                if(weekSelected[i] == true) {  
//                                	distributeCopyData(dataSource[i]);
//                                }  
//                            }  
//                        }  
//                    };  
//                builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), btnListener); 
//                builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null); 
//                builder.create().show();
//    		}
//    	});
    	
    	ldSaveButton.setOnClickListener(new OnClickListener() {
    		@Override
 			public void onClick(View v) {
    			String scheData = distributeFragmentData();
 				if (!scheData.equals(tempSchedule)) {
 					Map<String, String> param = new HashMap<String, String>();
 					RequestParams paramMap = new RequestParams();
 					param.put(Constants.SCHEDULE, scheData);
 					paramMap.put(getResources().getString(R.string.param_schedule), scheData);
 					updateWeb(paramMap, param);
 				} else {
 					getFragmentManager().popBackStackImmediate();
 				}
    		}
    	});
    }
    
    private void copyExec() {
    	weekSelected = new boolean[]{false,false,false,false,false,false};
		final String[] dataSource = getWeekData();
		String curWeek =  weekData[week - 1];
		
		Builder builder = new AlertDialog.Builder(getActivity());  
		String title = getResources().getString(R.string.lang_txt_week_copy);
		title = title.replace("{0}", curWeek);
        builder.setTitle(title);   
        DialogInterface.OnMultiChoiceClickListener mutiListener =   
            new DialogInterface.OnMultiChoiceClickListener() {  
                  
                @Override  
                public void onClick(DialogInterface dialogInterface,   
                        int which, boolean isChecked) {  
                	weekSelected[which] = isChecked;  
                }  
            };  
        builder.setMultiChoiceItems(dataSource, weekSelected, mutiListener);  
        DialogInterface.OnClickListener btnListener =   
            new DialogInterface.OnClickListener() {  
                @Override  
                public void onClick(DialogInterface dialogInterface, int which) {    
                    for(int i = 0; i < weekSelected.length; i++) {  
                        if(weekSelected[i] == true) {  
                        	distributeCopyData(dataSource[i]);
                        }  
                    }  
                }  
            };  
        builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), btnListener); 
        builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null); 
        builder.create().show();
    }
    
    private String distributeFragmentData() {
    	String retStr = "";
    	for (int i = 1; i < 8; i++) {
    		Fragment fragment = mAdapter.getItem(i);
    		retStr = retStr + getFragmentData(fragment);
    	}
    	
    	return retStr;
    }
    
    private String getFragmentData(Fragment fragment) {
    	String retStr = "";
    	
    	TextView timeStartText = (TextView) fragment.getView().findViewById(R.id.timeStartText);
    	TextView timeLeaveText = (TextView) fragment.getView().findViewById(R.id.timeLeaveText);
    	TextView timeReturnText = (TextView) fragment.getView().findViewById(R.id.timeReturnText);
    	TextView timeSleepText = (TextView) fragment.getView().findViewById(R.id.timeSleepText);
    	TextView tempStartText = (TextView) fragment.getView().findViewById(R.id.tempStartText);
    	TextView tempLeaveText = (TextView) fragment.getView().findViewById(R.id.tempLeaveText);
    	TextView tempReturnText = (TextView) fragment.getView().findViewById(R.id.tempReturnText);
    	TextView tempSleepText = (TextView) fragment.getView().findViewById(R.id.tempSleepText);
    	
    	retStr = retStr + getTimeData(timeStartText);
    	retStr = retStr + getTempData(tempStartText);
    	retStr = retStr + getTimeData(timeLeaveText);
    	retStr = retStr + getTempData(tempLeaveText);
    	retStr = retStr + getTimeData(timeReturnText);
    	retStr = retStr + getTempData(tempReturnText);
    	retStr = retStr + getTimeData(timeSleepText);
    	retStr = retStr + getTempData(tempSleepText);
    	
    	return retStr;
    }
    
    private String getTimeData(TextView v) {
    	String[] tempArray = null;
    	tempArray = v.getText().toString().split(":");
    	return tempArray[0] + tempArray[1];
    }
    
    private String getTempData(TextView v) {
    	String tempValue = v.getText().toString();
    	tempValue = tempValue.substring(0, tempValue.length() - 1);
    	if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    		String val = Util.fahrenheit2Centigrade(Double.valueOf(tempValue), 0);
    		if (val.length() == 1) {
    			val = "0"+ val;
    		}
    		return val;
		} else {
			String[] str = tempValue.split("\\.");
			tempValue = str[0];
			if (tempValue.length() == 1) {
				tempValue = "0" + tempValue;
			}
			return tempValue;	
		}
    }
    
    private void distributeCopyData(String week) {
    	
    	int index = 0;
    	if (weekData[0].equals(week)) {
    		index = 1;
    	} else if (weekData[1].equals(week)) {
    		index = 2;
    	} else if (weekData[2].equals(week)) {
    		index = 3;
    	} else if (weekData[3].equals(week)) {
    		index = 4;
    	} else if (weekData[4].equals(week)) {
    		index = 5;
    	} else if (weekData[5].equals(week)) {
    		index = 6;
    	} else if (weekData[6].equals(week)) {
    		index = 7;
    	}
    	
    	Fragment fromFragment = mAdapter.getItem(currentPage);
    	Fragment toFragment = mAdapter.getItem(index);
    	copyData(fromFragment, toFragment);
    }
    
    private void copyData(Fragment fromFragment, Fragment toFragment) {
    	TextView fromTimeStartText = (TextView) fromFragment.getView().findViewById(R.id.timeStartText);
    	TextView fromTimeLeaveText = (TextView) fromFragment.getView().findViewById(R.id.timeLeaveText);
    	TextView fromTimeReturnText = (TextView) fromFragment.getView().findViewById(R.id.timeReturnText);
    	TextView fromTimeSleepText = (TextView) fromFragment.getView().findViewById(R.id.timeSleepText);
    	TextView fromTempStartText = (TextView) fromFragment.getView().findViewById(R.id.tempStartText);
    	TextView fromTempLeaveText = (TextView) fromFragment.getView().findViewById(R.id.tempLeaveText);
    	TextView fromTempReturnText = (TextView) fromFragment.getView().findViewById(R.id.tempReturnText);
    	TextView fromTempSleepText = (TextView) fromFragment.getView().findViewById(R.id.tempSleepText);
    	
    	TextView toTimeStartText = (TextView) toFragment.getView().findViewById(R.id.timeStartText);
    	TextView toTimeLeaveText = (TextView) toFragment.getView().findViewById(R.id.timeLeaveText);
    	TextView toTimeReturnText = (TextView) toFragment.getView().findViewById(R.id.timeReturnText);
    	TextView toTimeSleepText = (TextView) toFragment.getView().findViewById(R.id.timeSleepText);
    	TextView toTempStartText = (TextView) toFragment.getView().findViewById(R.id.tempStartText);
    	TextView toTempLeaveText = (TextView) toFragment.getView().findViewById(R.id.tempLeaveText);
    	TextView toTempReturnText = (TextView) toFragment.getView().findViewById(R.id.tempReturnText);
    	TextView toTempSleepText = (TextView) toFragment.getView().findViewById(R.id.tempSleepText);
    	
    	toTimeStartText.setText(fromTimeStartText.getText());
    	toTimeLeaveText.setText(fromTimeLeaveText.getText());
    	toTimeReturnText.setText(fromTimeReturnText.getText());
    	toTimeSleepText.setText(fromTimeSleepText.getText());
    	toTempStartText.setText(fromTempStartText.getText());
    	toTempLeaveText.setText(fromTempLeaveText.getText());
    	toTempReturnText.setText(fromTempReturnText.getText());
    	toTempSleepText.setText(fromTempSleepText.getText());
    }
    
    private String[] getWeekData() {
    	String[] dataSource = new String[6];
    	
    	for (int i = 0; i < weekData.length; i++) {
    		
			if (i < week - 1) {
				dataSource[i] = weekData[i];
			} else if (i > week - 1) {
				dataSource[i - 1] = weekData[i];
			}
		}

    	return dataSource;
    }
    
    private void backEvent() {
    	ldBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
 				String scheData = distributeFragmentData();
 				if (!scheData.equals(tempSchedule) && CheckUtil.requireCheck(tempSchedule)) {
 					new AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.lang_dialog_title))
                    .setMessage(getResources().getString(R.string.lang_mess_schedule_alert))
                    .setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), 
                        new DialogInterface.OnClickListener() {   
	                 			@Override
	                 			public void onClick(DialogInterface dialog, int which) {
	                 				dialog.dismiss();
	                 				getFragmentManager().popBackStackImmediate();
	                 			}
                        })
                    .setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), null)
                    .show();
 				} else {
 					getFragmentManager().popBackStackImmediate();
 				}
				
 			}
 		});	
    }
    
    private void setWeekIndex() {
//    	int index = 0;
//    	if (getResources().getString(R.string.lang_txt_sche_mon).equals(tv.getText())) {
//    		index = 1;
//		} else if (getResources().getString(R.string.lang_txt_sche_tues).equals(tv.getText())) {
//			index = 2;
//		} else if (getResources().getString(R.string.lang_txt_sche_wed).equals(tv.getText())) {
//			index = 3;
//		} else if (getResources().getString(R.string.lang_txt_sche_thur).equals(tv.getText())) {
//			index = 4;
//		} else if (getResources().getString(R.string.lang_txt_sche_fri).equals(tv.getText())) {
//			index = 5;
//		} else if (getResources().getString(R.string.lang_txt_sche_sat).equals(tv.getText())) {
//			index = 6;
//		} else if (getResources().getString(R.string.lang_txt_sche_sun).equals(tv.getText())) {
//			index = 7;
//		}
//    	
//    	if (index != 0) {
//    		currentPage = index;
//	    	vPager.setCurrentItem(currentPage, false);
//			setWeekText(index);
//    	}
		setWeekText(Constants.VALUE_ONLINE_ONE);
		currentPage = week;
    	vPager.setCurrentItem(currentPage, false);
    }
    
    private void updateDb(Map<String, String> param) {
    	param.put(Constants.MAC, mac);
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	db.getTLingDongTemp().update(param);
    }
    
    private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_upd_device).toString();
    	String url = getResources().getText(R.string.url_upd_device).toString();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
    	webParam = param;
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) { 
            	getFragmentManager().popBackStackImmediate();
            	updateDb(webParam);
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	Toast.makeText(getActivity(), 
            			getResources().getString(R.string.lang_mess_exception)
            			, Toast.LENGTH_SHORT).show();
            }   
        });
    	
    }
        
    private void setDataToPage() {
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	LingDongTempBean ldTempBean = db.getTLingDongTemp().findOne(arg);
    	if (ldTempBean != null) {
    		tempSchedule = ldTempBean.getSchedule();
    	}
    	
    	
    	String online = listItemBean.getIsOnline();
    	setEnabled(online);
    	setWeekText(online);
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		ldTitleBg.setBackgroundResource(R.drawable.pm25_title);
    	} else {
    		ldTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
    	}
    }
    
    private void setEnabled(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		ldSaveButton.setEnabled(true);
    		firstText.setEnabled(true);
			secondText.setEnabled(true);
			thirdText.setEnabled(true);
			fourthText.setEnabled(true);
			fifthText.setEnabled(true);
			sixthText.setEnabled(true);
			seventhText.setEnabled(true);
    	} else {
    		ldSaveButton.setEnabled(false);
    		firstText.setEnabled(false);
			secondText.setEnabled(false);
			thirdText.setEnabled(false);
			fourthText.setEnabled(false);
			fifthText.setEnabled(false);
			sixthText.setEnabled(false);
			seventhText.setEnabled(false);
    	}
    }
    
    private void setWeekText(String online) {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		if (week == 1) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.VISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			firstLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 2) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.VISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 3) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.VISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 4) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.VISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 5) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.VISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 6) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.VISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} else if (week == 7) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.VISIBLE);
    			seventhLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text_sel));
    		} 
    	} else {
    		if (week == 1) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.VISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			firstLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 2) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.VISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 3) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.VISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 4) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.VISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 5) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.VISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 6) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.VISIBLE);
    			seventhLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		} else if (week == 7) {
    			firstText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			secondText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			thirdText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fourthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			fifthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			sixthText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
    			seventhText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
    			firstText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			secondText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			thirdText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fourthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			fifthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			sixthText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			seventhText.setTextColor(getResources().getColor(R.color.color_schedule_week_text));
    			firstLineView.setVisibility(View.INVISIBLE);
    			secondLineView.setVisibility(View.INVISIBLE);
    			thirdLineView.setVisibility(View.INVISIBLE);
    			fourthLineView.setVisibility(View.INVISIBLE);
    			fifthLineView.setVisibility(View.INVISIBLE);
    			sixthLineView.setVisibility(View.INVISIBLE);
    			seventhLineView.setVisibility(View.VISIBLE);
    			seventhLineView.setBackgroundColor(getResources().getColor(R.color.color_schedule_week_text));
    		}
    	}
    }
    private class MyOnPageChangeListener implements OnPageChangeListener{
		
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			
		}
		
		@Override
		public void onPageScrollStateChanged(int arg0) {

		}
		
		@Override
		public void onPageSelected(int arg0) {
			if (arg0 == mAdapter.getCount() - 1) {
				currentPage = 1;
				vPager.setCurrentItem(currentPage, false);
	        } else if (arg0 == 0) {
	        	currentPage = mAdapter.getCount() - 2;
	        	vPager.setCurrentItem(currentPage, false);
	        } else {
	        	currentPage = arg0;
	        }
			week = currentPage;
			setWeekText(Constants.VALUE_ONLINE_ONE);
//			setWeekText(arg0);
		}
	}
    
//    private void setWeekText(int arg0) {
//    	
//    	if (arg0 == 1) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//		} else if (arg0 == 2) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//		} else if (arg0 == 3) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//		} else if (arg0 == 4) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//		} else if (arg0 == 5) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//		} else if (arg0 == 6) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//		} else if (arg0 == 7) {
//			firstText.setText(getResources().getString(R.string.lang_txt_sche_thur));
//	    	secondText.setText(getResources().getString(R.string.lang_txt_sche_fri));
//	    	thirdText.setText(getResources().getString(R.string.lang_txt_sche_sat));
//	    	fourthText.setText(getResources().getString(R.string.lang_txt_sche_sun));
//	    	fifthText.setText(getResources().getString(R.string.lang_txt_sche_mon));
//	    	sixthText.setText(getResources().getString(R.string.lang_txt_sche_tues));
//	    	seventhText.setText(getResources().getString(R.string.lang_txt_sche_wed));
//		}
//    }
	
	private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
		ArrayList<Fragment> list;
		public MyFragmentPagerAdapter(FragmentManager fm, ArrayList<Fragment> list) {
			super(fm);
			this.list = list;
			
		}
		
		
		@Override
		public int getCount() {
			return list.size();
		}
		
		@Override
		public Fragment getItem(int arg0) {
			return list.get(arg0);
		}
		
		@Override  
		public int getItemPosition(Object object) {  
		    return POSITION_NONE;  
		}
		
	}
    
}