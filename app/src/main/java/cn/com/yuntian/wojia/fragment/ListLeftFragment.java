package cn.com.yuntian.wojia.fragment;
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.hf.smartlink.model.WifiStatus;
import com.hf.smartlink.utils.Utils;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.activity.MainActivity;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.SlideSwitch;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.SlideListAdapter;
import cn.com.yuntian.wojia.util.AppContext;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;
//import java.util.Locale;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import android.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import java.util.Iterator;
//import com.cn.hailin.android.layout.CustomProgressDialog;

/**
 * ListLeftFragment
 * 
 * @author chenwh
 */
public class ListLeftFragment extends Fragment {
	
	private TextView leftMenuArticle, leftMenuChangePwd, leftMenuLogout, leftMenuUserNm, leftMenuWifi;
	
	private TextView leftMenuRegister;
	
	private SlideSwitch slideSwitchBtn;
	
	private LinearLayout registerLinearLayout, changePwdLinearLayout, leftMenuUserRule;

	private HailinDB db;
	
	private SlidingMenu sm;
	private TextView leftMenuWifiAp;

	// 加载布局
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.list_left_fragment, container, false);
//		return inflater.inflate(R.layout.list_left_fragment, null);
	}

    @Override
    public void onStart() {
        super.onStart();
       
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        MainActivity mActivity = (MainActivity) getActivity();
        sm = mActivity.getSlidingMenu();
        
        db = new HailinDB(getActivity());
        initView();
    }
    
    private void initView() {
 		leftMenuArticle = (TextView) getView().findViewById(R.id.leftMenuArticle);
 		leftMenuChangePwd = (TextView) getView().findViewById(R.id.leftMenuChangePwd);
 		leftMenuLogout = (TextView) getView().findViewById(R.id.leftMenuLogout);
 		leftMenuWifi = (TextView) getView().findViewById(R.id.leftMenuWifi);
 		slideSwitchBtn = (SlideSwitch) getView().findViewById(R.id.leftMenuSlideSwitchBtn); 
 		leftMenuUserNm =  (TextView) getView().findViewById(R.id.leftMenuUserNm);
 		leftMenuRegister = (TextView) getView().findViewById(R.id.leftMenuRegister);
 		registerLinearLayout = (LinearLayout) getView().findViewById(R.id.registerLinearLayout);
 		changePwdLinearLayout = (LinearLayout) getView().findViewById(R.id.changePwdLinearLayout);
 		leftMenuUserRule = (LinearLayout) getView().findViewById(R.id.leftMenuUserRule);
//		leftMenuWifiAp = (TextView) getView().findViewById(R.id.leftMenuWifiAp);
//		leftMenuWifiAp.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
//				List<ScanResult> scanResults = wm.getScanResults();
//				for (int i = 0; i < scanResults.size(); i++) {
//					if (Utils.removeDoubleQuotes(scanResults.get(i).SSID).equals(getSSid())){
//						Utils.saveLastScanResult(getActivity(),scanResults.get(i));
//						WifiStatus.getInstanse(AppContext.getContext()).load();
//						break;
//					}
//				}
//				APSmartlinkFragment fragment = new APSmartlinkFragment();
//				redirectPage(fragment,true);
//				setSlidingMenuOption();
//			}
//		});


		if (Constants.USER_TYPE_TWO.equals(UserInfo.UserInfo.getUserType())) {
 	 		registerLinearLayout.setVisibility(View.VISIBLE);
 	 		changePwdLinearLayout.setVisibility(View.GONE);
 	 		leftMenuUserRule.setVisibility(View.VISIBLE);
 	 		leftMenuUserNm.setVisibility(View.GONE);
 		} else {
 	 		leftMenuUserNm.setText(UserInfo.UserInfo.getUserName());
 	 		leftMenuUserNm.setVisibility(View.VISIBLE);
 	 		registerLinearLayout.setVisibility(View.GONE);
 	 		changePwdLinearLayout.setVisibility(View.VISIBLE);
 	 		leftMenuUserRule.setVisibility(View.GONE);
 		}
 		
 		leftMenuUserRule.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				userAlert();
 			}
 		});

 		leftMenuRegister.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				RegisterFragment registerFragment = new RegisterFragment();
 				redirectPage(registerFragment, true);
 				setSlidingMenuOption();
 			}
 		});
 		
 		leftMenuArticle.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
// 				Intent intent = new Intent();        
// 	            intent.setAction("android.intent.action.VIEW");    
// 	            Uri content_url = Uri.parse(getResources().getString(R.string.url_webview));   
// 	            intent.setData(content_url);  
// 	            startActivity(intent);
// 				WebView webView = new WebView(getActivity());
// 				WebSettings webSettings =   webView .getSettings();       
// 				webSettings.setUseWideViewPort(true);//设置此属性，可任意比例缩放
// 				webSettings.setJavaScriptEnabled(true);
// 		        //用file://来指定本地文件
//// 				webView.loadUrl("file:///android_asset/html/protocol.html");
// 				  //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
// 				webView.loadUrl("http://www.baidu.com");
// 				webView.setWebViewClient(new WebViewClient(){
// 					@Override
//	 		        public boolean shouldOverrideUrlLoading(WebView view, String url) {
//	 		            // TODO Auto-generated method stub
//	 		               //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
//	 		            view.loadUrl(url);
//	 		            return true;
//	 		        }
// 		       });
 				
 				WebviewFragment fragment = new WebviewFragment();
 				redirectPage(fragment, true);
 				setSlidingMenuOption();
 			}
 		});
 		
 		leftMenuChangePwd.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				PasswordChangeFragment pwdChangeFragment = new PasswordChangeFragment();
 				redirectPage(pwdChangeFragment, true);
 				setSlidingMenuOption();
 			}
 		});
 		
 		leftMenuWifi.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				SmartlinkFragment smartlinkFragment = new SmartlinkFragment();
				SmartlinkFragment.reloadAddGlg();
 				redirectPage(smartlinkFragment, true);
 				setSlidingMenuOption();
 			}
 		});
 		
 		leftMenuLogout.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				Builder builder = new Builder(getActivity());
 				builder.setMessage(getResources().getString(R.string.lang_dialog_logout));  
 				builder.setTitle(getResources().getString(R.string.lang_dialog_title)); 
 				builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
 						new DialogInterface.OnClickListener() {   
        			@Override
        			public void onClick(DialogInterface dialog, int which) {
        				dialog.dismiss();    
//        				db.getTUser().delete();
        				Map<String, String> param = new HashMap<String, String>();
     	 				param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
     	 				param.put(Constants.USER_STATUS, Constants.USER_STATUS_THREE);
     			        db.getTUser().update(param);
         				UserInfo.UserInfo.clear();
         	        	LoginFragment loginFragment = new LoginFragment();
         				redirectPage(loginFragment, false);	
         				setSlidingMenuOption();
        			}
        		});  
           	 
 				builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), 
 						new DialogInterface.OnClickListener() {   
	    			@Override
	    			public void onClick(DialogInterface dialog, int which) {
	    				dialog.dismiss();
	    			}
 				});  
    		
 				builder.create().show();
 			}
 		});
 		
 		slideSwitchBtn.SetOnChangedListener(new SlideSwitch.OnChangedListener() {
 			@Override
 			public void OnChanged(boolean checkState) {
 				String tempUnit = "";
 				if (checkState){  
 		            // 摄氏度被设置
 					tempUnit = Constants.TEMP_VALUE_ONE;
 		        } else {  
 		        	// 华氏度被设置
 					tempUnit = Constants.TEMP_VALUE_TWO; 
 		        }  
 				if (!UserInfo.UserInfo.getTempUnit().equals(tempUnit)) {
 					Map<String, String> param = new HashMap<String, String>();
 	 				param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
 	 				param.put(Constants.TEMP_UNIT, tempUnit);
 			        db.getTUser().update(param);
 			        UserInfo.UserInfo.setTempUnit(tempUnit);
 			        try {
	 			        if (sm != null) {
	 			        	View view = sm.getContent();
//	 			        	SlideCutListView sListView = (SlideCutListView) view.findViewById(R.id.contentList);
	 			        	GridView sListView = (GridView) view.findViewById(R.id.contentList);
	 			        	SlideListAdapter adapter = (SlideListAdapter) sListView.getAdapter();
	 			        	List<ListItemBean> mListItem = adapter.getListBean();
	 			        	setListItemBean(mListItem, getDataFromDb());
	 			        	adapter.notifyDataSetChanged();
//	 			        	if (sListView.getRVisible().size() > 0) {
//	 			        		sListView.initRVisible();
//	 	            		}
	 			        }
 			        } catch (Exception e) {
 			        	
 			        }
 			        
 				}
 			}
 		});
    }
    
    private void userAlert() {
//    	DialogUtil.showDialog(getActivity(), "please set deviceId promission", false);
    	Builder builder = new Builder(getActivity());
    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
    	builder.setMessage(getResources().getString(R.string.lang_mess_tempuser_alert));  
		builder.setNegativeButton(getResources().getString(R.string.lang_dialog_confirm), 
				new DialogInterface.OnClickListener() {   
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});  
	
		builder.create().show();
    }
    
    private List<ListItemBean> getDataFromDb() {
    	return db.getTDevice().findAllDevice();
    }
    
    private void redirectPage(Fragment fragment, boolean bl) {
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		//替换fragment
		transaction.replace(R.id.fragment_container, fragment);
		//添加到后台堆栈，也就是说能够按back键返回
		if (bl) {
			transaction.addToBackStack(null);
		}
		

		// Commit the transaction
		transaction.commit();
    }
    
    private void setSlidingMenuOption() {
        if (sm != null) {
        	sm.showContent();
        	sm.setSlidingEnabled(false);
        }
    }
    
    private void setListItemBean(List<ListItemBean> mListItem, List<ListItemBean> listBean) {
    	int j  = mListItem.size();
    	for (int i = (j-1); i >= 0; i--) {
    		mListItem.remove(i);
    	}
    	for (int i = 0; i< listBean.size(); i++) {
    		mListItem.add(listBean.get(i));
    	}
    }
	private String getSSid(){
		WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
		if(wm != null){
			WifiInfo wi = wm.getConnectionInfo();
			if(wi != null){
				String ssid = wi.getSSID();
				if(ssid.length()>2 && ssid.startsWith("\"") && ssid.endsWith("\"")){
					return ssid.substring(1,ssid.length()-1);
				}else{
					return ssid;
				}
			}
		}

		return "";
	}
}