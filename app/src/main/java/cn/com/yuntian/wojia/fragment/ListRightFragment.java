package cn.com.yuntian.wojia.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.activity.MainActivity;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.SlideListAdapter;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;
//import java.util.Iterator;
//import java.util.Locale;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import android.app.Fragment;
//import android.support.v4.app.FragmentTransaction;
//import com.cn.hailin.android.layout.CustomProgressDialog;

/**
 * ListRightFragment
 *
 * @author chenwh
 */
public class ListRightFragment extends Fragment {

    private TextView menuButton, addButton;

    private LinearLayout menuBlock, addBlock;

//	private SlideCutListView contentListView;

    private GridView contentListView;

    private SlideListAdapter contentListAdapter;

    private List<ListItemBean> mListItem = new ArrayList<ListItemBean>();
    ;

    private Handler handler = new Handler();

    private HailinDB db;

    private BackgroundTask bTask;

    private boolean isForeRun = true;

    private int delPosition;

    private SlidingMenu sm;

//	/**
//	 * 手指按下Y的坐标
//	 */
//	private int downY;
//	/**
//	 * 手指按下X的坐标
//	 */
//	private int downX;
//	
//	/**
//	 * 认为是用户滑动的最小距离
//	 */
//	private int mTouchSlop;
//	
//	private boolean isHolderSlide = false;
//	
//	private boolean isLeftSlide = false;

    // 加载布局
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//	    left = inflater.inflate(R.layout.list_left_fragment, container, false);
        return inflater.inflate(R.layout.list_right_fragment, container, false);
//		return inflater.inflate(R.layout.list_right_fragment, null);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!Util.IsHaveInternet(getActivity())) {
            Toast.makeText(getActivity().getApplicationContext(),
                    getResources().getString(R.string.lang_mess_no_net),
                    Toast.LENGTH_LONG).show();
        }

        MainActivity mActivity = (MainActivity) getActivity();
        sm = mActivity.getSlidingMenu();
        if (sm != null) {
            sm.setSlidingEnabled(true);
        }

        db = new HailinDB(getActivity());
//        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop();

//        args = this.getArguments();
//        isException = args.getBoolean("isException");	     
//        for (int i = 1; i < 6; i++) {
//        	ListItemBean tt = new ListItemBean();
//            tt.setDeviceName("heatingTemp--");
//            tt.setDeviceType("5");
//            tt.setIsOnline("1");
//            tt.setMac("1aass22334455");
//            HeatingTempBean pm25bean = new HeatingTempBean();
//            pm25bean.setSaveEnergy("1");
//            pm25bean.setStatusOnOff("1");
//            pm25bean.setTempHeat("c26");
//            pm25bean.setDisTemp("c28");
//            pm25bean.setMac("1aass22334455");
//            pm25bean.setUserName("1391650635@qq.com");
//            tt.setUserName("1391650635@qq.com");
//            tt.setHeatingTempBean(pm25bean);
//            db.getTDevice().add(tt);
//            db.getTHeatingTemp().add(pm25bean);	


//      String[] arg = {UserInfo.UserInfo.getUserName(), "123456789"};
//      FtkzyBean aBean = db.getTFtkzy().findOne(arg);
//      ListItemBean testBean = db.getTDevice().findOne(arg);
//      if (testBean == null) {
//    	  ListItemBean tt = new ListItemBean();
//          tt.setDeviceName("Ftkzy Test--");
//          tt.setDeviceType("10");
//          tt.setIsOnline("1");
//          tt.setMac("123456789");
//          tt.setUserName(UserInfo.UserInfo.getUserName());
//          db.getTDevice().add(tt);
////          Map<String, String> param = new HashMap<String, String>();
////    	  param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
////    	  param.put(Constants.MAC, "123456789");
////    	  param.put(Constants.IS_ONLINE, "1");
////          db.getTDevice().update(param);
//      }
//      if (aBean != null) {
//    	  Map param = new HashMap();
//    	  param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
//    	  param.put(Constants.MAC, "123456789");
//    	  param.put(Constants.IS_ONLINE, "1");
//    	  db.getTDevice().update(param);
//      } else {
//    	  ListItemBean tt = new ListItemBean();
//          tt.setDeviceName("Ftkzy Test--");
//          tt.setDeviceType("10");
//          tt.setIsOnline("1");
//          tt.setMac("123456789");
//          tt.setUserName(UserInfo.UserInfo.getUserName());
//          FtkzyBean bean = new FtkzyBean();
//          bean.setMac("123456789");
//          bean.setUserName(UserInfo.UserInfo.getUserName());
//          bean.setAuxiHeat("1");
//          bean.setBackTempFour("c20");
//          bean.setCollPump("0");
//          bean.setCollTempOne("c50");
//          bean.setPipePump("1");
//          bean.setTankTempFlg("0");
//          bean.setTankTempThree("c34");
//          bean.setTankTempTwo("c29");
//          db.getTDevice().add(tt);
//          db.getTFtkzy().add(bean);	
//      }
//        db.getTDevice().deleteForType(new String[]{getResources().getString(R.string.value_deviceid_a8100),
//        		getResources().getString(R.string.value_deviceid_atemp), 
//        		getResources().getString(R.string.value_deviceid_new_atemp), 
//        		getResources().getString(R.string.value_deviceid_ftkzy)});

        initView();
        setListItemBean(getDataFromDb());

        bTask = new BackgroundTask(getActivity(), db, "", "") {
            @Override
            public void exeTask() {
                if (bTask.isTaskContinue() && isForeRun) {

                    setListItemBean(getDataFromDb());
                    contentListAdapter.notifyDataSetChanged();

//            		if (contentListView.getRVisible().size() > 0) {
////            			contentListView.getChildAt(contentListView.getRVisible().get(0)
////            					- contentListView.getFirstVisiblePosition()).scrollTo(contentListView.getMHolderWidth(), 0);
//            			contentListView.initRVisible();
//            		}
                }
                handler.postDelayed(runnable, 5000);
            }
        };
        isForeRun = true;
        handler.post(runnable);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        isForeRun = false;
        super.onStop();
    }

    private Runnable runnable = new Runnable() {
        public void run() {
            if (isForeRun) {
                bTask.getDataFromWeb();
            }
        }
    };

    private void initView() {

        menuButton = (TextView) getView().findViewById(R.id.menuButton);
        addButton = (TextView) getView().findViewById(R.id.addButton);
        menuBlock = (LinearLayout) getView().findViewById(R.id.menuBlock);
        addBlock = (LinearLayout) getView().findViewById(R.id.addBlock);

        contentListView = (GridView) getView().findViewById(R.id.contentList);
        contentListAdapter = new SlideListAdapter(getActivity(), mListItem);
        contentListView.setAdapter(contentListAdapter);

// 		contentListView.setScrollEvent(contentListView);

        menuButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
// 				if (slidingLayout.getLeftLayout().getVisibility() != View.VISIBLE) {
// 					slidingLayout.getLeftLayout().setVisibility(View.VISIBLE);
// 				}
// 				if (slidingLayout.isLeftLayoutVisible()) {
// 					slidingLayout.scrollToRightLayout();
// 				} else {
// 					slidingLayout.scrollToLeftLayout();
// 				}
                if (sm != null) {
                    sm.toggle();
                }
            }
        });

        menuBlock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sm != null) {
                    sm.toggle();
                }
            }
        });

        addButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDeviceFragment fragment = new AddDeviceFragment();
                redirectPage(fragment, true);
            }
        });

        addBlock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AddDeviceFragment fragment = new AddDeviceFragment();
                redirectPage(fragment, true);
            }
        });

// 		contentListAdapter.setOnRightDeleteClickListener(new SlideListAdapter.onRightDeleteClickListener() {
//         	
//             @Override
//             public void onRightDeleteClick(View v, int position) {
//            	 delPosition = position;
//            	 Builder builder = new Builder(getActivity());
//            	 builder.setMessage(getResources().getString(R.string.lang_dialog_del_device));  
//            	 builder.setTitle(getResources().getString(R.string.lang_dialog_title)); 
//            	 builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
//  						new DialogInterface.OnClickListener() {   
//         			@Override
//         			public void onClick(DialogInterface dialog, int which) {
//         				dialog.dismiss();    
////         				contentListView.initRVisible();
////         				contentListView.getChildAt(delPosition-contentListView.getFirstVisiblePosition()).scrollTo(0, 0);
//         				String mac = mListItem.get(delPosition).getMac();
//         				DeleteDb(mListItem.get(delPosition));
//         				mListItem.remove(delPosition);
//         				contentListAdapter.notifyDataSetChanged(); 
////         				contentListView.setIsMenuSlide(true);
//         				delDevFromWeb(mac);
//         			}
//         		});  
//            	 
//  				builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), 
//  						new DialogInterface.OnClickListener() {   
// 	    			@Override
// 	    			public void onClick(DialogInterface dialog, int which) {
// 	    				dialog.dismiss();
// 	    			}
//  				});  
//     		
//  				builder.create().show();
//             }
// 		});

// 		contentListAdapter.setOnRightRenameClickListener(new SlideListAdapter.onRightRenameClickListener() {
//         	
//            @Override
//            public void onRightRenameClick(View v, int position) {
//            	Bundle nBundle = new Bundle();  
//    			nBundle.putString(Constants.DIS_DEV_NAME, mListItem.get(position).getDeviceName());		
//    			nBundle.putString(Constants.MAC, mListItem.get(position).getMac());	
//    			DeviceRenameFragment devNmFragment = new DeviceRenameFragment();
//    			devNmFragment.setArguments(nBundle);
//    			redirectPage(devNmFragment, true);
//            }
//        });

// 		contentListAdapter.setHolderOntouchListener(new SlideListAdapter.holderOntouchListener() {
//			
//			@Override
//			public boolean holderOntouch(View v, MotionEvent ev, int position) {
//				final int action = ev.getAction();
//				switch (action) {
//				
//				case MotionEvent.ACTION_DOWN:
////					Log.wtf("holder---ontouch", "down");
//					downX = (int) ev.getX();
//					downY = (int) ev.getY();
//					contentListView.getParent().requestDisallowInterceptTouchEvent(true);
//					break;
//					
//				case MotionEvent.ACTION_MOVE:
////					Log.wtf("holder---ontouch", "move");
////					Log.wtf("holder---ontouch", "move---disX---" + (downX - ev.getX()));
////					Log.wtf("holder---ontouch", "move---disY---" + (ev.getY() - downY));
////					Log.wtf("holder---ontouch", "move---mTouchSlop---" + mTouchSlop);
//					if ((Math.abs(ev.getX() - downX) > mTouchSlop && Math
//									.abs(ev.getY() - downY) < mTouchSlop)) {
////						Log.wtf("holder---ontouch", "move------GGGGGGGGGGG");
////						Log.wtf("holder---ontouch", "TTTTT-----move---disX---" + (downX - ev.getX()));
//						if (downX - ev.getX() < 0) {
//							isHolderSlide = true;
////							Log.wtf("holder---ontouch", "move------true");
//						} 
//						isLeftSlide = true;
//						return true;
//					}
//
//					break;
//					
//				case MotionEvent.ACTION_UP:
////					Log.wtf("holder---ontouch", "up");
//					if (isHolderSlide) {
////						Log.wtf("holder---ontouch", "up-------TRUE");
//						contentListView.initRVisible();
//         				contentListView.getChildAt(position-contentListView.getFirstVisiblePosition()).scrollTo(0, 0);
//						isHolderSlide = false;
//						return true;
//					} else if (isLeftSlide) {
//						isLeftSlide = false;
//						return true;
//					}
//					break;
//					
//				case MotionEvent.ACTION_CANCEL:
////					Log.wtf("holder---ontouch", "cancel");
//					if (isHolderSlide) {
//						contentListView.initRVisible();
//         				contentListView.getChildAt(position-contentListView.getFirstVisiblePosition()).scrollTo(0, 0);
//						isHolderSlide = false;
//						return true;
//					} else if (isLeftSlide) {
//						isLeftSlide = false;
//						return true;
//					}
//					break;
//				}
//				
//				return false;
//			}
//		});

        contentListView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (getResources().getString(R.string.value_deviceid_pm25).equals(
                        mListItem.get(position).getDeviceType())) {
                    Pm25Fragment fragment = new Pm25Fragment();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_pm25_voc).equals(
                        mListItem.get(position).getDeviceType())) {
                    Pm25VocFragment fragment = new Pm25VocFragment();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_atemp).equals(
                        mListItem.get(position).getDeviceType())
                        || getResources().getString(R.string.value_deviceid_a8100).equals(
                        mListItem.get(position).getDeviceType())) {
                    AmericanTempFragment fragment = new AmericanTempFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DEV_TYPE, mListItem.get(position).getDeviceType());
                    selectPage(fragment, position, bundle);
                } else if (getResources().getString(R.string.value_deviceid_green).equals(
                        mListItem.get(position).getDeviceType())
                        || getResources().getString(R.string.value_deviceid_lingdong_ac).equals(
                        mListItem.get(position).getDeviceType())) {
                    GreenFragment fragment = new GreenFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.DEV_TYPE, mListItem.get(position).getDeviceType());
                    selectPage(fragment, position, bundle);

//					LingdongCoolHeatFragment fragment = new LingdongCoolHeatFragment();
//					Bundle bundle = new Bundle();
//					selectPage(fragment, position, bundle);
                } else if (getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(
                        mListItem.get(position).getDeviceType())) {
                    LingdongCoolHeatFragment fragment = new LingdongCoolHeatFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", mListItem.get(position));
                    selectPage(fragment, position, bundle);
                } else if (getResources().getString(R.string.value_deviceid_heat_temp).equals(
                        mListItem.get(position).getDeviceType())) {


                    HeatingTempFragment fragment = new HeatingTempFragment();
                    selectPage(fragment, position, null);
//                    YiPuWTempFragment fragment = new YiPuWTempFragment();
//                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_co2_voc).equals(
                        mListItem.get(position).getDeviceType())) {
                    Co2VocFragment fragment = new Co2VocFragment();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_new_atemp).equals(
                        mListItem.get(position).getDeviceType())) {

                    AmerHomepgFragment fragment = new AmerHomepgFragment();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_lingdong_temp).equals(
                        mListItem.get(position).getDeviceType())) {
                    LingDongTempFragment fragment = new LingDongTempFragment();
// 					LingDongTempFragmentForWoci fragment = new LingDongTempFragmentForWoci();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(
                        mListItem.get(position).getDeviceType())){
                    YiPuWTempFragment fragment = new YiPuWTempFragment();
// 					LingDongTempFragmentForWoci fragment = new LingDongTempFragmentForWoci();
                    selectPage(fragment, position, null);
                } else if (getResources().getString(R.string.value_deviceid_ftkzy).equals(
                        mListItem.get(position).getDeviceType())) {
                    FtkzyFragment fragment = new FtkzyFragment();
                    selectPage(fragment, position, null);
                }
            }
        });

        contentListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {

                delPosition = arg2;
                new AlertDialog.Builder(getActivity()).setTitle(getResources().getString(R.string.lang_txt_select)).setIcon(
                        android.R.drawable.ic_dialog_info).setSingleChoiceItems(
                        getResources().getStringArray(R.array.lang_item_select),
                        0,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();
                                if (which == 0) {
                                    Bundle nBundle = new Bundle();
                                    nBundle.putString(Constants.DIS_DEV_NAME, mListItem.get(delPosition).getDeviceName());
                                    nBundle.putString(Constants.MAC, mListItem.get(delPosition).getMac());
                                    DeviceRenameFragment devNmFragment = new DeviceRenameFragment();
                                    devNmFragment.setArguments(nBundle);
                                    redirectPage(devNmFragment, true);
                                } else if (which == 1) {
                                    Builder builder = new Builder(getActivity());
                                    builder.setMessage(getResources().getString(R.string.lang_dialog_del_device));
                                    builder.setTitle(getResources().getString(R.string.lang_dialog_title));
                                    builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
// 							         				contentListView.initRVisible();
// 							         				contentListView.getChildAt(delPosition-contentListView.getFirstVisiblePosition()).scrollTo(0, 0);
                                                    String mac = mListItem.get(delPosition).getMac();
                                                    DeleteDb(mListItem.get(delPosition));
                                                    mListItem.remove(delPosition);
                                                    contentListAdapter.notifyDataSetChanged();
// 							         				contentListView.setIsMenuSlide(true);
                                                    delDevFromWeb(mac);
                                                }
                                            });

                                    builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel),
                                            new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });

                                    builder.create().show();
                                }

                            }
                        }
                ).setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), null).show();

                return true;
            }

        });

    }

    private void selectPage(Fragment fragment, int position, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        bundle.putString(Constants.MAC, mListItem.get(position).getMac());
        bundle.putString(Constants.DIS_DEV_NAME, mListItem.get(position).getDeviceName());
        fragment.setArguments(bundle);
        redirectPage(fragment, true);
    }

    private void redirectPage(Fragment fragment, boolean bl) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.setCustomAnimations(
                R.anim.push_left_in, R.anim.push_left_out);
        //替换fragment
        transaction.replace(R.id.fragment_container, fragment);
        //添加到后台堆栈，也就是说能够按back键返回
        if (bl) {
            transaction.addToBackStack(null);
        }


        // Commit the transaction
        transaction.commit();
        if (sm != null) {
            sm.setSlidingEnabled(false);
        }
    }

    private List<ListItemBean> getDataFromDb() {
        return db.getTDevice().findAllDevice();
    }

    private void setListItemBean(List<ListItemBean> listBean) {
        int j = mListItem.size();
        for (int i = (j - 1); i >= 0; i--) {
            mListItem.remove(i);
        }
        for (int i = 0; i < listBean.size(); i++) {
            mListItem.add(listBean.get(i));
        }
    }

    private void delDevFromWeb(String mac) {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_del_dev).toString();
        String url = getResources().getText(R.string.url_del_dev).toString();
        RequestParams paramMap = new RequestParams();
        paramMap.put(getResources().getString(R.string.param_mac), mac);
        HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
                Log.e(this.getClass().getName(), e.getMessage(), e);
            }
        });
    }

    private void DeleteDb(ListItemBean lstBean) {
        Map<String, String> param = new HashMap<String, String>();
        param.put(Constants.MAC, lstBean.getMac());
        param.put(Constants.USER_NAME,
                UserInfo.UserInfo.getUserName());
        db.getTDevice().delete(param);
        if (getResources().getString(R.string.value_deviceid_pm25).equals(lstBean.getDeviceType())) {
            db.getTPM25().delete(param);
            db.getTHistory().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_pm25_voc).equals(lstBean.getDeviceType())) {
            db.getTPM25Voc().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_atemp).equals(lstBean.getDeviceType()) ||
                getResources().getString(R.string.value_deviceid_a8100).equals(lstBean.getDeviceType()) ||
                getResources().getString(R.string.value_deviceid_new_atemp).equals(lstBean.getDeviceType())) {
            db.getTAmerTemp().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_green).equals(lstBean.getDeviceType()) ||
                getResources().getString(R.string.value_deviceid_lingdong_ac).equals(lstBean.getDeviceType()) ||
                getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(lstBean.getDeviceType())) {
            db.getTGreenTemp().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_heat_temp).equals(lstBean.getDeviceType())) {
            db.getTHeatingTemp().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_co2_voc).equals(lstBean.getDeviceType())) {
            db.getTCo2Voc().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_lingdong_temp).equals(lstBean.getDeviceType())) {
            db.getTLingDongTemp().delete(param);
        } else if (getResources().getString(R.string.value_deviceid_ftkzy).equals(lstBean.getDeviceType())) {
            db.getTFtkzy().delete(param);
        }else if (getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(lstBean.getDeviceType())) {
            db.gettYiPuWTemp().delete(param);
        }
    }

//    private void setSlidingMenuOption() {
//        if (sm != null) {
//        	sm.setSlidingEnabled(false);
//        }
//    }
}