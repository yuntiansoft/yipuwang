package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.activity.MainActivity;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.LoginProcesss;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.EncoderHandler;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.Util;

/**
 * LoginFragment
 * 
 * @author chenwh
 *
 */
public class LoginFragment extends Fragment {

	// ��������������ı���
	private EditText etName, etPass;
	private TextView bnLogin,messageText, logLinkRegister, logLinkForPwd, logbtntemplogin;
	private ImageView logArrowForPwd, logArrowForRegister;
	
	private String user = null;

	private CustomProgressDialog progressDialog= null;
	
	private static boolean loginPageStatus = false;
	
	private HailinDB db;
	
	private String userType = null;
	
	private String userName = null;
	
	private String devId = null;
	
	private String userStatus = null;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.login_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        MainActivity mActivity = (MainActivity) getActivity();
        SlidingMenu sm = mActivity.getSlidingMenu();
        if (sm != null) {
        	sm.setSlidingEnabled(false);
        }
        
        db = new HailinDB(getActivity());
        
        loginPageStatus = true;
        etName = (EditText) getView().findViewById(R.id.logUserText);
        etPass = (EditText) getView().findViewById(R.id.logPwdText);
		bnLogin = (TextView) getView().findViewById(R.id.logbtnlogin);
		messageText = (TextView) getView().findViewById(R.id.logMessageText);
		logLinkForPwd = (TextView) getView().findViewById(R.id.logLinkForPwdText);
		logLinkRegister = (TextView) getView().findViewById(R.id.logLinkRegisterText);
//		logArrowRegister = (ImageView) getView().findViewById(R.id.logArrowRegister);
		logArrowForPwd = (ImageView) getView().findViewById(R.id.logArrowForPwd);
		logArrowForRegister = (ImageView) getView().findViewById(R.id.logArrowForRegister);
		logbtntemplogin = (TextView) getView().findViewById(R.id.logbtntemplogin);

		
		List<Map<String, String>> userInfo = db.getTUser().findAll();
		if (userInfo != null && userInfo.size() > 0) {	
			userType = userInfo.get(0).get(Constants.USER_TYPE);
			userName =  userInfo.get(0).get(Constants.USER_NAME);
			userStatus = userInfo.get(0).get(Constants.USER_STATUS);
			if (Constants.USER_TYPE_ONE.equals(userType) && 
					Constants.USER_STATUS_ONE.equals(userStatus)) {
				etName.setText(userName);
			}
		}
		
		bnLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (check()) {
					
//			        new Thread() {
//	                    public void run() {
	                    	messageText.setText("");
	    					progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
	    			        progressDialog.show();
	                    	LoginProcesss lp = new LoginProcesss(
	    			        		getActivity(), 
	    			        		user,
	    			        		etPass.getText().toString(),
	    			        		Constants.USER_TYPE_ONE,
	    			        		progressDialog,
	    			        		messageText);
	    			        lp.login();
//	                    }
//	                }.start();
			        
				}
			}				
		});
		
		logbtntemplogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				messageText.setText("");
				final TelephonyManager tm = (TelephonyManager) 
						getActivity().getSystemService(Context.TELEPHONY_SERVICE);
				boolean bl = false;

				try {
					devId = tm.getDeviceId();
					if (!CheckUtil.requireCheck(devId) || "000000000000000".equals(devId)) {
						bl = true;
					} 
				} catch (Exception e) {
					bl = true;
				}
				
				if (bl) {
					alert();
				} else {
					if (Constants.USER_TYPE_TWO.equals(userType)) {
						LoginProcesss lp = new LoginProcesss(getActivity());
						if (Constants.USER_STATUS_TWO.equals(userStatus)) {
							lp.autoLogin(false);
						} else {
							Map<String, String> params = new HashMap<String, String>();
			    	    	params.put(Constants.USER_NAME, userName);
			    	    	params.put(Constants.USER_STATUS, Constants.USER_STATUS_TWO);
			    	    	db.getTUser().update(params);
			    	    	lp.autoLogin(true);
						}
					} else if (Constants.USER_TYPE_ONE.equals(userType)) {
						userExitAlert();
					} else {
						tempUserRegister();
					}
				}
			}			
		});
		
		logLinkRegister.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectRegister();
			}
						
		});
		
//		logArrowRegister.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				redirectRegister();
//			}					
//		});
		
		logLinkForPwd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectPwdReset();
			}
						
		});
		
		logArrowForPwd.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectPwdReset();
			}					
		}); 
		
		logArrowForRegister.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectRegister();
			}					
		}); 
		
    }
    
    private boolean check() {
    	if (!requireCheck()) {
    		return false;
    	}
    	if (!emailCheck()) {
    		return false;
    	}
    	return true;
    }
    
    private void alert() {
//    	DialogUtil.showDialog(getActivity(), "please set deviceId promission", false);
    	Builder builder = new Builder(getActivity());
    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
    	builder.setMessage(getResources().getString(R.string.lang_mess_devid_noperm));  
		builder.setNegativeButton(getResources().getString(R.string.lang_dialog_confirm), 
				new DialogInterface.OnClickListener() {   
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});  
	
		builder.create().show();
    }
    
    private void userExitAlert() {
    	Builder builder = new Builder(getActivity());
    	builder.setTitle(getResources().getString(R.string.lang_dialog_title));  
    	builder.setMessage(getResources().getString(R.string.lang_mess_user_exist_alert));  
		builder.setNegativeButton(getResources().getString(R.string.lang_dialog_cancel), 
				new DialogInterface.OnClickListener() {   
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});  
		builder.setPositiveButton(getResources().getString(R.string.lang_dialog_confirm), 
					new DialogInterface.OnClickListener() {   
 			@Override
 			public void onClick(DialogInterface dialog, int which) {
 				tempUserRegister();
 			}
 		});  
	
		builder.create().show();
    }
    
    private void tempUserRegister() {
    	progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
        progressDialog.show();
        String password = EncoderHandler.encodeByMD5("yuntian" + devId);
    	LoginProcesss lp = new LoginProcesss(
        		getActivity(), 
        		devId,
        		password,
        		Constants.USER_TYPE_TWO,
        		progressDialog,
        		messageText);
        lp.tempUserRegister(devId);
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(etName.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_req));
    		return false;
    	}
    	if (TextUtils.isEmpty(etPass.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_req));
    		return false;
    	}
    	return true;
    }
    
    private boolean emailCheck() {
    	user = Util.replaceFullWidth(etName.getText().toString());
    	if (!CheckUtil.emailCheck(user)) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_valid));
    		return false;
    	}
    	return true;
    }
    
	private void redirectRegister() {
		loginPageStatus = false;
		RegisterFragment regFragment = new RegisterFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();;
		transaction.replace(R.id.fragment_container, regFragment);
//		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		if (getFragmentManager().getBackStackEntryCount() == 0) {
//			transaction.addToBackStack(this.getClass().getName());
//		}
		
		// Commit the transaction
		transaction.commit();
	}
	
	private void redirectPwdReset() {
		loginPageStatus = false;
		PasswordResetFragment pwdResFragment = new PasswordResetFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();;
		transaction.replace(R.id.fragment_container, pwdResFragment);
//		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		if (getFragmentManager().getBackStackEntryCount() == 0) {
//			transaction.addToBackStack(this.getClass().getName());
//		}
		// Commit the transaction
		transaction.commit();
	}
    
//    private void sendHttp() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_loginandroid).toString();
//    	Map<String, String> paramMap = new HashMap<String, String>();
//    	paramMap.put(getResources().getText(R.string.param_client_id).toString(), 
//    			getResources().getText(R.string.value_client_id).toString());
//    	paramMap.put(getResources().getText(R.string.param_client_secret).toString(), 
//    			getResources().getText(R.string.value_client_secret).toString());
//    	paramMap.put(getResources().getText(R.string.param_grant_type).toString(), 
//    			getResources().getText(R.string.value_grant_type).toString());
//    	paramMap.put(getResources().getText(R.string.param_user_nm).toString(), "test");
//    	paramMap.put(getResources().getText(R.string.param_password).toString(), "test");
////    	paramMap.put(getResources().getText(R.string.param_user_nm).toString(), 
////    			etName.getText().toString());
////    	paramMap.put(getResources().getText(R.string.param_password).toString(), 
////    			etPass.getText().toString());
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT) {
//    		@Override
//    		protected void onSuccess(JSONObject response) {
////    			stopProgressDialog();
////    			progressDialog.dismiss();
//    			// ����ʱ��
//    				try {
//						String tokenType = response.getString(
//								getResources().getText(R.string.param_token_type).toString());
//						String acessToken = response.getString(
//	    						getResources().getText(R.string.param_access_token).toString());
//						UserInfo.UserInfo.put(
//								getResources().getText(R.string.param_token_type).toString(), tokenType);
//						UserInfo.UserInfo.put(
//								getResources().getText(R.string.param_access_token).toString(), acessToken);
//						UserInfo.authority = tokenType + "-" + acessToken;
//						
//						ListFragment newFragment = new ListFragment();
//	    				FragmentTransaction transaction = getFragmentManager().beginTransaction();
//	    				transaction.replace(R.id.fragment_container, newFragment);
//	    				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//	    				transaction.addToBackStack(this.getClass().getName());
//
//	    				// Commit the transaction
//	    				transaction.commit();	
//	    				progressDialog.dismiss();
//	    				
//					} catch (NotFoundException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						progressDialog.dismiss();
//					} catch (JSONException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//						progressDialog.dismiss();
//					}				   			
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			progressDialog.dismiss();
//    			// �������쳣
//				DialogUtil.showDialog(getActivity(), 
//						getResources().getText(R.string.server_exception).toString(), false);
//    		}
//    	};
//    	
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, paramMap), callback);
//		} catch (UnsupportedEncodingException e1) {
//			// TODO Auto-generated catch block
//			progressDialog.dismiss();
//			// �������쳣
//			DialogUtil.showDialog(getActivity(), 
//					getResources().getText(R.string.server_exception).toString(), false);
//		}
//    }
	
	public static boolean getLoginPageStatus() {
		return loginPageStatus;
	}
	
	public static void setLoginPageStatus(boolean bl) {
		loginPageStatus = bl;
	}
}