package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class PasswordChangeFragment extends Fragment {

	// ��������������ı���
	private EditText oldPwdText, newPwdText, newPwdAgainText;
	// ���������������ť
	private TextView bnPwdChange, messageText, pwdChangeBackButton;

	private CustomProgressDialog progressDialog= null;
	
	private String userNm, password;
	
	private LinearLayout pwdChangeLayoutView;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.pwd_change_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop(); 
        oldPwdText = (EditText) getView().findViewById(R.id.oldPwdText);
        newPwdText = (EditText) getView().findViewById(R.id.newPwdText);
        newPwdAgainText = (EditText) getView().findViewById(R.id.newPwdAgainText);
        messageText = (TextView) getView().findViewById(R.id.pwdChangeMessageText);
        pwdChangeBackButton = (TextView) getView().findViewById(R.id.pwdChangeBackButton);
		// ��ȡ�����еİ�ť
        bnPwdChange = (TextView) getView().findViewById(R.id.pwdBtnChange);
        userNm = UserInfo.UserInfo.getUserName();
        password = UserInfo.UserInfo.getPassword();
        pwdChangeLayoutView = (LinearLayout) getView().findViewById(R.id.pwdChangeLayoutView);
		
        bnPwdChange.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				if (check()) {
 					messageText.setText("");
 					progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
			        progressDialog.show();
 					changePwd();
			        
 				}
 			}
 		});
        
        pwdChangeBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				getFragmentManager().popBackStackImmediate();
 			}
        });
        
        pwdChangeLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {	
					downX = (int) event.getX();
					isLeftMove = false;
					isRightMove = false;
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					if ((event.getX() - downX) > mTouchSlop) {
						isRightMove = true;
					} else if ((event.getX() - downX) < -mTouchSlop) {
						isLeftMove = true;
					}
					break;
				}
				case MotionEvent.ACTION_UP:
					if (isRightMove) {
						// ����һ������
						getFragmentManager().popBackStackImmediate();
					} else if (isLeftMove) {
	
					}
					break;
				}

				return true;
			}
 		});	
    }	
    
    private boolean check() {
    	if (!requireCheck()) {
    		return false;
    	}
    	if (!passwordCheck()) {
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(oldPwdText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_old_pwd_req));
    		return false;
    	}
    	if (TextUtils.isEmpty(newPwdText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_new_pwd_req));
    		return false;
    	}
    	if (TextUtils.isEmpty(newPwdAgainText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwdag_req));
    		return false;
    	}
    	return true;
    }
    
    private boolean passwordCheck() {
    	if (!newPwdText.getText().toString().equals(newPwdAgainText.getText().toString())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_valid));
    		return false;
    	}
    	if (oldPwdText.getText().toString().equals(newPwdText.getText().toString())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_sanme));
    		return false;
    	}
    	if (!oldPwdText.getText().toString().equals(this.password)) {
    		messageText.setText(getResources().getString(R.string.lang_mess_old_pwd_error));
    		return false;
    	}
    	if (newPwdText.getText().toString().length() < 6 
    			|| newPwdText.getText().toString().length() > 14) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_length));
    		return false;
    	}
    	return true;
    }
    
    private void changePwd() {
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getText(R.string.url_pwd_change).toString();
//    	Map<String, String> rawParams = new HashMap<String, String>();
//    	rawParams.put(getResources().getString(R.string.param_old_pwd), oldPwdText.getText().toString());
//    	rawParams.put(getResources().getString(R.string.param_password), 
//    			newPwdText.getText().toString());
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT)
//    	{
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    			// ���ĳɹ�
//    	    	UserInfo.UserInfo.put(getResources().getString(R.string.param_password), 
//    	    			newPwdText.getText().toString());
//    	    	// ����DB
//    	    	Map<String, String> params = new HashMap<String, String>();
//    	    	params.put(Constants.USER_NAME, userNm);
//    	    	params.put(Constants.PASSWORD, password);
//    	    	HailinDB db = new HailinDB(getActivity());
//    	    	db.getTUser().update(params);
//    	    	
//    	    	// ���ĳɹ�message��ʾ
//    	    	messageText.setText(getResources().getString(R.string.lang_mess_pwd_change_sucess));
// 				if (progressDialog != null) {
// 					progressDialog.dismiss();
// 				}
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//    			}
//    		}  
//    	};
//
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, rawParams), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//			
//			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//			}
//		}
    	
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getText(R.string.url_pwd_change).toString();
    	String url = getResources().getText(R.string.url_pwd_change).toString();
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_old_pwd), oldPwdText.getText().toString());
    	paramMap.put(getResources().getString(R.string.param_new_pwd), 
    			newPwdText.getText().toString());
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseString) {     					
            	// ���ĳɹ�
    	    	UserInfo.UserInfo.setPassword(newPwdText.getText().toString()); 
    	    	password = newPwdText.getText().toString();

    	    	// ����DB
    	    	Map<String, String> params = new HashMap<String, String>();
    	    	params.put(Constants.USER_NAME, userNm);
    	    	params.put(Constants.PASSWORD, password);
    	    	HailinDB db = new HailinDB(getActivity());
    	    	db.getTUser().update(params);
    	    	
    	    	// ���ĳɹ�message��ʾ
    	    	messageText.setText(getResources().getString(R.string.lang_mess_pwd_change_sucess));
 				if (progressDialog != null) {
 					progressDialog.dismiss();
 				}				    					
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
    			messageText.setText(getResources().getString(R.string.lang_mess_exception));
    			if (progressDialog != null) {
    				progressDialog.dismiss();
    			}
            }   
        });
    }

}