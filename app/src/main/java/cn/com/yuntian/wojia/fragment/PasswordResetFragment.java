package cn.com.yuntian.wojia.fragment;

import org.apache.http.Header;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.Util;


/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class PasswordResetFragment extends Fragment {

	private TextView pwdReset, messageText, pwdReslinkLogin;
	
	private EditText userName;
	
//	private ImageView pwdResArrowLogin;
	
	private String user = null;
	
	private CustomProgressDialog progressDialog= null;



	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.pwd_reset_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        pwdReset = (TextView) getView().findViewById(R.id.pwdResBtnReset);
        userName = (EditText) getView().findViewById(R.id.pwdResUserText);
        messageText = (TextView) getView().findViewById(R.id.pwdResMessageText);
        pwdReslinkLogin = (TextView) getView().findViewById(R.id.pwdResLinkLoginText);
//        pwdResArrowLogin = (ImageView) getView().findViewById(R.id.pwdResArrowLogin);
        
        pwdReset.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (check()) {
					progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
			        progressDialog.show();
					emailResend();
				}			
			}				
		});
        
        pwdReslinkLogin.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectLogin();
			}				
		});
        
//        pwdResArrowLogin.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				redirectLogin();
//			}				
//		});
    }
    
    private void redirectLogin() {
    	LoginFragment loginFragment = new LoginFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragment_container, loginFragment);
		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		transaction.addToBackStack(this.getClass().getName());
		// Commit the transaction
		transaction.commit();
    }
    
    private boolean check() {
    	if (!requireCheck()) {
    		return false;
    	}
    	if (!emailCheck()) {
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(userName.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_req));
    		return false;
    	}
    	return true;
    }
    
    private boolean emailCheck() {
    	user = Util.replaceFullWidth(userName.getText().toString());
    	if (!CheckUtil.emailCheck(user)) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_valid));
    		return false;
    	}
    	return true;
    }
    
    private void emailResend() {
//    	messageText.setText("");
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_pwd_email_resend);
//    	Map<String, String> rawParams = new HashMap<String, String>();
//    	rawParams.put(getResources().getString(R.string.param_user_nm), userName.getText().toString());
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT)
//    	{
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    			// ���ͳɹ�
//    			PasswordVerifyMailFragment pwdVefMailFragment = new PasswordVerifyMailFragment();
//    			Bundle nBundle = new Bundle();  
//    			nBundle.putString(Constants.KEY_USER_NAME, userName.getText().toString());
//    			pwdVefMailFragment.setArguments(nBundle);
// 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
// 				transaction.replace(R.id.fragment_container, pwdVefMailFragment);
// 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
// 				transaction.addToBackStack(this.getClass().getName());
// 				// Commit the transaction
// 				transaction.commit();	
// 				
// 				if (progressDialog != null) {
// 					progressDialog.dismiss();
// 				}
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    			
//    			if (e instanceof HttpResponseException) {
//    				HttpResponseException he = (HttpResponseException) e;
//    				if (getResources().getString(R.string.value_code_fourZeroOne).equals(
//    						String.valueOf(he.getStatusCode()))) {
//    					messageText.setText(getResources().getString(R.string.lang_mess_user_notexist));
//    				} else if (getResources().getString(R.string.value_code_fourZeroZero).equals(
//    						String.valueOf(he.getStatusCode()))) {
//    					messageText.setText(getResources().getString(R.string.lang_mess_user_noverify));
//    				}
//    			} else {
//    				messageText.setText(getResources().getString(R.string.lang_mess_exception));
//    			}
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//    			}
//    		} 
//    	};
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, rawParams), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//			}
//		}
    	
    	messageText.setText("");
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_pwd_email_resend);
    	String url = getResources().getString(R.string.url_pwd_email_resend);
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(
    			R.string.param_user_nm), user);	
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {     					
            	// ���ͳɹ�
    			PasswordVerifyMailFragment pwdVefMailFragment = new PasswordVerifyMailFragment();
    			Bundle nBundle = new Bundle();  
    			nBundle.putString(Constants.KEY_USER_NAME, user);
    			pwdVefMailFragment.setArguments(nBundle);
 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
 				transaction.replace(R.id.fragment_container, pwdVefMailFragment);
 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
// 				transaction.addToBackStack(this.getClass().getName());
 				// Commit the transaction
 				transaction.commit();	
 				
 				if (progressDialog != null) {
 					progressDialog.dismiss();
 				}		
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);

				if (getResources().getString(R.string.value_code_fourZeroFour).equals(
						String.valueOf(statusCode))) {
					messageText.setText(getResources().getString(R.string.lang_mess_user_notexist));
				} else if (getResources().getString(R.string.value_code_fourZeroThree).equals(
						String.valueOf(statusCode))) {
					messageText.setText(getResources().getString(R.string.lang_mess_user_noverify));
				} else {
					messageText.setText(getResources().getString(R.string.lang_mess_exception));
				}
    			
    			if (progressDialog != null) {
    				progressDialog.dismiss();
    			}
            }   
        });
    }

}