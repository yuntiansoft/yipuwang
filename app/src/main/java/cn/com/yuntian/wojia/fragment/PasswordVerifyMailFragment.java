package cn.com.yuntian.wojia.fragment;

import org.apache.http.Header;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.Util;
//import com.cn.hailin.android.httpclient.AbstractAsyncResponseListener;
//import com.cn.hailin.android.httpclient.AsyncHttpClient;
//import com.cn.hailin.android.httpclient.getRequest;
//import com.cn.hailin.android.layout.CustomProgressDialog;
//import com.cn.hailin.android.util.ProgressDialogUtil;

/**
 * PasswordVerifyMailFragment
 * 
 * @author chenwh
 *
 */
public class PasswordVerifyMailFragment extends Fragment {

	private TextView emailResend, pwdVerMailLinkLoginText, messageText;
	
//	private ImageView emlArrowExit;
	
//	private CustomProgressDialog progressDialog= null;
	
	private long oldTime = 0;
	
	private long newTime = 0;
	
	private boolean emailResendBl = true;
	
	private boolean exitFlg = true;
	
    private Handler mHandler = new Handler();//ȫ ��handler

    private int index = 0;


	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.pwd_verify_mail_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),
			        Toast.LENGTH_LONG).show(); 
		}
//        if (oldTime == 0) {
//        	oldTime = System.currentTimeMillis();
//        }
        emailResend = (TextView) getView().findViewById(R.id.pwdVerMailResend);
        pwdVerMailLinkLoginText = (TextView) getView().findViewById(R.id.pwdVerMailLinkLoginText);
        messageText = (TextView) getView().findViewById(R.id.pwdVerMailMessageText);
        
        emailResendBl = true;
        exitFlg = true;
        oldTime = 0;
        index = 0;
//        emlArrowExit = (ImageView) getView().findViewById(R.id.pwdVerMailArrowExit);
        
//        emailResend.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
////				progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
////		        progressDialog.show();
////		        oldTime = System.currentTimeMillis();
////				emailResend();
//				
////				if (oldTime == 0) {
////					oldTime = System.currentTimeMillis();
////					emailResend();
////				} else {
//					newTime = System.currentTimeMillis();
//					if (newTime - oldTime > 59000) {
//						oldTime = System.currentTimeMillis();
//						emailResend();
//					} else {
//						String msg = getResources().getString(R.string.lang_mess_delay_right);	
//						messageText.setText(msg.replace("{0}", 
//								String.valueOf((int) (60000+oldTime-newTime)/1000)));
//					}
////				}
//			}				
//		});
        
        emailResend.setOnClickListener(new TimeOnclisten());//����¼�
        
        pwdVerMailLinkLoginText.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				redirectLogin();
			}				
		});
        
//        emlArrowExit.setOnClickListener(new OnClickListener() {
//			public void onClick(View v) {
//				redirectLogin();
//			}				
//		});
    }
    
    @Override
    public void onStop() {
    	exitFlg = false;
    	super.onStop();
    }
    
    private void redirectLogin() {
    	LoginFragment loginFragment = new LoginFragment();
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
		transaction.replace(R.id.fragment_container, loginFragment);
		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//		transaction.addToBackStack(this.getClass().getName());
		// Commit the transaction
		transaction.commit();
    }
    
    private void emailResend() {
//    	messageText.setText("");
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_pwd_email_resend);
//    	Map<String, String> rawParams = new HashMap<String, String>();
//    	Bundle args = this.getArguments();
//    	String userName = args.getString(Constants.KEY_USER_NAME);	
//    	rawParams.put(getResources().getString(R.string.param_user_nm), userName);
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT)
//    	{
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    			
//    			threadSleep();
// 				if (progressDialog != null) {
// 					progressDialog.dismiss();
// 				}
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    			threadSleep();
//    			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//    			}
//    		}  
//    	};
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, rawParams), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//			threadSleep();
//			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//			}
//		}
    	
    	messageText.setText("");
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_pwd_email_resend);
    	String url = getResources().getString(R.string.url_pwd_email_resend);
    	Bundle args = this.getArguments();
    	String userName = args.getString(Constants.KEY_USER_NAME);
    	
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_user_nm), userName);
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {     					
//            	threadSleep();
// 				if (progressDialog != null) {
// 					progressDialog.dismiss();
// 				}	
            	messageText.setText(getResources().getString(R.string.lang_email_resend_msg));
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
//    			threadSleep();
    			messageText.setText(getResources().getString(R.string.lang_mess_exception));

//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//    			}
            }   
        });
    }
    
//    private void threadSleep() {
//    	long newTime = System.currentTimeMillis();
//		long sleepTime = 30000 - (newTime - oldTime);
//		if (sleepTime > 0) {
//			try {
//				Thread.sleep(sleepTime);
//			} catch (InterruptedException e) {
//				Log.e(this.getClass().getName(), e.getMessage());
//			}
//		}
//    }
    
	private class TimeOnclisten implements OnClickListener{
		@Override
		public void onClick(View v) {
			if (emailResendBl) {
				newTime =  System.currentTimeMillis();
				if (oldTime != 0) {
					index = 61 - (int) (newTime - oldTime)/1000;
					if (index < 0) {
						index = 0;
					}
				}
				if (index == 0) {
					if (exitFlg) {
						emailResend();
						oldTime = System.currentTimeMillis();
					}
				} else {
					emailResendBl = false;
					new Thread(new ClassCut()).start();//��������ʱ
				}
			}
		}
	}
	
	
	class ClassCut implements Runnable{//����ʱ�߼����߳�
		@Override
		public void run() {
			while (index > 0) {//��������ʱִ�е�ѭ��
				index--;
				if (!emailResendBl) {
					mHandler.post(new Runnable() {//ͨ������UI���߳����޸���ʾ��ʣ��ʱ��
						@Override
						public void run() {
							if (index > 0 && exitFlg) {
								String msg = getResources().getString(R.string.lang_mess_delay_right);	
								messageText.setText(msg.replace("{0}", String.valueOf(index)));
							}
						}
					});
				}
				
				try {
					if (index > 0) {
						Thread.sleep(1000);//�߳�����һ����     ������ǵ���ʱ�ļ��ʱ��
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			

			//�����ǵ���ʱ�����߼�
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					if (exitFlg) {
						oldTime = 0;
						messageText.setText("");//һ�ֵ���ʱ����  �޸�ʣ��ʱ��Ϊһ����
					}
				}
			});
				
			emailResendBl = true;
		}
	}
	
}