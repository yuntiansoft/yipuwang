package cn.com.yuntian.wojia.fragment;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.HistoryBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.PM25Bean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class Pm25Fragment extends Fragment {
	
	private TextView messageText, pm25Text, tempText, humiText, tempUnitText, pm25BackButton, pm25TitleText;
	
	private ImageView arrowImage, leftArrowImage, rightArrowImage,pm25ColCirBg, pm25LeftArrowPic, pm25RightArrowPic;
	
	private ImageView pm25TempImage, pm25HumiImage;
	
	private LinearLayout pm25LayoutView, pm25StripImg;
	
//	private CustomImageView pm25ColCirBg;
	
//	private LinearLayout stripImage;
	
	private FrameLayout pm25TitleFrame;
	
	private HailinDB db;
	
	private String mac;
	
	private String online = "";
	
	private String tempOnline = "";
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private Bitmap mBitmap;
	
	private final String colorOnline = "#37419A";
	private final String colorNotOnline = "#686868";
	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
	private GraphicalView mChartView;
	private LinearLayout chartLayout, pm25PicDisplay;
	private boolean chartBl = false;
	private TextView pm25TipText, pm25TipMessageText;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
	
	private SimpleDateFormat sdfE = new SimpleDateFormat("MMM d, yyyy h:m:s a", Locale.ENGLISH);
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private int pm25 = 0;
	
	private int averWidth = 0;
	
	private int screenWidth = 0;
	
//	private int pm25Temp = 0;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		return inflater.inflate(R.layout.pm25_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart(); 
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}

        Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
        db = new HailinDB(getActivity());
        getHistory();
        
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
        DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		averWidth = screenWidth/8;
        
        pm25Text = (TextView) getView().findViewById(R.id.pm25Text);
        messageText = (TextView) getView().findViewById(R.id.pm25MessageText);
        tempText = (TextView) getView().findViewById(R.id.pm25TempText);
        tempUnitText =  (TextView) getView().findViewById(R.id.pm25TempUnitText);
        humiText = (TextView) getView().findViewById(R.id.pm25humiText);
        arrowImage = (ImageView) getView().findViewById(R.id.pm25arrowImg);
        leftArrowImage = (ImageView) getView().findViewById(R.id.pm25LeftArrowLink);
        rightArrowImage = (ImageView) getView().findViewById(R.id.pm25RightArrowLink);
        pm25LayoutView = (LinearLayout) getView().findViewById(R.id.pm25LayoutView);
        pm25ColCirBg = (ImageView) getView().findViewById(R.id.pm25ColCirBg);
        pm25TempImage = (ImageView) getView().findViewById(R.id.pm25TempImage);
        pm25HumiImage = (ImageView) getView().findViewById(R.id.pm25HumiImage);
//        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop();  
        pm25BackButton = (TextView) getView().findViewById(R.id.pm25BackButton);
        pm25TitleText = (TextView) getView().findViewById(R.id.pm25TitleText);
        pm25StripImg = (LinearLayout) getView().findViewById(R.id.pm25StripImg);
        pm25TitleFrame =  (FrameLayout) getView().findViewById(R.id.pm25TitleFrame);
        
        pm25TipText = (TextView) getView().findViewById(R.id.pm25TipText);
//        pm25TipText.setPadding(0, 20, 0, 0);
//        if (!chartBl) {
//        	pm25TipText.setHeight(0);
//        }
        
        pm25TipMessageText = (TextView) getView().findViewById(R.id.pm25TipMessageText);
        pm25LeftArrowPic = (ImageView) getView().findViewById(R.id.pm25LeftArrowPic);
        pm25RightArrowPic = (ImageView) getView().findViewById(R.id.pm25RightArrowPic);
        
        chartLayout = (LinearLayout) getView().findViewById(R.id.chart);
        Bitmap blaCirBg = BitmapFactory.decodeResource(getResources(),  
                R.drawable.pm25_bla_cir_bg); 
    	
        LayoutParams lp = (LayoutParams) chartLayout.getLayoutParams();
        lp.height = blaCirBg.getHeight();
        lp.width = blaCirBg.getWidth();
        
        pm25TipText.setWidth(blaCirBg.getWidth());
        pm25TipMessageText.setWidth(blaCirBg.getWidth());
        
        pm25PicDisplay = (LinearLayout) getView().findViewById(R.id.pm25PicDisplay);
        
		
		String devNm = bundle.getString(Constants.DIS_DEV_NAME);
		pm25TitleText.setText(devNm);

		setDataToPage();
		setChartView();
        setLayoutEvent();
        
		bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_pm25)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
            	handler.postDelayed(runnable, 5000);
        	}
        };
		isForeRun = true;
		handler.post(runnable);	
    }
    
    private void setLayoutEvent() {
    	
    	leftArrowImage.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
 				if (chartBl) {
// 					pm25TipText.setHeight(0);
 					chartLayout.setVisibility(View.INVISIBLE);
 					pm25TipMessageText.setVisibility(View.GONE);
 					pm25LeftArrowPic.setVisibility(View.GONE);
 					pm25RightArrowPic.setVisibility(View.GONE);
 					pm25TipText.setVisibility(View.GONE);
 	 				pm25PicDisplay.setVisibility(View.VISIBLE);
 	 				rightArrowImage.setVisibility(View.VISIBLE);
 	 				messageText.setVisibility(View.VISIBLE);
 	 				chartBl = false;
 				} else {
 					getFragmentManager().popBackStackImmediate();
 				}
				

 			}
 		});	
    	
    	pm25BackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();

 			}
 		});	
    	
    	rightArrowImage.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
// 				redirectPage();
// 				pm25TipText.setHeight(60);
 				chartLayout.setVisibility(View.VISIBLE);
 				pm25TipText.setVisibility(View.VISIBLE);
 				pm25TipMessageText.setVisibility(View.VISIBLE);
 				pm25LeftArrowPic.setVisibility(View.INVISIBLE);
 				pm25RightArrowPic.setVisibility(View.INVISIBLE);
 				pm25PicDisplay.setVisibility(View.INVISIBLE);
 				rightArrowImage.setVisibility(View.INVISIBLE);
 				messageText.setVisibility(View.GONE);
 				chartBl = true;
 				if (mChartView != null) {
 					viewRepaint();
 				}
 				setTipMessages();
 			}
 		});
    	
    	pm25LayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
//				switch (event.getAction()) {
//				case MotionEvent.ACTION_DOWN: {	
//					downX = (int) event.getX();
//					isLeftMove = false;
//					isRightMove = false;
//					break;
//				}
//				case MotionEvent.ACTION_MOVE: {
//					if ((event.getX() - downX) > mTouchSlop) {
//						isRightMove = true;
//					} else if ((event.getX() - downX) < -mTouchSlop) {
//						isLeftMove = true;
//					}
//					break;
//				}
//				case MotionEvent.ACTION_UP:
//					if (isRightMove) {
//						// ����һ������
//						getFragmentManager().popBackStackImmediate();
//					} else if (isLeftMove) {
//						// PM2.5����ͼ
////						redirectPage();		
//					}
//					break;
//				}
				
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    	pm25PicDisplay.setOnTouchListener(new TouchListener());
    	mChartView.setOnTouchListener(new TouchListener());	
    }
    
    private void setChartView() {
    	mRenderer.setApplyBackgroundColor(true);
	    mRenderer.setAxisTitleTextSize(16);
	    mRenderer.setChartTitleTextSize(20);
	    mRenderer.setLabelsTextSize(15);
	    mRenderer.setLegendTextSize(15);
	    mRenderer.setYAxisMax(550);
	    mRenderer.setXAxisMax(9.5);
	    mRenderer.setMargins(new int[] { 0, 30, 0, 0 });
	    mRenderer.setPointSize(5);
	    mRenderer.setChartTitle(
	    		getResources().getString(R.string.lang_chart_pm25_title));
	    if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorOnline));
	    } else {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorNotOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorNotOnline));
	    }
	    mRenderer.setYLabelsAlign(Align.RIGHT);
	    mRenderer.setShowLegend(false);  
	    
	    mRenderer.setXLabels(0);
        mRenderer.addXTextLabel(1, 
        		getResources().getString(R.string.value_chart_abscissa_eight));
        mRenderer.addXTextLabel(2, 
        		getResources().getString(R.string.value_chart_abscissa_seven));
        mRenderer.addXTextLabel(3, 
        		getResources().getString(R.string.value_chart_abscissa_six));
        mRenderer.addXTextLabel(4, 
        		getResources().getString(R.string.value_chart_abscissa_five));
        mRenderer.addXTextLabel(5, 
        		getResources().getString(R.string.value_chart_abscissa_four));
        mRenderer.addXTextLabel(6, 
        		getResources().getString(R.string.value_chart_abscissa_three));
        mRenderer.addXTextLabel(7, 
        		getResources().getString(R.string.value_chart_abscissa_two));
        mRenderer.addXTextLabel(8, 
        		getResources().getString(R.string.value_chart_abscissa_one));
        mRenderer.addXTextLabel(9, 
        		getResources().getString(R.string.value_chart_abscissa_now));

        mRenderer.addYTextLabel(0, "0");
        mRenderer.addYTextLabel(100, "100");
        mRenderer.addYTextLabel(200, "200");
        mRenderer.addYTextLabel(300, "300");
        mRenderer.addYTextLabel(400, "400");
        mRenderer.addYTextLabel(500, "500");
	   
        setSeriesData();
        
        XYSeriesRenderer rNoDisplay = new XYSeriesRenderer();
//        rNoDisplay.setColor(Color.WHITE);
//        rNoDisplay.setPointStyle(PointStyle.POINT);
        rNoDisplay.setFillPoints(false);
        rNoDisplay.setDisplayChartValues(false);
        mRenderer.addSeriesRenderer(rNoDisplay);
        
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setColor(Color.WHITE);
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setFillPoints(true);
        renderer.setDisplayChartValues(true);
        renderer.setDisplayChartValuesDistance(10);
        mRenderer.addSeriesRenderer(renderer);
       

        if (mChartView == null) {
            mChartView = ChartFactory.getLineChartView(getActivity(), mDataset, mRenderer);
            mChartView.setBackgroundColor(Color.parseColor("#37419A"));
            mChartView.setClickable(false);
            chartLayout.addView(mChartView);
        } else {
        	viewRepaint();
        }
    }
    
    private void viewRepaint() {
    	mDataset.clear();
    	setSeriesData();
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorOnline));
	    } else {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorNotOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorNotOnline));
	    }
    	mChartView.repaint();
    }
    
    private void setSeriesData() {
    	XYSeries seriesNoDisplay = new XYSeries("");
        seriesNoDisplay.add( 0, 0);
        mDataset.addSeries(seriesNoDisplay);

        String[] args = {UserInfo.UserInfo.getUserName(), mac};
        List<HistoryBean> hList = db.getTHistory().findList(args);
        XYSeries series = new XYSeries("");
        int length = Integer.valueOf(getResources().getString(R.string.value_history_amount)).intValue();
		if (hList.size() < length) {
			length = hList.size();
		}
        for (int i = 0; i < length; i++) {
        	HistoryBean bean = hList.get(i);
        	if (CheckUtil.requireCheck(bean.getDisPm25In())) {
        		series.add(i + 1, Integer.valueOf(bean.getDisPm25In().trim()).intValue());
        	}
        }
        series.add(9, pm25);
        mDataset.addSeries(series);
//        setTipMessages();
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
        
    private void setDataToPage() {
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	PM25Bean pm25Bean = db.getTPM25().findOne(arg);
    	
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	if (!CheckUtil.requireCheck(online) || !tempOnline.equals(online)) {
    		setOnline();
    		if (mChartView != null) {
    			viewRepaint();
    		}
    	}
    	tempOnline = online;
    	
    	if (pm25Bean != null) {   		
    		if (CheckUtil.requireCheck(pm25Bean.getDisPm25In())) {
    			pm25 = Integer.valueOf(pm25Bean.getDisPm25In()).intValue();
    		}
    		pm25Text.setText(String.valueOf(pm25));
    		tempText.setText(Util.getValue(Util.getTempDegree(pm25Bean.getDisTemp()), 0));
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			tempUnitText.setText(getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			tempUnitText.setText(getResources().getString(R.string.value_temp_fah));
    		}
    		
    		humiText.setText(pm25Bean.getDisHumi() + "%");
    		if (pm25 > 99) {
    			pm25Text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
    		} else {
    			pm25Text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 90);
    		}
    		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
    				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT); 
    		int arrowWidth;
    		int angle=0;
    		if (pm25 < 301) {
    			arrowWidth = averWidth*pm25/50;	
    			angle = 270*pm25/300-360;		
    		} else if (pm25 < 500){
    			arrowWidth = averWidth*6+((screenWidth-averWidth*6)/2)*(pm25-300)/100;
    			angle = 270+90*(pm25-300)/200-360;
    		} else {
    			arrowWidth = screenWidth;
    			angle = 0;
    		}
    		params.leftMargin = arrowWidth-10; 
    		arrowImage.setLayoutParams(params);
    		setMessageText();
    		
    		
    		int drawable = 0;
    		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    			drawable = R.drawable.pm25_col_cir_bg;
        	} else {
        		drawable = R.drawable.pm25_col_cir_bg_notonline;
        	}
			mBitmap = BitmapFactory.decodeResource(
					getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true); 
	        Canvas canvas = new Canvas(mBitmap);
	        Paint paint = new Paint();  
	        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
	        paint.setAntiAlias(true);
	        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
	        canvas.drawArc(rectf, 270, angle, true, paint);
	        pm25ColCirBg.setImageBitmap(mBitmap);	
    	}
     }
    
    private void setMessageText() {
    	if (pm25 < 36) {
    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_one));
    	} else if (pm25 < 76) {
    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_two));
    	} else if (pm25 < 116) {
    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_three));
    	} else if (pm25 < 151) {
    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_four));
    	} else {
    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_five));
    	} 	
//    	} else if (pm25 < 301) {
//    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_five));
//    	} else {
//    		messageText.setText(getResources().getString(R.string.lang_pm25_tip_six));
//    	}    	
    }
    
    private class TouchListener implements OnTouchListener {

    	@SuppressLint("ClickableViewAccessibility")
    	@Override
		public boolean onTouch(View arg0, MotionEvent event) {
			
//			switch (event.getAction()) {
//			case MotionEvent.ACTION_DOWN: {	
//				downX = (int) event.getX();
//				isLeftMove = false;
//				isRightMove = false;
//				break;
//			}
//			case MotionEvent.ACTION_MOVE: {
//				if ((event.getX() - downX) > mTouchSlop) {
//					isRightMove = true;
//				} else if ((event.getX() - downX) < -mTouchSlop) {
//					isLeftMove = true;
//				}
//				break;
//			}
//			case MotionEvent.ACTION_UP:
//				if (chartBl) {
//					if (isRightMove) {
////						pm25TipText.setHeight(0);
//						pm25TipText.setVisibility(View.GONE);
//						pm25TipMessageText.setVisibility(View.GONE);
//						pm25LeftArrowPic.setVisibility(View.GONE);
//						pm25RightArrowPic.setVisibility(View.GONE);
//						chartLayout.setVisibility(View.INVISIBLE);
//		 				pm25PicDisplay.setVisibility(View.VISIBLE);
//		 				rightArrowImage.setVisibility(View.VISIBLE);
//		 				messageText.setVisibility(View.VISIBLE);
//		 				chartBl = false;
//					}
//				} else {
//					if (isRightMove) {
//						// ����pm25����
//						getFragmentManager().popBackStackImmediate();
//					} else if (isLeftMove) {
////						pm25TipText.setHeight(60);
//						pm25TipText.setVisibility(View.VISIBLE);
//						pm25TipMessageText.setVisibility(View.VISIBLE);
//						chartLayout.setVisibility(View.VISIBLE);
//						pm25LeftArrowPic.setVisibility(View.INVISIBLE);
//						pm25RightArrowPic.setVisibility(View.INVISIBLE);
//		 				pm25PicDisplay.setVisibility(View.INVISIBLE);
//		 				rightArrowImage.setVisibility(View.INVISIBLE);
//		 				messageText.setVisibility(View.GONE);
//		 				chartBl = true;
//		 				if (mChartView != null) {
//		 					viewRepaint();
//		 				}
//		 				setTipMessages();
//					}
//				}
//				
//				break;
//			}
			
			obtainVelocityTracker(event);
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: 	
					downX = (int) event.getX();
					downY = (int) event.getY();
					isLeftMove = false;
					isRightMove = false;
					break;
				case MotionEvent.ACTION_MOVE: 
					break;
				case MotionEvent.ACTION_UP:
					if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
						if ((event.getX() - downX) > mTouchSlop) {
							isRightMove = true;
						} else if ((event.getX() - downX) < -mTouchSlop) {
							isLeftMove = true;
						}
					}
					if (chartBl) {
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
//								pm25TipText.setHeight(0);
								pm25TipText.setVisibility(View.GONE);
								pm25TipMessageText.setVisibility(View.GONE);
								pm25LeftArrowPic.setVisibility(View.GONE);
								pm25RightArrowPic.setVisibility(View.GONE);
								chartLayout.setVisibility(View.INVISIBLE);
				 				pm25PicDisplay.setVisibility(View.VISIBLE);
				 				rightArrowImage.setVisibility(View.VISIBLE);
				 				messageText.setVisibility(View.VISIBLE);
				 				chartBl = false;
							} 
							releaseVelocityTracker();
						}
					} else {
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						} else if (isLeftMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
//								pm25TipText.setHeight(60);
								pm25TipText.setVisibility(View.VISIBLE);
								pm25TipMessageText.setVisibility(View.VISIBLE);
								chartLayout.setVisibility(View.VISIBLE);
								pm25LeftArrowPic.setVisibility(View.INVISIBLE);
								pm25RightArrowPic.setVisibility(View.INVISIBLE);
				 				pm25PicDisplay.setVisibility(View.INVISIBLE);
				 				rightArrowImage.setVisibility(View.INVISIBLE);
				 				messageText.setVisibility(View.GONE);
				 				chartBl = true;
				 				if (mChartView != null) {
				 					viewRepaint();
				 				}
				 				setTipMessages();
							} 
							
							releaseVelocityTracker();
						}
					}
					break;
			}

			return true;
		}
    	
    }
    
    private void setTipMessages() {
    	int i = (int) (Math.random()*10);
    	switch (i) {  
	        case 0:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_one));
	            break;
	        case 1:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_two));
	            break;  
	        case 2:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_three));
	            break;  
	        case 3:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_four));
	            break; 
	        case 4:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_five));
	            break;  
	        case 5:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_six));
	            break; 
	        case 6:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_seven));
	            break;
	        case 7:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_eight));
	            break;
	        case 8:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_nine));
	            break;
	        case 9:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_ten));
	            break;
	        default:
	        	pm25TipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_eight));
	            break;  
    	}
    } 
    	
    	
    private void setOnline() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		pm25LayoutView.setBackgroundResource(R.drawable.pm25_bg);
    		pm25BackButton.setBackgroundResource(R.drawable.back_arrow);
    		pm25PicDisplay.setBackgroundResource(R.drawable.pm25_bla_cir_bg);
    		pm25TitleFrame.setBackgroundResource(R.drawable.pm25_title);
    		pm25StripImg.setBackgroundResource(R.drawable.pm25_num_bg);
    		leftArrowImage.setBackgroundResource(R.drawable.pm25_left_arrow);
    		rightArrowImage.setBackgroundResource(R.drawable.pm25_right_arrow);
    		arrowImage.setBackgroundResource(R.drawable.pm25_valuebar_index);
    		pm25TempImage.setBackgroundResource(R.drawable.pm25_temp_bg);
    		pm25HumiImage.setBackgroundResource(R.drawable.pm25_humi_bg);
    	} else {
    		pm25LayoutView.setBackgroundResource(R.drawable.pm25_bg_notonline);
    		pm25BackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		pm25PicDisplay.setBackgroundResource(R.drawable.pm25_bla_cir_bg_notonline);
    		pm25TitleFrame.setBackgroundResource(R.drawable.pm25_title_notonline);
    		pm25StripImg.setBackgroundResource(R.drawable.pm25_num_bg_notonline);
    		leftArrowImage.setBackgroundResource(R.drawable.pm25_left_arrow_notonline);
    		rightArrowImage.setBackgroundResource(R.drawable.pm25_right_arrow_notonline);
    		arrowImage.setBackgroundResource(R.drawable.pm25_valuebar_index_notonline);
    		pm25TempImage.setBackgroundResource(R.drawable.pm25_temp_bg_notonline);
    		pm25HumiImage.setBackgroundResource(R.drawable.pm25_humi_bg_notonline);
    	}
    }
    
    private void getHistory() {
    	new Thread() {
            public void run() {
            	getPMHistory();
            }
	 
        }.start();
    }

	private void getPMHistory() {
//		String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_get_history);
		String url = getResources().getString(R.string.url_get_history);
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	paramMap.put(getResources().getString(R.string.param_latest), 
    			getResources().getString(R.string.value_history_amount));
        HttpClientUtil.post(url, paramMap, new JsonHttpResponseHandler() {
            /**
             * Returns when request succeeds
             *
             * @param statusCode http response status line
             * @param headers    response headers if any
             * @param response   parsed response if any
             */
        	@Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray jsonArray) {
        		Map<String, String> paramMap = new HashMap<String, String>();
        		paramMap.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
        		paramMap.put(Constants.MAC, mac);
        		db.getTHistory().delete(paramMap);
        		
//				JSONArray jsonArray = new JSONArray(response);
				int length = Integer.valueOf(getResources().getString(R.string.value_history_amount)).intValue();
				if (jsonArray.length() < length) {
					length = jsonArray.length();
				}
				try {
					for (int i = 0; i < length; i++) {	
						HistoryBean hb = new HistoryBean();
						JSONObject jsonb;
						
							jsonb = jsonArray.getJSONObject(i);
						
						String time = jsonb.getString(getResources().getString(R.string.param_his_date_time));
	
						hb.setMac(jsonb.getString(getResources().getString(R.string.param_mac)));
						hb.setUserName(UserInfo.UserInfo.getUserName());
						hb.setTime(sdf.format(sdfE.parse(time)));
						
						String jStr = jsonb.getString(getResources().getString(R.string.param_his_detail));
						if (CheckUtil.requireCheck(jStr)) {
							JSONObject jb = new JSONObject(jsonb.getString(getResources().getString(R.string.param_his_detail)));
							if (jb != null) {	
								Iterator<?> keys = jb.keys();
								while (keys.hasNext()) {             		
									String key = (String) keys.next();
									if (getResources().getString(R.string.param_dis_humi).equals(key)) {
										hb.setDisHumi(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_temp).equals(key)) {
										hb.setDisTemp(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_pm25_in).equals(key)) {
										hb.setDisPm25In(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_pm25_out).equals(key)) {
										hb.setDisPm25Out(jb.getString(key));
									} 
								}
							}
						}						
						
						db.getTHistory().add(hb);
					}
				} catch (Exception e) {

				}
        	}
			
        
          

//            @Override
//            public void onSuccess(String response) {
//            	try {  	
//            		Map<String, String> paramMap = new HashMap<String, String>();
//            		paramMap.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
//            		paramMap.put(Constants.MAC, mac);
//            		db.getTHistory().delete(paramMap);
//            		
//					JSONArray jsonArray = new JSONArray(response);
//					int length = Integer.valueOf(getResources().getString(R.string.value_history_amount)).intValue();
//					if (jsonArray.length() < length) {
//						length = jsonArray.length();
//					}
//
//					for (int i = 0; i < length; i++) {	
//						HistoryBean hb = new HistoryBean();
//						JSONObject jsonb = jsonArray.getJSONObject(i);
//						String time = jsonb.getString(getResources().getString(R.string.param_his_date_time));
//
//						hb.setMac(jsonb.getString(getResources().getString(R.string.param_mac)));
//						hb.setUserName(UserInfo.UserInfo.getUserName());
//						hb.setTime(sdf.format(sdfE.parse(time)));
//						
//						String jStr = jsonb.getString(getResources().getString(R.string.param_his_detail));
//						if (CheckUtil.requireCheck(jStr)) {
//							JSONObject jb = new JSONObject(jsonb.getString(getResources().getString(R.string.param_his_detail)));
//							if (jb != null) {	
//								Iterator<?> keys = jb.keys();
//								while (keys.hasNext()) {             		
//									String key = (String) keys.next();
//									if (getResources().getString(R.string.param_dis_humi).equals(key)) {
//										hb.setDisHumi(jb.getString(key));
//									} else if (getResources().getString(R.string.param_dis_temp).equals(key)) {
//										hb.setDisTemp(jb.getString(key));
//									} else if (getResources().getString(R.string.param_dis_pm25_in).equals(key)) {
//										hb.setDisPm25In(jb.getString(key));
//									} else if (getResources().getString(R.string.param_dis_pm25_out).equals(key)) {
//										hb.setDisPm25Out(jb.getString(key));
//									} 
//								}
//							}
//						}						
//						
//						db.getTHistory().add(hb);
//					}
//				} catch (JSONException e) {
//					Log.e(this.getClass().getName(), e.getMessage(), e);
//					e.printStackTrace();
//				} catch (ParseException e) {
//					Log.e(this.getClass().getName(), e.getMessage(), e);
//					e.printStackTrace();
//				}
//            }
//            
//            @Override
//            public void onFailure(Throwable e) {
//            	Log.e(this.getClass().getName(), e.getMessage(), e);	
//            }   
        });
	}
	
	private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
	
}