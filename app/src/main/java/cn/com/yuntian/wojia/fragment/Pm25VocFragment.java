package cn.com.yuntian.wojia.fragment;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.HistoryBean;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.PM25VocBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

/**
 * Pm25VocFragment
 * 
 * @author chenwh
 *
 */
public class Pm25VocFragment extends Fragment {
	
	private TextView messageText, pm25Text, tempText, humiText, tempUnitText, pm25VocTipText, pm25VocTipMessageText;
	
	private TextView pm25VocBackButton, pm25VocTitleText, vocTipTextLeft, vocTipTextMiddle, vocTipTextRight, vocTipTextError;
	
	private ImageView arrowImage, pm25VocColCirBg, pm25VocPic, pm25VocTempImage, pm25VocHumiImage, leftArrowImage, rightArrowImage;
	
	private ImageView pm25VocLeftArrowPic, pm25VocRightArrowPic;
	
	private LinearLayout pm25VocLayoutView, pm25VocLinearLayout, pm25VocPicDisplay, pm25VocStripImg, chartLayout;
	
	private FrameLayout pm25VocTitleFrame;
	
	private HailinDB db;
	
	private String mac;
	
	private String online = "";
	
	private String tempOnline = "";
	
	private Handler handler = new Handler();
	
	private boolean isForeRun = true;
	
	private BackgroundTask bTask;
	
	private Bitmap mBitmap;
	
	private final String colorOnline = "#37419A";
	
	private final String colorNotOnline = "#686868";
	
	private XYMultipleSeriesDataset mDataset = new XYMultipleSeriesDataset();
	
	private XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
	
	private GraphicalView mChartView;
	
	private boolean chartBl = false;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
	
	private SimpleDateFormat sdfE = new SimpleDateFormat("MMM d, yyyy h:m:s a", Locale.ENGLISH);
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
	private int downY;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
	private VelocityTracker mVelocityTracker;
	
	private int pm25 = 0;
	
	private int voc = 0;
	
	int averWidth = 0;
	
	int screenWidth = 0;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		
		return inflater.inflate(R.layout.pm25_voc_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();   

        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
        db = new HailinDB(getActivity());
        getHistory();
        
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20; 
        mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
        DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;
		averWidth = screenWidth/8;
        
        pm25Text = (TextView) getView().findViewById(R.id.pm25VocText);
        messageText = (TextView) getView().findViewById(R.id.pm25VocMessageText);
        tempText = (TextView) getView().findViewById(R.id.pm25VocTempText);
        tempUnitText =  (TextView) getView().findViewById(R.id.pm25VocTempUnitText);
        humiText = (TextView) getView().findViewById(R.id.pm25VocHumiText);
        arrowImage = (ImageView) getView().findViewById(R.id.pm25VocArrowImg);
        leftArrowImage = (ImageView) getView().findViewById(R.id.pm25VocLeftArrowLink);
        rightArrowImage = (ImageView) getView().findViewById(R.id.pm25VocRightArrowLink);
        pm25VocLayoutView = (LinearLayout) getView().findViewById(R.id.pm25VocLayoutView);
        pm25VocColCirBg = (ImageView) getView().findViewById(R.id.pm25VocColCirBg);
        pm25VocTempImage = (ImageView) getView().findViewById(R.id.pm25VocTempImage);
        pm25VocHumiImage = (ImageView) getView().findViewById(R.id.pm25VocHumiImage); 
        pm25VocBackButton = (TextView) getView().findViewById(R.id.pm25VocBackButton);
        pm25VocTitleText = (TextView) getView().findViewById(R.id.pm25VocTitleText);
        pm25VocStripImg = (LinearLayout) getView().findViewById(R.id.pm25VocStripImg);
        pm25VocTitleFrame = (FrameLayout) getView().findViewById(R.id.pm25VocTitleFrame);
        
        pm25VocTipText = (TextView) getView().findViewById(R.id.pm25VocTipText);
        pm25VocTipMessageText = (TextView) getView().findViewById(R.id.pm25VocTipMessageText);
        pm25VocLeftArrowPic = (ImageView) getView().findViewById(R.id.pm25VocLeftArrowPic);
        pm25VocRightArrowPic = (ImageView) getView().findViewById(R.id.pm25VocRightArrowPic);
        
        vocTipTextLeft = (TextView) getView().findViewById(R.id.vocTipTextLeft);
        vocTipTextMiddle = (TextView) getView().findViewById(R.id.vocTipTextMiddle);
        vocTipTextRight = (TextView) getView().findViewById(R.id.vocTipTextRight);
        vocTipTextError = (TextView) getView().findViewById(R.id.vocTipTextError);
        pm25VocLinearLayout =  (LinearLayout) getView().findViewById(R.id.pm25VocLinearLayout);
        pm25VocPicDisplay = (LinearLayout) getView().findViewById(R.id.pm25VocPicDisplay);
        pm25VocPic = (ImageView) getView().findViewById(R.id.pm25VocPic);
        
        chartLayout = (LinearLayout) getView().findViewById(R.id.chart);
        Bitmap blaCirBg = BitmapFactory.decodeResource(getResources(),  
                R.drawable.pm25_bla_cir_bg); 
        LayoutParams lp = (LayoutParams) chartLayout.getLayoutParams();
        lp.height = blaCirBg.getHeight();
        lp.width = blaCirBg.getWidth();
        pm25VocTipText.setWidth(blaCirBg.getWidth());
        pm25VocTipMessageText.setWidth(blaCirBg.getWidth());
        
		String devNm = bundle.getString(Constants.DIS_DEV_NAME);
		pm25VocTitleText.setText(devNm);

		setDataToPage();
		setChartView();
        setLayoutEvent();
        
		bTask = new BackgroundTask(
				getActivity(), 
				db,
				mac,
				getResources().getString(R.string.value_deviceid_pm25_voc)) {
        	@Override
        	public void exeTask() {
        		if (bTask.isTaskContinue() && isForeRun) {
        			setDataToPage(); 
            	} 
            	handler.postDelayed(runnable, 5000);
        	}
        };
		isForeRun = true;
		handler.post(runnable);	
    }
    
    private void setLayoutEvent() {
    	
    	leftArrowImage.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
 				if (chartBl) {
// 					pm25TipText.setHeight(0);
 					chartLayout.setVisibility(View.INVISIBLE);
 					pm25VocTipMessageText.setVisibility(View.GONE);
 					pm25VocLeftArrowPic.setVisibility(View.GONE);
 					pm25VocRightArrowPic.setVisibility(View.GONE);
 					pm25VocTipText.setVisibility(View.GONE);
 	 				pm25VocPicDisplay.setVisibility(View.VISIBLE);
 	 				rightArrowImage.setVisibility(View.VISIBLE);
 	 				messageText.setVisibility(View.VISIBLE);
 	 				chartBl = false;
 				} else {
 					getFragmentManager().popBackStackImmediate();
 				}
 			}
 		});	
    	
    	pm25VocBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});	
    	
    	rightArrowImage.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
// 				redirectPage();
// 				pm25TipText.setHeight(60);
 				chartLayout.setVisibility(View.VISIBLE);
 				pm25VocTipText.setVisibility(View.VISIBLE);
 				pm25VocTipMessageText.setVisibility(View.VISIBLE);
 				pm25VocLeftArrowPic.setVisibility(View.INVISIBLE);
 				pm25VocRightArrowPic.setVisibility(View.INVISIBLE);
 				pm25VocPicDisplay.setVisibility(View.INVISIBLE);
 				rightArrowImage.setVisibility(View.INVISIBLE);
 				messageText.setVisibility(View.GONE);
 				chartBl = true;
 				if (mChartView != null) {
 					viewRepaint();
 				}
 				setTipMessages();
 			}
 		});
    	
    	pm25VocLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: 	
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE: 
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
 		});	
    	
    	pm25VocPicDisplay.setOnTouchListener(new TouchListener());
    	mChartView.setOnTouchListener(new TouchListener());	
    }
    
    private void setChartView() {
    	mRenderer.setApplyBackgroundColor(true);
	    mRenderer.setAxisTitleTextSize(16);
	    mRenderer.setChartTitleTextSize(20);
	    mRenderer.setLabelsTextSize(15);
	    mRenderer.setLegendTextSize(15);
	    mRenderer.setYAxisMax(550);
	    mRenderer.setXAxisMax(9.5);
	    mRenderer.setMargins(new int[] { 0, 30, 0, 0 });
	    mRenderer.setPointSize(5);
	    mRenderer.setChartTitle(
	    		getResources().getString(R.string.lang_chart_pm25_title));
	    if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorOnline));
	    } else {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorNotOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorNotOnline));
	    }
	    mRenderer.setYLabelsAlign(Align.RIGHT);
	    mRenderer.setShowLegend(false);  
	    
	    mRenderer.setXLabels(0);
        mRenderer.addXTextLabel(1, 
        		getResources().getString(R.string.value_chart_abscissa_eight));
        mRenderer.addXTextLabel(2, 
        		getResources().getString(R.string.value_chart_abscissa_seven));
        mRenderer.addXTextLabel(3, 
        		getResources().getString(R.string.value_chart_abscissa_six));
        mRenderer.addXTextLabel(4, 
        		getResources().getString(R.string.value_chart_abscissa_five));
        mRenderer.addXTextLabel(5, 
        		getResources().getString(R.string.value_chart_abscissa_four));
        mRenderer.addXTextLabel(6, 
        		getResources().getString(R.string.value_chart_abscissa_three));
        mRenderer.addXTextLabel(7, 
        		getResources().getString(R.string.value_chart_abscissa_two));
        mRenderer.addXTextLabel(8, 
        		getResources().getString(R.string.value_chart_abscissa_one));
        mRenderer.addXTextLabel(9, 
        		getResources().getString(R.string.value_chart_abscissa_now));

        mRenderer.addYTextLabel(0, "0");
        mRenderer.addYTextLabel(100, "100");
        mRenderer.addYTextLabel(200, "200");
        mRenderer.addYTextLabel(300, "300");
        mRenderer.addYTextLabel(400, "400");
        mRenderer.addYTextLabel(500, "500");
	   
        setSeriesData();
        
        XYSeriesRenderer rNoDisplay = new XYSeriesRenderer();
//        rNoDisplay.setColor(Color.WHITE);
//        rNoDisplay.setPointStyle(PointStyle.POINT);
        rNoDisplay.setFillPoints(false);
        rNoDisplay.setDisplayChartValues(false);
        mRenderer.addSeriesRenderer(rNoDisplay);
        
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setColor(Color.WHITE);
        renderer.setPointStyle(PointStyle.CIRCLE);
        renderer.setFillPoints(true);
        renderer.setDisplayChartValues(true);
        renderer.setDisplayChartValuesDistance(10);
        mRenderer.addSeriesRenderer(renderer);
       

        if (mChartView == null) {
            mChartView = ChartFactory.getLineChartView(getActivity(), mDataset, mRenderer);
            mChartView.setBackgroundColor(Color.parseColor("#37419A"));
            mChartView.setClickable(false);
            chartLayout.addView(mChartView);
        } else {
        	viewRepaint();
        }
    }
    
    private void viewRepaint() {
    	mDataset.clear();
    	setSeriesData();
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorOnline));
	    } else {
	    	mRenderer.setBackgroundColor(Color.parseColor(colorNotOnline));
	    	mRenderer.setMarginsColor(Color.parseColor(colorNotOnline));
	    }
    	mChartView.repaint();
    }
    
    private void setSeriesData() {
    	XYSeries seriesNoDisplay = new XYSeries("");
        seriesNoDisplay.add( 0, 0);
        mDataset.addSeries(seriesNoDisplay);

        String[] args = {UserInfo.UserInfo.getUserName(), mac};
        List<HistoryBean> hList = db.getTHistory().findList(args);
        XYSeries series = new XYSeries("");
        int length = Integer.valueOf(getResources().getString(R.string.value_history_amount)).intValue();
		if (hList.size() < length) {
			length = hList.size();
		}
        for (int i = 0; i < length; i++) {
        	HistoryBean bean = hList.get(i);
        	if (CheckUtil.requireCheck(bean.getDisPm25In())) {
        		series.add(i + 1, Integer.valueOf(bean.getDisPm25In().trim()).intValue());
        	}
        }
        series.add(9, pm25);
        mDataset.addSeries(series);
//        setTipMessages();
    }
    
    @Override
    public void onStop() {
    	isForeRun = false;
    	if (mBitmap != null) {
    		mBitmap.recycle();
    	}
    	super.onStop();
    }
    
    private Runnable runnable = new Runnable() {
        public void run () {  
        	if (isForeRun) {
        		bTask.getDataFromWeb();
        	} 
        }
    };
        
    private void setDataToPage() {
    	String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	PM25VocBean pm25VocBean = db.getTPM25Voc().findOne(arg);
    	if (listItemBean != null) {
    		online = listItemBean.getIsOnline();
    	}
    	if (!CheckUtil.requireCheck(online) || !tempOnline.equals(online)) {
    		setOnline();
    		if (mChartView != null) {
    			viewRepaint();
    		}
    	}
    	tempOnline = online;
    	
    	if (pm25VocBean != null) {   				
    		if (CheckUtil.requireCheck(pm25VocBean.getDisPm25In())) {
    			pm25 = Integer.valueOf(pm25VocBean.getDisPm25In()).intValue();
    		}
    		if (CheckUtil.requireCheck(pm25VocBean.getDisVoc())) {
    			voc = Integer.valueOf(pm25VocBean.getDisVoc()).intValue();
    		}
    		pm25Text.setText(String.valueOf(pm25));
    		tempText.setText(Util.getValue(Util.getTempDegree(pm25VocBean.getDisTemp()), 0));
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			tempUnitText.setText(getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			tempUnitText.setText(getResources().getString(R.string.value_temp_fah));
    		}
    		
    		humiText.setText(pm25VocBean.getDisHumi() + "%");
    		
    		if (pm25 > 99) {
    			pm25Text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 60);
    			pm25VocLinearLayout.setPadding(0, 20, 0, 0);
    		} else {
    			pm25Text.setTextSize(TypedValue.COMPLEX_UNIT_SP, 80);
    			pm25VocLinearLayout.setPadding(0, 0, 0, 0);
    		}
    		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
    				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT); 
    		int arrowWidth;
    		int angle=0;
    		if (pm25 < 301) {
    			arrowWidth = averWidth*pm25/50;	
    			angle = 270*pm25/300-360;		
    		} else if (pm25 < 500){
    			arrowWidth = averWidth*6+((screenWidth-averWidth*6)/2)*(pm25-300)/100;
    			angle = 270+90*(pm25-300)/200-360;
    		} else {
    			arrowWidth = screenWidth;
    			angle = 0;
    		}
    		params.leftMargin = arrowWidth-10; 
    		arrowImage.setLayoutParams(params);
    		
    		setVocPic();
    		setMessageText();
    		
    		int drawable = 0;
    		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    			drawable = R.drawable.pm25_col_cir_bg;
        	} else {
        		drawable = R.drawable.pm25_col_cir_bg_notonline;
        	}
        		
			mBitmap = BitmapFactory.decodeResource(
					getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true); 
	        Canvas canvas = new Canvas(mBitmap);
	        Paint paint = new Paint();  
	        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
	        paint.setAntiAlias(true);
	        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
	        canvas.drawArc(rectf, 270, angle, true, paint);
	        pm25VocColCirBg.setImageBitmap(mBitmap);	
    	}
    }
    
    private void setMessageText() {
    	if (pm25 < 76 && voc < 3) {
    		messageText.setText(getResources().getString(R.string.lang_voc_tip_one));
    	} else if (pm25 < 151 && voc < 6) {
    		messageText.setText(getResources().getString(R.string.lang_voc_tip_two));
    	} else {
    		messageText.setText(getResources().getString(R.string.lang_voc_tip_three));
    	} 
    }
    
    private class TouchListener implements OnTouchListener {

    	@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View arg0, MotionEvent event) {
			
			obtainVelocityTracker(event);
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: 	
					downX = (int) event.getX();
					downY = (int) event.getY();
					isLeftMove = false;
					isRightMove = false;
					break;
				case MotionEvent.ACTION_MOVE: 
					break;
				case MotionEvent.ACTION_UP:
					if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
						if ((event.getX() - downX) > mTouchSlop) {
							isRightMove = true;
						} else if ((event.getX() - downX) < -mTouchSlop) {
							isLeftMove = true;
						}
					}
					if (chartBl) {
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
//								pm25TipText.setHeight(0);
								pm25VocTipText.setVisibility(View.GONE);
								pm25VocTipMessageText.setVisibility(View.GONE);
								pm25VocLeftArrowPic.setVisibility(View.GONE);
								pm25VocRightArrowPic.setVisibility(View.GONE);
								chartLayout.setVisibility(View.INVISIBLE);
				 				pm25VocPicDisplay.setVisibility(View.VISIBLE);
				 				rightArrowImage.setVisibility(View.VISIBLE);
				 				messageText.setVisibility(View.VISIBLE);
				 				chartBl = false;
							} 
							releaseVelocityTracker();
						}
					} else {
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							} 
							
							releaseVelocityTracker();
						} else if (isLeftMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
	                        velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
	                        int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
//								pm25TipText.setHeight(60);
								pm25VocTipText.setVisibility(View.VISIBLE);
								pm25VocTipMessageText.setVisibility(View.VISIBLE);
								chartLayout.setVisibility(View.VISIBLE);
								pm25VocLeftArrowPic.setVisibility(View.INVISIBLE);
								pm25VocRightArrowPic.setVisibility(View.INVISIBLE);
				 				pm25VocPicDisplay.setVisibility(View.INVISIBLE);
				 				rightArrowImage.setVisibility(View.INVISIBLE);
				 				messageText.setVisibility(View.GONE);
				 				chartBl = true;
				 				if (mChartView != null) {
				 					viewRepaint();
				 				}
				 				setTipMessages();
							} 
							
							releaseVelocityTracker();
						}
					}
					break;
			}

			return true;
		}
    	
    }
    
    private void setTipMessages() {
    	int i = (int) (Math.random()*10);
    	switch (i) {  
	        case 0:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_one));
	            break;
	        case 1:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_two));
	            break;  
	        case 2:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_three));
	            break;  
	        case 3:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_four));
	            break; 
	        case 4:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_five));
	            break;  
	        case 5:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_six));
	            break; 
	        case 6:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_seven));
	            break;
	        case 7:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_eight));
	            break;
	        case 8:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_nine));
	            break;
	        case 9:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_ten));
	            break;
	        default:
	        	pm25VocTipMessageText.setText(getResources().getString(R.string.lang_pm25_life_tip_eight));
	            break;  
    	}
    } 
    
    private void setOnline() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
    		pm25VocLayoutView.setBackgroundResource(R.drawable.pm25_bg);
    		pm25VocBackButton.setBackgroundResource(R.drawable.back_arrow);
    		pm25VocPicDisplay.setBackgroundResource(R.drawable.pm25_bla_cir_bg);
    		pm25VocTitleFrame.setBackgroundResource(R.drawable.pm25_title);
    		pm25VocStripImg.setBackgroundResource(R.drawable.pm25_num_bg);
    		leftArrowImage.setBackgroundResource(R.drawable.pm25_left_arrow);
    		rightArrowImage.setBackgroundResource(R.drawable.pm25_right_arrow);
    		arrowImage.setBackgroundResource(R.drawable.pm25_valuebar_index);
    		pm25VocTempImage.setBackgroundResource(R.drawable.pm25_temp_bg);
    		pm25VocHumiImage.setBackgroundResource(R.drawable.pm25_humi_bg);
    		pm25VocPic.setBackgroundResource(R.drawable.voc_00);
    	} else {
    		pm25VocLayoutView.setBackgroundResource(R.drawable.pm25_bg_notonline);
    		pm25VocBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
    		pm25VocPicDisplay.setBackgroundResource(R.drawable.pm25_bla_cir_bg_notonline);
    		pm25VocTitleFrame.setBackgroundResource(R.drawable.pm25_title_notonline);
    		pm25VocStripImg.setBackgroundResource(R.drawable.pm25_num_bg_notonline);
    		leftArrowImage.setBackgroundResource(R.drawable.pm25_left_arrow_notonline);
    		rightArrowImage.setBackgroundResource(R.drawable.pm25_right_arrow_notonline);
    		arrowImage.setBackgroundResource(R.drawable.pm25_valuebar_index_notonline);
    		pm25VocTempImage.setBackgroundResource(R.drawable.pm25_temp_bg_notonline);
    		pm25VocHumiImage.setBackgroundResource(R.drawable.pm25_humi_bg_notonline);
    		pm25VocPic.setBackgroundResource(R.drawable.voc_00_notonline);
    	}
    }
    
    private void getHistory() {
    	new Thread() {
            public void run() {
            	getPMHistory();
            }
	 
        }.start();
    }

	private void getPMHistory() {
//		String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getString(R.string.url_get_history);
		String url = getResources().getString(R.string.url_get_history);
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_mac), mac);
    	paramMap.put(getResources().getString(R.string.param_latest), 
    			getResources().getString(R.string.value_history_amount));
        HttpClientUtil.post(url, paramMap, new JsonHttpResponseHandler() {
            /**
             * Returns when request succeeds
             *
             * @param statusCode http response status line
             * @param headers    response headers if any
             * @param response   parsed response if any
             */
        	@Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray jsonArray) {
        		Map<String, String> paramMap = new HashMap<String, String>();
        		paramMap.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
        		paramMap.put(Constants.MAC, mac);
        		db.getTHistory().delete(paramMap);
        		
//				JSONArray jsonArray = new JSONArray(response);
				int length = Integer.valueOf(getResources().getString(R.string.value_history_amount)).intValue();
				if (jsonArray.length() < length) {
					length = jsonArray.length();
				}
				try {
					for (int i = 0; i < length; i++) {	
						HistoryBean hb = new HistoryBean();
						JSONObject jsonb;
						
							jsonb = jsonArray.getJSONObject(i);
						
						String time = jsonb.getString(getResources().getString(R.string.param_his_date_time));
	
						hb.setMac(jsonb.getString(getResources().getString(R.string.param_mac)));
						hb.setUserName(UserInfo.UserInfo.getUserName());
						hb.setTime(sdf.format(sdfE.parse(time)));
						
						String jStr = jsonb.getString(getResources().getString(R.string.param_his_detail));
						if (CheckUtil.requireCheck(jStr)) {
							JSONObject jb = new JSONObject(jsonb.getString(getResources().getString(R.string.param_his_detail)));
							if (jb != null) {	
								Iterator<?> keys = jb.keys();
								while (keys.hasNext()) {             		
									String key = (String) keys.next();
									if (getResources().getString(R.string.param_dis_humi).equals(key)) {
										hb.setDisHumi(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_temp).equals(key)) {
										hb.setDisTemp(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_pm25_in).equals(key)) {
										hb.setDisPm25In(jb.getString(key));
									} else if (getResources().getString(R.string.param_dis_pm25_out).equals(key)) {
										hb.setDisPm25Out(jb.getString(key));
									} 
								}
							}
						}						
						
						db.getTHistory().add(hb);
					}
				} catch (Exception e) {

				}
        	} 
        });
	}
	
    private void obtainVelocityTracker(MotionEvent event) {
        if (mVelocityTracker == null) {
                mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
        if (mVelocityTracker != null) {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
        }
	}
    
    private void setVocPic() {
    	if (Constants.VALUE_ONLINE_ONE.equals(online)) {
	    	switch (voc) {
		        case 1:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_00);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		        	break;   
		        case 2:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_01);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 3:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_02);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 4:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_03);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break; 
		        case 5:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_04);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 6:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_05);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break; 
		        case 7:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_06);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        case 8:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_07);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        case 9:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_08);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        default: 
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_error);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.VISIBLE);
		            break;  
	    	} 
    	} else {
    		switch (voc) {
		        case 1:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_00_notonline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		        	break;   
		        case 2:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_01_notonline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 3:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_02_notonline);
		    		vocTipTextLeft.setVisibility(View.VISIBLE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 4:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_03_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break; 
		        case 5:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_04_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;  
		        case 6:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_05_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.VISIBLE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break; 
		        case 7:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_06_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        case 8:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_07_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        case 9:
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_08_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.VISIBLE);
		    		vocTipTextError.setVisibility(View.GONE);
		            break;
		        default:  
		        	pm25VocPic.setBackgroundResource(R.drawable.voc_error_notonline);
		    		vocTipTextLeft.setVisibility(View.GONE);
		    		vocTipTextMiddle.setVisibility(View.GONE);
		    		vocTipTextRight.setVisibility(View.GONE);
		    		vocTipTextError.setVisibility(View.VISIBLE);
		            break;  
	    	}
    	}
    }
}