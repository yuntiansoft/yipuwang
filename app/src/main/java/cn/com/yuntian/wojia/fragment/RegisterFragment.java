package cn.com.yuntian.wojia.fragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.Util;
//import com.cn.hailin.android.httpclient.AbstractAsyncResponseListener;
//import com.cn.hailin.android.httpclient.AsyncHttpClient;
//import com.cn.hailin.android.httpclient.getRequest;

/**
 * RegisterFragment
 * 
 * @author chenwh
 *
 */
public class RegisterFragment extends Fragment {

	// ��������������ı���
	private EditText rUserText, rPwdText, rPwdAgainText;
	// ���������������ť
	private TextView messageText, bnRegister, regArticleText;
    // checkBox
	private CheckBox regArticleCheck;
	
	private String user = null;
	
	private HailinDB db;
	
//	private boolean articleBl = false;

	// ���ͷ
//	private ImageView regArrowLogin;

	private CustomProgressDialog progressDialog= null;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.register_fragment, container, false);

	}

    @Override
    public void onStart() {
        super.onStart();
        if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(), 
					getResources().getString(R.string.lang_mess_no_net),  
			        Toast.LENGTH_LONG).show(); 
		}
        
        db = new HailinDB(getActivity());
        
        rUserText = (EditText) getView().findViewById(R.id.regUserText);
        rPwdText = (EditText) getView().findViewById(R.id.regPwdText);
        rPwdAgainText = (EditText) getView().findViewById(R.id.regPwdAgainText);
        messageText = (TextView) getView().findViewById(R.id.regMessageText);
		// ��ȡ�����е�������ť
        bnRegister = (TextView) getView().findViewById(R.id.regbtnRegister);
        regArticleCheck = (CheckBox) getView().findViewById(R.id.regArticleCheck);
        regArticleText = (TextView) getView().findViewById(R.id.regArticleText);
//        String url = getResources().getString(R.string.url_webview);
        String text = getResources().getString(R.string.lang_article_text);
//        regArticleText.setText(Html.fromHtml("<a href=" + "'" + url + "'" + ">" + text + "</a>"));
        regArticleText.setText(Html.fromHtml("<a href=" + "'" + "#" + "'" + ">" + text + "</a>"));
//        regArticleText.setText(Html.fromHtml("<a>" + text + "</a>"));
//        regArticleText.setMovementMethod(LinkMovementMethod.getInstance());
        
        regArticleText.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				WebviewFragment fragment = new WebviewFragment();
 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
 				//�滻fragment
 				transaction.replace(R.id.fragment_container, fragment);
 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
 				transaction.addToBackStack("register");
 				
 				// Commit the transaction
 				transaction.commit();
 			}
 		});
        
		// ��½link
//        rLinkLogin = (TextView) getView().findViewById(R.id.regLinkLoginText);
        // ���ͷ
//        regArrowLogin = (ImageView) getView().findViewById(R.id.regArrowLogin);
		
        bnRegister.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {
 				if (check()) {
 					messageText.setText("");
 					progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
			        progressDialog.show();
 					register();
			        
//			     // ע��ɹ�
//			        PasswordChangeFragment eResendFragment = new PasswordChangeFragment();
//	    			Bundle nBundle = new Bundle();  
//	    			nBundle.putString(Constants.KEY_USER_NAME, rUserText.getText().toString());
//	    			eResendFragment.setArguments(nBundle);
//	 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
//	 				transaction.replace(R.id.fragment_container, eResendFragment);
//	 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
//	 				transaction.addToBackStack(this.getClass().getName());
//	 				// Commit the transaction
//	 				transaction.commit();	
//	 				
//	 				if (progressDialog != null) {
//	 					progressDialog.dismiss();
//	 				}
 				}
 			}
 		});
        
//        rLinkLogin.setOnClickListener(new OnClickListener() {
// 			@Override
// 			public void onClick(View v) {
// 				redirectLogin();
// 			}
// 		});
        
//        regArrowLogin.setOnClickListener(new OnClickListener() {
// 			@Override
// 			public void onClick(View v) {
// 				redirectLogin();
// 			}
// 		});
    }	
    
//    private void redirectLogin() {
//    	LoginFragment loginFragment = new LoginFragment();
//		FragmentTransaction transaction = getFragmentManager().beginTransaction();
//		transaction.replace(R.id.fragment_container, loginFragment);
//		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
////		transaction.addToBackStack(this.getClass().getName());
//		// Commit the transaction
//		transaction.commit();
//    }
    
    private boolean check() {
    	if (!requireCheck()) {
    		return false;
    	}
    	if (!emailCheck()) {
    		return false;
    	}
    	if (!passwordCheck()) {
    		return false;
    	}
    	if (!articleCheck()) {
    		return false;
    	}
    	return true;
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(rUserText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_req));
    		return false;
    	}
    	if (TextUtils.isEmpty(rPwdText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_req));
    		return false;
    	}
    	if (TextUtils.isEmpty(rPwdAgainText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwdag_req));
    		return false;
    	}
    	return true;
    }
    
    private boolean emailCheck() {
    	user = Util.replaceFullWidth(rUserText.getText().toString());
    	if (!CheckUtil.emailCheck(user)) {
    		messageText.setText(getResources().getString(R.string.lang_mess_email_valid));
    		return false;
    	}
    	return true;
    }
    
    private boolean passwordCheck() {  	
    	if (!rPwdText.getText().toString().equals(rPwdAgainText.getText().toString())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_valid));
    		return false;
    	}
    	
    	if (rPwdText.getText().toString().length() < 6 
    			|| rPwdText.getText().toString().length() > 14) {
    		messageText.setText(getResources().getString(R.string.lang_mess_pwd_length));
    		return false;
    	}
    	
    	return true;
    }
    
    private boolean articleCheck() {
//    	if (!(regArticleCheck.isChecked() && articleBl)) {
    	if (!regArticleCheck.isChecked()) {
    		messageText.setText(getResources().getString(R.string.lang_mess_article_ok));
    		return false;
    	}
    	return true;
    }
    
    private void register() {
//    	String url = getResources().getString(R.string.url_base) 
//    			+ getResources().getText(R.string.url_register).toString();
//    	Map<String, String> rawParams = new HashMap<String, String>();
//    	rawParams.put(getResources().getString(R.string.param_user_nm), 
//    			rUserText.getText().toString());
//    	rawParams.put(getResources().getString(R.string.param_password), 
//    			rPwdText.getText().toString());
//    	rawParams.put(getResources().getString(R.string.param_confirm_password), 
//    			rPwdAgainText.getText().toString());
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT)
//    	{
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    			// ע��ɹ�
//    			EmailResendFragment eResendFragment = new EmailResendFragment();
//    			Bundle nBundle = new Bundle();  
//    			nBundle.putString(Constants.KEY_USER_NAME, rUserText.getText().toString());
//    			eResendFragment.setArguments(nBundle);
// 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
// 				transaction.replace(R.id.fragment_container, eResendFragment);
// 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
// 				transaction.addToBackStack(this.getClass().getName());
// 				// Commit the transaction
// 				transaction.commit();	
// 				
// 				if (progressDialog != null) {
// 					progressDialog.dismiss();
// 				}
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    			
//    			if (e instanceof HttpResponseException) {
//    				HttpResponseException he = (HttpResponseException) e;
//    				if (getResources().getString(R.string.value_code_exist).equals(
//    						String.valueOf(he.getStatusCode()))) {
//    					messageText.setText(getResources().getString(R.string.lang_mess_user_exist));
//    				}
//    			} else {
//    				messageText.setText(getResources().getString(R.string.lang_mess_exception));
//    			}
//    			if (progressDialog != null) {
//    				progressDialog.dismiss();
//    			}
//    		}  
//    	};
//    	try {
//			AsyncHttpClient.sendRequest(getActivity(), getRequest.getPostRequest(url, rawParams), callback);
//		} catch (UnsupportedEncodingException e1) {
//			Log.e(this.getClass().getName(), e1.getMessage());
//			
//			messageText.setText(getResources().getString(R.string.lang_mess_exception));
//			if (progressDialog != null) {
//				progressDialog.dismiss();
//			}
//		}
    	
    	List<Map<String, String>> userInfo = db.getTUser().findAll();
    	boolean bl = false;
    	String userNm = null;
    	String pwd = null;
		if (userInfo != null && userInfo.size() > 0) {	
			String userType = userInfo.get(0).get(Constants.USER_TYPE);
			userNm = userInfo.get(0).get(Constants.USER_NAME);
			pwd = userInfo.get(0).get(Constants.PASSWORD);
			if (Constants.USER_TYPE_TWO.equals(userType)) {
				bl = true;
			}
		}
		
		String url = null;
		RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getString(R.string.param_user_nm), 
    			user);
    	paramMap.put(getResources().getString(R.string.param_password), 
    			rPwdText.getText().toString());
    	paramMap.put(getResources().getString(R.string.param_confirm_password), 
    			rPwdAgainText.getText().toString());
    	if (bl) {
//    		url = getResources().getString(R.string.url_base) 
//        			+ getResources().getText(R.string.url_upgrade_account).toString();
    		url = getResources().getText(R.string.url_upgrade_account).toString();
    		paramMap.put(getResources().getString(R.string.param_original_username), userNm);
    		paramMap.put(getResources().getString(R.string.param_original_password), pwd);
    	} else {
//    		url = getResources().getString(R.string.url_base) 
//        			+ getResources().getText(R.string.url_register).toString();
    		url = getResources().getText(R.string.url_register).toString();
    	}
    	
    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {  
            	Map<String, String> userMap = new HashMap<String, String>();
				userMap.put(Constants.USER_NAME, user);
				userMap.put(Constants.PASSWORD, rPwdText.getText().toString());
				userMap.put(Constants.USER_TYPE, Constants.USER_TYPE_ONE);
				userMap.put(Constants.USER_STATUS, Constants.USER_STATUS_ONE);
				userMap.put(Constants.TOKEN_TYPE, Constants.TOKEN_TYPE);
				userMap.put(Constants.ACESS_TOKEN, Constants.ACESS_TOKEN);
				db.getTUser().delete();
				db.getTUser().add(userMap);
            	
				// ע��ɹ�
    			EmailResendFragment eResendFragment = new EmailResendFragment();
    			Bundle nBundle = new Bundle();  
    			nBundle.putString(Constants.KEY_USER_NAME, user);
    			eResendFragment.setArguments(nBundle);
 				FragmentTransaction transaction = getFragmentManager().beginTransaction();
 				transaction.replace(R.id.fragment_container, eResendFragment);
 				//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
// 				transaction.addToBackStack(this.getClass().getName());
 				// Commit the transaction
 				transaction.commit();	
 				
 				if (progressDialog != null) {
 					progressDialog.dismiss();
 				}					    					
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] response, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
    			
				if (getResources().getString(R.string.value_code_fourZeroNine).equals(
						String.valueOf(statusCode))) {
					messageText.setText(getResources().getString(R.string.lang_mess_user_exist));
				} else {
    				messageText.setText(getResources().getString(R.string.lang_mess_exception));
    			}
    			
    			if (progressDialog != null) {
    				progressDialog.dismiss();
    			}
            }   
        });
    }

}