package cn.com.yuntian.wojia.fragment;


//import com.cn.hailin.android.logic.SlideListAdapter.onRightItemClickListener;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.app.Dialog;

import cn.com.yuntian.wojia.R;

/**
 * RenameDialogFragment
 * 
 * @author chenwh
 */
public class RenameDialogFragment extends DialogFragment {

	public interface DialogClickListener {
		public void doPositiveClick(String titleNm, int position);
		public void doNegativeClick();
	}
  
	private DialogClickListener mListener;
	
	private static int mPosition;
	
	public void setDialogClickListener(DialogClickListener listener){
    	mListener = listener;
    }
  
  
	public static RenameDialogFragment newInstance(String title, String message, int position){
		RenameDialogFragment frag = new RenameDialogFragment();
		Bundle b = new Bundle();
		b.putString("title", title);
		b.putString("message", message);
		frag.setArguments(b);		  
		mPosition = position;
		
		return frag;
	}
	
//	@Override  
//    public void onCreate(Bundle savedInstanceState) {  
//        super.onCreate(savedInstanceState);  
//        
//        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle); 
//    }  
//	
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//
////		return inflater.inflate(R.layout.rename_dialog_fragment, container, false);
//		
//		View v = inflater.inflate(R.layout.rename_dialog_fragment, container, false);
//		
////		final Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
//
//
//		String title = getArguments().getString("title");
//		String message = getArguments().getString("message");
//		if (title != null && title.length() > 0) {
//			TextView t = (TextView) v.findViewById(R.id.title_text_view);
//			t.setText(title);
//		}
//
//		if (message != null && message.length() > 0) {
//			TextView m = (TextView) v.findViewById(R.id.content_text_view);
//			m.setText(message);
//		}
//
//		View ok = v.findViewById(R.id.dialog_ok);
//		View cancel = v.findViewById(R.id.dialog_cancel);
//
//		ok.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
////				this.dismiss();
//				if (mListener != null) {
//					mListener.doPositiveClick();
//				}
//
//			}
//
//		});
//
//		cancel.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
////				dialog.dismiss();
//				if (mListener != null) {
//					mListener.doNegativeClick();
//				}
//			}
//
//		});
//
////		dialog.setContentView(getView());
//
//		return v;
//
//	}
  
  
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog dialog = new Dialog(getActivity(), R.style.DialogStyle);
//		final Dialog dialog = new Dialog(getActivity());
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(FragmentActivity.LAYOUT_INFLATER_SERVICE);  
	    final View view = inflater.inflate(R.layout.rename_dialog_fragment, null, false);  
		String title = getArguments().getString("title");
		String message = getArguments().getString("message");
		if (title != null && title.length() > 0) {
			TextView t = (TextView) view.findViewById(R.id.title_text_view);
			t.setText(title);
		}

		
		if (message != null && message.length() > 0) {
			EditText m = (EditText) view.findViewById(R.id.content_text_view);
			m.setText(message);
			m.addTextChangedListener(new TextWatcher() {

				@Override
				public void afterTextChanged(Editable text) {
					Button b = (Button) view.findViewById(R.id.dialog_ok);
					if (text.toString().trim().length() == 0) {
						b.setEnabled(false);
					} else {
						b.setEnabled(true);
					}
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					
				}

				@Override
				public void onTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) {
					
				}
//				@Override
//				public boolean onKey(View v, int keyCode, KeyEvent event) {
//					switch (event.getAction()) {
//					case KeyEvent.:
//					defalt:
//						break;
//					
//					}
//					return false;
//				}
			});
		}

		Button ok = (Button) view.findViewById(R.id.dialog_ok);
		Button cancel = (Button) view.findViewById(R.id.dialog_cancel);

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (mListener != null) {
					EditText et = (EditText) view.findViewById(R.id.content_text_view); 
					String titleNm = et.getText().toString();
					mListener.doPositiveClick(titleNm, mPosition);
				}
			}

		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				if (mListener != null) {
					mListener.doNegativeClick();
				}
			}

		});

		dialog.setContentView(view);

		return dialog;
	}
	  
}