package cn.com.yuntian.wojia.fragment;

import org.apache.http.Header;

import android.annotation.SuppressLint;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.hf.smartlink.model.WifiStatus;
import com.hf.smartlink.utils.Utils;
import com.hiflying.smartlink.ISmartLinker;
import com.hiflying.smartlink.OnSmartLinkListener;
import com.hiflying.smartlink.SmartLinkedModule;
import com.hiflying.smartlink.v7.MulticastSmartLinker;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.util.List;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.util.AppContext;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.ProgressDialogUtil;
import cn.com.yuntian.wojia.util.UserInfo;

/**
 * SmartlinkFragment
 * 
 * @author chenwh
 *
 */
public class SmartlinkFragment extends Fragment implements OnSmartLinkListener {

	private EditText wifiPwdText;
	private TextView messageText, connectButton, ssidText, smartLinkBackButton;
    // checkBox
	private CheckBox wifiPwdCheck;
	private static final String TAG = "SmartlinkFragment";
	private LinearLayout smartLinkView;
	
	private CustomProgressDialog progressDialog = null;
	
	private String mac = null; 

	private int mTouchSlop;

	private int downX;
	
	private boolean isLeftMove = false;
	
	private boolean isRightMove = false;
	
	private boolean isRun = true;
	
	private boolean mIsConncting = false;
	
	protected ISmartLinker mSnifferSmartLinker;
	
	private BroadcastReceiver mWifiChangedReceiver;
	
	private HailinDB db;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.smartlink_fragment, container, false);
	}

    @Override
    public void onStart() {
        super.onStart();
		if (goback){
			getFragmentManager().popBackStackImmediate();
			goback = false;
			return;
		}
        isRun = true;
        mIsConncting = false;
        mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop(); 
        mac = null;
        
        db = new HailinDB(getActivity());
        
        mSnifferSmartLinker = MulticastSmartLinker.getInstance();
        
        wifiPwdText = (EditText) getView().findViewById(R.id.wifiPwdText);
        messageText = (TextView) getView().findViewById(R.id.messageText);
        connectButton = (TextView) getView().findViewById(R.id.connectButton);
        ssidText = (TextView) getView().findViewById(R.id.ssidText);
        smartLinkBackButton = (TextView) getView().findViewById(R.id.smartLinkBackButton);
//        newAmerBackText = (TextView) getView().findViewById(R.id.newAmerBackText);
        wifiPwdCheck = (CheckBox) getView().findViewById(R.id.wifiPwdCheck);
        smartLinkView = (LinearLayout) getView().findViewById(R.id.smartLinkView);
        
        ssidText.setText(getSSid());
        
        wifiPwdCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {
        	@Override
 			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
        		if(wifiPwdCheck.isChecked()){
        			wifiPwdText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                 } else {
                	wifiPwdText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                 }
        	}
        });
		
        smartLinkBackButton.setOnClickListener(new OnClickListener() {
 			@Override
 			public void onClick(View v) {	
				getFragmentManager().popBackStackImmediate();
 			}
 		});
        
//        newAmerBackText.setOnClickListener(new OnClickListener() {
// 			@Override
// 			public void onClick(View v) {	
//				getFragmentManager().popBackStackImmediate();
// 			}
// 		});
        
        smartLinkView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {	
					downX = (int) event.getX();
					isLeftMove = false;
					isRightMove = false;
					break;
				}
				case MotionEvent.ACTION_MOVE: {
					if ((event.getX() - downX) > mTouchSlop) {
						isRightMove = true;
					} else if ((event.getX() - downX) < -mTouchSlop) {
						isLeftMove = true;
					}
					break;
				}
				case MotionEvent.ACTION_UP:
					if (isRightMove) {
						getFragmentManager().popBackStackImmediate();
					} else if (isLeftMove) {
	
					}
					break;
				}

				return true;
			}
 		});	
        
        connectButton.setOnClickListener(new OnClickListener() {

 			@Override
 			public void onClick(View v) {
//				final APSmartlinkFragment apSmartlinkFragment = new APSmartlinkFragment();
//				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.LinkCustomDialogTheme);
//				builder.setMessage("是否使用AP方式连接").setNegativeButton("否", new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//
//					}
//				}).setPositiveButton("是", new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						redirectPage(apSmartlinkFragment);
//					}
//				});
//				builder.create().show();

				if (check()) {
 					if(!mIsConncting){
						try {
							String ss = ssidText.getText().toString().trim();
							String ps = wifiPwdText.getText().toString().trim();

							mSnifferSmartLinker.setOnSmartLinkListener(SmartlinkFragment.this);
							//
							mSnifferSmartLinker.start(getActivity(), ps, ss);
							mIsConncting = true;
							progressDialog = ProgressDialogUtil.getProgressDialogUtil(getActivity());
					        progressDialog.show();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
 				}
 			}
 		});
        
        mWifiChangedReceiver = new BroadcastReceiver() {	
			@Override
			public void onReceive(Context context, Intent intent) {
				ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
				if (networkInfo != null && networkInfo.isConnected()) {
					ssidText.setText(getSSid());
					wifiPwdText.requestFocus();
					connectButton.setEnabled(true);
				} else {
					ssidText.setText("No Wifi");
					connectButton.setEnabled(false);
					dialogDismiss();
				}
			}
		};
		
		getActivity().registerReceiver(mWifiChangedReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
		Log.e("isAPFail", "onStart: "+isAPFail );
		if (isAPFail){
			dialogList();
			isAPFail = false;
		}

    }
    
    private void dialogDismiss() {
    	if (progressDialog != null) {
			progressDialog.dismiss();
			mSnifferSmartLinker.setOnSmartLinkListener(null);
			mSnifferSmartLinker.stop();
			mIsConncting = false;
		}
    }
    
    private void showToast(String text) {
    	Toast t = Toast.makeText(getActivity(), null, Toast.LENGTH_LONG);  
    	LinearLayout layout = (LinearLayout) t.getView();  
    	layout.setOrientation(LinearLayout.HORIZONTAL);  
    	layout.setGravity(Gravity.CENTER);
    	
    	TextView tv = new TextView(getActivity()); 
    	tv.setLayoutParams(new  LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));  
    	tv.setGravity(Gravity.CENTER);  
    	tv.setTextColor(Color.WHITE);
    	tv.setTextSize(20);
//    	tv.setPadding(10, 0, 0, 0);  
    	tv.setText(text);  
    	layout.addView(tv);  
    	t.show();
    }

	@Override
	public void onResume() {
		super.onResume();

	}

	@Override
    public void onStop() {	
    	isRun = false;
    	mac = null;
    	super.onStop();
		try {
			getActivity().unregisterReceiver(mWifiChangedReceiver);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    private boolean check() {
    	if (!requireCheck()) {
    		return false;
    	}
    	return true;
    }
    
    private boolean requireCheck() {
    	if (TextUtils.isEmpty(ssidText.getText())) {
    		messageText.setText(getResources().getString(R.string.lang_mess_no_route));
    		return false;
    	}
    	
    	return true;
    }
    
    private String getSSid(){
		WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
		if(wm != null){
			WifiInfo wi = wm.getConnectionInfo();
			if(wi != null){
				String ssid = wi.getSSID();
				if(ssid.length()>2 && ssid.startsWith("\"") && ssid.endsWith("\"")){
					return ssid.substring(1,ssid.length()-1);
				}else{
					return ssid;
				}
			}
		}

		return "";
	}
    
    @Override
    public void onDestroy() {

		super.onDestroy();
		mSnifferSmartLinker.setOnSmartLinkListener(null);

	}


	@Override
	public void onLinked(final SmartLinkedModule module) {
		mac = module.getMac();
//		if (isRun) {
//			hand.sendEmptyMessage(3);
//		}
	}


	@Override
	public void onCompleted() {
		hand.sendEmptyMessage(2);
	}


	@Override
	public void onTimeOut() {

//		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.LinkCustomDialogTheme);
//		builder.setMessage("是否使用AP方式连接").setNegativeButton("否", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//
//			}
//		}).setPositiveButton("是", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				redirectPage(apSmartlinkFragment);
//			}
//		});
//		builder.create().show();
//		dialogList();
		dialogDismiss();
		
		if (isRun) {
			hand.sendEmptyMessage(1);
		}
	}	
	
	@SuppressLint("HandlerLeak")
	Handler hand = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			
			switch (msg.what) {
			case 1:	
				showToast(getResources().getString(R.string.lang_mess_timeout));

				break;
			case 2:
				addDevice();
				break;
			case 3:
				break;
			default:
				break;
			}
		};
	};
	
	private void addDevice() {
		if (!CheckUtil.requireCheck(mac)) {
			retPage(false);
		}
		
		String[] arg = {UserInfo.UserInfo.getUserName(), mac};
    	ListItemBean listItemBean = db.getTDevice().findOne(arg);
    	if (listItemBean != null && mac.equals(listItemBean.getMac())) {
    		retPage(true);
    	} else {
    		sendHttp();
    	}
	}
	
	@SuppressLint("DefaultLocale")
	private void retPage(boolean bl) {
		dialogDismiss();
		
		if (bl) {
			showToast(getResources().getString(R.string.lang_config_sucess));
			getFragmentManager().popBackStackImmediate();
		} else {
			showToast(getResources().getString(R.string.lang_config_fail));
		}
	}
	
	private void sendHttp() {
//    	String url = getResources().getText(R.string.url_base).toString() 
//    			+ getResources().getText(R.string.url_add_device).toString();
		String url = getResources().getText(R.string.url_add_device).toString();
    	RequestParams paramMap = new RequestParams();
    	paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);

    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {  
            	retPage(true);
				Log.e(TAG, "onsuccess: "+new String(response));
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
			    if (getResources().getString(R.string.value_code_fourZeroZero).equals(
					String.valueOf(statusCode))) {
			    	retPage(true);
					Log.e(TAG, "onFailure: "+new String(responseString));
				} else {
					retPage(false);
					Log.e(TAG, "onFailure: "+new String(responseString));
				}
            }
        });
    }
	private void redirectPage(Fragment fragment) {
		FragmentTransaction transaction = getFragmentManager().beginTransaction();
//		transaction.setCustomAnimations(
//				R.anim.push_left_in, R.anim.push_left_out);
		//�滻fragment
		transaction.replace(R.id.fragment_container, fragment);
		//��ӵ���̨��ջ��Ҳ����˵�ܹ���back������
		transaction.addToBackStack(null);

		// Commit the transaction
		transaction.commit();
	}
	private void dialogList() {

		WifiManager wm = (WifiManager) getActivity().getSystemService(Context.WIFI_SERVICE);
		List<ScanResult> scanResults = wm.getScanResults();
		for (int i = 0; i < scanResults.size(); i++) {
			if (Utils.removeDoubleQuotes(scanResults.get(i).SSID).equals(ssidText.getText().toString())){
				Log.e(TAG, "dialogList:load:BSSID :"+scanResults.get(i).BSSID);
				Utils.saveLastScanResult(getActivity(),scanResults.get(i));
				WifiStatus.getInstanse(AppContext.getContext()).load();
				break;
			}
		}
//		Utils.saveScanResultPassword(getActivity(),,wifiPwdText.getText().toString());
		String items[] = {"使用AP方式配置", "继续使用SmartLink"};

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("配置失败");
		// builder.setMessage("是否确认退出?"); //设置内容

		// 设置列表显示，注意设置了列表显示就不要设置builder.setMessage()了，否则列表不起作用。
		builder.setItems(items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (which == 0){
					APSmartlinkFragment apSmartlinkFragment = new APSmartlinkFragment();
					Bundle bundle = new Bundle();
					bundle.putString("wifipwd",wifiPwdText.getText().toString());
					apSmartlinkFragment.setArguments(bundle);
					redirectPage(apSmartlinkFragment);
				}else {
					dialog.dismiss();
				}
			}
		});
//		builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//				Toast.makeText(getActivity(), "确定", Toast.LENGTH_SHORT)
//						.show();
//			}
//		});
		builder.create().show();
	}
	private static boolean isAPFail = false;
	private static boolean goback = false;
	public static void setIsAPFail(boolean b){
		isAPFail = b;
		if (!b){
			goback = true;
		}
	}
	public static void reloadAddGlg(){
		isAPFail = false;
		goback = false;
	}

}