package cn.com.yuntian.wojia.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import cn.com.yuntian.wojia.R;


/**
 * AddDeviceFragment
 * 
 * @author chenwh
 *
 */
public class WebviewFragment extends Fragment {
	
	private WebView webview;
	
	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		return inflater.inflate(R.layout.webview_fragment, container, false);
	}

    @Override
    public void onStart() {
        super.onStart();
        
        webview = (WebView) getView().findViewById(R.id.webView);
        webview.loadUrl(getResources().getString(R.string.url_webview));
        webview.setBackgroundColor(0);
        
    }
    
  
   
		
}