package cn.com.yuntian.wojia.fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout.LayoutParams;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.logic.BackgroundTask;
import cn.com.yuntian.wojia.logic.ListItemBean;
import cn.com.yuntian.wojia.logic.YiPuWTempBean;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;

;

/**
 * LingDongTempFragment
 * 
 * @author chenwh
 *
 */
public class YiPuWTempFragment extends Fragment {

	private TextView heatingBackButton, heatingTempValueText, heatingTempUnitText, heatingTempText;

//	private ImageView heatingColCirBg;


	private TextView heatingCloseButton, heatingTitleText, coverText;

	private LinearLayout heatingTempBg, heatingLayoutView, heatingPicDisplay;

	private FrameLayout  heatingTitleBg;

	private SeekBar heatingSeekBar, heatingSeekBarOffline;

	private HailinDB db;

	private String mac;

	private String devNm;

	private Handler handler = new Handler();

	private boolean isForeRun = true;

	private BackgroundTask bTask;

	private Bitmap mBitmap;

	private final int maxCTemp = 35;

	private final int minCTemp = 5;

	private final int maxFTemp = 95;

	private final int minFTemp = 41;

	private int heatDefaultTemp = 20;

	private int maxTemp;

	private int minTemp;

	private int screenWidth;

	private double tempStand;

	private static boolean pageIsUpd = false;

	private YiPuWTempBean yiPuWTempBean;

	private YiPuWTempBean bakBean;

	private Map<String, String> webParam;

	private String statusOnOff = "";

	private String status = "";

	private int heatingTempWidth;

	private boolean proBl = false;

	private int heatSaveEnergy = 18;

	private double heatDefaultSaveEnergy = 0;

	private boolean reLoadPageBl = true;

//	int angle = 0;

	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;

	/**
	 * ��ָ����X������
	 */
	private int downX;

	private int downY;

//	private boolean isLeftMove = false;

	private boolean isRightMove = false;

	private float mMinimumVelocity;

	private float mMaximumVelocity;

	private VelocityTracker mVelocityTracker;

	private boolean bTaskBl = true;
	private TextView nomBtnImg;
	private TextView nomBtnTxt;
	private TextView forBtnImg;
	private TextView forBtnTxt;
	private TextView enhBtnImg;
	private TextView enhBtnTxt;
	private String forbidMode;
	private TextView offBtnTxt;

	// ���ز���
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {


		return inflater.inflate(R.layout.yipuw_temp_fragment, container, false);

	}

	@Override
	public void onStart() {
		super.onStart();

		if (!Util.IsHaveInternet(getActivity())) {
			Toast.makeText(getActivity().getApplicationContext(),
					getResources().getString(R.string.lang_mess_no_net),
					Toast.LENGTH_LONG).show();
		}

		reLoadPageBl = true;
		db = new HailinDB(getActivity());
		mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop() + 20;
		mMinimumVelocity = ViewConfiguration.get(getActivity()).getScaledMinimumFlingVelocity();
		mMaximumVelocity = ViewConfiguration.get(getActivity()).getScaledMaximumFlingVelocity();
		DisplayMetrics dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		screenWidth = dm.widthPixels;

		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			maxTemp = maxFTemp;
			minTemp = minFTemp;
			heatDefaultTemp = Integer.valueOf(Util.centigrade2Fahrenheit(heatDefaultTemp, 0)).intValue();
			heatSaveEnergy = Integer.valueOf(Util.centigrade2Fahrenheit(heatSaveEnergy, 0)).intValue();
		} else {
			maxTemp = maxCTemp;
			minTemp = minCTemp;
		}

		Bundle bundle = this.getArguments();
		mac  = bundle.getString(Constants.MAC);
		devNm = bundle.getString(Constants.DIS_DEV_NAME);

		initItem();
		setDataToPage();
		itemEvent();
		bTaskBl = true;
		bTask = new BackgroundTask(
				getActivity(),
				db,
				mac,
				getResources().getString(R.string.value_deviceid_heat_temp)) {
			@Override
			public void exeTask() {
				if (bTask.isTaskContinue() && isForeRun) {
					setDataToPage();
				}

				if (isForeRun) {
					if (!bTaskBl) {
						bTaskBl = true;
						handler.postDelayed(runnable, 5000);
					}
				}
			}
		};
		isForeRun = true;
		pageIsUpd = false;

		handler.post(runnable);

	}

	private void initItem() {

		heatingBackButton = (TextView) getView().findViewById(R.id.heatingBackButton);
		heatingTempValueText = (TextView) getView().findViewById(R.id.heatingTempValueText);
		heatingTempUnitText = (TextView) getView().findViewById(R.id.heatingTempUnitText);
		heatingTempText = (TextView) getView().findViewById(R.id.heatingTempText);
		heatingTitleText = (TextView) getView().findViewById(R.id.heatingTitleText);
		heatingCloseButton = (TextView) getView().findViewById(R.id.ldOffBtnImg);
		coverText = (TextView) getView().findViewById(R.id.coverText);

		nomBtnImg = (TextView) getView().findViewById(R.id.nomBtnImg);
		nomBtnTxt = (TextView) getView().findViewById(R.id.nomBtnTxt);
		enhBtnImg = (TextView) getView().findViewById(R.id.enhBtnImg);
		enhBtnTxt = (TextView) getView().findViewById(R.id.enhBtnTxt);
		forBtnImg = (TextView) getView().findViewById(R.id.forBtnImg);
		forBtnTxt = (TextView) getView().findViewById(R.id.forBtnTxt);
		offBtnTxt = (TextView) getView().findViewById(R.id.offBtnTxt);

		heatingLayoutView = (LinearLayout) getView().findViewById(R.id.heatingLayoutView);
		heatingTempBg = (LinearLayout) getView().findViewById(R.id.heatingTempBg);
		heatingPicDisplay = (LinearLayout) getView().findViewById(R.id.heatingPicDisplay);

		heatingTitleBg =  (FrameLayout) getView().findViewById(R.id.heatingTitleBg);


		heatingSeekBar = (SeekBar) getView().findViewById(R.id.heatingSeekBar);
		heatingSeekBarOffline = (SeekBar) getView().findViewById(R.id.heatingSeekBarOffline);

		heatingTitleText.setText(devNm);

		int w = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);
		int h = View.MeasureSpec.makeMeasureSpec(0,View.MeasureSpec.UNSPECIFIED);

		heatingTempValueText.measure(w, h);
		heatingTempWidth = heatingTempValueText.getMeasuredWidth();

		// ������Ч���ȣ���Ļ����  - button���  + button�Ŀհ�(2)
//		tempStand = ((double)screenWidth)/(maxTemp - minTemp);
	}

	@Override
	public void onStop() {
		isForeRun = false;
		reLoadPageBl = false;
		if (mBitmap != null) {
			mBitmap.recycle();
		}
		super.onStop();
	}

	private Runnable runnable = new Runnable() {
		public void run () {
			bTaskBl = false;
			if (isForeRun) {
				bTask.getDataFromWeb();
			}
		}
	};

	private void itemEvent() {
		btnEvent();
		seekbarEvent();
		backEvent();
	}

	private void seekbarEvent() {
		heatingSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			String setText;
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (proBl) {
					setTempTextLeftMoving(progress);
					double temp = minTemp + ((double) progress*screenWidth/100)/tempStand;
					if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
						heatingTempValueText.setText(setText + getResources().getString(R.string.value_temp_fah));
					} else {
						setText = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue());
						heatingTempValueText.setText(setText + getResources().getString(R.string.value_temp_cen));
					}

				}

			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				proBl = true;
				pageIsUpd = true;
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				proBl = false;

				if (setText == null || setText.startsWith(Constants.FAH_START_KEY)
						|| setText.startsWith(Constants.CEN_START_KEY)) {
					return;
				}

				if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
					setText = Constants.FAH_START_KEY + setText;
				} else {
					setText = Constants.CEN_START_KEY + setText;
				}
				Map<String, String> param = new HashMap<String, String>();
				RequestParams paramMap = new RequestParams();
				param.put(Constants.TEMP_HEAT, setText);
				bakBean.setTempHeat(setText);
				paramMap.put(getResources().getString(R.string.param_temp_heat), setText);

				if (Constants.VALUE_ENERGY_ONE.equals(bakBean.getSaveEnergy())) {

					param.put(Constants.SAVE_ENERGY, Constants.VALUE_ENERGY_ZERO);
					paramMap.put(getResources().getString(R.string.param_save_energy), String.valueOf(false));
					bakBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
				}

				updateWeb(paramMap, param);
			}
		});


	}


	private void btnEvent() {
		coverText.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
			}
		});

		heatingCloseButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				pageIsUpd = true;
				if (getResources().getString(R.string.value_status_off).equals(
						bakBean.getStatusOnOff())) {
					statusOnOff = getResources().getString(R.string.value_status_on);
//	    			setDisTempImage(Constants.VALUE_ONLINE_ONE);

//		    			if (!CheckUtil.requireCheck(bakBean.getSaveEnergy())) {
//			    			heatingEnergyImg.setBackgroundResource(R.drawable.green_energy);
//			    			heatingEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
//			    		} else if (Constants.VALUE_ENERGY_ZERO.equals(bakBean.getSaveEnergy())) {
//			    			heatingEnergyImg.setBackgroundResource(R.drawable.green_energy);
//			    			heatingEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_bg);
//			    		} else {
//			    			heatingEnergyImg.setBackgroundResource(R.drawable.green_energy_sel);
//			    			heatingEnergyBgImg.setBackgroundResource(R.drawable.ld_leaf_light_bg);
//			    		}
				} else {
					statusOnOff = getResources().getString(R.string.value_status_off);
//	    			setDisTempImage(Constants.VALUE_ONLINE_ONE);
				}
				setStatusOnOffInfo();
				Map<String, String> param = new HashMap<String, String>();
				param.put(Constants.STATUS_ON_OFF, statusOnOff);
				bakBean.setStatusOnOff(statusOnOff);

				RequestParams paramMap = new RequestParams();
				paramMap.put(getResources().getString(R.string.param_status_onoff), statusOnOff);
				updateWeb(paramMap, param);
			}
		});
		nomBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pageIsUpd = true;
				String heatMode = yiPuWTempBean.getHeatMode();
				if (!nomBtnImg.isSelected()){
					nomBtnImg.setSelected(true);
					heatMode = getResources().getString(R.string.value_heat_mode_nor);
					enhBtnImg.setSelected(false);
				}

				Map<String, String> param = new HashMap<>();
				RequestParams paramMap = new RequestParams();
				param.put(Constants.HEAT_MODE, heatMode);
				bakBean.setHeatMode(heatMode);
				paramMap.put(getResources().getString(R.string.param_heat_mode), heatMode);
				updateWeb(paramMap,param);
			}
		});
		enhBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pageIsUpd = true;
				String heatMode = yiPuWTempBean.getHeatMode();
				if (!enhBtnImg.isSelected()){
					enhBtnImg.setSelected(true);
					heatMode = getResources().getString(R.string.value_heat_mode_enh);
					nomBtnImg.setSelected(false);
				}

				Map<String, String> param = new HashMap<>();
				RequestParams paramMap = new RequestParams();
				param.put(Constants.HEAT_MODE, heatMode);
				bakBean.setHeatMode(heatMode);
				paramMap.put(getResources().getString(R.string.param_heat_mode), heatMode);
				updateWeb(paramMap,param);
			}
		});
		forBtnImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pageIsUpd = true;
				String forbidMode;
				if (getResources().getString(R.string.value_forbid_mode_low).equals(yiPuWTempBean.getForbidMode())){
					forBtnImg.setBackgroundResource(R.drawable.btn_for_mid_selector);
					forbidMode = getResources().getString(R.string.value_forbid_mode_mid);
					forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_mid));
				}else if (getResources().getString(R.string.value_forbid_mode_mid).equals(yiPuWTempBean.getForbidMode())){
					forBtnImg.setBackgroundResource(R.drawable.btn_for_none_selector);
					forbidMode = getResources().getString(R.string.value_forbid_mode_none);
					forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_mone));
				}else {
					forBtnImg.setBackgroundResource(R.drawable.btn_for_low_selector);
					forbidMode = getResources().getString(R.string.value_forbid_mode_low);
					forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_low));
				}
				yiPuWTempBean.setForbidMode(forbidMode);
				Map<String, String> param = new HashMap<>();
				RequestParams paramMap = new RequestParams();
				param.put(Constants.FORBID_MODE, forbidMode);
				bakBean.setForbidMode(forbidMode);
				paramMap.put(getResources().getString(R.string.param_forbid_mode), forbidMode);
				updateWeb(paramMap,param);
			}
		});
	}

	private void backEvent() {

		heatingBackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getFragmentManager().popBackStackImmediate();
			}
		});



		heatingLayoutView.setOnTouchListener(new OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View arg0, MotionEvent event) {
				obtainVelocityTracker(event);
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						downX = (int) event.getX();
						downY = (int) event.getY();
//						isLeftMove = false;
						isRightMove = false;
						break;
					case MotionEvent.ACTION_MOVE:
						break;
					case MotionEvent.ACTION_UP:
						if (Math.abs(event.getY() - downY) - Math.abs(event.getX() - downX) <= 0) {
							if ((event.getX() - downX) > mTouchSlop) {
								isRightMove = true;
							} else if ((event.getX() - downX) < -mTouchSlop) {
//								isLeftMove = true;
							}
						}
						if (isRightMove) {
							final VelocityTracker velocityTracker = mVelocityTracker;
							velocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
							int initialVelocity = (int) velocityTracker.getXVelocity();
							if (Math.abs(initialVelocity) > mMinimumVelocity) {
								// ����һ������
								getFragmentManager().popBackStackImmediate();
							}

							releaseVelocityTracker();
						}
						break;
				}

				return true;
			}
		});
	}

	private void setDataToPage() {
//    	Map<String, String> param = new HashMap<String, String>();
//    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
//    	param.put(Constants.MAC, mac);
//    	param.put(Constants.IS_ONLINE, "1");
//    	db.getTDevice().update(param);
		String[] arg = {UserInfo.UserInfo.getUserName(), mac};
		ListItemBean listItemBean = db.getTDevice().findOne(arg);
		yiPuWTempBean = db.gettYiPuWTemp().findOne(arg);
//		yiPuWTempBean = new YiPuWTempBean();
		if (listItemBean != null) {
			String adminOnly = listItemBean.getAdminOnly();
			if (CheckUtil.requireCheck(adminOnly) && getResources().getString(R.string.value_admin_only_pc).equals(adminOnly)) {
				coverText.setVisibility(View.VISIBLE);
				heatingTitleText.setText(devNm + getResources().getString(R.string.lang_txt_auth));
			} else {
				coverText.setVisibility(View.GONE);
				heatingTitleText.setText(devNm);
			}
		}
		String heatMode = yiPuWTempBean.getHeatMode();

		if (!CheckUtil.requireCheck(heatMode)){
			heatMode = getResources().getString(R.string.value_heat_mode_def);
		}
		if (getResources().getString(R.string.value_heat_mode_enh).equals(heatMode)){
			enhBtnImg.setSelected(true);
			nomBtnImg.setSelected(false);

		}else {
			enhBtnImg.setSelected(false);
			nomBtnImg.setSelected(true);
		}

		forbidMode = yiPuWTempBean.getForbidMode();
		if (!CheckUtil.requireCheck(forbidMode)){
			forbidMode = getResources().getString(R.string.value_forbid_mode_def);
		}
		if (getResources().getString(R.string.value_forbid_mode_low).equals(forbidMode)){
			forBtnImg.setBackgroundResource(R.drawable.btn_for_low_selector);
			forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_low));
		}else if (getResources().getString(R.string.value_forbid_mode_mid).equals(forbidMode)){
			forBtnImg.setBackgroundResource(R.drawable.btn_for_mid_selector);
			forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_mid));
		}else {
			forBtnImg.setBackgroundResource(R.drawable.btn_for_none_selector);
			forBtnTxt.setText(getResources().getString(R.string.lang_txt_forbid_mone));
		}

		try {
//	    	if (heatDefaultSaveEnergy == 0) {
			String heatTempSaveEnergy = Util.getTempDegree(yiPuWTempBean.getTempHeatSaveEnergy());
			if (CheckUtil.requireCheck(heatTempSaveEnergy)) {
				heatDefaultSaveEnergy = Double.valueOf(heatTempSaveEnergy).doubleValue();
			} else {
				heatDefaultSaveEnergy = heatSaveEnergy;
			}
//	    	}
		} catch (Exception e) {
			if (heatDefaultSaveEnergy == 0) {
				heatDefaultSaveEnergy = heatSaveEnergy;
			}
		}
		if (heatDefaultSaveEnergy > maxTemp) {
			heatDefaultSaveEnergy = maxTemp;
		} else if (heatDefaultSaveEnergy < minTemp) {
			heatDefaultSaveEnergy = minTemp;
		}

		statusOnOff = yiPuWTempBean.getStatusOnOff();
		if (!CheckUtil.requireCheck(statusOnOff)) {
			statusOnOff = getResources().getString(R.string.value_status_on);
		}
		status = yiPuWTempBean.getStatus();
//    	if (!(CheckUtil.requireCheck(status) && status.equals(
//    			getResources().getString(R.string.value_status_one)))) {
//    		status = getResources().getString(R.string.value_status_four);
//    	}

		String online = "";
		if (listItemBean != null) {
			online = listItemBean.getIsOnline();
		}
		setOnline(online);

		String disTemp = Util.getTempDegree(yiPuWTempBean.getDisTemp());
		if (!CheckUtil.requireCheck(disTemp)) {
			disTemp = "0";
		}
		heatingTempText.setText(disTemp);
		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
			heatingTempUnitText.setText(
					getResources().getString(R.string.value_temp_cen));
		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			heatingTempUnitText.setText(
					getResources().getString(R.string.value_temp_fah));
		}

		double heatTemp;
		if (Constants.VALUE_ENERGY_ONE.equals(yiPuWTempBean.getSaveEnergy())) {
			heatTemp = heatDefaultSaveEnergy;
		} else {
			heatTemp = getTempHeat(yiPuWTempBean.getTempHeat());
		}

		int tempDefaultMin = -100;
		int tempDefaultMax = -100;
		try {
			String tempHeatDefaultMax = Util.getTempDegree(yiPuWTempBean.getTempHeatDefaultMax());
			if (CheckUtil.requireCheck(tempHeatDefaultMax)) {
				tempDefaultMax = Double.valueOf(tempHeatDefaultMax).intValue();
			}
		} catch (Exception e) {

		}

		try {
			String tempHeatDefaultMin = Util.getTempDegree(yiPuWTempBean.getTempHeatDefaultMin());
			if (CheckUtil.requireCheck(tempHeatDefaultMin)) {
				tempDefaultMin = Double.valueOf(tempHeatDefaultMin).intValue();
			}
		} catch (Exception e) {

		}

		if (tempDefaultMin != -100 && tempDefaultMax != -100) {
			if (tempDefaultMin < tempDefaultMax) {
				minTemp = tempDefaultMin;
				maxTemp = tempDefaultMax;
			}
		} else if (tempDefaultMin == -100 && tempDefaultMax != -100 ) {
			if (minTemp < tempDefaultMax) {
				maxTemp = tempDefaultMax;
			}
		} else if (tempDefaultMax == -100 && tempDefaultMin != -100) {
			if (tempDefaultMin < maxTemp) {
				minTemp = tempDefaultMin;
			}
		}


		tempStand = ((double)screenWidth)/(maxTemp - minTemp);


		setTempValue(heatTemp);
		setTempMove(heatTemp);

		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			setStatusOnOffInfo();
		} else {
			setButtonValue(false);
		}

//    	int currentTemp = Double.valueOf(disTemp).intValue();
//    	if (currentTemp > maxTemp) {
//    		currentTemp = maxTemp;
//    	}
//    	if (currentTemp < minTemp) {
//    		currentTemp = minTemp;
//    	}
//    	angle = (currentTemp - maxTemp)*360/(maxTemp-minTemp);
//		setDisTempImage(online);

		bakBean = (YiPuWTempBean) yiPuWTempBean.clone();
	}

	private double getTempHeat(String tempHeat) {
		double heatTemp;
		if (!CheckUtil.requireCheck(tempHeat)) {
			heatTemp  = (double) heatDefaultTemp;
		} else {
			heatTemp =  Double.valueOf(Util.getTempDegree(tempHeat)).doubleValue();
			if (heatTemp > maxTemp) {
				heatTemp = maxTemp;
			} else if (heatTemp < minTemp) {
				heatTemp = minTemp;
			}
		}
		return heatTemp;
	}

	private void setTempMove(double temp) {
		int progress = (int) (temp-minTemp) * 100/(maxTemp - minTemp);
		heatingSeekBar.setProgress(progress);
		heatingSeekBarOffline.setProgress(progress);
		setTempTextLeftMoving(progress);
	}

	private void setTempTextLeftMoving(int progress) {
		LinearLayout.LayoutParams tempValueParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		int leftValueMovex = progress*screenWidth/100 - heatingTempWidth/2;
		int i = (int)((40 - progress)/2.6);
		leftValueMovex = leftValueMovex + i;
		if (leftValueMovex < 0) {
			leftValueMovex = 0;
		} else if (leftValueMovex + heatingTempWidth > screenWidth) {
			leftValueMovex = screenWidth - heatingTempWidth;
		}
		tempValueParams.leftMargin = leftValueMovex;
		heatingTempValueText.setLayoutParams(tempValueParams);
	}

	private void setVisible(String online) {
		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			heatingSeekBar.setVisibility(View.VISIBLE);
			heatingSeekBarOffline.setVisibility(View.INVISIBLE);
		} else {
			heatingSeekBar.setVisibility(View.INVISIBLE);
			heatingSeekBarOffline.setVisibility(View.VISIBLE);
		}
	}

	private void setTempValue(double temp) {
		if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
			String value = String.valueOf(new BigDecimal(temp).setScale(0, BigDecimal.ROUND_HALF_UP).intValue());
			heatingTempValueText.setText(value + getResources().getString(R.string.value_temp_fah));

		} else {
			String value = String.valueOf(new BigDecimal(temp).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue());
			heatingTempValueText.setText(value + getResources().getString(R.string.value_temp_cen));
		}
	}

	private void updateDb(Map<String, String> param) {
		param.put(Constants.MAC, mac);
		param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
		db.gettYiPuWTemp().update(param);
	}

	private void updateWeb(RequestParams paramMap, Map<String, String> param) {
//    	String url = getResources().getText(R.string.url_base).toString()
//    			+ getResources().getText(R.string.url_upd_device).toString();
		String url = getResources().getText(R.string.url_upd_device).toString();
		paramMap.put(getResources().getText(R.string.param_mac).toString(), mac);
		webParam = param;

		HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, byte[] responseString) {
				updateDb(webParam);
				pageIsUpd = false;
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, byte[] responseString, Throwable e) {
				if (reLoadPageBl) {
					setDataToPage();
				}

				pageIsUpd = false;
			}
		});
	}

	public static boolean getPageIsUpd () {
		return pageIsUpd;
	}

	private void setOnline(String online) {
		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
			setEnable();
			setVisible(online);
//    		heatingLayoutView.setBackgroundResource(R.drawable.green_bg);
			heatingTitleBg.setBackgroundResource(R.drawable.pm25_title);
			heatingBackButton.setBackgroundResource(R.drawable.back_arrow);
			heatingTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg);
			heatingTempBg.setBackgroundResource(R.drawable.green_temp_bg);
			heatingCloseButton.setSelected(false);


		} else {
			setDisEnable();
			setVisible(online);
//    		heatingLayoutView.setBackgroundResource(R.drawable.green_bg_offline);
			heatingTitleBg.setBackgroundResource(R.drawable.pm25_title_notonline);
			heatingBackButton.setBackgroundResource(R.drawable.back_arrow_notonline);
			heatingPicDisplay.setBackgroundResource(R.drawable.heating_cir_offline);
			heatingTempValueText.setBackgroundResource(R.drawable.green_temp_val_bg_offline);
			heatingTempBg.setBackgroundResource(R.drawable.green_temp_bg_offline);
			heatingCloseButton.setSelected(true);


		}
	}

	private void setCirBg() {
		if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
			if (getResources().getString(R.string.value_status_one).equals(status)) {
				heatingPicDisplay.setBackgroundResource(R.drawable.heating_cir_start_on);
			} else {
				heatingPicDisplay.setBackgroundResource(R.drawable.heating_cir_start_off);
			}
		} else {
			heatingPicDisplay.setBackgroundResource(R.drawable.heating_cir_close);
		}
	}

	private void setEnable() {
		setItemEnable();
		heatingCloseButton.setEnabled(true);

	}

	private void setDisEnable() {
		setItemDisEnable();
		heatingCloseButton.setEnabled(false);

	}

	private void setItemEnable() {
		forBtnImg.setEnabled(true);
		nomBtnImg.setEnabled(true);
		enhBtnImg.setEnabled(true);
		forBtnImg.setEnabled(true);
		heatingSeekBar.setEnabled(true);
	}

	private void setItemDisEnable() {
		forBtnImg.setEnabled(false);
		nomBtnImg.setEnabled(false);
		enhBtnImg.setEnabled(false);
		forBtnImg.setEnabled(false);
		heatingSeekBar.setEnabled(false);
		heatingSeekBarOffline.setEnabled(false);
	}

	private void setStatusOnOffInfo() {
		setButtonValue(true);
		setCirBg();
		if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
			setItemEnable();
			heatingSeekBar.setVisibility(View.VISIBLE);
			heatingTempValueText.setVisibility(View.VISIBLE);
		} else {
			setItemDisEnable();
			heatingSeekBar.setVisibility(View.INVISIBLE);
			heatingTempValueText.setVisibility(View.INVISIBLE);
		}
	}

	private void setButtonValue(boolean bl) {
		if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
			offBtnTxt.setText(getResources().getString(R.string.lang_heating_btn_value_off));
			heatingCloseButton.setSelected(false);
			if (bl) {
				heatingCloseButton.setEnabled(true);
			} else {
				heatingCloseButton.setEnabled(false);
			}
		} else {
			offBtnTxt.setText(getResources().getString(R.string.lang_heating_btn_value_on));
			heatingCloseButton.setSelected(true);
			if (bl) {
				heatingCloseButton.setEnabled(true);
			} else {
				heatingCloseButton.setEnabled(false);
			}
		}
	}

//	private void setDisTempImage(String online) {
//		int drawable = 0;
//		if (Constants.VALUE_ONLINE_ONE.equals(online)) {
//			if (getResources().getString(R.string.value_status_on).equals(statusOnOff)) {
//				drawable = R.drawable.heating_temp_cir_top;
//			} else {
//				drawable = R.drawable.green_temp_cir_top;
//			}
//    	} else {
//    		drawable = R.drawable.green_temp_cir_top_offline;
//    	}
//		mBitmap = BitmapFactory.decodeResource(
//    			getResources(), drawable).copy(Bitmap.Config.ARGB_8888, true);
//        Canvas canvas = new Canvas(mBitmap);
//        Paint paint = new Paint();
//        RectF rectf = new RectF(0, 0, mBitmap.getWidth(),  mBitmap.getHeight());
//        paint.setAntiAlias(true);
//        paint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
//        canvas.drawArc(rectf, 270, angle, true, paint);
//        heatingColCirBg.setImageBitmap(mBitmap);
//	}

	private void obtainVelocityTracker(MotionEvent event) {
		if (mVelocityTracker == null) {
			mVelocityTracker = VelocityTracker.obtain();
		}
		mVelocityTracker.addMovement(event);
	}

	private void releaseVelocityTracker() {
		if (mVelocityTracker != null) {
			mVelocityTracker.recycle();
			mVelocityTracker = null;
		}
	}
}