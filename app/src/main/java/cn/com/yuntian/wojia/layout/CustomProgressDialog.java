package cn.com.yuntian.wojia.layout;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.view.WindowManager.LayoutParams;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.com.yuntian.wojia.R;


/********************************************************************
 * [Summary]
 *       
 * [Remarks]
 *       
 *******************************************************************/

public class CustomProgressDialog extends Dialog {
	private Context context = null;
	private static CustomProgressDialog customProgressDialog = null;
	
	public CustomProgressDialog(Context context){
		super(context);
		this.context = context;
	}
	
	public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
    }
	
	public CustomProgressDialog createDialog(){
		customProgressDialog = new CustomProgressDialog(context, R.style.CustomProgressDialog);
		customProgressDialog.setContentView(R.layout.progress_dialog);
		customProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		LayoutParams lay = customProgressDialog.getWindow().getAttributes();  
		setParams(lay); 
		
		return customProgressDialog;
	}
 
    public void onWindowFocusChanged(boolean hasFocus){
    	
    	if (customProgressDialog == null){
    		return;
    	}
    	
        ImageView imageView = (ImageView) customProgressDialog.findViewById(R.id.loadingImageView);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();
    }
    
    private void setParams(LayoutParams lay) {  
    	  DisplayMetrics dm = new DisplayMetrics();  
    	  ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);  
    	  Rect rect = new Rect();  
    	  View view = getWindow().getDecorView();  
    	  view.getWindowVisibleDisplayFrame(rect);  
    	  lay.height = dm.heightPixels - rect.top;  
    	  lay.width = dm.widthPixels;  
    }  
 
    /**
     * 
     * [Summary]
     *       setTitile ����
     * @param strTitle
     * @return
     *
     */
    public CustomProgressDialog setTitile(String strTitle){
    	return customProgressDialog;
    }
    
    /**
     * 
     * [Summary]
     *       setMessage ��ʾ����
     * @param strMessage
     * @return
     *
     */
    public void setMessage(String strMessage){
    	TextView tvMsg = (TextView)customProgressDialog.findViewById(R.id.id_tv_loadingmsg);
    	
    	if (tvMsg != null){
    		tvMsg.setText(strMessage);
    	}
    	
//    	return customProgressDialog;
    }
    public TextView getText(){
		return (TextView)customProgressDialog.findViewById(R.id.message);
	}
	public void setText(String text){
		((TextView)customProgressDialog.findViewById(R.id.message)).setText(text);
	}

}
