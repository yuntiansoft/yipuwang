package cn.com.yuntian.wojia.layout;

import java.util.ArrayList;

import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.View.OnTouchListener;
//import android.view.WindowManager;
//import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Scroller;
//import android.widget.TextView;
//import android.widget.Toast;
//import com.cn.hailin.android.R;
//import com.cn.hailin.android.util.Constants;
//import android.annotation.SuppressLint;
import android.content.Context;

/**
 * 
 * @author chenwh
 * 
 */
public class SlideCutListView extends ListView implements OnTouchListener {
	/**
	 * ��ǰ������ListView��position
	 */
	private int slidePosition;
	/**
	 * ��ָ����Y������
	 */
	private int downY;
	/**
	 * ��ָ����X������
	 */
	private int downX;
	
//	private int mLastX = 0;
//	/**
//	 * ��Ļ���
//	 */
//	private int screenWidth;
	
	private int mHolderWidth = 180;
	
	private ArrayList<Integer> rVisible = new ArrayList<Integer>();
	
	/**
	 * ListView��item
	 */
	private View itemView;
	/**
	 * ������
	 */
	private Scroller scroller;
//	private static final int SNAP_VELOCITY = 200;
	/**
	 * �ٶ�׷�ٶ���
	 */
	private VelocityTracker velocityTracker;
	/**
	 * LIstItem�Ƿ���Ӧ������Ĭ��Ϊ����Ӧ
	 */
	private boolean isItemSlide = false;
	
	private boolean isScroll = false;
	
	/**
	 * SlideMenu�Ƿ���Ӧ������Ĭ��Ϊ����Ӧ
	 */
//	private boolean isMenuSlide = true;
	
//	/**
//	 * ��ʾ״̬��Ĭ��ΪListView��ʾ
//	 */
//	private String displayStatus = Constants.DISPLAY_STATUS_1;
	
	/**
	 * ��Ϊ���û���������С����
	 */
	private int mTouchSlop;
	
	private float mMinimumVelocity;
	
	private float mMaximumVelocity;
	
//	/**
//	 *  �Ƴ�item��Ļص��ӿ�
//	 */
//	private RemoveListener mRemoveListener;
//	/**
//	 * ����ָʾitem������Ļ�ķ���,�����������,��һ��ö��ֵ�����
//	 */
//	private RemoveDirection removeDirection;

//	// ����ɾ�������ö��ֵ
//	public enum RemoveDirection {
//		RIGHT, LEFT;
//	}


	public SlideCutListView(Context context) {
		this(context, null);
	}

	public SlideCutListView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public SlideCutListView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
//		screenWidth = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getWidth();
		scroller = new Scroller(context);
		mTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
		mHolderWidth = Math.round(TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, mHolderWidth, getResources()
                        .getDisplayMetrics()));
		mMinimumVelocity = ViewConfiguration.get(context).getScaledMinimumFlingVelocity();
        mMaximumVelocity = ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
		setOnTouchListener(this);
	}
	
//	/**
//	 * ���û���ɾ���Ļص��ӿ�
//	 * @param removeListener
//	 */
//	public void setRemoveListener(RemoveListener removeListener) {
//		this.mRemoveListener = removeListener;
//	}

	/**
	 * �ַ��¼�����Ҫ�������жϵ�������Ǹ�item, �Լ�ͨ��postDelayed��������Ӧ���һ����¼�
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		isScroll = false;
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN: {
//			this.setLongClickable(true);
//			addVelocityTracker(event);
//			isMenuSlide = true;
			
			// ����scroller������û�н������˴���BUG
			if (!scroller.isFinished()) {
//				isMenuSlide = false;
//				return super.dispatchTouchEvent(event);
//				return false;
				isScroll = true;
				return true;
			}
			downX = (int) event.getX();
			downY = (int) event.getY();

			slidePosition = pointToPosition(downX, downY);

			// ��Ч��position, �����κδ���
			if (slidePosition == AdapterView.INVALID_POSITION) {
				return super.dispatchTouchEvent(event);
			}

			// ��ȡ���ǵ����item view
			itemView = getChildAt(slidePosition - getFirstVisiblePosition());
			
			// �Ҳ�ɾ����ʾ
			if (rVisible.size() > 0) {
				if (slidePosition != rVisible.get(0)) {
					getChildAt(rVisible.get(0) - getFirstVisiblePosition()).scrollTo(0, 0);
//					isMenuSlide = true;
					rVisible = new ArrayList<Integer>();
				}
			}
			
			break;
		}
		case MotionEvent.ACTION_MOVE: {
			if ((Math.abs(event.getX() - downX) > mTouchSlop && Math
							.abs(event.getY() - downY) < mTouchSlop)
					&& ((rVisible.size() == 0 && downX - event.getX() > 0)
					|| rVisible.size() > 0)) {
				isItemSlide = true;
			} 
			break;
		}
		case MotionEvent.ACTION_UP: {
			if (rVisible.size() > 0) {
				if (slidePosition == rVisible.get(0)) {
					isItemSlide = true;
				}
			}
//			recycleVelocityTracker();
			break;
		}
		case MotionEvent.ACTION_CANCEL:
			if (rVisible.size() > 0) {
				if (slidePosition == rVisible.get(0)) {
					isItemSlide = true;
				}
			}
//			recycleVelocityTracker();
			break;
		}

		return super.dispatchTouchEvent(event);
//		return false;
	}

//	/**
//	 * ���һ�����getScrollX()���ص������Ե�ľ��룬������View���ԵΪԭ�㵽��ʼ�����ľ��룬�������ұ߻���Ϊ��ֵ
//	 */
//	private void scrollRight() {
//		final int delta = (screenWidth + itemView.getScrollX());
//		// ����startScroll����������һЩ�����Ĳ�����������computeScroll()�����е���scrollTo������item
//		scroller.startScroll(itemView.getScrollX(), 0, -delta, 0,
//				Math.abs(delta));
//		postInvalidate(); // ˢ��itemView
//	}

//	/**
//	 * ���󻬶���������������֪�����󻬶�Ϊ��ֵ
//	 */
//	private void scrollLeft() {
//		final int delta = (screenWidth - itemView.getScrollX());
//		// ����startScroll����������һЩ�����Ĳ�����������computeScroll()�����е���scrollTo������item
//		scroller.startScroll(itemView.getScrollX(), 0, delta, 0,
//				Math.abs(delta));
//	}

	/**
	 * ������ָ����itemView�ľ������ж��ǹ�������ʼλ�û�������������ҹ���
	 */
	private void scrollByDistanceX() {
//		if (rVisible.size() > 0) {
//			isMenuSlide = true;
//			rVisible = new ArrayList<Integer>();
//			// ���ص�ԭʼλ��,Ϊ��͵����������ֱ�ӵ���scrollTo����
//			itemView.scrollTo(0, 0);
//		} else {
			if (itemView.getScrollX() >= mHolderWidth / 2) {
//				isMenuSlide = false;
				rVisible = new ArrayList<Integer>();
				rVisible.add(slidePosition);
				smoothScrollTo(mHolderWidth);
			} else {
//				isMenuSlide = true;
				rVisible = new ArrayList<Integer>();
				// ���ص�ԭʼλ��,Ϊ��͵����������ֱ�ӵ���scrollTo����
				itemView.scrollTo(0, 0);
			}
//		}
//		// �����������ľ��������Ļ�Ķ���֮һ��������ɾ��
//		if (itemView.getScrollX() >= mHolderWidth / 2) {
//			Log.e("onTouchEvent", "itemView.getScrollX() >= mHolderWidth / 2");
//			isMenuSlide = false;
//			rVisible = new ArrayList<Integer>();
//			rVisible.add(slidePosition);
//			smoothScrollTo(mHolderWidth);
//		} else if (itemView.getScrollX() < 0) {
//			Log.e("onTouchEvent", "itemView.getScrollX() <= -mHolderWidth / 2");
//			isMenuSlide = true;
//			rVisible = new ArrayList<Integer>();
//			smoothScrollTo(-mHolderWidth);
//		} else {
//			Log.e("onTouchEvent", "scrollTo(0, 0)");
//			isMenuSlide = true;
//			rVisible = new ArrayList<Integer>();
//			// ���ص�ԭʼλ��,Ϊ��͵����������ֱ�ӵ���scrollTo����
//			itemView.scrollTo(0, 0);
//		}

	}
	
	private void smoothScrollTo(int destX) {
        // ����������ָ��λ��
        int scrollX = itemView.getScrollX();
        int delta = destX - scrollX;
        scroller.startScroll(scrollX, 0, delta, 0, Math.abs(delta));
        invalidate();
    }

	/**
	 * ���������϶�ListView item���߼�
	 */
	@Override
	public boolean onTouch(View v, MotionEvent ev) {
		if (isScroll) {
			return true;
		}
		if (itemView != null) {
	        int scrollX = itemView.getScrollX();
			if (isItemSlide && slidePosition != AdapterView.INVALID_POSITION) {
				requestDisallowInterceptTouchEvent(true);
				addVelocityTracker(ev);
				final int action = ev.getAction();
				int x = (int) ev.getX();
				switch (action) {
				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_MOVE:
	//				MotionEvent cancelEvent = MotionEvent.obtain(ev);
	//	            cancelEvent.setAction(MotionEvent.ACTION_CANCEL |
	//	                       (ev.getActionIndex()<< MotionEvent.ACTION_POINTER_INDEX_SHIFT));
	//	            onTouchEvent(cancelEvent);
		            
	//				int deltaX = downX - x;
	//				if (deltaX < 0) {
	//					if (deltaX < (-mHolderWidth)) {
	//						deltaX = -mHolderWidth;
	//					}		
	//				} else if (deltaX > mHolderWidth) {
	//					deltaX = mHolderWidth;
	//				}
	//				downX = x;
	
					// ��ָ�϶�itemView����, deltaX����0���������С��0���ҹ�
	//				itemView.scrollBy(deltaX, 0);
//					if (rVisible.size() > 0) {
//						if (x - downX <= 0) {
////							this.setLongClickable(false);
//							return true;
//						} 
//					}
					
					int deltaX = x - downX;
		            int newScrollX = scrollX - deltaX;
		            if (deltaX != 0) {
		                if (newScrollX < 0) {
		                    newScrollX = 0;
		                } else if (newScrollX > mHolderWidth) {
		                    newScrollX = mHolderWidth;
		                }
//		                isMenuSlide = false;
		                itemView.scrollTo(newScrollX, 0);
		            }
		            downX = x;
//		            this.setLongClickable(false);
					return true;  //�϶���ʱ��ListView������
				case MotionEvent.ACTION_UP:
//					int velocityX = getScrollVelocity();
					final VelocityTracker mVelocityTracker = velocityTracker;
					mVelocityTracker.computeCurrentVelocity(1000, mMaximumVelocity);
					int velocityX = (int) mVelocityTracker.getXVelocity();
					if (velocityX > mMinimumVelocity) {
//						isMenuSlide = true;
						rVisible = new ArrayList<Integer>();
						itemView.scrollTo(0, 0);
					} else if (velocityX < -mMinimumVelocity) {
//						isMenuSlide = false;
						rVisible = new ArrayList<Integer>();
						rVisible.add(slidePosition);
						smoothScrollTo(mHolderWidth);
					} else {
						scrollByDistanceX();
					}
					
					recycleVelocityTracker();
					// ��ָ�뿪��ʱ��Ͳ���Ӧ���ҹ���
					isItemSlide = false;
//					this.setLongClickable(false);
					return true;
				case MotionEvent.ACTION_CANCEL:
					final VelocityTracker velocity = velocityTracker;
					velocity.computeCurrentVelocity(1000, mMaximumVelocity);
					int velocityXMove = (int) velocity.getXVelocity();
					if (velocityXMove > mMinimumVelocity) {
//						isMenuSlide = true;
						rVisible = new ArrayList<Integer>();
						itemView.scrollTo(0, 0);
					} else if (velocityXMove < -mMinimumVelocity) {
//						isMenuSlide = false;
						rVisible = new ArrayList<Integer>();
						rVisible.add(slidePosition);
						smoothScrollTo(mHolderWidth);
					} else {
						scrollByDistanceX();
					}
					
					recycleVelocityTracker();
					// ��ָ�뿪��ʱ��Ͳ���Ӧ���ҹ���
					isItemSlide = false;
//					this.setLongClickable(false);
					return true;
				}
			} else {
			}
		}

		//����ֱ�ӽ���ListView������onTouchEvent�¼�
//		return super.onTouchEvent(ev);
		
		return false;
	}

	@Override
	public void computeScroll() {
		// ����startScroll��ʱ��scroller.computeScrollOffset()����true��
		if (scroller.computeScrollOffset()) {
			// ��ListView item���ݵ�ǰ�Ĺ���ƫ�������й���
			itemView.scrollTo(scroller.getCurrX(), scroller.getCurrY());
			
			postInvalidate();

			// ��������������ʱ����ûص��ӿ�
			if (scroller.isFinished()) {
//				if (mRemoveListener == null) {
//					throw new NullPointerException("RemoveListener is null, we should called setRemoveListener()");
//				}
//				
//				itemView.scrollTo(0, 0);
//				mRemoveListener.removeItem(removeDirection, slidePosition);
			}
		}
	}

	/**
	 * ����û����ٶȸ�����
	 * 
	 * @param event
	 */
	private void addVelocityTracker(MotionEvent event) {
		if (velocityTracker == null) {
			velocityTracker = VelocityTracker.obtain();
		}

		velocityTracker.addMovement(event);
	}

	/**
	 * �Ƴ��û��ٶȸ�����
	 */
	private void recycleVelocityTracker() {
		if (velocityTracker != null) {
			velocityTracker.recycle();
			velocityTracker = null;
		}
	}

//	/**
//	 * ��ȡX����Ļ����ٶ�,����0���һ�������֮����
//	 * 
//	 * @return
//	 */
//	private int getScrollVelocity() {
//		velocityTracker.computeCurrentVelocity(1000);
//		int velocity = (int) velocityTracker.getXVelocity();
//		return velocity;
//	}
	
//	/**
//	 * ��ȡSlideMenu�Ƿ���Ӧ������״̬
//	 * 
//	 * @return
//	 */
//	public boolean getIsMenuSlide() {
//		return isMenuSlide;
//	}
	
//	/**
//	 * ��ȡSlideMenu�Ƿ���Ӧ������״̬
//	 * 
//	 * @return
//	 */
//	public void setIsMenuSlide(boolean bl) {
//		isMenuSlide = bl;
//	}
	
	
	
//	/**
//	 * ���õ�ǰ��ʾ״̬
//	 * 
//	 */
//	public void setDisplayStatus(String displayStatus) {
//		this.displayStatus = displayStatus;
//	}
	
//	/**
//	 * ��ȡ��ǰ��ʾ״̬
//	 * 
//	 * @return
//	 */
//	public String getDisplayStatus() {
//		return this.displayStatus;
//	}

//	/**
//	 * 
//	 * ��ListView item������Ļ���ص�����ӿ�
//	 * ������Ҫ�ڻص�����removeItem()���Ƴ���Item,Ȼ��ˢ��ListView
//	 * 
//	 * @author xiaanming
//	 *
//	 */
//	public interface RemoveListener {
//		public void removeItem(RemoveDirection direction, int position);
//	}
	
	public void initRVisible() {
		this.rVisible = new ArrayList<Integer>();
	}
	
	public ArrayList<Integer> getRVisible() {
		return this.rVisible;
	}
	
	public int getMHolderWidth() {
		return this.mHolderWidth;
	}

}