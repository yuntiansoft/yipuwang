package cn.com.yuntian.wojia.layout;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;


public class SlideSwitch extends View implements OnTouchListener {  
	  
    private boolean nowChoose = false;// ��¼��ǰ��ť�Ƿ�򿪣�trueΪ�򿪣�falseΪ�ر�  
    private boolean onSlip = false;// ��¼�û��Ƿ��ڻ���  
    private float downX, nowX; // ����ʱ��x����ǰ��x  
    private Rect btn_on, btn_off;// �򿪺͹ر�״̬�£��α��Rect  
  
    private boolean isChgLsnOn = false;//�Ƿ����ü���  
    private OnChangedListener changedLis;  
  
    private Bitmap bg_cen, bg_fah, slip_btn;  
  
    public SlideSwitch(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        init();  
    }  
  
    public SlideSwitch(Context context) {  
        super(context);  
        init();  
    }  

    private void init() { 
        // ����ͼƬ��Դ  
    	bg_cen = BitmapFactory.decodeResource(getResources(),  
                R.drawable.slid_bg_cen);  
    	bg_fah = BitmapFactory.decodeResource(getResources(),  
                R.drawable.slid_bg_fah);
        slip_btn = BitmapFactory.decodeResource(getResources(),  
                R.drawable.fan_on);  
        // �����Ҫ��Rect����  
        btn_on = new Rect(0, 0, bg_cen.getWidth(), slip_btn.getHeight());  
        btn_off = new Rect(bg_fah.getWidth() - slip_btn.getWidth(), 0,  
        		bg_fah.getWidth(), slip_btn.getHeight()); 
        
 		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
 			nowChoose = true;
 			nowX = bg_cen.getWidth();
 		} 
      
        setOnTouchListener(this);  
    }  
      
	@Override  
    protected void onDraw(Canvas canvas) {    
          
        Matrix matrix = new Matrix();  
        Paint paint = new Paint();  
        float x;  
          
          
            if (nowX<(bg_cen.getWidth()/2)) //������ǰ�������εı�����ͬ,�ڴ����ж�  
                canvas.drawBitmap(bg_fah, matrix, paint);//�����ر�ʱ�ı���  
            else  
                canvas.drawBitmap(bg_cen, matrix, paint);//������ʱ�ı���   
                  
            if (onSlip) {//�Ƿ����ڻ���״̬,    
                if(nowX >= bg_cen.getWidth())//�Ƿ񻮳�ָ����Χ,�������α��ܵ���ͷ,����������ж�  
                    x = bg_cen.getWidth() - slip_btn.getWidth()/2;//��ȥ�α�1/2�ĳ���  
                else  
                    x = nowX - slip_btn.getWidth()/2;  
            } else {  
                if(nowChoose)//�������ڵĿ���״̬���û��α��λ��   
                    x = btn_off.left;  
                else  
                    x = btn_on.left;  
            }  
              
            if (x < 0 ) //���α�λ�ý����쳣�ж�..  
                x = 0;  
            else if(x > bg_cen.getWidth() - slip_btn.getWidth())  
                x = bg_cen.getWidth() - slip_btn.getWidth();  
              
            canvas.drawBitmap(slip_btn, x, 0, paint);//�����α�.     
            
            super.onDraw(canvas);
         
    }  
  
    @Override  
    public boolean onTouch(View v, MotionEvent event) {  
          
        switch (event.getAction()) {//���ݶ�����ִ�д���  
              
            case MotionEvent.ACTION_MOVE://����  
                nowX = event.getX();  
                break;  
            case MotionEvent.ACTION_DOWN://����  
                if (event.getX() > bg_cen.getWidth() || event.getY() > bg_cen.getHeight())   
                    return false;  
                onSlip = true;  
                downX = event.getX();  
                nowX = downX;  
                break;  
            case MotionEvent.ACTION_UP://�ɿ�  
                onSlip = false;  
                boolean lastChoose = nowChoose;  
                if (event.getX() >= (bg_cen.getWidth()/2))   
                    nowChoose = true;  
                else   
                    nowChoose = false;  
                if(isChgLsnOn && (lastChoose != nowChoose))//��������˼�����,�͵����䷽��.  
                    changedLis.OnChanged(nowChoose);  
                break;  
            default:  
                break;  
        }  
        invalidate();  
        return true;  
    }  
      
    public void SetOnChangedListener(OnChangedListener l){//���ü�����,��״̬�޸ĵ�ʱ��  
        isChgLsnOn = true;  
        changedLis = l;  
    }  
      
    public interface OnChangedListener {  
        abstract void OnChanged(boolean checkState);  
    }  
} 

