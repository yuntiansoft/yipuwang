package cn.com.yuntian.wojia.logic;

/**
 * AmerTempBean
 * @author chenwh
 *
 */
public class AmerTempBean implements Cloneable {
	
	private String mac = null;
	
	private String tempHeat = null;
	
	private String tempCool = null;
	
	private String mod = null;
	
	private String fanMod = null;
	
	private String promable = null;
	
	private String humi = null;
	
	private String status = null;
	
	private String disTemp = null; 
	
	private String disHumi = null; 
	
	private String userName = null;
	
	private String deadZoneTemp = null;
	
	private String autoStatus = null;
	
	private String emerStatus = null;
	
	private String permStatus = null;
	
	private String oriMod = null;
	
	private String heatStatus = null;
	
	private String coldStatus = null;
	
	private String oriStatus = null;

	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the tempHeat
	 */
	public String getTempHeat() {
		return tempHeat;
	}

	/**
	 * @param tempHeat the tempHeat to set
	 */
	public void setTempHeat(String tempHeat) {
		this.tempHeat = tempHeat;
	}

	/**
	 * @return the tempCool
	 */
	public String getTempCool() {
		return tempCool;
	}

	/**
	 * @param tempCool the tempCool to set
	 */
	public void setTempCool(String tempCool) {
		this.tempCool = tempCool;
	}

	/**
	 * @return the mod
	 */
	public String getMod() {
		return mod;
	}

	/**
	 * @param mod the mod to set
	 */
	public void setMod(String mod) {
		this.mod = mod;
	}

	/**
	 * @return the fanMod
	 */
	public String getFanMod() {
		return fanMod;
	}

	/**
	 * @param fanMod the fanMod to set
	 */
	public void setFanMod(String fanMod) {
		this.fanMod = fanMod;
	}

	/**
	 * @return the promable
	 */
	public String getPromable() {
		return promable;
	}

	/**
	 * @param promable the promable to set
	 */
	public void setPromable(String promable) {
		this.promable = promable;
	}

	/**
	 * @return the humi
	 */
	public String getHumi() {
		return humi;
	}

	/**
	 * @param humi the humi to set
	 */
	public void setHumi(String humi) {
		this.humi = humi;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the disTemp
	 */
	public String getDisTemp() {
		return disTemp;
	}

	/**
	 * @param disTemp the disTemp to set
	 */
	public void setDisTemp(String disTemp) {
		this.disTemp = disTemp;
	}

	/**
	 * @return the disHumi
	 */
	public String getDisHumi() {
		return disHumi;
	}

	/**
	 * @param disHumi the disHumi to set
	 */
	public void setDisHumi(String disHumi) {
		this.disHumi = disHumi;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the deadZoneTemp
	 */
	public String getDeadZoneTemp() {
		return deadZoneTemp;
	}

	/**
	 * @param deadZoneTemp the deadZoneTemp to set
	 */
	public void setDeadZoneTemp(String deadZoneTemp) {
		this.deadZoneTemp = deadZoneTemp;
	} 
	
	/**
	 * @return the autoStatus
	 */
	public String getAutoStatus() {
		return autoStatus;
	}

	/**
	 * @param autoStatus the autoStatus to set
	 */
	public void setAutoStatus(String autoStatus) {
		this.autoStatus = autoStatus;
	}

	/**
	 * @return the emerStatus
	 */
	public String getEmerStatus() {
		return emerStatus;
	}

	/**
	 * @param emerStatus the emerStatus to set
	 */
	public void setEmerStatus(String emerStatus) {
		this.emerStatus = emerStatus;
	}

	/**
	 * @return the permStatus
	 */
	public String getPermStatus() {
		return permStatus;
	}

	/**
	 * @param permStatus the permStatus to set
	 */
	public void setPermStatus(String permStatus) {
		this.permStatus = permStatus;
	}

	/**
	 * @return the oriMod
	 */
	public String getOriMod() {
		return oriMod;
	}

	/**
	 * @param oriMod the oriMod to set
	 */
	public void setOriMod(String oriMod) {
		this.oriMod = oriMod;
	}

	/**
	 * @return the coldStatus
	 */
	public String getColdStatus() {
		return coldStatus;
	}

	/**
	 * @param coldStatus the coldStatus to set
	 */
	public void setColdStatus(String coldStatus) {
		this.coldStatus = coldStatus;
	}

	/**
	 * @return the heatStatus
	 */
	public String getHeatStatus() {
		return heatStatus;
	}

	/**
	 * @param heatStatus the heatStatus to set
	 */
	public void setHeatStatus(String heatStatus) {
		this.heatStatus = heatStatus;
	}

	/**
	 * @return the oriStatus
	 */
	public String getOriStatus() {
		return oriStatus;
	}

	/**
	 * @param oriStatus the oriStatus to set
	 */
	public void setOriStatus(String oriStatus) {
		this.oriStatus = oriStatus;
	}
	
	public Object clone(){
		AmerTempBean o = null;
        try{
            o = (AmerTempBean) super.clone();
        } catch (CloneNotSupportedException e) {
           
        }
        return o;
    }
}
