package cn.com.yuntian.wojia.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.fragment.AmerFanFragment;
import cn.com.yuntian.wojia.fragment.AmerHomepgFragment;
import cn.com.yuntian.wojia.fragment.AmerStatusFragment;
import cn.com.yuntian.wojia.fragment.AmerTempFragment;
import cn.com.yuntian.wojia.fragment.AmericanTempFragment;
import cn.com.yuntian.wojia.fragment.FtkzyFragment;
import cn.com.yuntian.wojia.fragment.GreenFragment;
import cn.com.yuntian.wojia.fragment.HeatingTempFragment;
import cn.com.yuntian.wojia.fragment.LingDongTempFragment;
import cn.com.yuntian.wojia.fragment.LingdongCoolHeatFragment;
import cn.com.yuntian.wojia.fragment.YiPuWTempFragment;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;
//import com.cn.hailin.android.util.Util;


/**
 * BackgroundTask
 * 
 * @author chenwh
 * @param <Header>
 */
public class BackgroundTask {
	
	private Context context;
	private HailinDB db;
	private boolean isTaskContinue = false;
//	private boolean isHistory = false;
	private boolean listBl = false;
	private boolean pm25Bl = false;
	private boolean pm25VocBl = false;
	private boolean amerBl = false;
	private boolean amerHomeBl = false;
	private boolean amerFanBl = false;
	private boolean amerStaBl = false;
	private boolean amerTempBl = false;
	private boolean greenBl = false;
	private boolean heatingBl = false;
	private boolean co2VocBl = false;
	private boolean lingDongBl = false;
	private boolean lingDongScheBl = false;
	private boolean ftkzyBl = false;
	private boolean yiPuWBl = false;
	private String mac;
	private String type;
	private int errorCount = 0;


	public BackgroundTask(Context context, HailinDB db, String mac, String type) {
		this.context = context;
		this.db = db;
		this.mac = mac;
		this.type = type;
	}

//	public boolean taskIsContinue() {  	
//    	KeyguardManager mKeyguardManager = (KeyguardManager) 
//    			context.getSystemService(Context.KEYGUARD_SERVICE);   
//        if (mKeyguardManager.inKeyguardRestrictedInputMode() 
//        		|| getTopActivityName()) {  
//            return false;
//        }      
//        return true;
//    }
//    
//    private  boolean  getTopActivityName(){
//        String topActivityClassName=null;
//        String packageName = context.getPackageName();
//        ActivityManager activityManager = (ActivityManager)(context.getSystemService(
//        		android.content.Context.ACTIVITY_SERVICE )) ;
//        List<RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(1) ;
//        if (runningTaskInfos != null) {
//        	ComponentName f=runningTaskInfos.get(0).topActivity;
//            topActivityClassName = f.getClassName();
//        }
//        if (packageName!=null && topActivityClassName !=null 
//        		&& topActivityClassName.startsWith(packageName)) {
//            return false;
//        } 
//        return true;
//    } 
    
    public synchronized void getDataFromWeb() {

    	listBl = false;
    	pm25Bl = false;
    	amerBl = false;
    	amerHomeBl = false;
    	amerFanBl = false;
    	amerStaBl = false;
    	amerTempBl = false;
    	greenBl = false;
    	pm25VocBl = false;
    	heatingBl = false;
    	isTaskContinue = false;
//    	isHistory = false;
    	co2VocBl = false;
    	lingDongBl = false;
    	lingDongScheBl = false;
    	ftkzyBl = false;
		yiPuWBl = false;
//    	if (context.getResources().getString(R.string.value_deviceid_atemp).equals(
//				type)) {
//    		if (AmericanTempFragment.getPageIsUpd()) {
//    			exeTask();
//			}
//		} else
		if (context.getResources().getString(R.string.value_deviceid_green).equals(
				type)) {
			if (GreenFragment.getPageIsUpd()) {
				exeTask();
			}
		} else if (context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(
				type)) {
			if (LingdongCoolHeatFragment.getPageIsUpd()) {
				exeTask();
			}
		} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals(
				type)) {
			if (HeatingTempFragment.getPageIsUpd()) {
				exeTask();
			}
//		} else if (Constants.AMER_HOMEPG_FRAGMENT.equals(type)) {
//			if (AmerHomepgFragment.getPageIsUpd()) {
//				exeTask();
//			}
//		} else if (Constants.AMER_STATUS_FRAGMENT.equals(type)) {
//			if (AmerStatusFragment.getPageIsUpd()) {
//				exeTask();
//			}
//		} else if (Constants.AMER_FAN_FRAGMENT.equals(type)) {
//			if (AmerFanFragment.getPageIsUpd()) {
//				exeTask();
//			}
//		} else if (Constants.AMER_TEMP_FRAGMENT.equals(type)) {
//			if (AmerTempFragment.getPageIsUpd()) {
//				exeTask();
//			}
		} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(
				type)) {
			if (LingDongTempFragment.getPageIsUpd()|| YiPuWTempFragment.getPageIsUpd()) {
				exeTask();
			}
		}
		else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(type)) {
			if (FtkzyFragment.getPageIsUpd()) {
				exeTask();
			}
		}else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(type)) {
			if (YiPuWTempFragment.getPageIsUpd()) {
				exeTask();
			}
		}

//        String url = context.getResources().getString(R.string.url_base)
//    			+ context.getResources().getText(R.string.url_get_list).toString();
    	String url = context.getResources().getText(R.string.url_get_list).toString();
        HttpClientUtil.get(url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray json) {
            	// 用户信息
				try {
//					JSONArray json = new JSONArray(response);
					List<ListItemBean> listItem = changeToListBean(json);
	    			compareData(listItem);
	    			deleteMobileDate(listItem);
	    			if (context.getResources().getString(R.string.value_deviceid_pm25).equals(
	    					type)) {
	    				if (pm25Bl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals(
	    					type)) {
	    				if (pm25VocBl) {
	    					setTaskContinue(true);
	    				}
//	    			} else if (context.getResources().getString(R.string.value_deviceid_atemp).equals(type)) {
//	    				if (amerBl) {
//	    					setTaskContinue(true);
//	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_green).equals(
	    					type)
	    					|| context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(
	    							type)) {
	    				if (greenBl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals(
	    					type)) {
	    				if (heatingBl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals(
	    					type)) {
	    				if (co2VocBl) {
	    					setTaskContinue(true);
	    				}
//	    			} else if (Constants.AMER_HOMEPG_FRAGMENT.equals(type)) {
//	    				if (amerHomeBl) {
//	    					setTaskContinue(true);
//	    				}
//	    			} else if (Constants.AMER_STATUS_FRAGMENT.equals(type)) {
//	    				if (amerStaBl) {
//	    					setTaskContinue(true);
//	    				}
//	    			} else if (Constants.AMER_FAN_FRAGMENT.equals(type)) {
//	    				if (amerFanBl) {
//	    					setTaskContinue(true);
//	    				}
//	    			} else if (Constants.AMER_TEMP_FRAGMENT.equals(type)) {
//	    				if (amerTempBl) {
//	    					setTaskContinue(true);
//	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(
	    					type)) {
	    				if (lingDongBl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(
	    					type)) {
	    				if (ftkzyBl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(
							type)) {
						if (yiPuWBl) {
							setTaskContinue(true);
						}
					} else if (Constants.LINGDONG_TEMP_SCHEDULE_FRAGMENT.equals(
	    					type)) {
	    				if (lingDongScheBl) {
	    					setTaskContinue(true);
	    				}
	    			} else if (listBl) {
	    				setTaskContinue(true);
	    			}
	    			errorCount = 0;
	    			exeTask();
				} catch (JSONException e) {
					Log.e(this.getClass().getName(), e.getMessage(), e);
	    			doException();
				}
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONArray errorResponse) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
    			doException();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject errorResponse) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
				try {
					if (errorResponse != null) {
						String msg = errorResponse.getString(context.getResources().getString(R.string.param_error));
		            	if (context.getResources().getString(R.string.value_code_fourZeroOne).equals(String.valueOf(statusCode))
		        				&& context.getResources().getString(R.string.value_err_invalid_token).equals(msg)) {
		            		errorCount = 0;
		        			getAuthorizatin();
		        		}
					}
				} catch (Exception ex) {

				}
            	doException();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
            	doException();
            }

        });
    }
    
    private void compareData(List<ListItemBean> webData) {
    	List<ListItemBean> listDbPm25Bean = db.getTDevice().findPm25();
    	List<ListItemBean> listDbPm25VocBean = db.getTDevice().findPm25Voc();
//    	List<ListItemBean> listDbAmerBean = db.getTDevice().findAmerTemp(" != ", 
//    			context.getResources().getString(R.string.value_deviceid_new_atemp));
    	List<ListItemBean> listDbGreenBean = db.getTDevice().findGreenTemp();
    	List<ListItemBean> listDbHeatingBean = db.getTDevice().findHeatingTemp();
    	List<ListItemBean> listDbCo2VocBean = db.getTDevice().findCo2Voc();
//    	List<ListItemBean> listDbNewAmerBean = db.getTDevice().findAmerTemp(" = ", 
//    			context.getResources().getString(R.string.value_deviceid_new_atemp));
    	List<ListItemBean> listDbLingDongBean = db.getTDevice().findLingDongTemp();
    	List<ListItemBean> listDbFtkzyBean = db.getTDevice().findFtkzy();
		List<ListItemBean> listDbYiPuWBean = db.getTDevice().findYiPuWTemp();
    	for (int i = 0; i < webData.size(); i++) {
    		ListItemBean webBean = webData.get(i);
    		if (context.getResources().getString(R.string.value_deviceid_pm25).equals(
    				webBean.getDeviceType())) {
    			compareSubPm25Data(webBean, listDbPm25Bean);
    		} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals(
    				webBean.getDeviceType())) {
    			compareSubPm25VocData(webBean, listDbPm25VocBean);
//    		} else if ((context.getResources().getString(R.string.value_deviceid_a8100).equals(
//    				webBean.getDeviceType()))
//    				|| (context.getResources().getString(R.string.value_deviceid_atemp).equals(
//    	    				webBean.getDeviceType()))) {
//    			compareSubAmerData(webBean, listDbAmerBean);
    		} else if (context.getResources().getString(R.string.value_deviceid_green).equals(
    				webBean.getDeviceType())
    				|| (context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals(
    	    				webBean.getDeviceType()))
    				|| (context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(
    	    				webBean.getDeviceType()))) {
    			compareSubGreenData(webBean, listDbGreenBean);
    		} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals(
    				webBean.getDeviceType())) {
    			compareSubHeatingData(webBean, listDbHeatingBean);
    		} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals(
    				webBean.getDeviceType())) {
    			compareSubCo2VocData(webBean, listDbCo2VocBean);
//    		} else if (context.getResources().getString(R.string.value_deviceid_new_atemp).equals(
//    				webBean.getDeviceType())) {
//    			compareSubNewAmerData(webBean, listDbNewAmerBean);
    		} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(
    				webBean.getDeviceType())) {
    			compareSubLingDongData(webBean, listDbLingDongBean);
    		} else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(
    				webBean.getDeviceType())) {
    			compareSubFtkzyData(webBean, listDbFtkzyBean);
    		}
			else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(
					webBean.getDeviceType())) {
				compareSubYiPuWData(webBean, listDbYiPuWBean);
			}
    	}
    }

	private void compareSubYiPuWData(ListItemBean webBean, List<ListItemBean> listDbYiPuWBean) {
		int count = 0;
		if (listDbYiPuWBean.size() == 0) {
			listBl = true;
			db.getTDevice().add(webBean);
			db.gettYiPuWTemp().add(webBean.getYiPuWTempBean());
		} else {
			for (int i = 0; i < listDbYiPuWBean.size(); i++ ) {
				ListItemBean dbBean =  listDbYiPuWBean.get(i);
				YiPuWTempBean yiPuWDbBwan = dbBean.getYiPuWTempBean();
				YiPuWTempBean yiPuWWebBwan = webBean.getYiPuWTempBean();

				if (!dbBean.getMac().equals(webBean.getMac())) {
					count++;
					continue;
				}

				boolean onlineBl = compareOnline(webBean, dbBean);
				boolean deviceBl = compareDeviceData(webBean, dbBean);
				if (deviceBl || onlineBl) {
					if (mac.equals(yiPuWWebBwan.getMac())) {
						if (!YiPuWTempFragment.getPageIsUpd()) {
							listBl = true;
							updateDeviceDb(webBean);
							if (onlineBl) {
								yiPuWBl = true;
							}
						}
					} else {
						listBl = true;
						updateDeviceDb(webBean);
					}
				}

				if (compareYiPuWData(yiPuWWebBwan, yiPuWDbBwan)) {
					if (mac.equals(yiPuWWebBwan.getMac())) {
						if (!YiPuWTempFragment.getPageIsUpd()) {
							heatingBl = true;
							updateYiPuWDb(yiPuWWebBwan);
						}
					} else {
						listBl = true;
						updateYiPuWDb(yiPuWWebBwan);
					}
				}

				break;
			}

			if (count == listDbYiPuWBean.size()) {
				String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
				YiPuWTempBean yiPuWTempBean = db.gettYiPuWTemp().findOne(arg);
				if (yiPuWTempBean == null) {
					listBl = true;
					db.getTDevice().add(webBean);
					db.gettYiPuWTemp().add(webBean.getYiPuWTempBean());
				}
			}
		}
	}

	private void updateYiPuWDb(YiPuWTempBean webBean) {
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constants.MAC, webBean.getMac());
		params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
		params.put(Constants.DIS_TEMP, webBean.getDisTemp());
		params.put(Constants.STATUS_ON_OFF, webBean.getStatusOnOff());
		params.put(Constants.STATUS, webBean.getStatus());
		params.put(Constants.TEMP_HEAT, webBean.getTempHeat());
		params.put(Constants.SAVE_ENERGY, webBean.getSaveEnergy());
		params.put(Constants.TEMP_HEAT_SAVE_ENERGY, webBean.getTempHeatSaveEnergy());
		params.put(Constants.TEMP_HEAT_DEFAULT_MAX,webBean.getTempHeatDefaultMax());
		params.put(Constants.TEMP_HEAT_DEFAULT_MIN,webBean.getTempHeatDefaultMin());
		params.put(Constants.HEAT_MODE,webBean.getHeatMode());
		params.put(Constants.FORBID_MODE,webBean.getForbidMode());
		db.gettYiPuWTemp().update(params);
	}

	private boolean compareYiPuWData(YiPuWTempBean yiPuWWebBwan, YiPuWTempBean yiPuWDbBwan) {
		boolean bl = false;
		if (!(yiPuWDbBwan.getDisTemp() == null && yiPuWWebBwan.getDisTemp() == null)) {
			if (yiPuWDbBwan.getDisTemp() != null && yiPuWWebBwan.getDisTemp() != null) {
				if (!yiPuWDbBwan.getDisTemp().equals(yiPuWWebBwan.getDisTemp())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getStatusOnOff() == null && yiPuWWebBwan.getStatusOnOff() == null)) {
			if (yiPuWDbBwan.getStatusOnOff() != null && yiPuWWebBwan.getStatusOnOff() != null) {
				if (!yiPuWDbBwan.getStatusOnOff().equals(yiPuWWebBwan.getStatusOnOff())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getStatus() == null && yiPuWWebBwan.getStatus() == null)) {
			if (yiPuWDbBwan.getStatus() != null && yiPuWWebBwan.getStatus() != null) {
				if (!yiPuWDbBwan.getStatus().equals(yiPuWWebBwan.getStatus())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getTempHeat() == null && yiPuWWebBwan.getTempHeat() == null)) {
			if (yiPuWDbBwan.getTempHeat() != null && yiPuWWebBwan.getTempHeat() != null) {
				if (!yiPuWDbBwan.getTempHeat().equals(yiPuWWebBwan.getTempHeat())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getSaveEnergy() == null && yiPuWWebBwan.getSaveEnergy() == null)) {
			if (yiPuWDbBwan.getSaveEnergy() != null && yiPuWWebBwan.getSaveEnergy() != null) {
				if (!yiPuWDbBwan.getSaveEnergy().equals(yiPuWWebBwan.getSaveEnergy())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getTempHeatSaveEnergy() == null && yiPuWWebBwan.getTempHeatSaveEnergy() == null)) {
			if (yiPuWDbBwan.getTempHeatSaveEnergy() != null && yiPuWWebBwan.getTempHeatSaveEnergy() != null) {
				if (!yiPuWDbBwan.getTempHeatSaveEnergy().equals(yiPuWWebBwan.getTempHeatSaveEnergy())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getTempHeatDefaultMax() == null && yiPuWWebBwan.getTempHeatDefaultMax() == null)) {
			if (yiPuWDbBwan.getTempHeatDefaultMax() != null && yiPuWWebBwan.getTempHeatDefaultMax() != null) {
				if (!yiPuWDbBwan.getTempHeatDefaultMax().equals(yiPuWWebBwan.getTempHeatDefaultMax())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getTempHeatDefaultMin() == null && yiPuWWebBwan.getTempHeatDefaultMin() == null)) {
			if (yiPuWDbBwan.getTempHeatDefaultMin() != null && yiPuWWebBwan.getTempHeatDefaultMin() != null) {
				if (!yiPuWDbBwan.getTempHeatDefaultMin().equals(yiPuWWebBwan.getTempHeatDefaultMin())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getHeatMode() == null && yiPuWWebBwan.getHeatMode() == null)) {
			if (yiPuWDbBwan.getHeatMode() != null && yiPuWWebBwan.getHeatMode() != null) {
				if (!yiPuWDbBwan.getHeatMode().equals(yiPuWWebBwan.getHeatMode())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(yiPuWDbBwan.getForbidMode() == null && yiPuWWebBwan.getForbidMode() == null)) {
			if (yiPuWDbBwan.getForbidMode() != null && yiPuWWebBwan.getForbidMode() != null) {
				if (!yiPuWDbBwan.getForbidMode().equals(yiPuWWebBwan.getForbidMode())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}

		return bl;

	}

	private void deleteMobileDate(List<ListItemBean> webData) {
    	List<ListItemBean> listDbBean = db.getTDevice().findAllDevice();
    	Map<String, String> param = new HashMap<String, String>();
    	param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	for (int i = 0; i < listDbBean.size(); i++) {
    		ListItemBean dbBean = listDbBean.get(i);
    		String dbMac = dbBean.getMac();
    		param.put(Constants.MAC, dbMac);
    		boolean bl = true;
    		for (int j = 0; j < webData.size(); j++) {
    			String webMac = webData.get(j).getMac();
    			if (dbMac.equals(webMac)) {
    				bl = false;
    				continue;
    			}
    		}
    		if (bl) {
	    		if (context.getResources().getString(R.string.value_deviceid_pm25).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTPM25().delete(param);
	    		} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTPM25Voc().delete(param);
//	    		} else if ((context.getResources().getString(R.string.value_deviceid_a8100).equals(
//	    				dbBean.getDeviceType()))
//	    				|| (context.getResources().getString(R.string.value_deviceid_atemp).equals(
//	    						dbBean.getDeviceType()))
//						|| (context.getResources().getString(R.string.value_deviceid_new_atemp).equals(
//	    						dbBean.getDeviceType()))) {
//	    			db.getTAmerTemp().delete(param);
	    		} else if (context.getResources().getString(R.string.value_deviceid_green).equals(
	    				dbBean.getDeviceType())
	    				|| (context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals(
	    						dbBean.getDeviceType()))
						|| (context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(
	    						dbBean.getDeviceType()))) {
	    			db.getTGreenTemp().delete(param);
	    		} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTHeatingTemp().delete(param);
	    		} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTCo2Voc().delete(param);
	    		} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTLingDongTemp().delete(param);
	    		} 
	    		else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(
	    				dbBean.getDeviceType())) {
	    			db.getTFtkzy().delete(param);
	    		}
				else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(
						dbBean.getDeviceType())) {
					db.gettYiPuWTemp().delete(param);
				}
	    		db.getTDevice().delete(param);
	    		listBl = true;
    		}
    	}
    }
    
    private void compareSubHeatingData(ListItemBean webBean, List<ListItemBean> listDbHeatingBean) {
    	int count = 0;
    	if (listDbHeatingBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTHeatingTemp().add(webBean.getHeatingTempBean());
    	} else {
    		for (int i = 0; i < listDbHeatingBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbHeatingBean.get(i);
        		HeatingTempBean heatingDbBean = dbBean.getHeatingTempBean();
        		HeatingTempBean heatingWebBean = webBean.getHeatingTempBean();
	
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(heatingWebBean.getMac())) {
        				if (!HeatingTempFragment.getPageIsUpd()) {
        					listBl = true;
                			updateDeviceDb(webBean);
                			if (onlineBl) {
                				heatingBl = true;
                			}
        				} 
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareHeatingData(heatingWebBean, heatingDbBean)) {
        			if (mac.equals(heatingWebBean.getMac())) {
        				if (!HeatingTempFragment.getPageIsUpd()) {
        					heatingBl = true;
        					updateHeatingDb(heatingWebBean);
        				}
        			} else {
        				listBl = true;
        				updateHeatingDb(heatingWebBean);
        			}	
        		}

        		break;
        	}
    		
    		if (count == listDbHeatingBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			HeatingTempBean heatingTempBean = db.getTHeatingTemp().findOne(arg);
    			if (heatingTempBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTHeatingTemp().add(webBean.getHeatingTempBean());
    			}
        	}
    	}
    }
    
    private void compareSubLingDongData(ListItemBean webBean, List<ListItemBean> listDbLingDongBean) {
    	int count = 0;
    	if (listDbLingDongBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTLingDongTemp().add(webBean.getLingDongTempBean());
    	} else {
    		for (int i = 0; i < listDbLingDongBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbLingDongBean.get(i);
        		LingDongTempBean lingDongDbBean = dbBean.getLingDongTempBean();
        		LingDongTempBean lingDongWebBean = webBean.getLingDongTempBean();
	
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(lingDongWebBean.getMac())) {
        				if (!LingDongTempFragment.getPageIsUpd()&&!YiPuWTempFragment.getPageIsUpd()) {
        					listBl = true;
                			updateDeviceDb(webBean);
                			if (onlineBl) {
                				if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(type)) {
                					lingDongBl = true;
                				} else if (Constants.LINGDONG_TEMP_SCHEDULE_FRAGMENT.equals(type)) {
                					lingDongScheBl = true;
                				}
                			}
        				} 
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareLingDongData(lingDongWebBean, lingDongDbBean)) {
        			if (mac.equals(lingDongWebBean.getMac())) {
        				if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(type)) {
	        				if (!LingDongTempFragment.getPageIsUpd()&&!YiPuWTempFragment.getPageIsUpd()) {
	        					lingDongBl = true;
	        					updateLingDongDb(lingDongWebBean);
	        				}
        				} else if (Constants.LINGDONG_TEMP_SCHEDULE_FRAGMENT.equals(type)) {
//        					lingDongScheBl = true;
            				updateLingDongDb(lingDongWebBean);
        				}
        			} else {
        				listBl = true;
        				updateLingDongDb(lingDongWebBean);
        			}	
        		}

        		break;
        	}
    		
    		if (count == listDbLingDongBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			LingDongTempBean lingDongTempBean = db.getTLingDongTemp().findOne(arg);
    			if (lingDongTempBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTLingDongTemp().add(webBean.getLingDongTempBean());
    			}
        	}
    	}
    }
    
    private void compareSubFtkzyData(ListItemBean webBean, List<ListItemBean> listDbFtkzyBean) {
    	int count = 0;
    	if (listDbFtkzyBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTFtkzy().add(webBean.getFtkzyBean());
    	} else {
    		for (int i = 0; i < listDbFtkzyBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbFtkzyBean.get(i);
        		FtkzyBean ftkzyDbBean = dbBean.getFtkzyBean();
        		FtkzyBean ftkzyWebBean = webBean.getFtkzyBean();
	
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(ftkzyWebBean.getMac())) {
        				if (!FtkzyFragment.getPageIsUpd()) {
        					listBl = true;
                			updateDeviceDb(webBean);
                			if (onlineBl) {
                				ftkzyBl = true;
                			}
        				} 
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareFtkzyData(ftkzyWebBean, ftkzyDbBean)) {
        			if (mac.equals(ftkzyWebBean.getMac())) {
        				if (!FtkzyFragment.getPageIsUpd()) {
        					ftkzyBl = true;
        					updateFtkzyDb(ftkzyWebBean);
        				}
        			} else {
        				listBl = true;
        				updateFtkzyDb(ftkzyWebBean);
        			}	
        		}

        		break;
        	}
    		
    		if (count == listDbFtkzyBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			FtkzyBean ftkzyBean = db.getTFtkzy().findOne(arg);
    			if (ftkzyBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTFtkzy().add(webBean.getFtkzyBean());
    			}
        	}
    	}
    }
    
    private void compareSubGreenData(ListItemBean webBean, List<ListItemBean> listDbGreenBean) {
    	int count = 0;
    	if (listDbGreenBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTGreenTemp().add(webBean.getGreenTempBean());
    	} else {
    		for (int i = 0; i < listDbGreenBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbGreenBean.get(i);
        		GreenTempBean greenDbBean = dbBean.getGreenTempBean();
        		GreenTempBean greenWebBean = webBean.getGreenTempBean();
	
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(greenWebBean.getMac())) {
        				boolean bl;
        				if (context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat)
        						.equals(webBean.getDeviceType())) {
        					bl = LingdongCoolHeatFragment.getPageIsUpd();
        				} else {
        					bl = GreenFragment.getPageIsUpd();
        				}
        				if (!bl) {
        					listBl = true;
                			updateDeviceDb(webBean);
                			if (onlineBl) {
                				greenBl = true;
                			}
        				} 
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareGreenData(greenWebBean, greenDbBean)) {
        			if (mac.equals(greenWebBean.getMac())) {
        				boolean bl;
        				if (context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat)
        						.equals(webBean.getDeviceType())) {
        					bl = LingdongCoolHeatFragment.getPageIsUpd();
        				} else {
        					bl = GreenFragment.getPageIsUpd();
        				}
        				if (!bl) {
        					greenBl = true;
        					updateGreenDb(greenWebBean);
        				}
        			} else {
        				listBl = true;
            			updateGreenDb(greenWebBean);
        			}	
        		}

        		break;
        	}
    		
    		if (count == listDbGreenBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			GreenTempBean greenTempBean = db.getTGreenTemp().findOne(arg);
    			if (greenTempBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTGreenTemp().add(webBean.getGreenTempBean());
    			}
        	}
    	}
    }
    
    private void compareSubAmerData(ListItemBean webBean, List<ListItemBean> listDbAmerBean) {
    	int count = 0;
    	if (listDbAmerBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTAmerTemp().add(webBean.getAmerTempBean());
    	} else {
    		for (int i = 0; i < listDbAmerBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbAmerBean.get(i);
        		AmerTempBean amerDbBean = dbBean.getAmerTempBean();
        		AmerTempBean amerWebBean = webBean.getAmerTempBean();

        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(amerWebBean.getMac())) {
        				if (!AmericanTempFragment.getPageIsUpd()) {
        					listBl = true;
                			updateDeviceDb(webBean);
                			if (onlineBl) {
                				amerBl = true;
                			}
        				} 
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareAmerData(amerWebBean, amerDbBean)) {	
        			if (mac.equals(amerWebBean.getMac())) {
        				if (!AmericanTempFragment.getPageIsUpd()) {
        					amerBl = true;
        					updateAmerDb(amerWebBean);
        				}
        			} else {
        				listBl = true;
        				updateAmerDb(amerWebBean);
        			}
        		}

        		break;
        	}
    		
    		if (count == listDbAmerBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			AmerTempBean amerTempBean = db.getTAmerTemp().findOne(arg);
    			if (amerTempBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTAmerTemp().add(webBean.getAmerTempBean());
    			}
        	}
    	}
    }
    
    private void compareSubNewAmerData(ListItemBean webBean, List<ListItemBean> listDbNewAmerBean) {
    	int count = 0;
    	if (listDbNewAmerBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		webBean.getAmerTempBean().setOriStatus(webBean.getAmerTempBean().getStatus());
    		db.getTAmerTemp().add(webBean.getAmerTempBean());
    	} else {
    		for (int i = 0; i < listDbNewAmerBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbNewAmerBean.get(i);
        		AmerTempBean amerDbBean = dbBean.getAmerTempBean();
        		AmerTempBean amerWebBean = webBean.getAmerTempBean();

        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		boolean deviceBl = compareDeviceData(webBean, dbBean);
        		if (deviceBl || onlineBl) {		
        			if (mac.equals(amerWebBean.getMac())) {
	        			if (Constants.AMER_HOMEPG_FRAGMENT.equals(type)) {
	        				if (!AmerHomepgFragment.getPageIsUpd()) {
	        					listBl = true;
	                			updateDeviceDb(webBean);
	                			if (onlineBl) {
	                				amerHomeBl = true;
	                			}
	        				} 		
		    			} else if (Constants.AMER_STATUS_FRAGMENT.equals(type)) {
		    				if (!AmerStatusFragment.getPageIsUpd()) {
	        					listBl = true;
	                			updateDeviceDb(webBean);
	                			if (onlineBl) {
	                				amerStaBl = true;
	                			}
	        				} 			
		    			} else if (Constants.AMER_FAN_FRAGMENT.equals(type)) {
		    				if (!AmerFanFragment.getPageIsUpd()) {
	        					listBl = true;
	                			updateDeviceDb(webBean);
	                			if (onlineBl) {
	                				amerFanBl = true;
	                			}
	        				} 			
		    			} else if (Constants.AMER_TEMP_FRAGMENT.equals(type)) {
		    				if (!AmerTempFragment.getPageIsUpd()) {
	        					listBl = true;
	                			updateDeviceDb(webBean);
	                			if (onlineBl) {
	                				amerTempBl = true;
	                			}
	        				} 		
		    			}
        			} else {
        				listBl = true;
            			updateDeviceDb(webBean);
        			}
        		}
        		
        		if (compareAmerData(amerWebBean, amerDbBean)) {	
        			String oriSta = amerDbBean.getOriStatus();
    				String sta = amerWebBean.getStatus();
    				if (!sta.equals(oriSta) 
    						&& !context.getResources().getString(R.string.value_status_four).equals(sta)) {
    					amerWebBean.setOriStatus(sta);
    				} else {
    					amerWebBean.setOriStatus(oriSta);
    				}
        			if (mac.equals(amerWebBean.getMac())) {
        				if (Constants.AMER_HOMEPG_FRAGMENT.equals(type)) {
	        				if (!AmerHomepgFragment.getPageIsUpd()) {
	        					updateAmerDb(amerWebBean);
	                			amerHomeBl = true;
	        				} 		
		    			} else if (Constants.AMER_STATUS_FRAGMENT.equals(type)) {
		    				if (!AmerStatusFragment.getPageIsUpd()) {
	        					updateAmerDb(amerWebBean);
	        					boolean bl = false;
	        					if (!(amerDbBean.getStatus() == null && amerWebBean.getStatus() == null)) {
	        						if (amerDbBean.getStatus() != null && amerWebBean.getStatus() != null) {
	        							if (!amerDbBean.getStatus().equals(amerWebBean.getStatus())) {
	        								bl = true;
	        				    		}	
	        						} else {
	        							bl = true;
	        						}
	        					}
	                			if (bl) {
	                				amerStaBl = true;
	                			}
	        				} 			
		    			} else if (Constants.AMER_FAN_FRAGMENT.equals(type)) {
		    				if (!AmerFanFragment.getPageIsUpd()) {
	        					updateAmerDb(amerWebBean);
	        					boolean bl = false;
	        					if (!(amerDbBean.getFanMod() == null && amerWebBean.getFanMod() == null)) {
	        						if (amerDbBean.getFanMod() != null && amerWebBean.getFanMod() != null) {
	        							if (!amerDbBean.getFanMod().equals(amerWebBean.getFanMod())) {
	        								bl = true;
	        				    		}	
	        						} else {
	        							bl = true;
	        						}
	        					}
	                			if (bl) {
	                				amerFanBl = true;
	                			}
	        				} 			
		    			} else if (Constants.AMER_TEMP_FRAGMENT.equals(type)) {
		    				if (!AmerTempFragment.getPageIsUpd()) {
	        					updateAmerDb(amerWebBean);
	        					boolean bl = false;
	        					if (!(amerDbBean.getStatus() == null && amerWebBean.getStatus() == null)) {
	        						if (amerDbBean.getStatus() != null && amerWebBean.getStatus() != null) {
	        							if (!amerDbBean.getStatus().equals(amerWebBean.getStatus())) {
	        								bl = true;
	        				    		}	
	        						} else {
	        							bl = true;
	        						}
	        					}
	        					if (!(amerDbBean.getTempHeat() == null && amerWebBean.getTempHeat() == null)) {
	        						if (amerDbBean.getTempHeat() != null && amerWebBean.getTempHeat() != null) {
	        							if (!amerDbBean.getTempHeat().equals(amerWebBean.getTempHeat())) {
	        								bl = true;
	        				    		}	
	        						} else {
	        							bl = true;
	        						}
	        					}
	        					if (!(amerDbBean.getTempCool() == null && amerWebBean.getTempCool() == null)) {
	        						if (amerDbBean.getTempCool() != null && amerWebBean.getTempCool() != null) {
	        							if (!amerDbBean.getTempCool().equals(amerWebBean.getTempCool())) {
	        								bl = true;
	        				    		}	
	        						} else {
	        							bl = true;
	        						}
	        					}
	                			if (bl) {
	                				amerTempBl = true;
	                			}
	        				} 		
		    			}
        			} else {
        				listBl = true;
        				updateAmerDb(amerWebBean);
        			}
        		}

        		break;
        	}
    		
    		if (count == listDbNewAmerBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			AmerTempBean amerTempBean = db.getTAmerTemp().findOne(arg);
    			if (amerTempBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		webBean.getAmerTempBean().setOriStatus(webBean.getAmerTempBean().getStatus());
	        		db.getTAmerTemp().add(webBean.getAmerTempBean());
    			}
        	}
    	}
    }
    
    private void compareSubPm25Data(ListItemBean webBean, List<ListItemBean> listDbPm25Bean) {
    	int count = 0;
    	if (listDbPm25Bean.size() == 0) {
    		listBl = true;
//    		isHistory = true;
    		db.getTDevice().add(webBean);
    		db.getTPM25().add(webBean.getPm25bean());
    		
    	} else {
    		for (int i = 0; i < listDbPm25Bean.size(); i++ ) {
        		ListItemBean dbBean =  listDbPm25Bean.get(i);
        		PM25Bean pm25DbBean = dbBean.getPm25bean();
        		PM25Bean pm25WebBean = webBean.getPm25bean();
        		
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		if (compareDeviceData(webBean, dbBean) || onlineBl) {
        			listBl = true;
        			updateDeviceDb(webBean);
        			if (mac.equals(pm25WebBean.getMac()) && onlineBl) {
        				pm25Bl = true;
        			}
        		}
        		
        		if (comparePM25Data(pm25WebBean, pm25DbBean)) {
        			if (mac.equals(pm25WebBean.getMac())) {
        				pm25Bl = true;
        			}
        			
        			listBl = true;
        			updatePm25Db(pm25WebBean);
        		}

        		break;
        	}
    		
    		if (count == listDbPm25Bean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			PM25Bean pm25Bean = db.getTPM25().findOne(arg);
    			if (pm25Bean == null) {
	    			listBl = true;
	//    			isHistory = true;
	        		db.getTDevice().add(webBean);
	        		db.getTPM25().add(webBean.getPm25bean());
    			}
        	}
    	}
    }
    
    private void compareSubPm25VocData(ListItemBean webBean, List<ListItemBean> listDbPm25VocBean) {
    	int count = 0;
    	if (listDbPm25VocBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTPM25Voc().add(webBean.getPm25VocBean());
    	} else {
    		for (int i = 0; i < listDbPm25VocBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbPm25VocBean.get(i);
        		PM25VocBean pm25VocDbBean = dbBean.getPm25VocBean();
        		PM25VocBean pm25VocWebBean = webBean.getPm25VocBean();
        		
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		if (compareDeviceData(webBean, dbBean) || onlineBl) {
        			listBl = true;
        			updateDeviceDb(webBean);
        			if (mac.equals(pm25VocWebBean.getMac()) && onlineBl) {
        				pm25VocBl = true;
        			}
        		}
        		
        		if (comparePM25VocData(pm25VocWebBean, pm25VocDbBean)) {
        			if (mac.equals(pm25VocWebBean.getMac())) {
        				pm25VocBl = true;
        			}
        			
        			listBl = true;
        			updatePm25VocDb(pm25VocWebBean);
        		}

        		break;
        	}
    		
    		if (count == listDbPm25VocBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			PM25VocBean pm25VocBean = db.getTPM25Voc().findOne(arg);
    			if (pm25VocBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTPM25Voc().add(webBean.getPm25VocBean());
    			}
        	}
    	}
    }
    
    private void compareSubCo2VocData(ListItemBean webBean, List<ListItemBean> listDbCo2VocBean) {
    	int count = 0;
    	if (listDbCo2VocBean.size() == 0) {
    		listBl = true;
    		db.getTDevice().add(webBean);
    		db.getTCo2Voc().add(webBean.getCo2VocBean());
    	} else {
    		for (int i = 0; i < listDbCo2VocBean.size(); i++ ) {
        		ListItemBean dbBean =  listDbCo2VocBean.get(i);
        		Co2VocBean co2VocDbBean = dbBean.getCo2VocBean();
        		Co2VocBean co2VocWebBean = webBean.getCo2VocBean();
        		
        		if (!dbBean.getMac().equals(webBean.getMac())) {
        			count++;
        			continue;
        		} 
        		
        		boolean onlineBl = compareOnline(webBean, dbBean);
        		if (compareDeviceData(webBean, dbBean) || onlineBl) {
        			listBl = true;
        			updateDeviceDb(webBean);
        			if (mac.equals(co2VocWebBean.getMac()) && onlineBl) {
        				co2VocBl = true;
        			}
        		}
        		
        		if (compareCo2VocData(co2VocWebBean, co2VocDbBean)) {
        			if (mac.equals(co2VocWebBean.getMac())) {
        				co2VocBl = true;
        			}
        			
        			listBl = true;
        			updateCo2VocDb(co2VocWebBean);
        		}

        		break;
        	}
    		
    		if (count == listDbCo2VocBean.size()) {
    			String[] arg = {UserInfo.UserInfo.getUserName(), webBean.getMac()};
    			Co2VocBean co2VocBean = db.getTCo2Voc().findOne(arg);
    			if (co2VocBean == null) {
	    			listBl = true;
	        		db.getTDevice().add(webBean);
	        		db.getTCo2Voc().add(webBean.getCo2VocBean());
    			}
        	}
    	}
    }
    
    private void updateDeviceDb(ListItemBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.DIS_DEV_NAME, webBean.getDeviceName());
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.IS_ONLINE, webBean.getIsOnline());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.ADMIN_ONLY, webBean.getAdminOnly());
		db.getTDevice().update(params);
    }
    
    private void updatePm25Db(PM25Bean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.DIS_HUMI, webBean.getDisHumi());
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_PM25_IN, webBean.getDisPm25In());
    	params.put(Constants.DIS_PM25_OUT, webBean.getDisPm25Out());
		db.getTPM25().update(params);
    }
    
    private void updatePm25VocDb(PM25VocBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.DIS_HUMI, webBean.getDisHumi());
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_VOC, webBean.getDisVoc());
    	params.put(Constants.DIS_PM25_IN, webBean.getDisPm25In());
    	params.put(Constants.DIS_PM25_OUT, webBean.getDisPm25Out());
		db.getTPM25Voc().update(params);
    }
    
    private void updateCo2VocDb(Co2VocBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_VOC, webBean.getDisVoc());
    	params.put(Constants.DIS_CO2, webBean.getDisCo2());
		db.getTCo2Voc().update(params);
    }
    
    private void updateAmerDb(AmerTempBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DEAD_ZONE_TEMP, webBean.getDeadZoneTemp());
    	params.put(Constants.DIS_HUMI, webBean.getDisHumi());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.FAN_MOD, webBean.getFanMod());
    	params.put(Constants.HUMI, webBean.getHumi());
    	params.put(Constants.MOD, webBean.getMod());
    	params.put(Constants.PROMABLE, webBean.getPromable());
    	params.put(Constants.STATUS, webBean.getStatus());
    	params.put(Constants.TEMP_COOL, webBean.getTempCool());
    	params.put(Constants.TEMP_HEAT, webBean.getTempHeat());
    	params.put(Constants.HEAT_STATUS, webBean.getHeatStatus());
    	params.put(Constants.COLD_STATUS, webBean.getColdStatus());
    	params.put(Constants.AUTO_STATUS, webBean.getAutoStatus());
    	params.put(Constants.EMER_STATUS, webBean.getEmerStatus());
    	params.put(Constants.PERM_STATUS, webBean.getPermStatus());
    	params.put(Constants.ORI_MOD, webBean.getOriMod());
    	params.put(Constants.ORI_STATUS, webBean.getOriStatus());

		db.getTAmerTemp().update(params);
    }
    
    private void updateGreenDb(GreenTempBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.FAN_MOD, webBean.getFanMod());
    	params.put(Constants.MOD, webBean.getMod());
    	params.put(Constants.STATUS, webBean.getStatus());
    	params.put(Constants.STATUS_ON_OFF, webBean.getStatusOnOff());
    	params.put(Constants.TEMP_COOL, webBean.getTempCool());
    	params.put(Constants.TEMP_HEAT, webBean.getTempHeat());
    	params.put(Constants.SAVE_ENERGY, webBean.getSaveEnergy());
    	params.put(Constants.TEMP_COOL_SAVE_ENERGY, webBean.getTempCoolSaveEnergy());
    	params.put(Constants.TEMP_HEAT_SAVE_ENERGY, webBean.getTempHeatSaveEnergy());
    	params.put(Constants.INTS_HEAT_STAT, webBean.getIntsHeatStat());
    	params.put(Constants.HEAT_OUT_STAT, webBean.getHeatOutStat());
		params.put(Constants.TEMP_HEAT_DEFAULT_MIN,webBean.getTempHeatDefaultMin());
		params.put(Constants.TEMP_HEAT_DEFAULT_MAX,webBean.getTempHeatDefaultMAx());
		db.getTGreenTemp().update(params);
    }
    
    private void updateHeatingDb(HeatingTempBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.STATUS_ON_OFF, webBean.getStatusOnOff());
    	params.put(Constants.STATUS, webBean.getStatus());
    	params.put(Constants.TEMP_HEAT, webBean.getTempHeat());
    	params.put(Constants.SAVE_ENERGY, webBean.getSaveEnergy());
    	params.put(Constants.TEMP_HEAT_SAVE_ENERGY, webBean.getTempHeatSaveEnergy());
		params.put(Constants.TEMP_HEAT_DEFAULT_MAX,webBean.getTempHeatDefaultMax());
		params.put(Constants.TEMP_HEAT_DEFAULT_MIN,webBean.getTempHeatDefaultMin());
		db.getTHeatingTemp().update(params);
    }
    
    private void updateLingDongDb(LingDongTempBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.DIS_TEMP, webBean.getDisTemp());
    	params.put(Constants.STATUS_ON_OFF, webBean.getStatusOnOff());
    	params.put(Constants.STATUS, webBean.getStatus());
    	params.put(Constants.TEMP_HEAT, webBean.getTempHeat());
    	params.put(Constants.TEMP_OUT, webBean.getTempOut());
    	params.put(Constants.TEMP_ENERGY, webBean.getTempEnergy());
    	params.put(Constants.TEMP_COMFORT, webBean.getTempComfort());
    	params.put(Constants.HEAT_MODE, webBean.getHeatMode());
    	params.put(Constants.LINKAGE_INPUT, webBean.getLinkageInput());
    	params.put(Constants.LINKAGE_OUTPUT, webBean.getLinkageOutput());
    	params.put(Constants.CLOSE_BOILER, webBean.getCloseBoiler());
    	params.put(Constants.TEMP_HEAT_DEFAULT_MAX, webBean.getTempHeatDefaultMax());
    	params.put(Constants.TEMP_HEAT_DEFAULT_MIN, webBean.getTempHeatDefaultMin());
//    	params.put(Constants.SCHEDULE, webBean.getSchedule());

		db.getTLingDongTemp().update(params);
    }
    
    private void updateFtkzyDb(FtkzyBean webBean) {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put(Constants.MAC, webBean.getMac());
    	params.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
    	params.put(Constants.COLL_TEMP_ONE, webBean.getCollTempOne());
    	params.put(Constants.TANK_TEMP_TWO, webBean.getTankTempTwo());
    	params.put(Constants.TANK_TEMP_THREE, webBean.getTankTempThree());
    	params.put(Constants.BACK_TEMP_FOUR, webBean.getBackTempFour());
    	params.put(Constants.TANK_TEMP_FLG, webBean.getTankTempFlg());
    	params.put(Constants.COLL_PUMP, webBean.getCollPump());
    	params.put(Constants.PIPE_PUMP, webBean.getPipePump());
    	params.put(Constants.AUXI_HEAT, webBean.getAuxiHeat());
    	params.put(Constants.COLL_PUMP_ON_DEV, webBean.getCollPumpOnDev());
    	params.put(Constants.PIPE_PUMP_ON_DEV, webBean.getPipePumpOnDev());
    	params.put(Constants.AUXI_HEAT_ON_DEV, webBean.getAuxiHeatOnDev());

		db.getTFtkzy().update(params);
    }
    
    private boolean compareDeviceData(ListItemBean webData, ListItemBean dbBean) {
    	boolean ret = false;
    	if (!dbBean.getDeviceName().equals(webData.getDeviceName())) {
    		ret  = true;
		}	
    	return ret;
    }
    
    private boolean compareOnline(ListItemBean webData, ListItemBean dbBean) {
    	boolean ret = false;
    	if (!(dbBean.getIsOnline() == null && webData.getIsOnline() == null)) {
			if (dbBean.getIsOnline() != null && webData.getIsOnline() != null) {
				if (!dbBean.getIsOnline().equals(webData.getIsOnline())) {
					ret  = true;
	    		}	
			} else {
				ret  = true;
			}
		}
    	
    	if (!(dbBean.getAdminOnly() == null && webData.getAdminOnly() == null)) {
			if (dbBean.getAdminOnly() != null && webData.getAdminOnly() != null) {
				if (!dbBean.getAdminOnly().equals(webData.getAdminOnly())) {
					ret  = true;
	    		}	
			} else {
				ret  = true;
			}
		}
    	
    	return ret;
    }
    
    private boolean comparePM25Data(PM25Bean webPM25Bean, PM25Bean dbPM25Bean) {
    	boolean bl = false;
		if (!(dbPM25Bean.getDisPm25In() == null && webPM25Bean.getDisPm25In() == null)) {
			if (dbPM25Bean.getDisPm25In() != null && webPM25Bean.getDisPm25In() != null) {
				if (!dbPM25Bean.getDisPm25In().equals(webPM25Bean.getDisPm25In())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25Bean.getDisPm25Out() == null && webPM25Bean.getDisPm25Out() == null)) {
			if (dbPM25Bean.getDisPm25Out() != null && webPM25Bean.getDisPm25Out() != null) {
				if (!dbPM25Bean.getDisPm25Out().equals(webPM25Bean.getDisPm25Out())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25Bean.getDisHumi() == null && webPM25Bean.getDisHumi() == null)) {
			if (dbPM25Bean.getDisHumi() != null && webPM25Bean.getDisHumi() != null) {
				if (!dbPM25Bean.getDisHumi().equals(webPM25Bean.getDisHumi())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25Bean.getDisTemp() == null && webPM25Bean.getDisTemp() == null)) {
			if (dbPM25Bean.getDisTemp() != null && webPM25Bean.getDisTemp() != null) {
				if (!dbPM25Bean.getDisTemp().equals(webPM25Bean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean comparePM25VocData(PM25VocBean webPM25VocBean, PM25VocBean dbPM25VocBean) {
    	boolean bl = false;
		if (!(dbPM25VocBean.getDisPm25In() == null && webPM25VocBean.getDisPm25In() == null)) {
			if (dbPM25VocBean.getDisPm25In() != null && webPM25VocBean.getDisPm25In() != null) {
				if (!dbPM25VocBean.getDisPm25In().equals(webPM25VocBean.getDisPm25In())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25VocBean.getDisPm25Out() == null && webPM25VocBean.getDisPm25Out() == null)) {
			if (dbPM25VocBean.getDisPm25Out() != null && webPM25VocBean.getDisPm25Out() != null) {
				if (!dbPM25VocBean.getDisPm25Out().equals(webPM25VocBean.getDisPm25Out())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25VocBean.getDisVoc() == null && webPM25VocBean.getDisVoc() == null)) {
			if (dbPM25VocBean.getDisVoc() != null && webPM25VocBean.getDisVoc() != null) {
				if (!dbPM25VocBean.getDisVoc().equals(webPM25VocBean.getDisVoc())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25VocBean.getDisHumi() == null && webPM25VocBean.getDisHumi() == null)) {
			if (dbPM25VocBean.getDisHumi() != null && webPM25VocBean.getDisHumi() != null) {
				if (!dbPM25VocBean.getDisHumi().equals(webPM25VocBean.getDisHumi())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbPM25VocBean.getDisTemp() == null && webPM25VocBean.getDisTemp() == null)) {
			if (dbPM25VocBean.getDisTemp() != null && webPM25VocBean.getDisTemp() != null) {
				if (!dbPM25VocBean.getDisTemp().equals(webPM25VocBean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean compareCo2VocData(Co2VocBean webCo2VocBean, Co2VocBean dbCo2VocBean) {
    	boolean bl = false;
		if (!(dbCo2VocBean.getDisVoc() == null && webCo2VocBean.getDisVoc() == null)) {
			if (dbCo2VocBean.getDisVoc() != null && webCo2VocBean.getDisVoc() != null) {
				if (!dbCo2VocBean.getDisVoc().equals(webCo2VocBean.getDisVoc())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbCo2VocBean.getDisCo2() == null && webCo2VocBean.getDisCo2() == null)) {
			if (dbCo2VocBean.getDisCo2() != null && webCo2VocBean.getDisCo2() != null) {
				if (!dbCo2VocBean.getDisCo2().equals(webCo2VocBean.getDisCo2())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean compareAmerData(AmerTempBean webAmerBean, AmerTempBean dbAmerBean) {
    	boolean bl = false;
		if (!(dbAmerBean.getDeadZoneTemp() == null && webAmerBean.getDeadZoneTemp() == null)) {
			if (dbAmerBean.getDeadZoneTemp() != null && webAmerBean.getDeadZoneTemp() != null) {
				if (!dbAmerBean.getDeadZoneTemp().equals(webAmerBean.getDeadZoneTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getDisHumi() == null && webAmerBean.getDisHumi() == null)) {
			if (dbAmerBean.getDisHumi() != null && webAmerBean.getDisHumi() != null) {
				if (!dbAmerBean.getDisHumi().equals(webAmerBean.getDisHumi())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getDisTemp() == null && webAmerBean.getDisTemp() == null)) {
			if (dbAmerBean.getDisTemp() != null && webAmerBean.getDisTemp() != null) {
				if (!dbAmerBean.getDisTemp().equals(webAmerBean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getFanMod() == null && webAmerBean.getFanMod() == null)) {
			if (dbAmerBean.getFanMod() != null && webAmerBean.getFanMod() != null) {
				if (!dbAmerBean.getFanMod().equals(webAmerBean.getFanMod())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getHumi() == null && webAmerBean.getHumi() == null)) {
			if (dbAmerBean.getHumi() != null && webAmerBean.getHumi() != null) {
				if (!dbAmerBean.getHumi().equals(webAmerBean.getHumi())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getMod() == null && webAmerBean.getMod() == null)) {
			if (dbAmerBean.getMod() != null && webAmerBean.getMod() != null) {
				if (!dbAmerBean.getMod().equals(webAmerBean.getMod())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getPromable() == null && webAmerBean.getPromable() == null)) {
			if (dbAmerBean.getPromable() != null && webAmerBean.getPromable() != null) {
				if (!dbAmerBean.getPromable().equals(webAmerBean.getPromable())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getStatus() == null && webAmerBean.getStatus() == null)) {
			if (dbAmerBean.getStatus() != null && webAmerBean.getStatus() != null) {
				if (!dbAmerBean.getStatus().equals(webAmerBean.getStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getTempCool() == null && webAmerBean.getTempCool() == null)) {
			if (dbAmerBean.getTempCool() != null && webAmerBean.getTempCool() != null) {
				if (!dbAmerBean.getTempCool().equals(webAmerBean.getTempCool())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getTempHeat() == null && webAmerBean.getTempHeat() == null)) {
			if (dbAmerBean.getTempHeat() != null && webAmerBean.getTempHeat() != null) {
				if (!dbAmerBean.getTempHeat().equals(webAmerBean.getTempHeat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getColdStatus() == null && webAmerBean.getColdStatus() == null)) {
			if (dbAmerBean.getColdStatus() != null && webAmerBean.getColdStatus() != null) {
				if (!dbAmerBean.getColdStatus().equals(webAmerBean.getColdStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getHeatStatus() == null && webAmerBean.getHeatStatus() == null)) {
			if (dbAmerBean.getHeatStatus() != null && webAmerBean.getHeatStatus() != null) {
				if (!dbAmerBean.getHeatStatus().equals(webAmerBean.getHeatStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getAutoStatus() == null && webAmerBean.getAutoStatus() == null)) {
			if (dbAmerBean.getAutoStatus() != null && webAmerBean.getAutoStatus() != null) {
				if (!dbAmerBean.getAutoStatus().equals(webAmerBean.getAutoStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getEmerStatus() == null && webAmerBean.getEmerStatus() == null)) {
			if (dbAmerBean.getEmerStatus() != null && webAmerBean.getEmerStatus() != null) {
				if (!dbAmerBean.getEmerStatus().equals(webAmerBean.getEmerStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getPermStatus() == null && webAmerBean.getPermStatus() == null)) {
			if (dbAmerBean.getPermStatus() != null && webAmerBean.getPermStatus() != null) {
				if (!dbAmerBean.getPermStatus().equals(webAmerBean.getPermStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbAmerBean.getOriMod() == null && webAmerBean.getOriMod() == null)) {
			if (dbAmerBean.getOriMod() != null && webAmerBean.getOriMod() != null) {
				if (!dbAmerBean.getOriMod().equals(webAmerBean.getOriMod())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean compareGreenData(GreenTempBean webGreenBean, GreenTempBean dbGreenBean) {
    	boolean bl = false;
		if (!(dbGreenBean.getDisTemp() == null && webGreenBean.getDisTemp() == null)) {
			if (dbGreenBean.getDisTemp() != null && webGreenBean.getDisTemp() != null) {
				if (!dbGreenBean.getDisTemp().equals(webGreenBean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getFanMod() == null && webGreenBean.getFanMod() == null)) {
			if (dbGreenBean.getFanMod() != null && webGreenBean.getFanMod() != null) {
				if (!dbGreenBean.getFanMod().equals(webGreenBean.getFanMod())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getMod() == null && webGreenBean.getMod() == null)) {
			if (dbGreenBean.getMod() != null && webGreenBean.getMod() != null) {
				if (!dbGreenBean.getMod().equals(webGreenBean.getMod())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getStatus() == null && webGreenBean.getStatus() == null)) {
			if (dbGreenBean.getStatus() != null && webGreenBean.getStatus() != null) {
				if (!dbGreenBean.getStatus().equals(webGreenBean.getStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getStatusOnOff() == null && webGreenBean.getStatusOnOff() == null)) {
			if (dbGreenBean.getStatusOnOff() != null && webGreenBean.getStatusOnOff() != null) {
				if (!dbGreenBean.getStatusOnOff().equals(webGreenBean.getStatusOnOff())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempCool() == null && webGreenBean.getTempCool() == null)) {
			if (dbGreenBean.getTempCool() != null && webGreenBean.getTempCool() != null) {
				if (!dbGreenBean.getTempCool().equals(webGreenBean.getTempCool())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempHeat() == null && webGreenBean.getTempHeat() == null)) {
			if (dbGreenBean.getTempHeat() != null && webGreenBean.getTempHeat() != null) {
				if (!dbGreenBean.getTempHeat().equals(webGreenBean.getTempHeat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getSaveEnergy() == null && webGreenBean.getSaveEnergy() == null)) {
			if (dbGreenBean.getSaveEnergy() != null && webGreenBean.getSaveEnergy() != null) {
				if (!dbGreenBean.getSaveEnergy().equals(webGreenBean.getSaveEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempCoolSaveEnergy() == null && webGreenBean.getTempCoolSaveEnergy() == null)) {
			if (dbGreenBean.getTempCoolSaveEnergy() != null && webGreenBean.getTempCoolSaveEnergy() != null) {
				if (!dbGreenBean.getTempCoolSaveEnergy().equals(webGreenBean.getTempCoolSaveEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempHeatSaveEnergy() == null && webGreenBean.getTempHeatSaveEnergy() == null)) {
			if (dbGreenBean.getTempHeatSaveEnergy() != null && webGreenBean.getTempHeatSaveEnergy() != null) {
				if (!dbGreenBean.getTempHeatSaveEnergy().equals(webGreenBean.getTempHeatSaveEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
		if (!(dbGreenBean.getIntsHeatStat() == null && webGreenBean.getIntsHeatStat() == null)) {
			if (dbGreenBean.getIntsHeatStat() != null && webGreenBean.getIntsHeatStat() != null) {
				if (!dbGreenBean.getIntsHeatStat().equals(webGreenBean.getIntsHeatStat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
		if (!(dbGreenBean.getHeatOutStat() == null && webGreenBean.getHeatOutStat() == null)) {
			if (dbGreenBean.getHeatOutStat() != null && webGreenBean.getHeatOutStat() != null) {
				if (!dbGreenBean.getHeatOutStat().equals(webGreenBean.getHeatOutStat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempHeatDefaultMin() == null && webGreenBean.getTempHeatDefaultMin() == null)) {
			if (dbGreenBean.getTempHeatDefaultMin() != null && webGreenBean.getTempHeatDefaultMin() != null) {
				if (!dbGreenBean.getTempHeatDefaultMin().equals(webGreenBean.getTempHeatDefaultMin())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(dbGreenBean.getTempHeatDefaultMAx() == null && webGreenBean.getTempHeatDefaultMAx() == null)) {
			if (dbGreenBean.getTempHeatDefaultMAx() != null && webGreenBean.getTempHeatDefaultMAx() != null) {
				if (!dbGreenBean.getTempHeatDefaultMAx().equals(webGreenBean.getTempHeatDefaultMAx())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean compareHeatingData(HeatingTempBean webHeatingBean, HeatingTempBean dbHeatingBean) {
    	boolean bl = false;
		if (!(dbHeatingBean.getDisTemp() == null && webHeatingBean.getDisTemp() == null)) {
			if (dbHeatingBean.getDisTemp() != null && webHeatingBean.getDisTemp() != null) {
				if (!dbHeatingBean.getDisTemp().equals(webHeatingBean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getStatusOnOff() == null && webHeatingBean.getStatusOnOff() == null)) {
			if (dbHeatingBean.getStatusOnOff() != null && webHeatingBean.getStatusOnOff() != null) {
				if (!dbHeatingBean.getStatusOnOff().equals(webHeatingBean.getStatusOnOff())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getStatus() == null && webHeatingBean.getStatus() == null)) {
			if (dbHeatingBean.getStatus() != null && webHeatingBean.getStatus() != null) {
				if (!dbHeatingBean.getStatus().equals(webHeatingBean.getStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getTempHeat() == null && webHeatingBean.getTempHeat() == null)) {
			if (dbHeatingBean.getTempHeat() != null && webHeatingBean.getTempHeat() != null) {
				if (!dbHeatingBean.getTempHeat().equals(webHeatingBean.getTempHeat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getSaveEnergy() == null && webHeatingBean.getSaveEnergy() == null)) {
			if (dbHeatingBean.getSaveEnergy() != null && webHeatingBean.getSaveEnergy() != null) {
				if (!dbHeatingBean.getSaveEnergy().equals(webHeatingBean.getSaveEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getTempHeatSaveEnergy() == null && webHeatingBean.getTempHeatSaveEnergy() == null)) {
			if (dbHeatingBean.getTempHeatSaveEnergy() != null && webHeatingBean.getTempHeatSaveEnergy() != null) {
				if (!dbHeatingBean.getTempHeatSaveEnergy().equals(webHeatingBean.getTempHeatSaveEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getTempHeatDefaultMax() == null && webHeatingBean.getTempHeatDefaultMax() == null)) {
			if (dbHeatingBean.getTempHeatDefaultMax() != null && webHeatingBean.getTempHeatDefaultMax() != null) {
				if (!dbHeatingBean.getTempHeatDefaultMax().equals(webHeatingBean.getTempHeatDefaultMax())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		if (!(dbHeatingBean.getTempHeatDefaultMin() == null && webHeatingBean.getTempHeatDefaultMin() == null)) {
			if (dbHeatingBean.getTempHeatDefaultMin() != null && webHeatingBean.getTempHeatDefaultMin() != null) {
				if (!dbHeatingBean.getTempHeatDefaultMin().equals(webHeatingBean.getTempHeatDefaultMin())) {
					bl = true;
				}
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }
    
    private boolean compareLingDongData(LingDongTempBean webLingDongBean, LingDongTempBean dbLingDongBean) {
    	boolean bl = false;
		if (!(dbLingDongBean.getDisTemp() == null && webLingDongBean.getDisTemp() == null)) {
			if (dbLingDongBean.getDisTemp() != null && webLingDongBean.getDisTemp() != null) {
				if (!dbLingDongBean.getDisTemp().equals(webLingDongBean.getDisTemp())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getStatusOnOff() == null && webLingDongBean.getStatusOnOff() == null)) {
			if (dbLingDongBean.getStatusOnOff() != null && webLingDongBean.getStatusOnOff() != null) {
				if (!dbLingDongBean.getStatusOnOff().equals(webLingDongBean.getStatusOnOff())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getStatus() == null && webLingDongBean.getStatus() == null)) {
			if (dbLingDongBean.getStatus() != null && webLingDongBean.getStatus() != null) {
				if (!dbLingDongBean.getStatus().equals(webLingDongBean.getStatus())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempHeat() == null && webLingDongBean.getTempHeat() == null)) {
			if (dbLingDongBean.getTempHeat() != null && webLingDongBean.getTempHeat() != null) {
				if (!dbLingDongBean.getTempHeat().equals(webLingDongBean.getTempHeat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getHeatMode() == null && webLingDongBean.getHeatMode() == null)) {
			if (dbLingDongBean.getHeatMode() != null && webLingDongBean.getHeatMode() != null) {
				if (!dbLingDongBean.getHeatMode().equals(webLingDongBean.getHeatMode())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempOut() == null && webLingDongBean.getTempOut() == null)) {
			if (dbLingDongBean.getTempOut() != null && webLingDongBean.getTempOut() != null) {
				if (!dbLingDongBean.getTempOut().equals(webLingDongBean.getTempOut())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempEnergy() == null && webLingDongBean.getTempEnergy() == null)) {
			if (dbLingDongBean.getTempEnergy() != null && webLingDongBean.getTempEnergy() != null) {
				if (!dbLingDongBean.getTempEnergy().equals(webLingDongBean.getTempEnergy())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempComfort() == null && webLingDongBean.getTempComfort() == null)) {
			if (dbLingDongBean.getTempComfort() != null && webLingDongBean.getTempComfort() != null) {
				if (!dbLingDongBean.getTempComfort().equals(webLingDongBean.getTempComfort())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getLinkageInput() == null && webLingDongBean.getLinkageInput() == null)) {
			if (dbLingDongBean.getLinkageInput() != null && webLingDongBean.getLinkageInput() != null) {
				if (!dbLingDongBean.getLinkageInput().equals(webLingDongBean.getLinkageInput())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getLinkageOutput() == null && webLingDongBean.getLinkageOutput() == null)) {
			if (dbLingDongBean.getLinkageOutput() != null && webLingDongBean.getLinkageOutput() != null) {
				if (!dbLingDongBean.getLinkageOutput().equals(webLingDongBean.getLinkageOutput())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getCloseBoiler() == null && webLingDongBean.getCloseBoiler() == null)) {
			if (dbLingDongBean.getCloseBoiler() != null && webLingDongBean.getCloseBoiler() != null) {
				if (!dbLingDongBean.getCloseBoiler().equals(webLingDongBean.getCloseBoiler())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempHeatDefaultMax() == null && webLingDongBean.getTempHeatDefaultMax() == null)) {
			if (dbLingDongBean.getTempHeatDefaultMax() != null && webLingDongBean.getTempHeatDefaultMax() != null) {
				if (!dbLingDongBean.getTempHeatDefaultMax().equals(webLingDongBean.getTempHeatDefaultMax())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbLingDongBean.getTempHeatDefaultMin() == null && webLingDongBean.getTempHeatDefaultMin() == null)) {
			if (dbLingDongBean.getTempHeatDefaultMin() != null && webLingDongBean.getTempHeatDefaultMin() != null) {
				if (!dbLingDongBean.getTempHeatDefaultMin().equals(webLingDongBean.getTempHeatDefaultMin())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}

//		if (!(dbLingDongBean.getSchedule() == null && webLingDongBean.getSchedule() == null)) {
//			if (dbLingDongBean.getSchedule() != null && webLingDongBean.getSchedule() != null) {
//				if (!dbLingDongBean.getSchedule().equals(webLingDongBean.getSchedule())) {
//					bl = true;
//	    		}	
//			} else {
//				bl = true;
//			}
//		}
		
    	return bl;
    }
    
    private boolean compareFtkzyData(FtkzyBean webFtkzyBean, FtkzyBean dbFtkzyBean) {
    	boolean bl = false;
		if (!(dbFtkzyBean.getCollTempOne() == null && webFtkzyBean.getCollTempOne() == null)) {
			if (dbFtkzyBean.getCollTempOne() != null && webFtkzyBean.getCollTempOne() != null) {
				if (!dbFtkzyBean.getCollTempOne().equals(webFtkzyBean.getCollTempOne())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getTankTempTwo() == null && webFtkzyBean.getTankTempTwo() == null)) {
			if (dbFtkzyBean.getTankTempTwo() != null && webFtkzyBean.getTankTempTwo() != null) {
				if (!dbFtkzyBean.getTankTempTwo().equals(webFtkzyBean.getTankTempTwo())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getTankTempThree() == null && webFtkzyBean.getTankTempThree() == null)) {
			if (dbFtkzyBean.getTankTempThree() != null && webFtkzyBean.getTankTempThree() != null) {
				if (!dbFtkzyBean.getTankTempThree().equals(webFtkzyBean.getTankTempThree())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getBackTempFour() == null && webFtkzyBean.getBackTempFour() == null)) {
			if (dbFtkzyBean.getBackTempFour() != null && webFtkzyBean.getBackTempFour() != null) {
				if (!dbFtkzyBean.getBackTempFour().equals(webFtkzyBean.getBackTempFour())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getTankTempFlg() == null && webFtkzyBean.getTankTempFlg() == null)) {
			if (dbFtkzyBean.getTankTempFlg() != null && webFtkzyBean.getTankTempFlg() != null) {
				if (!dbFtkzyBean.getTankTempFlg().equals(webFtkzyBean.getTankTempFlg())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getCollPump() == null && webFtkzyBean.getCollPump() == null)) {
			if (dbFtkzyBean.getCollPump() != null && webFtkzyBean.getCollPump() != null) {
				if (!dbFtkzyBean.getCollPump().equals(webFtkzyBean.getCollPump())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getPipePump() == null && webFtkzyBean.getPipePump() == null)) {
			if (dbFtkzyBean.getPipePump() != null && webFtkzyBean.getPipePump() != null) {
				if (!dbFtkzyBean.getPipePump().equals(webFtkzyBean.getPipePump())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getAuxiHeat() == null && webFtkzyBean.getAuxiHeat() == null)) {
			if (dbFtkzyBean.getAuxiHeat() != null && webFtkzyBean.getAuxiHeat() != null) {
				if (!dbFtkzyBean.getAuxiHeat().equals(webFtkzyBean.getAuxiHeat())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
		if (!(dbFtkzyBean.getCollPumpOnDev() == null && webFtkzyBean.getCollPumpOnDev() == null)) {
			if (dbFtkzyBean.getCollPumpOnDev() != null && webFtkzyBean.getCollPumpOnDev() != null) {
				if (!dbFtkzyBean.getCollPumpOnDev().equals(webFtkzyBean.getCollPumpOnDev())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getPipePumpOnDev() == null && webFtkzyBean.getPipePumpOnDev() == null)) {
			if (dbFtkzyBean.getPipePumpOnDev() != null && webFtkzyBean.getPipePumpOnDev() != null) {
				if (!dbFtkzyBean.getPipePumpOnDev().equals(webFtkzyBean.getPipePumpOnDev())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		if (!(dbFtkzyBean.getAuxiHeatOnDev() == null && webFtkzyBean.getAuxiHeatOnDev() == null)) {
			if (dbFtkzyBean.getAuxiHeatOnDev() != null && webFtkzyBean.getAuxiHeatOnDev() != null) {
				if (!dbFtkzyBean.getAuxiHeatOnDev().equals(webFtkzyBean.getAuxiHeatOnDev())) {
					bl = true;
	    		}	
			} else {
				bl = true;
			}
		}
		
    	return bl;
    }

	private List<ListItemBean> changeToListBean(JSONArray json) throws JSONException {
		List<ListItemBean> listItem = new ArrayList<ListItemBean>();
		if (json != null) {
			for (int i = 0; i < json.length(); i++) {
				JSONObject jb = json.getJSONObject(i);
				String deviceType = jb.getString(
						context.getResources().getString(R.string.param_device_type));
				if (context.getResources().getString(R.string.value_deviceid_pm25).equals(deviceType)) {
					listItem.add(setPm25Data(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals(deviceType)) {
					listItem.add(setPm25VocData(jb));
//        		} else if (context.getResources().getString(R.string.value_deviceid_a8100).equals(deviceType) 
//        				|| context.getResources().getString(R.string.value_deviceid_atemp).equals(deviceType)
//        				|| context.getResources().getString(R.string.value_deviceid_new_atemp).equals(deviceType)) {
//        			listItem.add(setAmerTempData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_green).equals(deviceType)
						|| context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals(deviceType)
						|| context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals(deviceType)) {
					listItem.add(setGreenTempData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals(deviceType)) {
					listItem.add(setHeatingTempData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals(deviceType)) {
					listItem.add(setCo2VocData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals(deviceType)) {
					listItem.add(setLingDongTempData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(deviceType)) {
					listItem.add(setFtkzyData(jb));
				} else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals(deviceType)) {
					listItem.add(setYiPuWData(jb));
				}
			}
		}

		return listItem;
	}
    
    private ListItemBean setPm25Data(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
		PM25Bean pm25Bean = new PM25Bean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_humi).equals(key)) {
				pm25Bean.setDisHumi(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				pm25Bean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				pm25Bean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_dis_pm25_in).equals(key)) {
				pm25Bean.setDisPm25In(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_dis_pm25_out).equals(key)) {
				pm25Bean.setDisPm25Out(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		pm25Bean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setPm25bean(pm25Bean);
		
		return listItemBean;
    }
    
    private ListItemBean setPm25VocData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
		PM25VocBean pm25VocBean = new PM25VocBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_humi).equals(key)) {
				pm25VocBean.setDisHumi(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				pm25VocBean.setDisTemp(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_voc).equals(key)) {
				pm25VocBean.setDisVoc(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				pm25VocBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_dis_pm25_in).equals(key)) {
				pm25VocBean.setDisPm25In(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_dis_pm25_out).equals(key)) {
				pm25VocBean.setDisPm25Out(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		pm25VocBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setPm25VocBean(pm25VocBean);
		
		return listItemBean;
    }
    
    private ListItemBean setCo2VocData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
		Co2VocBean co2VocBean = new Co2VocBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_voc).equals(key)) {
				co2VocBean.setDisVoc(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_co2).equals(key)) {
				co2VocBean.setDisCo2(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				co2VocBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		co2VocBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setCo2VocBean(co2VocBean);
		
		return listItemBean;
    }
    
    private ListItemBean setAmerTempData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
    	AmerTempBean aTempBean = new AmerTempBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_humi).equals(key)) {
				aTempBean.setDisHumi(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				aTempBean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				aTempBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat).equals(key)) {
				aTempBean.setTempHeat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_status).equals(key)) {
				aTempBean.setStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_cool).equals(key)) {
				aTempBean.setTempCool(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mod).equals(key)) {
				aTempBean.setMod(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_fan_mod).equals(key)) {
				aTempBean.setFanMod(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_promable).equals(key)) {
				aTempBean.setPromable(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_humi).equals(key)) {
				aTempBean.setHumi(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_dead_zone_temp).equals(key)) {
				aTempBean.setDeadZoneTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_cold_status).equals(key)) {
				aTempBean.setColdStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_heat_status).equals(key)) {
				aTempBean.setHeatStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_auto_status).equals(key)) {
				aTempBean.setAutoStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_emer_status).equals(key)) {
				aTempBean.setEmerStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_perm_status).equals(key)) {
				aTempBean.setPermStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_ori_mod).equals(key)) {
				aTempBean.setOriMod(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		aTempBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setAmerTempBean(aTempBean);
		
		return listItemBean;
    }
    
    private ListItemBean setGreenTempData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
    	GreenTempBean greenBean = new GreenTempBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				greenBean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				greenBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat).equals(key)) {
				greenBean.setTempHeat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_status).equals(key)) {
				greenBean.setStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_cool).equals(key)) {
				greenBean.setTempCool(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_cool_save_energy).equals(key)) {
				greenBean.setTempCoolSaveEnergy(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_save_energy).equals(key)) {
				greenBean.setTempHeatSaveEnergy(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mod).equals(key)) {
				greenBean.setMod(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_fan_mod).equals(key)) {
				greenBean.setFanMod(jb.getString(key));
			}else if  (context.getResources().getString(R.string.param_temp_heat_default_min).equals(key)) {
				greenBean.setTempHeatDefaultMin(jb.getString(key));
			}else if  (context.getResources().getString(R.string.param_temp_heat_default_max).equals(key)) {
				greenBean.setTempHeatDefaultMAx(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_save_energy).equals(key)) {
				if (jb.getBoolean(key)) {
					greenBean.setSaveEnergy(Constants.VALUE_ENERGY_ONE);
				} else {
					greenBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_status_onoff).equals(key)) {
				greenBean.setStatusOnOff(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_ints_heat_stat).equals(key)) {
				greenBean.setIntsHeatStat(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_heat_out_stat).equals(key)) {
				greenBean.setHeatOutStat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		greenBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setGreenTempBean(greenBean);
		
		return listItemBean;
    }
    
    private ListItemBean setHeatingTempData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
    	HeatingTempBean heatingBean = new HeatingTempBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				heatingBean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				heatingBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat).equals(key)) {
				heatingBean.setTempHeat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_save_energy).equals(key)) {
				heatingBean.setTempHeatSaveEnergy(jb.getString(key));
			}else if  (context.getResources().getString(R.string.param_temp_heat_default_max).equals(key)) {
				heatingBean.setTempHeatDefaultMax(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_default_min).equals(key)) {
				heatingBean.setTempHeatDefaultMin(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_save_energy).equals(key)) {
				if (jb.getBoolean(key)) {
					heatingBean.setSaveEnergy(Constants.VALUE_ENERGY_ONE);
				} else {
					heatingBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_status_onoff).equals(key)) {
				heatingBean.setStatusOnOff(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_status).equals(key)) {
				heatingBean.setStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		heatingBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setHeatingTempBean(heatingBean);
		
		return listItemBean;
    }
    
    private ListItemBean setLingDongTempData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
    	LingDongTempBean lingDongBean = new LingDongTempBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				lingDongBean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				lingDongBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat).equals(key)) {
				lingDongBean.setTempHeat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_status_onoff).equals(key)) {
				lingDongBean.setStatusOnOff(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_status).equals(key)) {
				lingDongBean.setStatus(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_out).equals(key)) {
				lingDongBean.setTempOut(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_energy).equals(key)) {
				lingDongBean.setTempEnergy(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_comfort).equals(key)) {
				lingDongBean.setTempComfort(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_heat_mode).equals(key)) {
				lingDongBean.setHeatMode(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_schedule).equals(key)) {
				lingDongBean.setSchedule(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_linkage_input).equals(key)) {
				lingDongBean.setLinkageInput(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_linkage_output).equals(key)) {
				lingDongBean.setLinkageOutput(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_close_boiler).equals(key)) {
				lingDongBean.setCloseBoiler(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_default_max).equals(key)) {
				lingDongBean.setTempHeatDefaultMax(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_default_min).equals(key)) {
				lingDongBean.setTempHeatDefaultMin(jb.getString(key));
			}
			else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		lingDongBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setLingDongTempBean(lingDongBean);
		
		return listItemBean;
    }
    
    private ListItemBean setFtkzyData(JSONObject jb) throws JSONException {
    	ListItemBean listItemBean = new ListItemBean();
    	FtkzyBean ftkzyBean = new FtkzyBean();
    	Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {             		
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				ftkzyBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_coll_temp_one).equals(key)) {
				ftkzyBean.setCollTempOne(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_tank_temp_two).equals(key)) {
				ftkzyBean.setTankTempTwo(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_tank_temp_three).equals(key)) {
				ftkzyBean.setTankTempThree(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_back_temp_four).equals(key)) {
				ftkzyBean.setBackTempFour(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_tank_temp_flg).equals(key)) {
				ftkzyBean.setTankTempFlg(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_coll_pump).equals(key)) {
				if (jb.getBoolean(key)) {
					ftkzyBean.setCollPump(Constants.VALUE_ONLINE_ONE);
				} else {
					ftkzyBean.setCollPump(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_pipe_pump).equals(key)) {
				if (jb.getBoolean(key)) {
					ftkzyBean.setPipePump(Constants.VALUE_ONLINE_ONE);
				} else {
					ftkzyBean.setPipePump(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_auxi_heat).equals(key)) {
				if (jb.getBoolean(key)) {
					ftkzyBean.setAuxiHeat(Constants.VALUE_ONLINE_ONE);
				} else {
					ftkzyBean.setAuxiHeat(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_coll_pump_on_dev).equals(key)) {
				ftkzyBean.setCollPumpOnDev(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_pipe_pump_on_dev).equals(key)) {
				ftkzyBean.setPipePumpOnDev(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_auxi_heat_on_dev).equals(key)) {
				ftkzyBean.setAuxiHeatOnDev(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			}
		}
		
		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		ftkzyBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setFtkzyBean(ftkzyBean);
		
		return listItemBean;
    }
    
    private void doException() {
    	if (errorCount++ > 2) {
			List<ListItemBean> dbData = db.getTDevice().findAll();
			if (checkOnline(dbData)) {
				if (!CheckUtil.requireCheck(mac)) {
					setTaskContinue(true);
				}	
			}
    	}
		exeTask();
    }
    
    private boolean checkOnline(List<ListItemBean> listItem) {
    	Map<String, String> param = new HashMap<String, String>();
    	if (listItem != null && listItem.size() > 0) {
    		for (int i = 0; i < listItem.size(); i++) {
    			ListItemBean listItemBean = listItem.get(i);
    			if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.getIsOnline())) {
    				listItem.get(i).setIsOnline(Constants.VALUE_ONLINE_ZERO);
    				param.put(Constants.IS_ONLINE, Constants.VALUE_ONLINE_ZERO);
    				param.put(Constants.MAC, listItem.get(i).getMac());
    				param.put(Constants.USER_NAME, 
    						UserInfo.UserInfo.getUserName());
    				db.getTDevice().update(param);
    				if (mac.equals(listItem.get(i).getMac())) {
    					setTaskContinue(true);
    				}
    			}
    		}
    	}
    	if (param.size() > 0) {
    		return true;
    	}
    	return false;
    }

	/**
	 * @return the isTaskContinue
	 */
	public boolean isTaskContinue() {
		return isTaskContinue;
	}

	/**
	 * @param isTaskContinue the isTaskContinue to set
	 */
	public void setTaskContinue(boolean isTaskContinue) {
		this.isTaskContinue = isTaskContinue;
	}
	
//	/**
//	 * @return the isHistory
//	 */
//	public boolean isHistory() {
//		return isHistory;
//	}
	
	public void exeTask() {
		
	};
	
//	private void exeException(Throwable e) {
//		HttpResponseException he = (HttpResponseException) e;
//		if (context.getResources().getString(R.string.value_code_fourZeroOne).equals(String.valueOf(he.getStatusCode()))
//				&& Constants.MSG_UNAUTHORIZED.equals(he.getMessage())) {
//			getAuthorizatin();
//		}
//
//	}
	
	private void getAuthorizatin() {
        RequestParams paramMap = new RequestParams();
//        String url = context.getResources().getText(R.string.url_base).toString() 
//  			+ context.getResources().getText(R.string.url_loginandroid).toString();
        String url = context.getResources().getText(R.string.url_loginandroid).toString();
        paramMap.put(context.getResources().getText(R.string.param_client_id).toString(), 
        		context.getResources().getText(R.string.value_client_id).toString());
        paramMap.put(context.getResources().getText(R.string.param_client_secret).toString(), 
        		context.getResources().getText(R.string.value_client_secret).toString());
        paramMap.put(context.getResources().getText(R.string.param_grant_type).toString(), 
        		context.getResources().getText(R.string.value_grant_type).toString());
        paramMap.put(context.getResources().getText(R.string.param_user_nm).toString(), 
        		UserInfo.UserInfo.getUserName());
        paramMap.put(context.getResources().getText(R.string.param_password).toString(), 
        		UserInfo.UserInfo.getPassword());
        
        HttpClientUtil.post(url, paramMap, new JsonHttpResponseHandler() {
        	@Override
        	public void onSuccess(int statusCode, Header[] headers, JSONObject jsonObject) {
        		if (jsonObject != null) {
        			try {
    					String tokenType = jsonObject.getString(
    							context.getResources().getText(R.string.param_token_type).toString());
    					String acessToken = jsonObject.getString(
    							context.getResources().getText(R.string.param_access_token).toString());
    					UserInfo.UserInfo.setTokenType(tokenType);
    					UserInfo.UserInfo.setAcessToken(acessToken);
    					UserInfo.UserInfo.setAuthority(tokenType + " " + acessToken);

    					Map<String, String> param = new HashMap<String, String>();
     	 				param.put(Constants.USER_NAME, UserInfo.UserInfo.getUserName());
     	 				param.put(Constants.TOKEN_TYPE, tokenType);
     	 				param.put(Constants.ACESS_TOKEN, acessToken);
     			        db.getTUser().update(param);
					} catch (JSONException e) {

					}
        		}
            }
        });
	}

	public ListItemBean setYiPuWData(JSONObject jb) throws JSONException {
		ListItemBean listItemBean = new ListItemBean();
		YiPuWTempBean yiPuWTempBean = new YiPuWTempBean();
		Iterator<?> keys = jb.keys();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			if (context.getResources().getString(R.string.param_device_type).equals(key)) {
				listItemBean.setDeviceType(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_device_nm).equals(key)) {
				listItemBean.setDeviceName(jb.getString(key));
			} else if (context.getResources().getString(R.string.param_dis_temp).equals(key)) {
				yiPuWTempBean.setDisTemp(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_mac).equals(key)) {
				yiPuWTempBean.setMac(jb.getString(key));
				listItemBean.setMac(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat).equals(key)) {
				yiPuWTempBean.setTempHeat(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_save_energy).equals(key)) {
				yiPuWTempBean.setTempHeatSaveEnergy(jb.getString(key));
			}else if  (context.getResources().getString(R.string.param_temp_heat_default_max).equals(key)) {
				yiPuWTempBean.setTempHeatDefaultMax(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_temp_heat_default_min).equals(key)) {
				yiPuWTempBean.setTempHeatDefaultMin(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_save_energy).equals(key)) {
				if (jb.getBoolean(key)) {
					yiPuWTempBean.setSaveEnergy(Constants.VALUE_ENERGY_ONE);
				} else {
					yiPuWTempBean.setSaveEnergy(Constants.VALUE_ENERGY_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_online).equals(key)) {
				if (jb.getBoolean(key)) {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ONE);
				} else {
					listItemBean.setIsOnline(Constants.VALUE_ONLINE_ZERO);
				}
			} else if  (context.getResources().getString(R.string.param_status_onoff).equals(key)) {
				yiPuWTempBean.setStatusOnOff(jb.getString(key));
			} else if  (context.getResources().getString(R.string.param_status).equals(key)) {
				yiPuWTempBean.setStatus(jb.getString(key));
			}else if (context.getResources().getString(R.string.param_heat_mode).equals(key)){
				yiPuWTempBean.setHeatMode(jb.getString(key));
			}
			else if (context.getResources().getString(R.string.param_forbid_mode).equals(key)){
				yiPuWTempBean.setForbidMode(jb.getString(key));
			}

			else if  (context.getResources().getString(R.string.param_admin_only).equals(key)) {
				listItemBean.setAdminOnly(jb.getString(key));
			}
		}

		listItemBean.setUserName(UserInfo.UserInfo.getUserName());
		yiPuWTempBean.setUserName(UserInfo.UserInfo.getUserName());
		listItemBean.setYiPuWTempBean(yiPuWTempBean);

		return listItemBean;
	}
}