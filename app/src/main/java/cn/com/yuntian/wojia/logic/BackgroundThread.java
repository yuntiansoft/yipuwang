package cn.com.yuntian.wojia.logic;


import android.os.Handler;
import android.os.HandlerThread;
import android.os.Process;

/**
 * Created by yuntian on 2017/11/15.
 */

public class BackgroundThread extends HandlerThread {
    private static final String THREAD_NAME = "BackgroundThread";
    private static BackgroundThread backgroundThread = null;
    private static Handler mHandler;
    private BackgroundThread() {
        super(THREAD_NAME, Process.THREAD_PRIORITY_BACKGROUND);
        start();
        if (mHandler == null){
            mHandler = new Handler(getLooper());
        }
    }

    public static synchronized BackgroundThread getInstance(){
        if (backgroundThread == null||!backgroundThread.isAlive()){
            backgroundThread = new BackgroundThread();
        }

        return backgroundThread;
    }

    public void post(Runnable runnable){
        mHandler.post(runnable);
    }

    public void postDelayed(Runnable runnable,long delay){
        mHandler.postDelayed(runnable,delay);
    }

    public void removeCallbacks(Runnable runnable){
        mHandler.removeCallbacks(runnable);
    }
}
