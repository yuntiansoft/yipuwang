package cn.com.yuntian.wojia.logic;

/**
 * Co2VocBean
 * @author chenwh
 *
 */
public class Co2VocBean implements Cloneable {
	
	private String mac = null;
	
	private String userName = null;
	
	private String disVoc = null; 
	
	private String disCo2 = null;
	
	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the disVoc
	 */
	public String getDisVoc() {
		return disVoc;
	}

	/**
	 * @param disVoc the disVoc to set
	 */
	public void setDisVoc(String disVoc) {
		this.disVoc = disVoc;
	}

	/**
	 * @return the disCo2
	 */
	public String getDisCo2() {
		return disCo2;
	}

	/**
	 * @param disCo2 the disCo2 to set
	 */
	public void setDisCo2(String disCo2) {
		this.disCo2 = disCo2;
	}
	
	public Object clone(){
		Co2VocBean o = null;
        try{
            o = (Co2VocBean) super.clone();
        } catch (CloneNotSupportedException e) {
           
        }
        return o;
    }

}
