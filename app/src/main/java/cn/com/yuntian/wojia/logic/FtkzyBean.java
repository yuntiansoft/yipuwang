package cn.com.yuntian.wojia.logic;

/**
 * LingDongTempBean
 * @author chenwh
 *
 */
public class FtkzyBean implements Cloneable {
	
	private String mac = null;
	
	private String userName = null;
	
	private String collTempOne = null;
	
	private String tankTempTwo = null;  
	
	private String tankTempThree = null;
	
	private String backTempFour = null;
	
	private String tankTempFlg = null;
	
	private String collPump = null;
	
	private String pipePump = null;
	
	private String auxiHeat = null;
	
	private String collPumpOnDev = null;
	
	private String pipePumpOnDev = null;
	
	private String auxiHeatOnDev = null;
	
	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the collTempOne
	 */
	public String getCollTempOne() {
		return collTempOne;
	}

	/**
	 * @param collTempOne the collTempOne to set
	 */
	public void setCollTempOne(String collTempOne) {
		this.collTempOne = collTempOne;
	}

	/**
	 * @return the tankTempTwo
	 */
	public String getTankTempTwo() {
		return tankTempTwo;
	}

	/**
	 * @param tankTempTwo the tankTempTwo to set
	 */
	public void setTankTempTwo(String tankTempTwo) {
		this.tankTempTwo = tankTempTwo;
	}

	/**
	 * @return the tankTempThree
	 */
	public String getTankTempThree() {
		return tankTempThree;
	}

	/**
	 * @param tankTempThree the tankTempThree to set
	 */
	public void setTankTempThree(String tankTempThree) {
		this.tankTempThree = tankTempThree;
	}

	/**
	 * @return the backTempFour
	 */
	public String getBackTempFour() {
		return backTempFour;
	}

	/**
	 * @param backTempFour the backTempFour to set
	 */
	public void setBackTempFour(String backTempFour) {
		this.backTempFour = backTempFour;
	}

	/**
	 * @return the tankTempFlg
	 */
	public String getTankTempFlg() {
		return tankTempFlg;
	}

	/**
	 * @param tankTempFlg the tankTempFlg to set
	 */
	public void setTankTempFlg(String tankTempFlg) {
		this.tankTempFlg = tankTempFlg;
	}

	/**
	 * @return the collPump
	 */
	public String getCollPump() {
		return collPump;
	}

	/**
	 * @param collPump the collPump to set
	 */
	public void setCollPump(String collPump) {
		this.collPump = collPump;
	}

	/**
	 * @return the pipePump
	 */
	public String getPipePump() {
		return pipePump;
	}

	/**
	 * @param pipePump the pipePump to set
	 */
	public void setPipePump(String pipePump) {
		this.pipePump = pipePump;
	}

	/**
	 * @return the auxiHeat
	 */
	public String getAuxiHeat() {
		return auxiHeat;
	}

	/**
	 * @param auxiHeat the auxiHeat to set
	 */
	public void setAuxiHeat(String auxiHeat) {
		this.auxiHeat = auxiHeat;
	}

	/**
	 * @return the collPumpOnDev
	 */
	public String getCollPumpOnDev() {
		return collPumpOnDev;
	}

	/**
	 * @param collPumpOnDev the collPumpOnDev to set
	 */
	public void setCollPumpOnDev(String collPumpOnDev) {
		this.collPumpOnDev = collPumpOnDev;
	}

	/**
	 * @return the pipePumpOnDev
	 */
	public String getPipePumpOnDev() {
		return pipePumpOnDev;
	}

	/**
	 * @param pipePumpOnDev the pipePumpOnDev to set
	 */
	public void setPipePumpOnDev(String pipePumpOnDev) {
		this.pipePumpOnDev = pipePumpOnDev;
	}

	/**
	 * @return the auxiHeatOnDev
	 */
	public String getAuxiHeatOnDev() {
		return auxiHeatOnDev;
	}

	/**
	 * @param auxiHeatOnDev the auxiHeatOnDev to set
	 */
	public void setAuxiHeatOnDev(String auxiHeatOnDev) {
		this.auxiHeatOnDev = auxiHeatOnDev;
	}

	public Object clone(){
		FtkzyBean o = null;
        try{
            o = (FtkzyBean) super.clone();
        } catch (CloneNotSupportedException e) {
           
        }
        return o;
    }
}
