package cn.com.yuntian.wojia.logic;

import java.io.Serializable;

/**
 * AmerTempBean
 * @author chenwh
 *
 */
public class GreenTempBean implements Cloneable {
	
	private String mac = null;
	
	private String tempHeat = null;
	
	private String tempCool = null;
	
	private String mod = null;
	
	private String fanMod = null;
	
	private String status = null;
	
	private String disTemp = null;  
	
	private String userName = null;
	
	private String saveEnergy = null;
	
	private String statusOnOff = null;
	
	private String tempCoolSaveEnergy = null;
	
	private String tempHeatSaveEnergy = null;
	
	private String intsHeatStat = null;
	
	private String heatOutStat = null;

	private String tempHeatDefaultMin = null;

	private String tempHeatDefaultMAx = null;
	
	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the tempHeat
	 */
	public String getTempHeat() {
		return tempHeat;
	}

	/**
	 * @param tempHeat the tempHeat to set
	 */
	public void setTempHeat(String tempHeat) {
		this.tempHeat = tempHeat;
	}

	/**
	 * @return the tempCool
	 */
	public String getTempCool() {
		return tempCool;
	}

	/**
	 * @param tempCool the tempCool to set
	 */
	public void setTempCool(String tempCool) {
		this.tempCool = tempCool;
	}

	/**
	 * @return the mod
	 */
	public String getMod() {
		return mod;
	}

	/**
	 * @param mod the mod to set
	 */
	public void setMod(String mod) {
		this.mod = mod;
	}

	/**
	 * @return the fanMod
	 */
	public String getFanMod() {
		return fanMod;
	}

	/**
	 * @param fanMod the fanMod to set
	 */
	public void setFanMod(String fanMod) {
		this.fanMod = fanMod;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the disTemp
	 */
	public String getDisTemp() {
		return disTemp;
	}

	/**
	 * @param disTemp the disTemp to set
	 */
	public void setDisTemp(String disTemp) {
		this.disTemp = disTemp;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the saveEnergy
	 */
	public String getSaveEnergy() {
		return saveEnergy;
	}

	/**
	 * @param saveEnergy the saveEnergy to set
	 */
	public void setSaveEnergy(String saveEnergy) {
		this.saveEnergy = saveEnergy;
	} 
	
	/**
	 * @return the statusOnOff
	 */
	public String getStatusOnOff() {
		return statusOnOff;
	}

	/**
	 * @param statusOnOff the statusOnOff to set
	 */
	public void setStatusOnOff(String statusOnOff) {
		this.statusOnOff = statusOnOff;
	}

	/**
	 * @return the tempCoolSaveEnergy
	 */
	public String getTempCoolSaveEnergy() {
		return tempCoolSaveEnergy;
	}

	/**
	 * @param tempCoolSaveEnergy the tempCoolSaveEnergy to set
	 */
	public void setTempCoolSaveEnergy(String tempCoolSaveEnergy) {
		this.tempCoolSaveEnergy = tempCoolSaveEnergy;
	}

	/**
	 * @return the tempHeatSaveEnergy
	 */
	public String getTempHeatSaveEnergy() {
		return tempHeatSaveEnergy;
	}

	/**
	 * @param tempHeatSaveEnergy the tempHeatSaveEnergy to set
	 */
	public void setTempHeatSaveEnergy(String tempHeatSaveEnergy) {
		this.tempHeatSaveEnergy = tempHeatSaveEnergy;
	}
	
	/**
	 * @return the intsHeatStat
	 */
	public String getIntsHeatStat() {
		return intsHeatStat;
	}

	/**
	 * @param intsHeatStat the intsHeatStat to set
	 */
	public void setIntsHeatStat(String intsHeatStat) {
		this.intsHeatStat = intsHeatStat;
	}

	/**
	 * @return the heatOutStat
	 */
	public String getHeatOutStat() {
		return heatOutStat;
	}

	/**
	 * @param heatOutStat the heatOutStat to set
	 */
	public void setHeatOutStat(String heatOutStat) {
		this.heatOutStat = heatOutStat;
	}

	public Object clone(){
		GreenTempBean o = null;
        try{
            o = (GreenTempBean) super.clone();
        } catch (CloneNotSupportedException e) {
           
        }
        return o;
    }
	public void setTempHeatDefaultMin(String tempHeatDefaultMin){
		this.tempHeatDefaultMin = tempHeatDefaultMin;
	}

	public String getTempHeatDefaultMin() {
		return tempHeatDefaultMin;
	}

	public String getTempHeatDefaultMAx() {
		return tempHeatDefaultMAx;
	}

	public void setTempHeatDefaultMAx(String tempHeatDefaultMAx) {
		this.tempHeatDefaultMAx = tempHeatDefaultMAx;
	}
}
