package cn.com.yuntian.wojia.logic;

/**
 * LingDongTempBean
 * @author chenwh
 *
 */
public class LingDongTempBean implements Cloneable {
	
	private String mac = null;
	
	private String tempHeat = null;
	
	private String disTemp = null;  
	
	private String userName = null;
	
	private String saveEnergy = null;
	
	private String statusOnOff = null;
	
	private String tempHeatSaveEnergy = null;
	
	private String status = null;
	
	private String tempOut = null;
	
	private String tempComfort = null;
	
	private String tempEnergy = null;
	
	private String heatMode = null;
	
	private String schedule = null;
	
	private String linkageInput = null;
	
	private String linkageOutput = null;
	
	private String closeBoiler = null;
	
	private String tempHeatDefaultMin = null;
	
	private String tempHeatDefaultMax = null;


	
	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the tempHeat
	 */
	public String getTempHeat() {
		return tempHeat;
	}

	/**
	 * @param tempHeat the tempHeat to set
	 */
	public void setTempHeat(String tempHeat) {
		this.tempHeat = tempHeat;
	}

	/**
	 * @return the disTemp
	 */
	public String getDisTemp() {
		return disTemp;
	}

	/**
	 * @param disTemp the disTemp to set
	 */
	public void setDisTemp(String disTemp) {
		this.disTemp = disTemp;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the saveEnergy
	 */
	public String getSaveEnergy() {
		return saveEnergy;
	}

	/**
	 * @param saveEnergy the saveEnergy to set
	 */
	public void setSaveEnergy(String saveEnergy) {
		this.saveEnergy = saveEnergy;
	} 
	
	/**
	 * @return the statusOnOff
	 */
	public String getStatusOnOff() {
		return statusOnOff;
	}

	/**
	 * @param statusOnOff the statusOnOff to set
	 */
	public void setStatusOnOff(String statusOnOff) {
		this.statusOnOff = statusOnOff;
	}

	/**
	 * @return the tempHeatSaveEnergy
	 */
	public String getTempHeatSaveEnergy() {
		return tempHeatSaveEnergy;
	}

	/**
	 * @param tempHeatSaveEnergy the tempHeatSaveEnergy to set
	 */
	public void setTempHeatSaveEnergy(String tempHeatSaveEnergy) {
		this.tempHeatSaveEnergy = tempHeatSaveEnergy;
	}
	
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * @return the tempOut
	 */
	public String getTempOut() {
		return tempOut;
	}

	/**
	 * @param tempOut the tempOut to set
	 */
	public void setTempOut(String tempOut) {
		this.tempOut = tempOut;
	}

	/**
	 * @return the tempComfort
	 */
	public String getTempComfort() {
		return tempComfort;
	}

	/**
	 * @param tempComfort the tempComfort to set
	 */
	public void setTempComfort(String tempComfort) {
		this.tempComfort = tempComfort;
	}

	/**
	 * @return the tempEnergy
	 */
	public String getTempEnergy() {
		return tempEnergy;
	}

	/**
	 * @param tempEnergy the tempEnergy to set
	 */
	public void setTempEnergy(String tempEnergy) {
		this.tempEnergy = tempEnergy;
	}

	/**
	 * @return the heatMode
	 */
	public String getHeatMode() {
		return heatMode;
	}

	/**
	 * @param heatMode the heatMode to set
	 */
	public void setHeatMode(String heatMode) {
		this.heatMode = heatMode;
	}
	
	/**
	 * @return the schedule
	 */
	public String getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the linkageInput
	 */
	public String getLinkageInput() {
		return linkageInput;
	}

	/**
	 * @param linkageInput the linkageInput to set
	 */
	public void setLinkageInput(String linkageInput) {
		this.linkageInput = linkageInput;
	}

	/**
	 * @return the linkageOutput
	 */
	public String getLinkageOutput() {
		return linkageOutput;
	}

	/**
	 * @param linkageOutput the linkageOutput to set
	 */
	public void setLinkageOutput(String linkageOutput) {
		this.linkageOutput = linkageOutput;
	}

	/**
	 * @return the closeBoiler
	 */
	public String getCloseBoiler() {
		return closeBoiler;
	}

	/**
	 * @param closeBoiler the closeBoiler to set
	 */
	public void setCloseBoiler(String closeBoiler) {
		this.closeBoiler = closeBoiler;
	}

	/**
	 * @return the tempHeatDefaultMin
	 */
	public String getTempHeatDefaultMin() {
		return tempHeatDefaultMin;
	}

	/**
	 * @param tempHeatDefaultMin the tempHeatDefaultMin to set
	 */
	public void setTempHeatDefaultMin(String tempHeatDefaultMin) {
		this.tempHeatDefaultMin = tempHeatDefaultMin;
	}

	/**
	 * @return the tempHeatDefaultMax
	 */
	public String getTempHeatDefaultMax() {
		return tempHeatDefaultMax;
	}

	/**
	 * @param tempHeatDefaultMax the tempHeatDefaultMax to set
	 */
	public void setTempHeatDefaultMax(String tempHeatDefaultMax) {
		this.tempHeatDefaultMax = tempHeatDefaultMax;
	}

	public Object clone(){
		LingDongTempBean o = null;
        try{
            o = (LingDongTempBean) super.clone();
        } catch (CloneNotSupportedException e) {
           
        }
        return o;
    }


}
