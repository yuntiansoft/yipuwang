package cn.com.yuntian.wojia.logic;

import java.io.Serializable;

/**
 * ListItemBean
 * @author chenwh
 *
 */
public class ListItemBean implements Comparable<ListItemBean>,Serializable {
	
	private String tableId = null;
	
	private String deviceType = null;
	
	private String deviceName = null;
	
	private String isOnline = null;
	
	private String mac = null;
	
	private String userName = null;
	
	private String adminOnly = null;
	
	private PM25Bean pm25bean = null;
	
	private AmerTempBean amerTempBean = null;
	
	private GreenTempBean greenTempBean = null;
	
	private PM25VocBean pm25VocBean = null;
	
	private HeatingTempBean heatingTempBean = null;
	
	private Co2VocBean co2VocBean = null;
	
	private LingDongTempBean lingDongTempBean = null;
	
	private FtkzyBean ftkzyBean = null;

	private YiPuWTempBean yiPuWTempBean = null;
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the pm25bean
	 */
	public PM25Bean getPm25bean() {
		return pm25bean;
	}

	/**
	 * @param pm25bean the pm25bean to set
	 */
	public void setPm25bean(PM25Bean pm25bean) {
		this.pm25bean = pm25bean;
	}

	/**
	 * @return the isOnline
	 */
	public String getIsOnline() {
		return isOnline;
	}

	/**
	 * @param isOnline the isOnline to set
	 */
	public void setIsOnline(String isOnline) {
		this.isOnline = isOnline;
	}

	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the adminOnly
	 */
	public String getAdminOnly() {
		return adminOnly;
	}

	/**
	 * @param adminOnly the adminOnly to set
	 */
	public void setAdminOnly(String adminOnly) {
		this.adminOnly = adminOnly;
	}

	/**
	 * @return the amerTempBean
	 */
	public AmerTempBean getAmerTempBean() {
		return amerTempBean;
	}

	/**
	 * @param amerTempBean the amerTempBean to set
	 */
	public void setAmerTempBean(AmerTempBean amerTempBean) {
		this.amerTempBean = amerTempBean;
	}

	/**
	 * @return the greenTempBean
	 */
	public GreenTempBean getGreenTempBean() {
		return greenTempBean;
	}

	/**
	 * @param greenTempBean the greenTempBean to set
	 */
	public void setGreenTempBean(GreenTempBean greenTempBean) {
		this.greenTempBean = greenTempBean;
	} 

	/**
	 * @return the tableId
	 */
	public String getTableId() {
		return tableId;
	}

	/**
	 * @param tableId the tableId to set
	 */
	public void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	/**
	 * @return the pm25VocTempBean
	 */
	public PM25VocBean getPm25VocBean() {
		return pm25VocBean;
	}

	/**
	 * @param pm25VocTempBean the pm25VocTempBean to set
	 */
	public void setPm25VocBean(PM25VocBean pm25VocBean) {
		this.pm25VocBean = pm25VocBean;
	}
	
	/**
	 * @return the heatingTempBean
	 */
	public HeatingTempBean getHeatingTempBean() {
		return heatingTempBean;
	}

	/**
	 * @param heatingTempBean the heatingTempBean to set
	 */
	public void setHeatingTempBean(HeatingTempBean heatingTempBean) {
		this.heatingTempBean = heatingTempBean;
	}
	
	/**
	 * @return the co2VocBean
	 */
	public Co2VocBean getCo2VocBean() {
		return co2VocBean;
	}

	/**
	 * @param co2VocBean the co2VocBean to set
	 */
	public void setCo2VocBean(Co2VocBean co2VocBean) {
		this.co2VocBean = co2VocBean;
	}
	
	/**
	 * @return the lingDongTempBean
	 */
	public LingDongTempBean getLingDongTempBean() {
		return lingDongTempBean;
	}

	/**
	 * @param lingDongTempBean the lingDongTempBean to set
	 */
	public void setLingDongTempBean(LingDongTempBean lingDongTempBean) {
		this.lingDongTempBean = lingDongTempBean;
	}
	
	/**
	 * @return the ftkzyBean
	 */
	public FtkzyBean getFtkzyBean() {
		return ftkzyBean;
	}

	/**
	 * @param ftkzyBean the ftkzyBean to set
	 */
	public void setFtkzyBean(FtkzyBean ftkzyBean) {
		this.ftkzyBean = ftkzyBean;
	}

	@Override
	public int compareTo(ListItemBean arg0) {
		int thisIsOnline = 0;
		int argIsOnline = 0;
		try {
			thisIsOnline = Integer.valueOf(this.isOnline).intValue();
		} catch (Exception e) {
			
		}
		try {
			argIsOnline = Integer.valueOf(arg0.getIsOnline()).intValue();
		} catch (Exception e) {
			
		}
		if (thisIsOnline < argIsOnline) {
			return 1;
		} else if (thisIsOnline > argIsOnline) {
			return -1;
		} else {
			if (Integer.valueOf(this.tableId) > Integer.valueOf(arg0.getTableId())) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	public YiPuWTempBean getYiPuWTempBean() {
		return yiPuWTempBean;
	}

	public void setYiPuWTempBean(YiPuWTempBean yiPuWTempBean) {
		this.yiPuWTempBean = yiPuWTempBean;
	}
}
