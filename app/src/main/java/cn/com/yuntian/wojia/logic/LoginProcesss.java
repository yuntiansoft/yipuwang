package cn.com.yuntian.wojia.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.res.Resources.NotFoundException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.TextView;



import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import com.loopj.android.http.RequestParams;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.db.HailinDB;
import cn.com.yuntian.wojia.fragment.EmailResendFragment;
import cn.com.yuntian.wojia.fragment.ListLeftFragment;
import cn.com.yuntian.wojia.fragment.ListRightFragment;
import cn.com.yuntian.wojia.fragment.LoginFragment;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.HttpClientUtil;
import cn.com.yuntian.wojia.util.UserInfo;

/**
 * ViewHolder
 * 
 * @author chenwh
 */
public class LoginProcesss {
	private FragmentActivity ctx = null;
	private String userId = null;
	private String pwd = null;
	private String userType = null;
	private CustomProgressDialog progressDialog = null;
//	private boolean blAutoLogin = false;
	private HailinDB db = null;
	private TextView messageText;
	
	public LoginProcesss(FragmentActivity ctx) {
		this.ctx = ctx;
		this.db = new HailinDB(ctx);
	}
	
	public LoginProcesss(FragmentActivity ctx, String userId, String pwd, String userType, 
			CustomProgressDialog progressDialog, TextView messageText) {
		this.ctx = ctx;
		this.userId = userId;
		this.pwd = pwd;
		this.userType = userType;
		this.progressDialog = progressDialog;
		this.db = new HailinDB(ctx);
		this.messageText = messageText;
	}
	
	public void login() {
//    	String url = ctx.getResources().getString(R.string.url_base) 
//    			+ ctx.getResources().getString(R.string.url_loginandroid);
//    	Map<String, String> paramMap = new HashMap<String, String>();
//    	paramMap.put(ctx.getResources().getString(R.string.param_client_id), 
//    			ctx.getResources().getString(R.string.value_client_id));
//    	paramMap.put(ctx.getResources().getString(R.string.param_client_secret), 
//    			ctx.getResources().getString(R.string.value_client_secret));
//    	paramMap.put(ctx.getResources().getString(R.string.param_grant_type), 
//    			ctx.getResources().getString(R.string.value_grant_type));
//    	paramMap.put(ctx.getResources().getString(R.string.param_user_nm), userId);
//    	paramMap.put(ctx.getResources().getString(R.string.param_password), pwd);
//    	AbstractAsyncResponseListener callback = new AbstractAsyncResponseListener(
//    			AbstractAsyncResponseListener.RESPONSE_TYPE_JSON_OBJECT) {
//    		@Override
//    		protected void onSuccess(JSONObject response) {
//    				// 出勤时间
//    				try {
//						String tokenType = response.getString(
//								ctx.getResources().getString(R.string.param_token_type));
//						String acessToken = response.getString(
//								ctx.getResources().getString(R.string.param_access_token));
//						
//						UserInfo.UserInfo.setUserName(userId);
//						UserInfo.UserInfo.setPassword(pwd);
//						UserInfo.UserInfo.setTokenType(tokenType);
//						UserInfo.UserInfo.setAcessToken(acessToken);
//						UserInfo.UserInfo.setAuthority(tokenType + " " + acessToken);
//						UserInfo.UserInfo.setTempUnit(
//								ctx.getResources().getString(R.string.lang_temp_default));
//						
//						ListFragment newFragment = new ListFragment();
//						if (!blAutoLogin) {
//							Map<String, String> userMap = new HashMap<String, String>();
//							userMap.put(Constants.USER_NAME, userId);
//							userMap.put(Constants.PASSWORD, pwd);
//							userMap.put(Constants.TOKEN_TYPE, tokenType);
//							userMap.put(Constants.ACESS_TOKEN, acessToken);
//							userMap.put(Constants.TEMP_UNIT, ctx.getResources().getString(R.string.lang_temp_default));
//							db.getTUser().delete();
//							db.getTUser().add(userMap);
//							
//							redirectPage(newFragment, true, null);
//						} else {
//							Map<String, String> param = new HashMap<String, String>();
//		 	 				param.put(Constants.USER_NAME, userId);
//		 	 				param.put(Constants.TOKEN_TYPE, tokenType);
//		 	 				param.put(Constants.ACESS_TOKEN, acessToken);
//		 			        db.getTUser().update(param);
//							redirectPage(newFragment, false, null);
//						}
//									
//	    				if (progressDialog != null && !blAutoLogin) {
//	    					progressDialog.dismiss();
//	    				}		    				
//					} catch (NotFoundException e) {
//						doException(ctx.getResources().getString(R.string.lang_mess_exception));
//					} catch (JSONException e) {
//						doException(ctx.getResources().getString(R.string.lang_mess_exception));
//					}				   			
//    		}
//    		
//    		@Override
//    		protected void onFailure(Throwable e) {
//    			Log.e(this.getClass().getName(), e.getMessage());
//    			
//    			if (!blAutoLogin) {
//    				if (e instanceof HttpResponseException) {
//        				HttpResponseException he = (HttpResponseException) e;
//        				if (ctx.getResources().getString(R.string.value_code_fourZeroOne).equals(
//        						String.valueOf(he.getStatusCode()))) {
//        					doException(ctx.getResources().getString(R.string.lang_mess_user_pwd_error));
//        				} else if (ctx.getResources().getString(R.string.value_code_fourZeroZero).equals(
//        					String.valueOf(he.getStatusCode()))) {
//        					JSONObject jb = null;
//        					try {
//								jb = new JSONObject(he.getMessage());
//								String msg = jb.getString(ctx.getResources().getString(
//										R.string.param_error_msg));
//	        					if (ctx.getResources().getString(
//	        							R.string.value_err_msg_locked).equals(msg)) {
//	        						EmailResendFragment eResendFragment = new EmailResendFragment();
//	                    			Bundle nBundle = new Bundle();  
//	                    			nBundle.putString(Constants.KEY_USER_NAME, userId);
//	                    			redirectPage(eResendFragment, true, nBundle);
//	        					} else if (ctx.getResources().getString(
//	        							R.string.value_err_msg_bad_cred).equals(msg)) {
//	        						doException(ctx.getResources().getString(R.string.lang_mess_user_pwd_error));
//	        					}	
//        					} catch (JSONException e1) {
//        						doException(ctx.getResources().getString(R.string.lang_mess_exception));
//							} 
//                			if (progressDialog != null && !blAutoLogin) {
//                				progressDialog.dismiss();
//                			}
//        				} else {
//        					doException(ctx.getResources().getString(R.string.lang_mess_exception));
//        				}
//        			} else {	
//        				doException(ctx.getResources().getString(R.string.lang_mess_exception));
//        			}
//    				
//        			
//    			} else {
//    				doException(null);
//    			}		
//    		}  			
//    	};
//    	
//    	try {
//			AsyncHttpClient.sendRequest(ctx, getRequest.getPostRequest(url, paramMap), callback);
//		} catch (UnsupportedEncodingException e1) {
//			doException(ctx.getResources().getString(R.string.lang_mess_exception));
//		}
    	    
        RequestParams requestParams = new RequestParams();
//        String url = ctx.getResources().getString(R.string.url_base)
//    			+ ctx.getResources().getString(R.string.url_loginandroid);
        String url =  ctx.getResources().getString(R.string.url_loginandroid);
        requestParams.put(ctx.getResources().getString(R.string.param_client_id), 
    			ctx.getResources().getString(R.string.value_client_id));
        requestParams.put(ctx.getResources().getString(R.string.param_client_secret), 
    			ctx.getResources().getString(R.string.value_client_secret));
        requestParams.put(ctx.getResources().getString(R.string.param_grant_type), 
    			ctx.getResources().getString(R.string.value_grant_type));
        requestParams.put(ctx.getResources().getString(R.string.param_user_nm), userId);
        requestParams.put(ctx.getResources().getString(R.string.param_password), pwd);
    	
        HttpClientUtil.post(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
				// 用户信息    
				try {
					String tokenType = response.getString(
							ctx.getResources().getString(R.string.param_token_type));
					String acessToken = response.getString(
							ctx.getResources().getString(R.string.param_access_token));
					
					UserInfo.UserInfo.setUserName(userId);
					UserInfo.UserInfo.setPassword(pwd);
					UserInfo.UserInfo.setTokenType(tokenType);
					UserInfo.UserInfo.setAcessToken(acessToken);
					UserInfo.UserInfo.setAuthority(tokenType + " " + acessToken);
					UserInfo.UserInfo.setTempUnit(
							ctx.getResources().getString(R.string.lang_temp_default_unit));
					UserInfo.UserInfo.setUserType(userType);
					UserInfo.UserInfo.setUserStatus(Constants.USER_STATUS_TWO);
					
//					ListFragment newFragment = new ListFragment();
					ListRightFragment newFragment = new ListRightFragment();
//					if (!blAutoLogin) {
						Map<String, String> userMap = new HashMap<String, String>();
						userMap.put(Constants.USER_NAME, userId);
						userMap.put(Constants.PASSWORD, pwd);
						userMap.put(Constants.TOKEN_TYPE, tokenType);
						userMap.put(Constants.ACESS_TOKEN, acessToken);
						userMap.put(Constants.TEMP_UNIT, ctx.getResources().getString(R.string.lang_temp_default_unit));
						userMap.put(Constants.USER_TYPE, userType);
						userMap.put(Constants.USER_STATUS, Constants.USER_STATUS_TWO);
						db.getTUser().delete();
						db.getTUser().add(userMap);
						
						redirectPage(newFragment, true, true, null);
//					} else {
//						Map<String, String> param = new HashMap<String, String>();
//	 	 				param.put(Constants.USER_NAME, userId);
//	 	 				param.put(Constants.TOKEN_TYPE, tokenType);
//	 	 				param.put(Constants.ACESS_TOKEN, acessToken);
//	 			        db.getTUser().update(param);
//						redirectPage(newFragment, false, null);
//					}
								
//    				if (progressDialog != null && !blAutoLogin) {
//    					progressDialog.dismiss();
//    				}	
    				if (progressDialog != null) {
    					progressDialog.dismiss();
    				}	
				} catch (NotFoundException e) {
					doException(ctx.getResources().getString(R.string.lang_mess_exception));
				} catch (JSONException e) {
					doException(ctx.getResources().getString(R.string.lang_mess_exception));
				}
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject jb) {
//            	if (!blAutoLogin) {
    				if (ctx.getResources().getString(R.string.value_code_fourZeroOne).equals(
    						String.valueOf(statusCode))) {
    					doException(ctx.getResources().getString(R.string.lang_mess_user_pwd_error));
    				} else if (ctx.getResources().getString(R.string.value_code_fourZeroZero).equals(
    						String.valueOf(statusCode))) {
//        					JSONObject jb = null;
    					try {
//								jb = new JSONObject(he.getMessage());
							String msg = jb.getString(ctx.getResources().getString(
									R.string.param_error_msg));
        					if (ctx.getResources().getString(
        							R.string.value_err_msg_locked).equals(msg)) {
//        						String url = ctx.getResources().getString(R.string.url_base) 
//        				    			+ ctx.getResources().getString(R.string.url_email_resend);
        						String url =  ctx.getResources().getString(R.string.url_email_resend);
        				    	RequestParams paramMap = new RequestParams();
        				    	paramMap.put(ctx.getResources().getString(R.string.param_user_nm), userId);
        				    	HttpClientUtil.post(url, paramMap, new AsyncHttpResponseHandler() {
									@Override
									public void onFailure(int arg0,
											Header[] arg1, byte[] arg2,
											Throwable arg3) {
										
									}

									@Override
									public void onSuccess(int arg0,
											Header[] arg1, byte[] arg2) {
										
									}      
        				        });
        						
        						EmailResendFragment eResendFragment = new EmailResendFragment();
                    			Bundle nBundle = new Bundle();  
                    			nBundle.putString(Constants.KEY_USER_NAME, userId);
                    			redirectPage(eResendFragment, true, false, nBundle);
        					} else if (ctx.getResources().getString(
        							R.string.value_err_msg_bad_cred).equals(msg)) {
        						doException(ctx.getResources().getString(R.string.lang_mess_user_pwd_error));
        					}	
    					} catch (JSONException e1) {
    						doException(ctx.getResources().getString(R.string.lang_mess_exception));
						} 
//            			if (progressDialog != null && !blAutoLogin) {
//            				progressDialog.dismiss();
//            			}
            			if (progressDialog != null) {
            				progressDialog.dismiss();
            			}
    				} else {
    					doException(ctx.getResources().getString(R.string.lang_mess_exception));
    				}
        			
//    			} else {
//    				doException(null);
//    			}	
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
            	Log.e(this.getClass().getName(), e.getMessage(), e);
            	doException(ctx.getResources().getString(R.string.lang_mess_exception));
            }
        });
    }
	
	public void autoLogin(boolean bl) {
		// 去用户表中取得数据
		List<Map<String, String>> userInfo = db.getTUser().findAll();
//		try {
////		List<ListItemBean> green = db.getTDevice().findGreenTemp();
////		List<ListItemBean> findTest = db.getTDevice().findTest();
//		List<ListItemBean> findTest = db.getTDevice().findAll();
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
		if (userInfo != null && userInfo.size() > 0) {	
//			String userType = userInfo.get(0).get(Constants.USER_TYPE);
			String userStatus = userInfo.get(0).get(Constants.USER_STATUS);
//			if (Constants.USER_TYPE_TWO.equals(userType)) {
//				
//			} else {
			if (Constants.USER_STATUS_THREE.equals(userStatus) 
					|| Constants.USER_STATUS_ONE.equals(userStatus)) {
				LoginFragment loginFrag = new LoginFragment();
				redirectPage(loginFrag, false, false, null);
			} else {
				this.userId = userInfo.get(0).get(Constants.USER_NAME);
				this.pwd = userInfo.get(0).get(Constants.PASSWORD);		
				String tokenType = userInfo.get(0).get(Constants.TOKEN_TYPE);
				String acessToken = userInfo.get(0).get(Constants.ACESS_TOKEN);
				String tempUnit = userInfo.get(0).get(Constants.TEMP_UNIT);
				String uType = userInfo.get(0).get(Constants.USER_TYPE);
				UserInfo.UserInfo.setUserName(this.userId);
				UserInfo.UserInfo.setPassword(this.pwd);
				UserInfo.UserInfo.setTokenType(tokenType);
				UserInfo.UserInfo.setAcessToken(acessToken);
				UserInfo.UserInfo.setAuthority(tokenType + " " + acessToken);
				UserInfo.UserInfo.setTempUnit(tempUnit);
				UserInfo.UserInfo.setUserType(uType);
				UserInfo.UserInfo.setUserStatus(Constants.USER_STATUS_TWO);
				
				ListRightFragment newFragment = new ListRightFragment();
				if (bl) {
					redirectPage(newFragment, true, true, null);
				} else {
					redirectPage(newFragment, false, true, null);
				}
			}
//			}
//			this.blAutoLogin = true;
//			login();
//			ListFragment newFragment = new ListFragment();
//			ListRightFragment newFragment = new ListRightFragment();
//			redirectPage(newFragment, false, true, null);
		} else {
//			LoginFragment loginFrag = new LoginFragment();
//			//要把Fragment对象加到Activity里 先得到FragmentManager对象,再得到FragmentTransaction对象
//			FragmentManager fm = ctx.getSupportFragmentManager();
//			FragmentTransaction ft = fm.beginTransaction();
//			//把对应的Fragment对象加到FragmentTransaction对象，第一个参数是父控件,第二个是Fragment对象
//			ft.add(R.id.fragment_container, loginFrag);
//			//最后执行
//			ft.commit();
			LoginFragment loginFrag = new LoginFragment();
			redirectPage(loginFrag, false, false, null);
		}
	}
	
	public void tempUserRegister(String devId) {
		RequestParams requestParams = new RequestParams();
//        String url = ctx.getResources().getString(R.string.url_base)
//    			+ ctx.getResources().getString(R.string.url_temp_register);
		String url = ctx.getResources().getString(R.string.url_temp_register);
        requestParams.put(ctx.getResources().getString(R.string.param_user_nm), devId);
    	
        HttpClientUtil.post(url, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            	login();
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable e, JSONObject jb) {
    			doException(ctx.getResources().getString(R.string.lang_mess_exception));	
            }
            
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable e) {
            	doException(ctx.getResources().getString(R.string.lang_mess_exception));
            }
        });
	}
	
	private void doException(String msg) {
//		if (progressDialog != null && !blAutoLogin) {
//			progressDialog.dismiss();
//		}
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
//		if (blAutoLogin) {
//			ListFragment newFragment = new ListFragment();
////			ListLeftFragment newFragment = new ListLeftFragment();
//			redirectPage(newFragment, false, null);
//		} else {
			// 服务器异常
			messageText.setText(msg);
//			DialogUtil.showDialog(ctx, 
//					ctx.getResources().getString(R.string.server_exception), false);
//		}				
	}
	
	private void redirectPage(Fragment fmObj, boolean fTransactionBl, boolean leftMenuBl, Bundle bundle) {	
		if (bundle != null) {
			fmObj.setArguments(bundle);
		}
		FragmentManager fm = ctx.getSupportFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		
//		if (blAutoLogin) {
//			transaction.setCustomAnimations(
//					android.R.anim.slide_in_left, android.R.anim.slide_out_right);
//		}
		if (leftMenuBl) {
			ListLeftFragment menuFragment = new ListLeftFragment();
			transaction.replace(R.id.menu, menuFragment);
		}
		if (fTransactionBl) {
			transaction.replace(R.id.fragment_container, fmObj);
			//添加到后台堆栈，也就是说能够按back键返回
//			transaction.addToBackStack(this.getClass().getName());
		} else {
			transaction.setCustomAnimations(
					R.anim.push_left_in, R.anim.push_left_in);
			transaction.add(R.id.fragment_container, fmObj);
		}
		
		
		// Commit the transaction
		transaction.commit();
		
		LoginFragment.setLoginPageStatus(false);
	}
	
}