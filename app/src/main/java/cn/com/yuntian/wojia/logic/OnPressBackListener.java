package cn.com.yuntian.wojia.logic;

import android.support.v4.app.Fragment;

/**
 * Created by yuntian on 2017/11/8.
 */

public interface OnPressBackListener {
    void onPressBack();
    void setSelectFragment(Fragment fragment);
}
