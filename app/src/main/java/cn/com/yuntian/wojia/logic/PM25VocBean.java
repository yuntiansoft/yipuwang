package cn.com.yuntian.wojia.logic;

/**
 * ListItemBean
 * @author chenwh
 *
 */
public class PM25VocBean {
	
	private String mac = null;
	
	private String disPm25In = null;
	
	private String disPm25Out = null;
	
	private String disTemp = null; 
	
	private String disHumi = null; 
	
	private String userName = null; 
	
	private String disVoc = null; 

	/**
	 * @return the disTemp
	 */
	public String getDisTemp() {
		return disTemp;
	}

	/**
	 * @param disTemp the disTemp to set
	 */
	public void setDisTemp(String disTemp) {
		this.disTemp = disTemp;
	}

	/**
	 * @return the disHumi
	 */
	public String getDisHumi() {
		return disHumi;
	}

	/**
	 * @param disHumi the disHumi to set
	 */
	public void setDisHumi(String disHumi) {
		this.disHumi = disHumi;
	}

	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the disPm25In
	 */
	public String getDisPm25In() {
		return disPm25In;
	}

	/**
	 * @param disPm25In the disPm25In to set
	 */
	public void setDisPm25In(String disPm25In) {
		this.disPm25In = disPm25In;
	}

	/**
	 * @return the disPm25Out
	 */
	public String getDisPm25Out() {
		return disPm25Out;
	}

	/**
	 * @param disPm25Out the disPm25Out to set
	 */
	public void setDisPm25Out(String disPm25Out) {
		this.disPm25Out = disPm25Out;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the disVoc
	 */
	public String getDisVoc() {
		return disVoc;
	}

	/**
	 * @param disVoc the disVoc to set
	 */
	public void setDisVoc(String disVoc) {
		this.disVoc = disVoc;
	}
     
}
