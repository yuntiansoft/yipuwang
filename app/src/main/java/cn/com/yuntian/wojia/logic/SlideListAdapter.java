package cn.com.yuntian.wojia.logic;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.util.CheckUtil;
import cn.com.yuntian.wojia.util.Constants;
import cn.com.yuntian.wojia.util.UserInfo;
import cn.com.yuntian.wojia.util.Util;


/**
 * SlideListAdapter
 * 
 * @author chenwh
 */
public class SlideListAdapter extends BaseAdapter {
	
	private LayoutInflater mInflater;
	private List<ListItemBean> listItemBean;
	private Context context;
	
	public SlideListAdapter(Context context, List<ListItemBean> listItemBean) {
        super();
        mInflater = ((Activity) context).getLayoutInflater();
        this.context = context;
        this.listItemBean = listItemBean;
    }
	 
    @Override
    public int getCount() {
    	return listItemBean==null?0:listItemBean.size();
    }

    @Override
    public Object getItem(int arg0) {
		return listItemBean==null?null:listItemBean.get(arg0);	
    }

    @Override
    public long getItemId(int arg0) {                        
            return arg0;
    }
    
    public List<ListItemBean> getListBean() {
    	return listItemBean;
    }
    
    @SuppressLint("InflateParams")
	@Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View itemView ;
    	if (context.getResources().getString(R.string.value_deviceid_pm25).equals
    			(listItemBean.get(position).getDeviceType())) {
    		itemView = mInflater.inflate(R.layout.list_item_pm25, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals
        			(listItemBean.get(position).getDeviceType())) {
        	itemView = mInflater.inflate(R.layout.list_item_pm25_voc, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_green).equals
    			(listItemBean.get(position).getDeviceType())
    			|| context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals
    			(listItemBean.get(position).getDeviceType())
    			|| context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals
    			(listItemBean.get(position).getDeviceType())) {
    		itemView = mInflater.inflate(R.layout.list_item_green, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals
    			(listItemBean.get(position).getDeviceType())) {
    		itemView = mInflater.inflate(R.layout.list_item_heating, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals
    			(listItemBean.get(position).getDeviceType())) {
    		itemView = mInflater.inflate(R.layout.list_item_co2_voc, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals
    			(listItemBean.get(position).getDeviceType())) {
    		itemView = mInflater.inflate(R.layout.list_item_lingdong, null);
    	} else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(listItemBean.get(position).getDeviceType())){
			itemView = mInflater.inflate(R.layout.list_item_ftk, null);
		}else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals
				(listItemBean.get(position).getDeviceType())) {
			itemView = mInflater.inflate(R.layout.list_item_yipuw, null);
		}
		else {
    		return null;
    	}
    	
    	RelativeLayout listItemLayout = (RelativeLayout) itemView.findViewById(R.id.listItemLayout);
    	RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
    	((TextView) itemView.findViewById(R.id.nameText)).setText(listItemBean.get(position).getDeviceName());
//    	LinearLayout delBlock = (LinearLayout) itemView.findViewById(R.id.delBlock);
//    	LinearLayout renameBlock = (LinearLayout) itemView.findViewById(R.id.renameBlock);
//    	  	
//
//    	delBlock.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mDeleteListener != null) {
//                	mDeleteListener.onRightDeleteClick(v, position);
//                }
//            }
//        });   
//      
//        renameBlock.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            	if (mRenameListener != null) {
//              	    mRenameListener.onRightRenameClick(v, position);
//                }
//            }
//        });
        
    	if (position == 0) {
    		lp.setMargins(Util.dip2px(context, 2), Util.dip2px(context, 2), Util.dip2px(context, 1), Util.dip2px(context, 2));
    	} else if (position == 1) {
    		lp.setMargins(Util.dip2px(context, 1), Util.dip2px(context, 2), Util.dip2px(context, 2), Util.dip2px(context, 2));
    	} else if (position%2 == 0) {
    		lp.setMargins(Util.dip2px(context, 2), 0, Util.dip2px(context, 1), Util.dip2px(context, 2));
    	} else {
    		lp.setMargins(Util.dip2px(context, 1), 0, Util.dip2px(context, 1), Util.dip2px(context, 2));
    	}
    	listItemLayout.setLayoutParams(lp);
    	
    	
		setValue(itemView, position);
		if (!Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position).getIsOnline())) {
			traversalView(listItemLayout);
		}

        return itemView;
    }
    
    private void setValue(View view, int position) {
    	if (context.getResources().getString(R.string.value_deviceid_pm25).equals
    			(listItemBean.get(position).getDeviceType())) {
    		int pm25 = 0;
    		if (CheckUtil.requireCheck(listItemBean.get(position).getPm25bean().getDisPm25In())) {
    			pm25 = Integer.valueOf(
    					listItemBean.get(position).getPm25bean().getDisPm25In()).intValue();
    		}  				
    		((TextView) view.findViewById(R.id.pm25Text)).setText(String.valueOf(pm25));
    		((TextView) view.findViewById(R.id.tempText)).setText(
    				Util.getValue(Util.getTempDegree(listItemBean.get(position).getPm25bean().getDisTemp()), 0));
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_fah));
    		}
    		((TextView) view.findViewById(R.id.humiText)).setText(
    				listItemBean.get(position).getPm25bean().getDisHumi() + "%");
    	} else if (context.getResources().getString(R.string.value_deviceid_pm25_voc).equals
    			(listItemBean.get(position).getDeviceType())) {
			int pm25 = 0;
			if (CheckUtil.requireCheck(listItemBean.get(position).getPm25VocBean().getDisPm25In())) {
				pm25 = Integer.valueOf(
						listItemBean.get(position).getPm25VocBean().getDisPm25In()).intValue();
			}
			int voc = 0;
			if (CheckUtil.requireCheck(listItemBean.get(position).getPm25VocBean().getDisVoc())) {
				voc = Integer.valueOf(
						listItemBean.get(position).getPm25VocBean().getDisVoc()).intValue();
			}
			((TextView) view.findViewById(R.id.pm25Text)).setText(String.valueOf(pm25));
			((TextView) view.findViewById(R.id.tempText)).setText(
					Util.getValue(Util.getTempDegree(listItemBean.get(position).getPm25VocBean().getDisTemp()), 0));
			if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
				((TextView) view.findViewById(R.id.tempUnit)).setText(
						context.getResources().getString(R.string.value_temp_cen));
			} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
				((TextView) view.findViewById(R.id.tempUnit)).setText(
						context.getResources().getString(R.string.value_temp_fah));
			}
			((TextView) view.findViewById(R.id.humiText)).setText(
					listItemBean.get(position).getPm25VocBean().getDisHumi() + "%");
			TextView vocText = ((TextView) view.findViewById(R.id.vocText));
			switch (voc) {
    		case 1:  
	        case 2:
	        case 3:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_normal));
	            break;  
	        case 4:
	        case 5:
	        case 6:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_Exc));
	        	break; 
	        case 7:
	        case 8:
	        case 9:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_ser_Exc));
	        	break; 
	        default: 
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_normal));
	            break;
			} 
    	} else if (context.getResources().getString(R.string.value_deviceid_green).equals
    			(listItemBean.get(position).getDeviceType())
    			|| context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals
    			(listItemBean.get(position).getDeviceType())
    			|| context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals
				(listItemBean.get(position).getDeviceType())) {
    		if (context.getResources().getString(R.string.value_deviceid_lingdong_ac).equals
					(listItemBean.get(position).getDeviceType()) 
					|| context.getResources().getString(R.string.value_deviceid_lingdong_cool_heat).equals
					(listItemBean.get(position).getDeviceType())) {
				((ImageView) view.findViewById(R.id.list_green_energy)).setVisibility(View.GONE);
			}
    		((TextView) view.findViewById(R.id.tempText)).setText(
    				Util.getTempDegree(listItemBean.get(position).getGreenTempBean().getDisTemp()));  		
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_fah));
    		}
    		
    		if (context.getResources().getString(R.string.value_status_off).equals(
					listItemBean.get(position).getGreenTempBean().getStatusOnOff())) {
    			((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.GONE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.VISIBLE);
				if (Constants.LANG_DEFALUT_ENG.equals(
						context.getResources().getString(R.string.lang_temp_default))) {
					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off_en);
				} else {
					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off);
				}
			} else {
				((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.VISIBLE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.GONE); 
				if (Constants.VALUE_ENERGY_ONE.equals(
						listItemBean.get(position).getGreenTempBean().getSaveEnergy())) {
					if (context.getResources().getString(R.string.value_deviceid_green).equals
							(listItemBean.get(position).getDeviceType())) {
						((ImageView) view.findViewById(R.id.list_green_energy)).setVisibility(View.VISIBLE);
	    				((ImageView) view.findViewById(R.id.list_green_energy)).setBackgroundResource(
	        					R.drawable.list_item_energy_on);
					} 
					((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	    					R.drawable.list_item_fan_low);
				} else {
					if (context.getResources().getString(R.string.value_deviceid_green).equals
							(listItemBean.get(position).getDeviceType())) {
						((ImageView) view.findViewById(R.id.list_green_energy)).setVisibility(View.VISIBLE);
						((ImageView) view.findViewById(R.id.list_green_energy)).setBackgroundResource(
	        					R.drawable.list_item_energy_off);
					}
					if (context.getResources().getString(R.string.value_status_five).equals(
	        				listItemBean.get(position).getGreenTempBean().getStatus())
	        				&& context.getResources().getString(R.string.value_fan_mod_two).equals(
	                				listItemBean.get(position).getGreenTempBean().getFanMod())) {
						((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	        					R.drawable.list_item_fan_high);
					} else {
						if (context.getResources().getString(R.string.value_fan_mod_two).equals(
	            				listItemBean.get(position).getGreenTempBean().getFanMod())) {
	            			((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	            					R.drawable.list_item_fan_auto);
	            		} else if (context.getResources().getString(R.string.value_fan_mod_three).equals(
	                				listItemBean.get(position).getGreenTempBean().getFanMod())) {
	            			((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	                					R.drawable.list_item_fan_low);
	        			} else if (context.getResources().getString(R.string.value_fan_mod_four).equals(
	            				listItemBean.get(position).getGreenTempBean().getFanMod())) {
	        				((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	            					R.drawable.list_item_fan_middle);
	        			} else if (context.getResources().getString(R.string.value_fan_mod_five).equals(
	            				listItemBean.get(position).getGreenTempBean().getFanMod())) {
	        				((ImageView) view.findViewById(R.id.list_item_fan)).setBackgroundResource(
	            					R.drawable.list_item_fan_high);
	        			}
					}
				}
			
    			if (context.getResources().getString(R.string.value_status_one).equals(
        				listItemBean.get(position).getGreenTempBean().getStatus())) {
        			((ImageView) view.findViewById(R.id.list_item_status)).setBackgroundResource(
        					R.drawable.list_item_status_heat);
        		} else if (context.getResources().getString(R.string.value_status_two).equals(
        				listItemBean.get(position).getGreenTempBean().getStatus())) {
        			((ImageView) view.findViewById(R.id.list_item_status)).setBackgroundResource(
        					R.drawable.list_item_status_cool);
        		} else if (context.getResources().getString(R.string.value_status_five).equals(
        				listItemBean.get(position).getGreenTempBean().getStatus())) {
        			((ImageView) view.findViewById(R.id.list_item_status)).setBackgroundResource(
        					R.drawable.list_item_status_vent);
        		} else {
        			if (Constants.LANG_DEFALUT_ENG.equals(
    						context.getResources().getString(R.string.lang_temp_default))) {
    					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
            					R.drawable.list_item_status_off_en);
    				} else {
    					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
            					R.drawable.list_item_status_off);
    				}
        		}
			}
    	}  else if (context.getResources().getString(R.string.value_deviceid_heat_temp).equals
    			(listItemBean.get(position).getDeviceType())) {
    		((TextView) view.findViewById(R.id.tempText)).setText(
    				Util.getTempDegree(listItemBean.get(position).getHeatingTempBean().getDisTemp()));  		
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_fah));
    		}	
//    		if (Constants.VALUE_ENERGY_ONE.equals(
//					listItemBean.get(position).getHeatingTempBean().getSaveEnergy())) {
//				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
//    					R.drawable.list_item_energy_on);
//			} else {
//				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
//    					R.drawable.list_item_energy_off);
//			}
			if (context.getResources().getString(R.string.value_status_off).equals(
					listItemBean.get(position).getHeatingTempBean().getStatusOnOff())) {
//    			((ImageView) view.findViewById(R.id.list_heating_status)).setVisibility(View.GONE);
//    			((ImageView) view.findViewById(R.id.list_heating_energy)).setVisibility(View.GONE);
//    			((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setVisibility(View.VISIBLE);
				((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.GONE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.VISIBLE); 
    			if (Constants.LANG_DEFALUT_ENG.equals(
						context.getResources().getString(R.string.lang_temp_default))) {
    				((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off_en);
    			} else {
    				((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off);
    			}
    		} else {
    			((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.VISIBLE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.GONE); 
    			((ImageView) view.findViewById(R.id.list_heating_energy)).setVisibility(View.VISIBLE);
    			if (Constants.VALUE_ENERGY_ONE.equals(
    					listItemBean.get(position).getHeatingTempBean().getSaveEnergy())) {
    				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
        					R.drawable.list_item_energy_on);
    			} else {
    				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
        					R.drawable.list_item_energy_off);
    			}
    			((ImageView) view.findViewById(R.id.list_heating_status)).setVisibility(View.VISIBLE);
    			((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setVisibility(View.GONE);
    			if (context.getResources().getString(R.string.value_status_one).equals(
    					listItemBean.get(position).getHeatingTempBean().getStatus())) {
    				((ImageView) view.findViewById(R.id.list_heating_status)).setBackgroundResource(
        					R.drawable.list_item_heat_on);
        		} else {
        			((ImageView) view.findViewById(R.id.list_heating_status)).setBackgroundResource(
        					R.drawable.list_item_heat_off);
        		}
    		}
    		
    	} else if (context.getResources().getString(R.string.value_deviceid_co2_voc).equals
    			(listItemBean.get(position).getDeviceType())) {
			int co2 = 0;
			int voc = 0;
			if (CheckUtil.requireCheck(listItemBean.get(position).getCo2VocBean().getDisCo2())) {
				co2 = Integer.valueOf(
						listItemBean.get(position).getCo2VocBean().getDisCo2()).intValue();
			}
			if (CheckUtil.requireCheck(listItemBean.get(position).getCo2VocBean().getDisVoc())) {
				voc = Integer.valueOf(
						listItemBean.get(position).getCo2VocBean().getDisVoc()).intValue();
			}
			((TextView) view.findViewById(R.id.co2Text)).setText(String.valueOf(co2));
			TextView airText = ((TextView) view.findViewById(R.id.airText));
			if (co2 == 0) {
				airText.setText(R.string.lang_txt_voc_error);
	    	} else if (co2 < 450) {
	    		airText.setText(R.string.lang_txt_co2_one);
	    	} else if (co2 < 1000) {
	    		airText.setText(R.string.lang_txt_co2_two);
	    	} else {
	    		airText.setText(R.string.lang_txt_co2_three);
	    	} 
			
			TextView vocText = ((TextView) view.findViewById(R.id.vocText));
			switch (voc) {
    		case 1:  
	        case 2:
	        case 3:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_normal));
	            break;  
	        case 4:
	        case 5:
	        case 6:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_Exc));
	        	break; 
	        case 7:
	        case 8:
	        case 9:
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_ser_Exc));
	        	break; 
	        default: 
	        	vocText.setText(context.getResources().getString(R.string.lang_txt_voc_normal));
	            break;
			} 
		} else if (context.getResources().getString(R.string.value_deviceid_lingdong_temp).equals
    			(listItemBean.get(position).getDeviceType())) {
    		((TextView) view.findViewById(R.id.tempText)).setText(
    				Util.getTempDegree(listItemBean.get(position).getLingDongTempBean().getDisTemp()));  		
    		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_cen));
    		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
    			((TextView) view.findViewById(R.id.tempUnit)).setText(
    					context.getResources().getString(R.string.value_temp_fah));
    		}	
    		if (context.getResources().getString(R.string.value_status_off).equals(
					listItemBean.get(position).getLingDongTempBean().getStatusOnOff())) {
    			((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.GONE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.VISIBLE);
    			if (Constants.LANG_DEFALUT_ENG.equals(
						context.getResources().getString(R.string.lang_temp_default))) {
    				((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off_en);
    			} else {
    				((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
        					R.drawable.list_item_status_off);
    			}
//    			((ImageView) view.findViewById(R.id.list_ld_status)).setBackgroundResource(
//    					R.drawable.list_item_heat_off);
    		} else {
    			((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.VISIBLE); 
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.GONE); 
				if (context.getResources().getString(R.string.value_mod_out).equals(
						listItemBean.get(position).getLingDongTempBean().getHeatMode())) {
					((ImageView) view.findViewById(R.id.list_ld_mode)).setVisibility(View.VISIBLE);
					((ImageView) view.findViewById(R.id.list_ld_mode)).setBackgroundResource(
	    					R.drawable.list_item_mod_out);
				} else if (context.getResources().getString(R.string.value_mod_energy).equals(
						listItemBean.get(position).getLingDongTempBean().getHeatMode())) {
					((ImageView) view.findViewById(R.id.list_ld_mode)).setVisibility(View.VISIBLE);
					((ImageView) view.findViewById(R.id.list_ld_mode)).setBackgroundResource(
	    					R.drawable.list_item_mod_energy);
				} else if (context.getResources().getString(R.string.value_mod_comfort).equals(
						listItemBean.get(position).getLingDongTempBean().getHeatMode())) {
					((ImageView) view.findViewById(R.id.list_ld_mode)).setVisibility(View.VISIBLE);
					((ImageView) view.findViewById(R.id.list_ld_mode)).setBackgroundResource(
	    					R.drawable.list_item_mod_comfort);
				} else {
					((ImageView) view.findViewById(R.id.list_ld_mode)).setVisibility(View.GONE);
				}
 
			
    			if (context.getResources().getString(R.string.value_status_one).equals(
    					listItemBean.get(position).getLingDongTempBean().getStatus())) {
    				((ImageView) view.findViewById(R.id.list_ld_status)).setBackgroundResource(
        					R.drawable.list_item_heat_on);
        		} else {
        			((ImageView) view.findViewById(R.id.list_ld_status)).setBackgroundResource(
        					R.drawable.list_item_heat_off);
        		}
    			if (Constants.LANG_DEFALUT_ENG.equals(
						context.getResources().getString(R.string.lang_temp_default))) {
    				((ImageView) view.findViewById(R.id.list_ld_statusonoff)).setBackgroundResource(
        					R.drawable.list_item_status_on_en);
    			} else {
    				((ImageView) view.findViewById(R.id.list_ld_statusonoff)).setBackgroundResource(
        					R.drawable.list_item_status_on);
    			}
    		}
		} else if (context.getResources().getString(R.string.value_deviceid_ftkzy).equals(listItemBean.get(position).getDeviceType())){
			boolean tankUpFlg = true;
			String tankUpTemp = listItemBean.get(position).getFtkzyBean().getTankTempThree();
			if (!CheckUtil.requireCheck(tankUpTemp)) {
				tankUpTemp = "0";
			} else if (tankUpTemp.startsWith("c") || tankUpTemp.startsWith("f")
					|| tankUpTemp.startsWith("C") || tankUpTemp.startsWith("F")) {
				tankUpTemp = Util.getValue(Util.getTempDegree(tankUpTemp), 0);
			} else {
				tankUpFlg = false;
			}
			((TextView) view.findViewById(R.id.tempText)).setText(tankUpTemp);
			if (tankUpFlg) {
				if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
					((TextView) view.findViewById(R.id.tempUnit)).setText(
							context.getResources().getString(R.string.value_temp_cen));
				} else {
					((TextView) view.findViewById(R.id.tempUnit)).setText(
							context.getResources().getString(R.string.value_temp_fah));
				}
			} else {
				((TextView) view.findViewById(R.id.tempUnit)).setText("");
			}

			if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position).getIsOnline())) {
//				view.setBackgroundResource(R.drawable.ftkzy_lst_bg);

				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getCollPump())) {
					((TextView) view.findViewById(R.id.listCollImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_coll_on60);
				} else {
					((TextView) view.findViewById(R.id.listCollImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_coll_off60);
				}
				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getPipePump())) {
					((TextView) view.findViewById(R.id.listPipeImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_pipe_on60);
				} else {
					((TextView) view.findViewById(R.id.listPipeImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_pipe_off60);
				}
				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getAuxiHeat())) {
					((TextView) view.findViewById(R.id.listAuxiImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_auxi_on60);
				} else {
					((TextView) view.findViewById(R.id.listAuxiImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_auxi_off60);
				}

			} else {
				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getCollPump())) {
					((TextView) view.findViewById(R.id.listCollImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_coll_on_offline60);
				} else {
					((TextView) view.findViewById(R.id.listCollImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_coll_off_offline60);
				}
				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getPipePump())) {
					((TextView) view.findViewById(R.id.listPipeImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_pipe_on_offline60);
				} else {
					((TextView) view.findViewById(R.id.listPipeImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_pipe_off_offline60);
				}
				if (Constants.VALUE_ONLINE_ONE.equals(listItemBean.get(position)
						.getFtkzyBean().getAuxiHeat())) {
					((TextView) view.findViewById(R.id.listAuxiImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_auxi_on_offline60);
				} else {
					((TextView) view.findViewById(R.id.listAuxiImg)).setBackgroundResource(
							R.drawable.ftkzy_lst_auxi_off_offline60);
				}
			}

		}else if (context.getResources().getString(R.string.value_deviceid_yipuwang_temp).equals
				(listItemBean.get(position).getDeviceType())) {
			((TextView) view.findViewById(R.id.tempText)).setText(
					Util.getTempDegree(listItemBean.get(position).getYiPuWTempBean().getDisTemp()));
			if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
				((TextView) view.findViewById(R.id.tempUnit)).setText(
						context.getResources().getString(R.string.value_temp_cen));
			} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
				((TextView) view.findViewById(R.id.tempUnit)).setText(
						context.getResources().getString(R.string.value_temp_fah));
			}
//    		if (Constants.VALUE_ENERGY_ONE.equals(
//					listItemBean.get(position).getHeatingTempBean().getSaveEnergy())) {
//				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
//    					R.drawable.list_item_energy_on);
//			} else {
//				((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
//    					R.drawable.list_item_energy_off);
//			}
			if (context.getResources().getString(R.string.value_status_off).equals(
					listItemBean.get(position).getYiPuWTempBean().getStatusOnOff())) {
//    			((ImageView) view.findViewById(R.id.list_heating_status)).setVisibility(View.GONE);
//    			((ImageView) view.findViewById(R.id.list_heating_energy)).setVisibility(View.GONE);
//    			((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setVisibility(View.VISIBLE);
				((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.GONE);
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.VISIBLE);
				if (Constants.LANG_DEFALUT_ENG.equals(
						context.getResources().getString(R.string.lang_temp_default))) {
					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
							R.drawable.list_item_status_off_en);
				} else {
					((ImageView) view.findViewById(R.id.statusOffImg)).setBackgroundResource(
							R.drawable.list_item_status_off);
				}
			} else {
				((LinearLayout) view.findViewById(R.id.eventBlock)).setVisibility(View.VISIBLE);
				((LinearLayout) view.findViewById(R.id.statusOffBlock)).setVisibility(View.GONE);
				((ImageView) view.findViewById(R.id.list_heating_energy)).setVisibility(View.VISIBLE);
				if (context.getResources().getString(R.string.value_heat_mode_enh).equals(
						listItemBean.get(position).getYiPuWTempBean().getHeatMode())) {
					((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
							R.drawable.btn_mergen_sel_icon);
				} else {
					((ImageView) view.findViewById(R.id.list_heating_energy)).setBackgroundResource(
							R.drawable.btn_heat_sel_icon);
				}
				((ImageView) view.findViewById(R.id.list_heating_status)).setVisibility(View.VISIBLE);
				if (context.getResources().getString(R.string.value_status_one).equals(
						listItemBean.get(position).getYiPuWTempBean().getStatus())) {
					((ImageView) view.findViewById(R.id.list_heating_status)).setBackgroundResource(
							R.drawable.list_item_heat_on);
				} else {
					((ImageView) view.findViewById(R.id.list_heating_status)).setBackgroundResource(
							R.drawable.list_item_heat_off);
				}
				if (context.getResources().getString(R.string.value_forbid_mode_low).equals(listItemBean.get(position).getYiPuWTempBean().getForbidMode())){
					((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setBackgroundResource(R.drawable.ld_btn_energy_sel_icon);
				}else if (context.getResources().getString(R.string.value_forbid_mode_mid).equals(listItemBean.get(position).getYiPuWTempBean().getForbidMode())){
					((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setBackgroundResource(R.drawable.ld_btn_comfort_sel_icon);
				}else {
					((ImageView) view.findViewById(R.id.list_heating_statusonoff)).setBackgroundResource(R.drawable.ld_btn_out_sel_icon);
				}
			}

		}
    }
    
    private void traversalView(ViewGroup viewGroup) {
    	try {
	    	
	        int count = viewGroup.getChildCount();
	        for (int i = 0; i < count; i++) {
	            View view = viewGroup.getChildAt(i);
	            if (view instanceof ViewGroup) {
	                traversalView((ViewGroup) view);
	            } else {
	            	setViewAipha(view);
	            }
	        }
    	} catch ( Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private void setViewAipha(View view) {
    	view.setAlpha(0.2f);
    }
    
    
    
    /**
     * 单击事件监听器
     */
    private onRightDeleteClickListener mDeleteListener = null;
    
    private onRightRenameClickListener mRenameListener = null;
    
    public void setOnRightDeleteClickListener(onRightDeleteClickListener listener){
    	mDeleteListener = listener;
    }
    
    public void setOnRightRenameClickListener(onRightRenameClickListener listener){
    	mRenameListener = listener;
    }

    public interface onRightDeleteClickListener {
        void onRightDeleteClick(View v, int position);
    }
    
    public interface onRightRenameClickListener {
        void onRightRenameClick(View v, int position);
    }	
    
}
