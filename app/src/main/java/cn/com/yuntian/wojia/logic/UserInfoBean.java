package cn.com.yuntian.wojia.logic;

public class UserInfoBean {
	
	private String userName = null;
	
	private String password = null;
	
	private String tokenType = null;
	
	private String acessToken = null;
	
	private String authority = null;
	
	private String tempUnit = null;
	
	private String userType = null;
	
	private String userStatus = null;

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the tokenType
	 */
	public String getTokenType() {
		return tokenType;
	}

	/**
	 * @param tokenType the tokenType to set
	 */
	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	/**
	 * @return the acessToken
	 */
	public String getAcessToken() {
		return acessToken;
	}

	/**
	 * @param acessToken the acessToken to set
	 */
	public void setAcessToken(String acessToken) {
		this.acessToken = acessToken;
	}

	/**
	 * @return the authority
	 */
	public String getAuthority() {
		return authority;
	}

	/**
	 * @param authority the authority to set
	 */
	public void setAuthority(String authority) {
		this.authority = authority;
	}

	/**
	 * @return the tempUnit
	 */
	public String getTempUnit() {
		return tempUnit;
	}

	/**
	 * @param tempUnit the tempUnit to set
	 */
	public void setTempUnit(String tempUnit) {
		this.tempUnit = tempUnit;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}

	/**
	 * @return the userStatus
	 */
	public String getUserStatus() {
		return userStatus;
	}

	/**
	 * @param userStatus the userStatus to set
	 */
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	
	public void clear() {
		this.acessToken = null;
		this.authority = null;
		this.password = null;
		this.tempUnit = null;
		this.tokenType = null;
		this.userName = null;
		this.userType = null;
		this.userStatus = null;
	}
}
