package cn.com.yuntian.wojia.logic;

import android.view.ViewGroup;
import android.widget.LinearLayout;


/**
 * ViewHolder
 * 
 * @author chenwh
 */
public class ViewHolder {
	
    private LinearLayout linearLayout;
    private ViewGroup deleteHolder;
    private ViewGroup renameHolder;
    
    public void setLinearLayout(LinearLayout linearLayout) {
		this.linearLayout = linearLayout;
	}
	
	public LinearLayout getLinearLayout() {
		return this.linearLayout;
	}
	
	public void setDeleteHolder(ViewGroup deleteHolder) {
		this.deleteHolder = deleteHolder;
	}
	
	public void setRenameHolder(ViewGroup renameHolder) {
		this.renameHolder = renameHolder;
	}
	
	public ViewGroup getDeleteHolder() {
		return this.deleteHolder;
	}
	
	public ViewGroup getRenameHolder() {
		return this.renameHolder;
	}
}