package cn.com.yuntian.wojia.logic;

/**
 * AmerTempBean
 * @author chenwh
 *
 */
public class YiPuWTempBean implements Cloneable {

	private String mac = null;

	private String tempHeat = null;

	private String disTemp = null;

	private String userName = null;

	private String saveEnergy = null;

	private String statusOnOff = null;

	private String tempHeatSaveEnergy = null;

	private String status = null;

	private String tempHeatDefaultMin = null;

	private String tempHeatDefaultMax = null;

	private String heatMode = null;

	private String forbidMode = null;
	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the tempHeat
	 */
	public String getTempHeat() {
		return tempHeat;
	}

	/**
	 * @param tempHeat the tempHeat to set
	 */
	public void setTempHeat(String tempHeat) {
		this.tempHeat = tempHeat;
	}

	/**
	 * @return the disTemp
	 */
	public String getDisTemp() {
		return disTemp;
	}

	/**
	 * @param disTemp the disTemp to set
	 */
	public void setDisTemp(String disTemp) {
		this.disTemp = disTemp;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the saveEnergy
	 */
	public String getSaveEnergy() {
		return saveEnergy;
	}

	/**
	 * @param saveEnergy the saveEnergy to set
	 */
	public void setSaveEnergy(String saveEnergy) {
		this.saveEnergy = saveEnergy;
	}

	/**
	 * @return the statusOnOff
	 */
	public String getStatusOnOff() {
		return statusOnOff;
	}

	/**
	 * @param statusOnOff the statusOnOff to set
	 */
	public void setStatusOnOff(String statusOnOff) {
		this.statusOnOff = statusOnOff;
	}

	/**
	 * @return the tempHeatSaveEnergy
	 */
	public String getTempHeatSaveEnergy() {
		return tempHeatSaveEnergy;
	}

	/**
	 * @param tempHeatSaveEnergy the tempHeatSaveEnergy to set
	 */
	public void setTempHeatSaveEnergy(String tempHeatSaveEnergy) {
		this.tempHeatSaveEnergy = tempHeatSaveEnergy;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public Object clone(){
		YiPuWTempBean o = null;
        try{
            o = (YiPuWTempBean) super.clone();
        } catch (CloneNotSupportedException e) {

        }
        return o;
    }

	public String getTempHeatDefaultMin() {
		return tempHeatDefaultMin;
	}

	public void setTempHeatDefaultMin(String tempHeatDefaultMin) {
		this.tempHeatDefaultMin = tempHeatDefaultMin;
	}

	public String getTempHeatDefaultMax() {
		return tempHeatDefaultMax;
	}

	public void setTempHeatDefaultMax(String tempHeatDefaultMax) {
		this.tempHeatDefaultMax = tempHeatDefaultMax;
	}


	public String getHeatMode() {
		return heatMode;
	}

	public void setHeatMode(String heatMode) {
		this.heatMode = heatMode;
	}

	public String getForbidMode() {
		return forbidMode;
	}

	public void setForbidMode(String forbidMode) {
		this.forbidMode = forbidMode;
	}
}
