package cn.com.yuntian.wojia.util;

import android.app.Application;
import android.content.Context;
import android.os.Environment;


import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;

import cn.com.yuntian.wojia.R;

public class AppContext extends Application {

    private static Context instance;
    
    private static String baseUrl;
    
    public static final String APP_ID = "9dd4ac7d4a";

    @Override
    public void onCreate() {
        instance = getApplicationContext();
        
        /***** Beta�߼����� *****/
        /**
         * true��ʾapp�����Զ���ʼ������ģ��;
         * false�����Զ���ʼ��;
         * �������������sdk��ʼ��Ӱ��app�����ٶȣ���������Ϊfalse��
         * �ں���ĳ��ʱ���ֶ�����Beta.init(getApplicationContext(),false);
         */
        Beta.autoInit = true;

        /**
         * true��ʾ��ʼ��ʱ�Զ��������;
         * false��ʾ�����Զ��������,��Ҫ�ֶ�����Beta.checkUpgrade()����;
         */
        Beta.autoCheckUpgrade = true;

        /**
         * ���������������Ϊ60s(Ĭ�ϼ������Ϊ0s)��60s��SDK���ظ����̨�������);
         */
        Beta.upgradeCheckPeriod = 60 * 1000;

        /**
         * ����������ʱΪ1s��Ĭ����ʱ3s����APP����1s���ʼ��SDK������Ӱ��APP�����ٶ�;
         */
        Beta.initDelay = 1 * 1000;

        /**
         * ����֪ͨ����ͼ�꣬largeIconIdΪ��Ŀ�е�ͼƬ��Դ;
         */
        Beta.largeIconId = R.drawable.wojia;

        /**
         * ����״̬��Сͼ�꣬smallIconIdΪ��Ŀ�е�ͼƬ��ԴId;
         */
        Beta.smallIconId = R.drawable.wojia;

        /**
         * ���ø��µ���Ĭ��չʾ��banner��defaultBannerIdΪ��Ŀ�е�ͼƬ��ԴId;
         * ����̨���õ�banner��ȡʧ��ʱ��ʾ��banner��Ĭ�ϲ�������չʾ��loading��;
         */
        Beta.defaultBannerId = R.drawable.wojia;

        /**
         * ����sd����DownloadΪ������Դ����Ŀ¼;
         * ����������Դ�ᱣ���ڴ�Ŀ¼����Ҫ��manifest�����WRITE_EXTERNAL_STORAGEȨ��;
         */
        Beta.storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

        /**
         * �����ȷ�ϵĵ�����APP�´������Զ�������ʱ���ٴ���ʾ;
         */
        Beta.showInterruptedStrategy = true;
        
        

        /**
         * ֻ������MainActivity����ʾ���µ���������activity�ϲ���ʾ����;
         * �����û�Ĭ������activity��������ʾ����;
         */
//        Beta.canShowUpgradeActs.add(MainActivity.class);

        /***** ͳһ��ʼ��Bugly��Ʒ������Beta *****/
        Bugly.init(this, APP_ID, false);
    }
    
    public static Context getContext(){
        return instance;
    }
    
    public static String getBaseUrl() {
    	String country = instance.getResources().getConfiguration().locale.getCountry(); 
//      String country1 = Locale.getDefault().getCountry(); 
    	if (("CN").equals(country)) {
    		baseUrl = instance.getResources().getString(R.string.url_base_domestic);
    	} else {
    		baseUrl = instance.getResources().getString(R.string.url_base_foreign);
    	}
    	return baseUrl;
    }
    public static String getHost() {
        String country = instance.getResources().getConfiguration().locale.getCountry();
//      String country1 = Locale.getDefault().getCountry();
        if (("CN").equals(country)) {
            return  "http://app.hailin.com";
        } else {
            return "http://app.hilin.com";
        }
    }

    
}
