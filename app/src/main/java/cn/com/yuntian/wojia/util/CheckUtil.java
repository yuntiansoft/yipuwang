/**
 * 
 */
package cn.com.yuntian.wojia.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @version 1.0
 */
public class CheckUtil {
	
	public static boolean requireCheck(String str) {
		if (str == null || str.length() == 0) {
    		return false;
    	}
    	return true;
	}
	
	public static boolean emailCheck(String str) {
		if (!requireCheck(str)) {
			return false;
		}
		String checkPattern = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
    	Pattern regex = Pattern.compile(checkPattern);
    	Matcher matcher = regex.matcher(str);
    	if (!matcher.matches()) {
    		return false;
    	}
    	return true;
	}

	
}
