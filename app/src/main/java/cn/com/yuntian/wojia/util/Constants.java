package cn.com.yuntian.wojia.util;

/** 
 * <p>Code constants class.</p>
 * 
 * @author chenwh
 */
public class Constants {

    /**  �Ƿ��Զ���¼key */
    public static final String AUTO_LOGIN = "AUTO_LOGIN";
    /**  UserName key */
    public static final String USER_NAME = "USER_NAME";
    /**  Password key */
    public static final String PASSWORD = "PASSWORD";
    /**  TOKEN_TYPE key */
    public static final String TOKEN_TYPE = "TOKEN_TYPE";
    /**  ACESS_TOKEN key */
    public static final String ACESS_TOKEN = "ACESS_TOKEN";
    /**  request head token key */
    public static final String AUTHORIZATION = "Authorization";
    /**  request head Accept */
    public static final String ACCECPT = "Accept";
    /**  request head Accept value */
    public static final String ACCECPT_VALUE = "application/json;charset=UTF-8";
    /**  ���϶Ȼ��϶� key */
    public static final String TEMP_UNIT = "TEMP_UNIT";
    /**  ��id  */
    public static final String TABLE_ID = "_id";
    /**  �û���  */
    public static final String TB_NAME_TABLE_USER= "TABLE_USER";
    /**  PM2.5��  */
    public static final String TB_NAME_TABLE_PM25= "TABLE_PM25";
    /**  PM2.5&Voc��  */
    public static final String TB_NAME_TABLE_PM25_VOC= "TABLE_PM25_VOC";
    /**  CO2&Voc��  */
    public static final String TB_NAME_TABLE_CO2_VOC= "TABLE_CO2_VOC";
    /**  PM2.5��ʷ��¼��  */
    public static final String TB_NAME_TABLE_HISTORY = "TABLE_HISTORY";
    /**  ��ʽ�¿�����  */
    public static final String TB_NAME_TABLE_AMER_TEMP = "TABLE_AMER_TEMP";
    /**  �̶�  */
    public static final String TB_NAME_TABLE_GREEN_TEMP = "TABLE_GREEN_TEMP";
    /**  ��ů�¿���  */
    public static final String TB_NAME_TABLE_HEATING_TEMP = "TABLE_HEATING_TEMP";
    /**  �鶯(�²�ů�¿���)  */
    public static final String TB_NAME_TABLE_LINGDONG_TEMP = "TABLE_LINGDONG_TEMP";
    /**  ���������  */
    public static final String TB_NAME_TABLE_FTKZY = "TABLE_FTKZY";
    /**  �豸�ܱ�  */
    public static final String TB_NAME_TABLE_DEVICE = "TB_NAME_TABLE_DEVICE";
    /**  ʱ�� */
    public static final String TIME = "TIME";
    /**  MAC��ַ  */
    public static final String MAC = "MAC";
    /**  �豸����  */
    public static final String DEV_TYPE = "DEV_TYPE";
    /**  �豸����  */
    public static final String DIS_DEV_NAME = "DIS_DEV_NAME";
    /**  �豸�Ƿ�����  */
    public static final String IS_ONLINE = "IS_ONLINE";
    /**  App��Pc����Ȩ��  */
    public static final String ADMIN_ONLY = "ADMIN_ONLY";
    /**  ��ʾ�¶�  */
    public static final String DIS_TEMP = "DIS_TEMP";
    /**  ��ʾʪ��  */
    public static final String DIS_HUMI = "DIS_HUMI";
    /**  ����VOCָ��  */
    public static final String DIS_VOC = "DIS_VOC";
    /**  ����CO2ָ��  */
    public static final String DIS_CO2 = "DIS_CO2";
    /**  ����pm2.5��������  */
    public static final String DIS_PM25_IN = "DIS_PM25_IN";
    /**  ����pm2.5��������  */
    public static final String DIS_PM25_OUT = "DIS_PM25_OUT";
    /**  ISONLIE: 0:���� 1:����  */
    public static final String VALUE_ONLINE_ZERO = "0";
    /**  ISONLIE: 0:���� 1:����  */
    public static final String VALUE_ONLINE_ONE = "1";
    /**  ҳ�洫ֵ��KEY(ע��ҳ�浽������֤ҳ��)  */
    public static final String KEY_USER_NAME = "KEY_USER_NAME";
    /**  ���϶���C��ͷ  */
    public static final String CEN_START_KEY = "c";
    /**  ���϶���F��ͷ  */
    public static final String FAH_START_KEY = "f";
    /**  ���϶�:1  */
    public static final String TEMP_VALUE_ONE = "1";
    /**  ���϶�:2  */
    public static final String TEMP_VALUE_TWO = "2";
    /**  ���������¶�  */
    public static final String TEMP_HEAT = "TEMP_HEAT";
    /**  ��������Ĭ���¶���Сֵ  */
    public static final String TEMP_HEAT_DEFAULT_MIN = "TEMP_HEAT_DEFAULT_MIN";
    /**  ��������Ĭ���¶����ֵ  */
    public static final String TEMP_HEAT_DEFAULT_MAX = "TEMP_HEAT_DEFAULT_MAX";
    /**  ��������״̬  */
    public static final String STATUS = "STATUS";
    /**  ���������¶�  */
    public static final String TEMP_COOL = "TEMP_COOL";
    /**  ϵͳ����ģʽ  */
    public static final String MOD = "MOD";
    /**  ����ʪ��  */
    public static final String HUMI = "HUMI";
    /**  ���÷���ģʽ  */
    public static final String FAN_MOD = "FAN_MOD";
    /**  ���ÿɱ��  */
    public static final String PROMABLE = "PROMABLE";
    /**  ����״̬  */
    public static final String STATUS_ON_OFF = "STATUS_ON_OFF";
    /**  ECO����ʱ����Ĭ���¶�  */
    public static final String TEMP_COOL_SAVE_ENERGY = "TEMP_COOL_SAVE_ENERGY";
    /**  ECO����ʱ����Ĭ���¶�  */
    public static final String TEMP_HEAT_SAVE_ENERGY = "TEMP_HEAT_SAVE_ENERGY";
    /**  ǿ�Ȱ�ť����״̬  */
    public static final String INTS_HEAT_STAT = "INTS_HEAT_STAT";
    /**  ǿ�ȷ������״̬  */
    public static final String HEAT_OUT_STAT = "HEAT_OUT_STAT";
    /**  �����¶�  */
    public static final String DEAD_ZONE_TEMP = "DEAD_ZONE_TEMP";
    /**  ���䰴ť��ʾ״̬  */
    public static final String COLD_STATUS = "COLD_STATUS";
    /**  ���Ȱ�ť��ʾ״̬  */
    public static final String HEAT_STATUS = "HEAT_STATUS";
    /**  �Զ���ť��ʾ״̬  */
    public static final String AUTO_STATUS = "AUTO_STATUS";
    /**  �������Ȱ�ť��ʾ״̬  */
    public static final String EMER_STATUS = "EMER_STATUS";
    /**  �̶�ģʽ�Ƿ�ѡ��  */
    public static final String PERM_STATUS = "PERM_STATUS";
    /**  ԭ��ϵͳ����ģʽ  */
    public static final String ORI_MOD = "ORI_MOD";
    /**  ԭ��ϵͳ����״̬  */
    public static final String ORI_STATUS = "ORI_STATUS";
    /**  ����һ������  */
    public static final String SAVE_ENERGY = "SAVE_ENERGY";
    /**  ������������¶�T1  */
    public static final String COLL_TEMP_ONE = "COLL_TEMP_ONE";
    /**  ˮ���ϲ��¶�T2  */
    public static final String TANK_TEMP_TWO = "TANK_TEMP_TWO";
    /**  ˮ���²��¶�T3  */
    public static final String TANK_TEMP_THREE = "TANK_TEMP_THREE";
    /**  ��ˮ��·�¶�T4  */
    public static final String BACK_TEMP_FOUR = "BACK_TEMP_FOUR";
    /**  ˮ���¶���ʾFlg  */
    public static final String TANK_TEMP_FLG = "TANK_TEMP_FLG";
    /**  ����̫���ܼ���ѭ����P1  */
    public static final String COLL_PUMP = "COLL_PUMP";
    /**  ��·ѭ����P2  */
    public static final String PIPE_PUMP = "PIPE_PUMP";
    /**  ���������EH  */
    public static final String AUXI_HEAT = "AUXI_HEAT";
    /**  ����̫���ܼ���ѭ����P1�����豸  */
    public static final String COLL_PUMP_ON_DEV = "COLL_PUMP_ON_DEV";
    /**  ��·ѭ����P2�����豸  */
    public static final String PIPE_PUMP_ON_DEV = "PIPE_PUMP_ON_DEV";
    /**  ���������EH�����豸  */
    public static final String AUXI_HEAT_ON_DEV = "AUXI_HEAT_ON_DEV";
    /**  һ������: 0:���ܹ�  1:���ܿ�   */
    public static final String VALUE_ENERGY_ZERO = "0";
    /**  һ������: 0:���ܹ�  1:���ܿ� */
    public static final String VALUE_ENERGY_ONE = "1";
    /**  δ��Ȩ��Ϣ  */
    public static final String MSG_UNAUTHORIZED = "Unauthorized";
    /**  Ӣ��ϵͳ:2  */
    public static final String LANG_DEFALUT_ENG = "2";
    /**  ����ϵͳ:1  */
    public static final String LANG_DEFALUT_CHN = "1";
    /**  ClassName: AMER_HOMEPG_FRAGMENT */
    public static final String AMER_HOMEPG_FRAGMENT = "AmerHomepgFragment";
    /**  ClassName: AMER_STATUS_FRAGMENT */
    public static final String AMER_STATUS_FRAGMENT = "AmerStatusFragment";
    /**  ClassName: AMER_FAN_FRAGMENT */
    public static final String AMER_FAN_FRAGMENT = "AmerFanFragment";
    /**  ClassName: AMER_TEMP_FRAGMENT */
    public static final String AMER_TEMP_FRAGMENT = "AmerTempFragment";
    /**  ClassName:  */
    public static final String LINGDONG_TEMP_SCHEDULE_FRAGMENT = "LingDongTempScheduleFragment";
    /**  �û����� */
    public static final String USER_TYPE = "USER_TYPE";
    /**  �û�״̬ */
    public static final String USER_STATUS = "USER_STATUS";
    /**  �û����� : 1:�����û���2����ʱ�û�*/
    public static final String USER_TYPE_ONE = "1";
    /**  �û����� : 1:�����û���2����ʱ�û� */
    public static final String USER_TYPE_TWO = "2";
    /**  �û�״̬ : 1:��ע�ᣬ2���ѵ�¼��3����ע��*/
    public static final String USER_STATUS_ONE = "1";
    /**  �û�״̬ : 1:��ע�ᣬ2���ѵ�¼��3����ע��*/
    public static final String USER_STATUS_TWO = "2";
    /**  �û�״̬ : 1:��ע�ᣬ2���ѵ�¼��3����ע��*/
    public static final String USER_STATUS_THREE = "3";
    /**  ���ģʽĬ���¶�  */
    public static final String TEMP_OUT = "TEMP_OUT";
    /**  ����ģʽĬ���¶�  */
    public static final String TEMP_ENERGY = "TEMP_ENERGY";
    /**  ����ģʽĬ���¶�  */
    public static final String TEMP_COMFORT = "TEMP_COMFORT";
    /**  �鶯����ģʽ  */
    public static final String HEAT_MODE = "HEAT_MODE";
    /**  ϵͳ����  */
    public static final String COUNTRY_CN = "CN";
    /**  SCHEDULE  */
    public static final String SCHEDULE = "SCHEDULE";
    /**  ��������  */
    public static final String LINKAGE_INPUT = "LINKAGE_INPUT";
    /**  �������  */
    public static final String LINKAGE_OUTPUT = "LINKAGE_OUTPUT";
    /**  ǿ�ƹرչ�¯����ʹ��  */
    public static final String CLOSE_BOILER = "CLOSE_BOILER";
    /**  �������  */
    public static final String WEEK = "WEEK";
    /**屏蔽模式选项 0:无屏蔽，1：抵挡屏蔽，2：中档屏蔽*/
    public static final String FORBID_MODE = "FORBID_MODE";
    public static final String TB_NAME_TABLE_YIPUW_TEMP = "TB_NAME_TABLE_YIPUW_TEMP";
}
