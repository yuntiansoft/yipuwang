/**
 * 
 */
package cn.com.yuntian.wojia.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;

import cn.com.yuntian.wojia.R;


/**
 * @version 1.0
 */
public class DialogUtil {
	// ����һ����ʾ��Ϣ�ĶԻ���
	public static void showDialog(final Context ctx, String msg,
			boolean closeSelf) {
		// ����һ��AlertDialog.Builder����
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx).setMessage(
				msg).setCancelable(false);
		if (closeSelf) {
			builder.setPositiveButton("ȷ��", new OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					// ������ǰActivity
					((Activity) ctx).finish();
				}
			});
		} else {
			builder.setPositiveButton("ȷ��", null);
		}
		builder.create().show();
	}

	// ����һ����ʾָ������ĶԻ���
	public static void showDialog(Context ctx, View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
				.setView(view).setCancelable(false)
				.setPositiveButton("ȷ��", null);
		builder.create().show();
	}
	
	public static Builder showDialog(Context ctx, String msg, String title) {
		Builder builder = new Builder(ctx);
		builder.setMessage(msg);  
		builder.setTitle(title);  
		builder.setPositiveButton(ctx.getResources().getString(R.string.lang_dialog_confirm),
				new DialogInterface.OnClickListener() {   
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();    	
			}
		});  
//		
//		builder.setPositiveButton("ȷ��", new DialogInterface.OnClickListener() {   
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();    
//				try {
//					uploadShopAddr(lat, lng);
//				} catch (Exception e) {
//					Logger.error(e.getMessage());
//					DialogUtil.showDialog(MainActivity.this, "����ϵͳ����", false);
//				}
//			}
//		});  
//		builder.setNegativeButton(ctx.getResources().getString(R.string.lang_dialog_cancel), 
//				new DialogInterface.OnClickListener() {   
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				dialog.dismiss();
//			}
//		});  
		
		builder.create().show();
		
		return builder;
	}
	
//	public void setOnRightRenameClickListener(onRightRenameClickListener listener){
//    	mRenameListener = listener;
//    }
	
	
}
