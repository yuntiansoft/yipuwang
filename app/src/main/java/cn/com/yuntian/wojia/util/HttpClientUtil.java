/**
 * 
 */
package cn.com.yuntian.wojia.util;

import android.os.Looper;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

/**
 * @version 1.0
 */
public class HttpClientUtil {
	
	private static AsyncHttpClient syncClient = new SyncHttpClient();
	
	private static AsyncHttpClient asyncClient = new AsyncHttpClient();
	
	// ����httpclient����
	private static AsyncHttpClient getHttpClient() {
		if (Looper.myLooper() == null) {
			setHeader(syncClient);
			return syncClient;
		} else {
			setHeader(asyncClient);
			return asyncClient;
		}
	}
	
	private static void setHeader(AsyncHttpClient client) {
		if (CheckUtil.requireCheck(UserInfo.UserInfo.getAuthority())) {
			client.addHeader(Constants.AUTHORIZATION, UserInfo.UserInfo.getAuthority());
		}
		client.addHeader(Constants.ACCECPT, Constants.ACCECPT_VALUE);
	}
	
	public static void post(String url, RequestParams paramMap,  AsyncHttpResponseHandler responseHandler) {
		String baseUrl = AppContext.getBaseUrl() + url;
		getHttpClient().post(baseUrl, paramMap, responseHandler);
		Log.e("HttpClientUtil", "post: " + paramMap.toString());
	}
	
	public static void get(String url, JsonHttpResponseHandler responseHandler) {
		String baseUrl = AppContext.getBaseUrl() + url;
		getHttpClient().get(baseUrl, responseHandler);
	}
	
}
