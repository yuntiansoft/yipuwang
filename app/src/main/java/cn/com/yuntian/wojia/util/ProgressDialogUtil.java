/**
 * 
 */
package cn.com.yuntian.wojia.util;

import android.content.Context;

import cn.com.yuntian.wojia.R;
import cn.com.yuntian.wojia.layout.CustomProgressDialog;

/**
 * @version 1.0
 */
public class ProgressDialogUtil {
	private static CustomProgressDialog progressDialog= null;
	
	public static CustomProgressDialog getProgressDialogUtil(Context context) {
		CustomProgressDialog pd= new CustomProgressDialog(context);	
		progressDialog = pd.createDialog();
		progressDialog.setMessage(context.getResources().getString(R.string.lang_processing) + "...");
		return progressDialog;
	}
}
