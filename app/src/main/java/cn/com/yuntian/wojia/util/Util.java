package cn.com.yuntian.wojia.util;

import java.math.BigDecimal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class Util {  
  
    /**  
     *   
     * <p>���϶�  = 32 + ���϶� �� 1.8��</p>  
     * @param degree ��Ҫת�����¶�  
     * @param scale ������С��λ  
     * @return  
     */  
    public static String centigrade2Fahrenheit(double degree, int scale) {  
        double d = 32 + degree * 1.8;  
        return new BigDecimal(d).setScale(scale, BigDecimal.ROUND_HALF_UP).toString();  
    }  
      
    /**  
     *   
     * <p>���϶�  = (���϶� - 32) �� 1.8��</p>  
     * @param degree ��Ҫת�����¶�  
     * @param scale ������С��λ  
     * @return  
     */  
    public static String fahrenheit2Centigrade(double degree, int scale) {  
        double d = (degree - 32) / 1.8;  
        return new BigDecimal(d).setScale(scale, BigDecimal.ROUND_HALF_UP).toString();  
    }  
    
    /**  
     *   
     * <p>���϶�  = (���϶� - 32) �� 1.8��</p>  
     * @param degree ��Ҫת�����¶�  
     * @param scale ������С��λ  
     * @return  
     */  
    public static String fahrenheit2Centigrade(double degree) {  
        int i = ((int) ((degree - 32) / 0.9)); 
        double d = (double) i / 2;
        return String.valueOf(d);  
    }  
    
    public static String getTempDegree(String value) {
    	String ret = "";
    	try {
	    	if (CheckUtil.requireCheck(value)) {
	    		if (value.startsWith(Constants.CEN_START_KEY)) {
	        		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
	        			ret = getValue(value.substring(1), 1);
	        		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {
	        			double temp = Double.valueOf(value.substring(1)).doubleValue();
	        			ret = centigrade2Fahrenheit(temp, 0);
	        		}
	        			
	        	} else if (value.startsWith(Constants.FAH_START_KEY)) {
	        		if (Constants.TEMP_VALUE_ONE.equals(UserInfo.UserInfo.getTempUnit())) {
	        			int temp = Integer.valueOf(value.substring(1)).intValue();
	        			ret = fahrenheit2Centigrade(temp);
	        		} else if (Constants.TEMP_VALUE_TWO.equals(UserInfo.UserInfo.getTempUnit())) {	
	        			ret = getValue(value.substring(1), 0);
	        		}
	        	} 
	    	}
    	} catch (Exception e) {
    		return "0";
    	}

    	return ret;
    }
    
    public static boolean IsHaveInternet(final Context context) { 
        try { 
            ConnectivityManager manger = (ConnectivityManager) 
                    context.getSystemService(Context.CONNECTIVITY_SERVICE); 
 
            NetworkInfo info = manger.getActiveNetworkInfo(); 
            return (info!=null && info.isConnected()); 
        } catch (Exception e) { 
            return false; 
        } 
    } 
    
    public static String getValue(String value, int scale) {
    	if (!CheckUtil.requireCheck(value)) {
    		return "0";
    	}
    	
    	return  new BigDecimal(value).setScale(scale, BigDecimal.ROUND_HALF_UP).toString();
    }
    
    public static String trimSpace(String str) {
    	if (str == null) {
    		return "";
    	}
    	while(str.startsWith(" ")){  
    		str = str.substring(1, str.length()).trim();  
		}  
		while(str.endsWith(" ")){  
			str = str.substring(0, str.length()-1).trim();  
		}  
		
		return str;
    }
    
    public static String replaceFullWidth(String str) {
    	str = trimSpace(str);
    	str = str.replace("��", "@");
    	str = str.replace("��", ".");
    	
    	return str;
    }
    
    public static int dip2px(Context context, float dpValue) {
	    final float scale = context.getResources().getDisplayMetrics().density;
	    return (int) (dpValue * scale + 0.5f);
	}
    
    public static String tenToHex(String str) {
		String ret = "";
		if (CheckUtil.requireCheck(str)) {
			ret = Integer.toHexString(Integer.valueOf(str));
			if (ret.length()%2 != 0) {
				ret = "0" + ret;
			}
		}
		return ret;
	}
    
}  