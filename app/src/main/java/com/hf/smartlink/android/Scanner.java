package com.hf.smartlink.android;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import cn.com.yuntian.wojia.R;


/**
 * A scanner to scan the wifi access point
 * @author ZhangGuoYin
 *
 */
public class Scanner extends Handler {

	private Context context;
	private WifiManager wifiManager;
	private int retry = 0;

	public Scanner(Context context) {
		this.context = context;
		wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
	}

	public void resume() {
		if (!hasMessages(0)) {
			sendEmptyMessage(0);
		}
	}

	public void pause() {
		retry = 0;
		removeMessages(0);
	}

	@Override
	public void handleMessage(Message message) {

		if (wifiManager.startScan()) {
			retry = 0;
		} else if (++retry >= 3) {
			retry = 0;
			Toast.makeText(context, R.string.wifi_fail_to_scan,
					Toast.LENGTH_LONG).show();
			return;
		}
		sendEmptyMessageDelayed(0, 6000);
	}
}