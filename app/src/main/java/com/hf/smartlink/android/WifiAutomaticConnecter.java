package com.hf.smartlink.android;

import java.util.List;

import com.hf.smartlink.utils.Constants;
import com.hf.smartlink.utils.Utils;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiConfiguration.KeyMgmt;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

public abstract class WifiAutomaticConnecter extends Handler {

    private static final String TAG = Constants.TAG + "WifiAutomaticConnecter";
    private Context context;
    private String ssid;
    private WifiManager wifiManager;
    private boolean useDefault;
    private boolean isResume;
    private int notFoundCount = 0;
    /**
     * @return the ssid
     */
    public String getSsid() {
        return ssid;
    }


    /**
     * It will use SSID setting in local
     *
     * @param context
     * @see Utils#getSettingApSSID(Context)
     */
    public WifiAutomaticConnecter(Context context, Looper looper) {
        super(looper);
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        useDefault = true;
    }


    public void handleMessage(Message msg) {
        // TODO Auto-generated method stub
        super.handleMessage(msg);
        synchronized (WifiAutomaticConnecter.class) {
            if (!isResume) return;

            if (useDefault) {
                ssid = Utils.getSettingApSSID(context);
            }

            if (ssid == null || ((ssid = ssid.trim()).length() == 0)) {
                retry();
                return;
            }

            //remove all of wifi configuration which has the same ssid
            List<WifiConfiguration> configs = wifiManager.getConfiguredNetworks();
            //disable others network except the default ap, and set the priority low
            if (configs != null) {
                for (WifiConfiguration wifiConfiguration : configs) {
                    if (!AccessPoint.removeDoubleQuotes(wifiConfiguration.SSID).equals(ssid)) {
                        wifiConfiguration.priority = 0;
                        wifiManager.disableNetwork(wifiConfiguration.networkId);
//                        Log.e(TAG, "disableNetwork: " + wifiConfiguration.SSID);
                        wifiManager.updateNetwork(wifiConfiguration);
                    }
                }
            }
            wifiManager.saveConfiguration();

            WifiInfo currWifiInfo = wifiManager.getConnectionInfo();
            String currSSID = currWifiInfo.getSSID();
//    	Log.d(TAG, "The current connection ssid is " + currSSID);
            if (currWifiInfo != null && currWifiInfo.getNetworkId() != -1 && currSSID != null
                    && !AccessPoint.removeDoubleQuotes(currSSID).equals(ssid)) {
                Log.d(TAG, "Disconnect AP-" + currSSID);
                wifiManager.disconnect();
                Log.e(TAG, "disconnect: " );
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            List<ScanResult> results = wifiManager.getScanResults();
            if (results == null) {
                retry();
                return;
            }

            AccessPoint accessPoint = null;
            for (ScanResult result : results) {
                // Ignore hidden and ad-hoc networks.
                if (result.SSID == null || result.SSID.length() == 0 ||
                        result.capabilities.contains("[IBSS]")) {
                    continue;
                }

                if (result.SSID.equals(ssid)) {
                    accessPoint = new AccessPoint(context, result);
                    break;
                }
            }

            if (accessPoint != null) {

                currWifiInfo = wifiManager.getConnectionInfo();
                currSSID = currWifiInfo.getSSID();
                Log.d(TAG, "The current connection ssid is " + currWifiInfo);

                if (currSSID == null && currWifiInfo.getSupplicantState().compareTo(SupplicantState.ASSOCIATING) >= 0 &&
                        currWifiInfo.getSupplicantState().compareTo(SupplicantState.COMPLETED) <= 0) {
                    try {
                        sendEmptyMessageDelayed(0, 5000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return;
                }
//                &&
                //if the current connection ap is not the given ssid, try to connect the given ssid
                if (!(currWifiInfo != null &&currWifiInfo.getNetworkId() != -1&&
                        currSSID != null && AccessPoint.removeDoubleQuotes(currSSID).equals(ssid))) {
                    //remove all of wifi configuration which has the same ssid

                    Log.e(TAG, "handleMessage: reconnectHailiWifi:"+AccessPoint.removeDoubleQuotes(currSSID).equals(ssid) );
                    configs = wifiManager.getConfiguredNetworks();
                    for (WifiConfiguration wifiConfiguration : configs) {
                        if (AccessPoint.removeDoubleQuotes(wifiConfiguration.SSID).equals(ssid)) {
                            wifiManager.removeNetwork(wifiConfiguration.networkId);
                        }
                    }
                    wifiManager.saveConfiguration();
                    Log.e(TAG, "networkid: "+ currWifiInfo.getNetworkId() );
                    Log.e(TAG, "currSSid: "+ AccessPoint.removeDoubleQuotes(currSSID));
                    wifiManager.disconnect();
                    Log.e(TAG, "disconnect: " );
                    if (accessPoint.getSecurity() == AccessPoint.SECURITY_NONE) {
                        // Shortcut for open networks.
                        Log.d(TAG, "try to connect open none wifi :" + ssid);
                        WifiConfiguration config = new WifiConfiguration();
                        config.SSID = AccessPoint.convertToQuotedString(accessPoint.getSsid());
                        config.allowedKeyManagement.set(KeyMgmt.NONE);
                        config.priority = Integer.MAX_VALUE;
                        config.BSSID = accessPoint.getBSSID();
//					config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                        int networkId = wifiManager.addNetwork(config);
                        Log.e(TAG, "networkId:add: "+ networkId);
                        connectOpenNone(accessPoint,networkId);
                    }
                }
            } else {
                if (notFoundCount<10){
                    notFoundCount ++;
                }else {
                    onSsidNotFind();
                }
            }

            retry();
        }
    }

    public void resume() {
        isResume = true;
        notFoundCount = 0;
        if (!hasMessages(0)) {
            sendEmptyMessage(0);
        }
    }

    public void pause() {
        synchronized (WifiAutomaticConnecter.class){
            isResume = false;
            Log.e(TAG, "pause: " );
        }
        removeMessages(0);
    }

    private void retry() {
        try {
            sendEmptyMessageDelayed(0, 8000);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * the wifi with the specific ssid is not find
     *
     * @see #WifiAutomaticConnecter(Context, String)
     */
    public void onSsidNotFind() {

    }

    /**
     * connect to the open none wifi, it doesn't need password
     *
     * @param accessPoint
     * @param networkId
     */
    public abstract void connectOpenNone(AccessPoint accessPoint, int networkId);


}
