package com.hf.smartlink.model;

import java.util.List;

import com.hf.smartlink.android.AccessPoint;
import com.hf.smartlink.utils.Utils;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import static com.hf.smartlink.utils.Constants.TAG;

public class WifiStatus {

    private boolean enable;
    private String ssid;
    private String BSSID;
    private int networkId = Integer.MIN_VALUE;
    private WifiManager wifiManager;
    private static WifiStatus wifiStatus;
    public static synchronized WifiStatus getInstanse(Context context){
        if (wifiStatus == null){
            wifiStatus = new WifiStatus(context);

        }
        return wifiStatus;
    }
    private WifiStatus(Context context) {
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    /**
     * @return the enable
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * @param enable the enable to set
     */
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     * @return the ssid
     */
    public String getSsid() {
        return ssid;
    }

    /**
     * @param ssid the ssid to set
     */
    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    /**
     * @return the networkId
     */
    public int getNetworkId() {
        return networkId;
    }

    /**
     * @param networkId the networkId to set
     */
    public void setNetworkId(int networkId) {
        this.networkId = networkId;
    }

    /**
     * @return the bSSID
     */
    public String getBSSID() {
        return BSSID;
    }

    /**
     * @param bSSID the bSSID to set
     */
    public void setBSSID(String bSSID) {
        BSSID = bSSID;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "WifiStatus [enable=" + enable + ", ssid=" + ssid + ", BSSID="
                + BSSID + ", networkId=" + networkId + ", wifiManager="
                + wifiManager + "]";
    }

    public void load() {

        enable = wifiManager.isWifiEnabled();
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        if (wifiInfo != null&&!"00:00:00:00:00:00".equals(wifiInfo.getBSSID())) {
            BSSID = wifiInfo.getBSSID();
            Log.e(TAG, "load:BSSID: " + BSSID);
            ssid = wifiInfo.getSSID() == null ? null : wifiInfo.getSSID().trim();
            Log.e(TAG, "load:ssid: " + ssid);
            networkId = wifiInfo.getNetworkId();
        }
    }

    public void reload(final String password) {

        if (!enable) {
            wifiManager.setWifiEnabled(false);
            Log.e(TAG, "reload: !enable");
        } else {
//			new Thread(){
//				@Override
//				public void run() {
//					super.run();
//                    List<WifiConfiguration> configurations = wifiManager.getConfiguredNetworks();
//                    int  mode = 0;
//                    for (WifiConfiguration wifiConfiguration : configurations) {
//                        if (wifiConfiguration.SSID.equals(ssid)) {
//                            mode= getSecurity(wifiConfiguration);
//                            wifiManager.removeNetwork(wifiConfiguration.networkId);
//                        }
//                    }
//                    wifiManager.setWifiEnabled(false);
//                    try{
//                        sleep(2000);
//                    }catch (Exception e){
//
//                    }
//                    wifiManager.setWifiEnabled(true);
//                    if (!wifiManager.isWifiEnabled()) {
//                        wifiManager.setWifiEnabled(true);
//                    }
//                    try{
//                        sleep(8000);
//                    }catch (Exception e){
//
//                    }
//
//                    WifiInfo current = wifiManager.getConnectionInfo();
//                    Log.e(TAG, "BSSID: "+BSSID );
//                    if (current!=null && current.getBSSID()!=null && current.getBSSID().equals(BSSID)) {
//                        Log.e(TAG, "reload:return " );
////				return;
//                    }else if (current!=null && current.getBSSID()!=null){
//                        wifiManager.disconnect();
//                    }
//
//                    WifiConfiguration config = new WifiConfiguration();
//                    config.allowedAuthAlgorithms.clear();
//                    config.allowedGroupCiphers.clear();
//                    config.allowedKeyManagement.clear();
//                    config.allowedPairwiseCiphers.clear();
//                    config.allowedProtocols.clear();
////						config.SSID = "\"" + ssid + "\"";
//                    config.SSID = ssid;
//                    Log.e(TAG, "config.SSID: " +config.SSID);
//                    if (mode == AccessPoint.SECURITY_NONE) {
////							config.wepKeys[0] = "";
//                        config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
////							config.wepTxKeyIndex = 0;
//                    } else if (password != null && !"".equals(password)) {
//                        if (mode == 1) {
//
//                            config.hiddenSSID = true;
//                            config.wepKeys[0]= "\""+password+"\"";
//                            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
//                            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
//                            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
//                            config.wepTxKeyIndex = 0;
//                        } else {
////					config.preSharedKey = "\"" + password + "\"";
//
//                            config.preSharedKey = "\""+password+"\"";
//                            Log.e(TAG, "reload:password " +password);
//                            Log.e(TAG, "reload:mode " +mode);
//                            config.hiddenSSID = true;
//                            config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
//                            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
//                            config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
//                            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
//                            config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
//                            config.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
//                            config.status = WifiConfiguration.Status.ENABLED;
//                        }
//                    }
//                    config.priority = Integer.MAX_VALUE;
//                    int mNetworkID = wifiManager.addNetwork(config);
//
//
//
//                    boolean enable = wifiManager.enableNetwork(mNetworkID, true);
//                    Log.e(TAG, "enable: " + enable);
//                    boolean reconnect = wifiManager.reconnect();
//                    Log.e(TAG, "reconnect: " + reconnect);



//				}
//			}.start();


//			wifiManager.reconnect();
            new Thread(){
                @Override
                public void run() {
                    super.run();
                    if (!wifiManager.isWifiEnabled()) {
                        wifiManager.setWifiEnabled(true);
                    }

                    WifiInfo current = wifiManager.getConnectionInfo();
                    Log.e(TAG, "BSSID: " + BSSID);
                    if (current != null && current.getBSSID() != null && current.getBSSID().equals(BSSID)) {
                        Log.e(TAG, "reload:getNetworkId: "+current.getNetworkId());
                        if (current.getNetworkId()!=-1){
                            Log.e(TAG, "reload:return ");
                            return;
                        }
                    }
                    wifiManager.disconnect();
//            wifiManager.reconnect();
                    List<WifiConfiguration> configurations = wifiManager.getConfiguredNetworks();
                    if (configurations != null) {

                        WifiConfiguration bestConfiguration = null;
                        WifiConfiguration betterConfiguration = null;
                        WifiConfiguration goodConfiguration = null;
                        boolean networkIdFind = false;
                        boolean ssidFind = false;
                        for (WifiConfiguration wifiConfiguration : configurations) {

                            networkIdFind = (wifiConfiguration.networkId == networkId);
//					ssidFind = (Utils.removeDoubleQuotes(wifiConfiguration.SSID).equals(ssid));
                            ssidFind = (wifiConfiguration.SSID.equals(ssid));
//                    Log.e(TAG, "networkId: " + wifiConfiguration.networkId);
//                    Log.e(TAG, "ssid: " + Utils.removeDoubleQuotes(wifiConfiguration.SSID));
//                    Log.e(TAG, "ssidsava: " + ssid);
                            if (networkIdFind && ssidFind) {
                                bestConfiguration = wifiConfiguration;
                                bestConfiguration.priority = Integer.MAX_VALUE;
                                break;
                            } else if (networkIdFind) {
                                betterConfiguration = wifiConfiguration;
                                betterConfiguration.priority = Integer.MAX_VALUE;
                                break;
                            } else if (ssidFind) {
                                goodConfiguration = wifiConfiguration;
                                if (goodConfiguration!=null)
                                    goodConfiguration.priority = Integer.MAX_VALUE;
                                break;
                            }
                        }

                        if (bestConfiguration != null) {
                            wifiManager.enableNetwork(bestConfiguration.networkId, true);
                            Log.e(TAG, "reload: bestConfiguration");
                        } else if (betterConfiguration != null) {

                            wifiManager.enableNetwork(betterConfiguration.networkId, true);
                            Log.e(TAG, "reload: betterConfiguration");
                        } else if (goodConfiguration != null) {

                            wifiManager.enableNetwork(goodConfiguration.networkId, true);
                            Log.e(TAG, "reload: goodConfiguration");
                        }
                        wifiManager.saveConfiguration();
                        wifiManager.reconnect();
                    }
                }
            }.start();
        }
    }

    static int getSecurity(WifiConfiguration config) {

        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_PSK)) {
            return 2;
        }

        if (config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.WPA_EAP) ||
                config.allowedKeyManagement.get(WifiConfiguration.KeyMgmt.IEEE8021X)) {
            return 3;
        }

        return (config.wepKeys[0] != null) ? 1 : 0;
    }
}
